import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatChipInputEvent } from '@angular/material';

export class Product
{
    rute_id: number;
    nama: string;
    alamat: string;
    no_hp: string;
    no_kendaraan: string;
    no_ktp: string;
    no_tiket: string;
    no_sim: string;

    constructor(product?)
    {
        if (product) {
            product = product;
            this.rute_id = product.rute_id;
            this.nama = product.nama;
            this.alamat = product.alamat;
            this.no_hp = product.no_hp;
            this.no_kendaraan = product.no_kendaraan;
            this.no_ktp = product.no_ktp;
            this.no_tiket = product.no_tiket;
            this.no_sim = product.no_sim;
        } else {
            product = {};
            this.rute_id = 0;
            this.nama = '';
            this.alamat = '';
            this.no_hp = '';
            this.no_kendaraan = '';
            this.no_ktp = '';
            this.no_tiket = '';
            this.no_sim = '';
        }
    }

    // addCategory(event: MatChipInputEvent): void
    // {
    //     const input = event.input;
    //     const value = event.value;

    //     // Add category
    //     if ( value )
    //     {
    //         this.categories.push(value);
    //     }

    //     // Reset the input value
    //     if ( input )
    //     {
    //         input.value = '';
    //     }
    // }

    // removeCategory(category)
    // {
    //     const index = this.categories.indexOf(category);

    //     if ( index >= 0 )
    //     {
    //         this.categories.splice(index, 1);
    //     }
    // }

    // addTag(event: MatChipInputEvent): void
    // {
    //     const input = event.input;
    //     const value = event.value;

    //     // Add tag
    //     if ( value )
    //     {
    //         this.tags.push(value);
    //     }

    //     // Reset the input value
    //     if ( input )
    //     {
    //         input.value = '';
    //     }
    // }

    // removeTag(tag)
    // {
    //     const index = this.tags.indexOf(tag);

    //     if ( index >= 0 )
    //     {
    //         this.tags.splice(index, 1);
    //     }
    // }
}
