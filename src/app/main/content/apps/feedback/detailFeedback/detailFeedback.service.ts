import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { MatSnackBar } from '@angular/material';
import { SessionService } from './../../session.service';

@Injectable()
export class DetailFeedbackService implements Resolve<any>
{
    routeParams: any;
    product: any;
    onProductChanged: BehaviorSubject<any> = new BehaviorSubject({});
    token = this._sessionService.get().token;

    constructor(
        private http: Http,
        private snackBar: MatSnackBar,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProduct()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getProduct(): Promise<any>
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            if ( this.routeParams.id === 'new' )
            {
                this.onProductChanged.next(false);
                resolve(false);
            }
            else
            {
                this.http.get(environment.setting.base_url + '/admin/feedbacks/' + this.routeParams.id, options)
                    .map((res: Response) => res.json())
                    .subscribe((response: any) => {
                        this.product = response;
                        this.onProductChanged.next(this.product);
                        resolve(response);
                    }, reject);
            }
        });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }

    saveProduct(product)
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/feedbacks/' + this.routeParams.id, product, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (reject) => {
                    let eror = JSON.parse(reject._body);
                    this.snackBar.open(eror.message, 'Error !', {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                });
        });
    }

    addProduct(product)
    {
        let options = new RequestOptions();
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        options.headers = headers;
        return new Promise((resolve, reject) => {
            this.http.post(environment.setting.base_url + '/feedback', product, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (reject) => {
                    let eror = JSON.parse(reject._body);
                    this.snackBar.open(eror.message, 'Error !', {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                });
        });
    }
}
