import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from './../../session.service';

@Injectable()
export class DaftarTiketService implements Resolve<any>
{
    products: any[];
    token = this._sessionService.get().token;
    onProductsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(
        private http: Http,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProducts()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }

    getProducts(): Promise<any>
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url+'/admin/tiket', options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    this.products = response['data'];
                    for (var i = 0; i < this.products.length; ++i) {
                        if (this.products[i].status != 1) {
                            this.products.splice(i, 1);
                            i = i - 1;
                        }
                    }
                    this.onProductsChanged.next(this.products);
                    resolve(response);
                }, reject);
        });
    }

    printTiket(tiket): Promise<any> {
        let options = new RequestOptions();
        let header = new Headers();
        header.append('Authorization', `Bearer ${this.token}`);
        header.append('Content-Type', 'application/json');
        header.append('Accept', 'application/pdf');
        options.headers = header;

        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url + "/admin/tiket/" + tiket + "/download", {
                    headers: header, responseType: ResponseContentType.Blob
                })
                .map((res) => {
                    return {
                        filename: `tiket-${tiket}.pdf`,
                        data: res.blob(),
                        success: true
                    };
                })
                .subscribe((res: any) => {
                    var url = window.URL.createObjectURL(res.data);
                    var a = document.createElement('a');
                    document.body.appendChild(a);
                    a.setAttribute('style', 'display: none');
                    a.href = url;
                    a.download = res.filename;
                    a.click();
                    window.URL.revokeObjectURL(url);
                    a.remove(); // remove the element
                    resolve(res);
                }, error => {
                    console.log('download error:', JSON.stringify(error));
                }, () => {
                    console.log('Completed file download.')
                })
        });
    }

}
