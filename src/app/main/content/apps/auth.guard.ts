import { Injectable } from '@angular/core';
import { 
	CanActivate, 
	ActivatedRouteSnapshot, 
	RouterStateSnapshot, 
	Router 
} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { AuthService } from './auth.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from './../../../../environments/environment';
import 'rxjs/add/operator/map';
import { SessionService } from './session.service';

@Injectable()
export class AuthGuard implements CanActivate {
	public isLoggedIn = false;
	public redirectUrl: string;

	constructor(private auth: AuthService, private route: Router, private http: Http, private _sessionService: SessionService){}

  	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
  		this.redirectUrl = state.url;

    	return this.checkLogin(this.redirectUrl);
  	}

  	checkLogin(url: string): boolean {
    		let obj = this._sessionService.get();
        // console.log(obj.token);

        if(this.isLoggedIn) {
           return true;
        } else {
            if (obj) {
                if(obj.token) {
                    this.isLoggedIn = true;
                    if (obj.email && obj.role) {
                        this.route.navigateByUrl(url);
                    } else {
                        this.route.navigateByUrl(url);
                    }                        
                } else {
                    this.isLoggedIn = false;
                    this.route.navigateByUrl('/apps/login');
                }
            } else {
                this.isLoggedIn = false;
                this.route.navigateByUrl('/apps/login');
            }
        }
  	}

}
