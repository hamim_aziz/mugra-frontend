export const environment = {
    production: true,
    hmr       : false,
    setting   : {
        base_url  : 'https://apiadminmudikgratis.dishub.jatimprov.go.id/api',
        oauth_url: 'https://apiadminmudikgratis.dishub.jatimprov.go.id/api/login'
    }
};
