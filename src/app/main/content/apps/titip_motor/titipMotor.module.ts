import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { DaftarTitipMotorComponent } from './daftarTitipMotor/daftarTitipMotor.component';
import { DaftarTitipMotorService } from './daftarTitipMotor/daftarTitipMotor.service';
import { DetailTitipMotorComponent } from './detailTitipMotor/detailTitipMotor.component';
import { DetailTitipMotorService } from './detailTitipMotor/detailTitipMotor.service';
import { FuseConfirmDialogComponent } from './../../../../core/components/confirm-dialog/confirm-dialog.component';
import { AgmCoreModule } from '@agm/core';
import { CdkTableModule } from '@angular/cdk/table';
import { DragDropSortableService, DndModule, DragDropService, DragDropConfig } from 'ng2-dnd';
import { AuthGuard } from './../auth.guard';
import { MatTableModule } from '@angular/material/table';

const routes: Routes = [
    {
        path     : 'daftarTitipMotor',
        component: DaftarTitipMotorComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DaftarTitipMotorService
        }
    },
    {
        path     : 'detailTitipMotor/:id',
        component: DetailTitipMotorComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DetailTitipMotorService
        }
    }
];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        CdkTableModule,
        DndModule,
        MatTableModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    exports : [
        CdkTableModule
    ],
    entryComponents: [
        DaftarTitipMotorComponent,
        FuseConfirmDialogComponent
    ],
    declarations: [
        DaftarTitipMotorComponent,
        DetailTitipMotorComponent
    ],
    providers   : [
        DaftarTitipMotorService,
        DetailTitipMotorService,
        DragDropSortableService, 
        DragDropService, 
        DragDropConfig
    ]
})
export class TitipMotorModule
{
}
