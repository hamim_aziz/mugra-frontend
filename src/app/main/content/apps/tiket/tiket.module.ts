import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { DaftarTiketComponent } from './daftarTiket/daftarTiket.component';
import { DaftarTiketService } from './daftarTiket/daftarTiket.service';
import { DetailTiketComponent } from './detailTiket/detailTiket.component';
import { DetailTiketService } from './detailTiket/detailTiket.service';
import { AgmCoreModule } from '@agm/core';
import { AuthGuard } from './../auth.guard';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

const routes: Routes = [
    {
        path     : 'daftarTiket',
        component: DaftarTiketComponent,
        canActivate : [AuthGuard]
    },
    {
        path     : 'detailTiket/:id',
        component: DetailTiketComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DetailTiketService
        }
    }

];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),
        SweetAlert2Module.forRoot()
    ],
    entryComponents: [
        DaftarTiketComponent,
    ],
    declarations: [
        DaftarTiketComponent,
        DetailTiketComponent
    ],
    providers   : [
        DaftarTiketService,
        DetailTiketService
    ]
})
export class TiketModule
{
}
