import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import * as shape from 'd3-shape';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';

import { ProjectDashboardService } from './project.service';
import { fuseAnimations } from '../../../../../core/animations';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from './../../../../../../environments/environment';
import { 
    CanActivate, 
    ActivatedRouteSnapshot, 
    RouterStateSnapshot, 
    Router 
} from '@angular/router';

import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { MatSnackBar } from '@angular/material';

import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SessionService } from './../../session.service';

@Component({
    selector     : 'fuse-project-dashboard',
    templateUrl  : './project.component.html',
    styleUrls    : ['./project.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class FuseProjectDashboardComponent implements OnInit, OnDestroy
{
    projects: any[];
    selectedProject: any;

    widgets: any;
    widget5: any = {};
    widget6: any = {};
    widget6Mudik: any;
    widget6Balik: any;
    widget7: any = {};
    widget8: any = {};
    widget9: any = {};
    widget11: any = {};
    dashboard: any;
    displayedColumns = ['name', 'username', 'email'];
    total_sisa_mudik = 0;
    total_sisa_balik = 0;

    formBuka: FormGroup;
    formTutup: FormGroup;

    dateNow = Date.now();
    token = this._sessionService.get().token;
    name = this._sessionService.get().email;
    role = this._sessionService.get().role;
    email = this._sessionService.get().email;

    constructor(private projectDashboardService: ProjectDashboardService, 
                private http: Http, 
                private formBuilder: FormBuilder,
                private router: Router,
                private snackBar: MatSnackBar,
                private _sessionService: SessionService)
    {
        this.projects = this.projectDashboardService.projects;
        // this.selectedProject = this.projects[0];
        this.widgets = this.projectDashboardService.widgets;

        /**
         * Widget 5
         */
        this.widget5 = {
            currentRange  : 'TW',
            xAxis         : true,
            yAxis         : true,
            gradient      : false,
            legend        : false,
            showXAxisLabel: false,
            xAxisLabel    : 'Days',
            showYAxisLabel: false,
            yAxisLabel    : 'Isues',
            scheme        : {
                domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
            },
            onSelect      : (ev) => {
                // console.log(ev);
            },
            supporting    : {
                currentRange  : '',
                xAxis         : false,
                yAxis         : false,
                gradient      : false,
                legend        : false,
                showXAxisLabel: false,
                xAxisLabel    : 'Days',
                showYAxisLabel: false,
                yAxisLabel    : 'Isues',
                scheme        : {
                    domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
                },
                curve         : shape.curveBasis
            }
        };

        /**
         * Widget 6
         */
        this.widget6 = {
            currentRange : 'TW',
            legend       : false,
            explodeSlices: false,
            labels       : true,
            doughnut     : true,
            gradient     : false,
            scheme       : {
                domain: ['#f44336', '#9c27b0', '#03a9f4', '#e91e63']
            },
            onSelect     : (ev) => {
                // console.log(ev);
            }
        };

        /**
         * Widget 7
         */
        this.widget7 = {
            currentRange: 'T'
        };

        /**
         * Widget 8
         */
        this.widget8 = {
            legend       : false,
            explodeSlices: false,
            labels       : true,
            doughnut     : false,
            gradient     : false,
            scheme       : {
                domain: ['#f44336', '#9c27b0', '#03a9f4', '#e91e63', '#ffc107']
            },
            onSelect     : (ev) => {
                // console.log(ev);
            }
        };

        /**
         * Widget 9
         */
        this.widget9 = {
            currentRange  : 'TW',
            xAxis         : false,
            yAxis         : false,
            gradient      : false,
            legend        : false,
            showXAxisLabel: false,
            xAxisLabel    : 'Days',
            showYAxisLabel: false,
            yAxisLabel    : 'Isues',
            scheme        : {
                domain: ['#42BFF7', '#C6ECFD', '#C7B42C', '#AAAAAA']
            },
            curve         : shape.curveBasis
        };

        setInterval(() => {
            this.dateNow = Date.now();
        }, 1000);

    }

    ngOnInit()
    {
        /**
         * Widget 11
         */
        this.widget11.onContactsChanged = new BehaviorSubject({});
        this.widget11.dataSource = new FilesDataSource(this.widget11);
        this.dashboard = this.projectDashboardService.dashboard;
        this.getConfigurasi();
        this.widget6Mudik = [
            {
                name: 'Online', 
                value: this.dashboard.pendaftar_mudik_online
            }, 
            {
                name: 'Operator', 
                value: this.dashboard.pendaftar_mudik_offline
            }
        ];
        this.widget6Balik = [
            {
                name: 'Online', 
                value: this.dashboard.pendaftar_balik_online
            }, 
            {
                name: 'Operator', 
                value: this.dashboard.pendaftar_balik_offline
            }
        ];
        this.formBuka = this.formBuilder.group({
            "tanggal_buka"    : new FormControl('', Validators.required),
            "bulan_buka"      : new FormControl('', Validators.required),
            "tahun_buka"      : new FormControl('', Validators.required),
            "jam_buka"        : new FormControl('', Validators.required),
            "menit_buka"      : new FormControl('', Validators.required),
            "detik_buka"      : new FormControl('', Validators.required)
        })
        this.formTutup = this.formBuilder.group({
            "tanggal_tutup"   : new FormControl('', Validators.required),
            "bulan_tutup"     : new FormControl('', Validators.required),
            "tahun_tutup"     : new FormControl('', Validators.required),
            "jam_tutup"       : new FormControl('', Validators.required),
            "menit_tutup"     : new FormControl('', Validators.required),
            "detik_tutup"     : new FormControl('', Validators.required)
        })
    }

    ngOnDestroy()
    {
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.token);
        options.headers = headers;
    }

    getConfigurasi(){
        let options = new RequestOptions();
        this.setHeader(options);
        this.http.get(environment.setting.base_url + "/admin/pendaftaran", options)
            .map((res: Response) => res.json())
            .subscribe((response: any) => {
                if (!response['status']) {
                    this.snackBar.open(response.message, 'OK', {
                        verticalPosition: 'top',
                        duration        : 2000
                    });
                } else {
                    this.formBuka.controls.tahun_buka.setValue(response.data.buka.slice(0, 4));
                    this.formBuka.controls.bulan_buka.setValue(response.data.buka.slice(5, 7));
                    this.formBuka.controls.tanggal_buka.setValue(response.data.buka.slice(8, 10));
                    this.formBuka.controls.jam_buka.setValue(response.data.buka.slice(11, 13));
                    this.formBuka.controls.menit_buka.setValue(response.data.buka.slice(14, 16));
                    this.formBuka.controls.detik_buka.setValue(response.data.buka.slice(17, 19));
                    this.formTutup.controls.tahun_tutup.setValue(response.data.tutup.slice(0, 4));
                    this.formTutup.controls.bulan_tutup.setValue(response.data.tutup.slice(5, 7));
                    this.formTutup.controls.tanggal_tutup.setValue(response.data.tutup.slice(8, 10));
                    this.formTutup.controls.jam_tutup.setValue(response.data.tutup.slice(11, 13));
                    this.formTutup.controls.menit_tutup.setValue(response.data.tutup.slice(14, 16));
                    this.formTutup.controls.detik_tutup.setValue(response.data.tutup.slice(17, 19));
                }
            }, err =>{
                let eror = JSON.parse(err._body);
                if (JSON.parse(err._body).message == 'Unauthenticated.') {
                    this.router.navigateByUrl('/apps/login');
                } else {
                    this.snackBar.open(eror.message, 'Error !', {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                }
            });
    }

    simpan(){
        var data = {
            'buka'  : this.formBuka.controls.tahun_buka.value + "-" +
                      this.formBuka.controls.bulan_buka.value + "-" +
                      this.formBuka.controls.tanggal_buka.value + " " +
                      this.formBuka.controls.jam_buka.value + ":" +
                      this.formBuka.controls.menit_buka.value + ":" +
                      this.formBuka.controls.detik_buka.value,
            'tutup' : this.formTutup.controls.tahun_tutup.value + "-" +
                      this.formTutup.controls.bulan_tutup.value + "-" +
                      this.formTutup.controls.tanggal_tutup.value + " " +
                      this.formTutup.controls.jam_tutup.value + ":" +
                      this.formTutup.controls.menit_tutup.value + ":" +
                      this.formTutup.controls.detik_tutup.value
        }
        let options = new RequestOptions();
        this.setHeader(options);
        this.http.put(environment.setting.base_url + "/admin/pendaftaran/update/1", data, options)
            .map((res: Response) => res.json())
            .subscribe((response: any) => {
                if (!response.status) {
                    this.snackBar.open(response.message, 'OK', {
                        verticalPosition: 'top',
                        duration        : 2000
                    });
                } else {
                    this.snackBar.open('Update Jadwal Pendaftaran Sukses', 'OK', {
                        verticalPosition: 'top',
                        duration        : 2000
                    });
                }
            }, (err) => {
                let eror = JSON.parse(err._body);
                this.snackBar.open(eror.message, 'Error !', {
                    verticalPosition: 'top',
                    duration        : 5000
                });
            });
    }

}

export class FilesDataSource extends DataSource<any>
{
    constructor(private widget11)
    {
        super();
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        return this.widget11.onContactsChanged;
    }

    disconnect()
    {
    }
}

