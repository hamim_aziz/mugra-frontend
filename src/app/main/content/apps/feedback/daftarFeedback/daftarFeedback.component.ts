import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DaftarFeedbackService } from './daftarFeedback.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { fuseAnimations } from '../../../../../core/animations';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from '@angular/router';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { FuseConfirmDialogComponent } from './../../../../../core/components/confirm-dialog/confirm-dialog.component';

@Component({
    selector   : 'fuse-e-commerce-products',
    templateUrl: './daftarFeedback.component.html',
    styleUrls  : ['./daftarFeedback.component.scss'],
    animations : fuseAnimations
})
export class DaftarFeedbackComponent implements OnInit
{
    hapus = false;
    dataSource: FilesDataSource | null;
    displayedColumns = ['id', 'nik', 'nilai', 'comment'];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private productsService: DaftarFeedbackService,
        private dialog: MatDialog,
        private router: Router
    )
    {
    }

    ngOnInit()
    {
        this.dataSource = new FilesDataSource(this.productsService, this.paginator, this.sort);
        Observable.fromEvent(this.filter.nativeElement, 'keyup')
                  .debounceTime(150)
                  .distinctUntilChanged()
                  .subscribe(() => {
                      if ( !this.dataSource )
                      {
                          return;
                      }
                      this.dataSource.filter = this.filter.nativeElement.value;
                  });
    }

    toggleHapus(){
        this.hapus == true ? this.hapus = false : this.hapus = true;
    }

    hapusDialog(id, asal, tujuan, navigate): void{
        var pesan = "Hapus Rute ";
        let deleteDialog = this.dialog.open(FuseConfirmDialogComponent, {
            width: '250',
            data: { id: id, asal: asal, tujuan: tujuan, url: "/admin/rute/", navigate: navigate}
        });

        deleteDialog.afterClosed().subscribe(() => {
            this.dataSource = new FilesDataSource(this.productsService, this.paginator, this.sort);
            Observable.fromEvent(this.filter.nativeElement, 'keyup')
                      .debounceTime(150)
                      .distinctUntilChanged()
                      .subscribe(() => {
                          if ( !this.dataSource )
                          {
                              return;
                          }
                          this.dataSource.filter = this.filter.nativeElement.value;
                      });

        })
    }

    clickFeedback(id): void{
        var url = '/apps/feedback/detailFeedback/' + id;
        var navigate = '/apps/rute/daftarRute';
        if (!this.hapus) {
            this.router.navigate([url]);
        } else {
            // this.hapusDialog(id, navigate);
        }
    }
}

export class FilesDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }

    constructor(
        private productsService: DaftarFeedbackService,
        private _paginator: MatPaginator,
        private _sort: MatSort
    )
    {
        super();
        this.filteredData = this.productsService.products;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this.productsService.onProductsChanged,
            this._paginator.page,
            this._filterChange,
            this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.productsService.products.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            data = this.sortData(data);

            // Grab the page's slice of data.
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data)
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    sortData(data): any[]
    {
        if ( !this._sort.active || this._sort.direction === '' )
        {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch ( this._sort.active )
            {
                case 'id':
                    [propertyA, propertyB] = [a.id, b.id];
                    break;
                case 'asal':
                    [propertyA, propertyB] = [a.asal, b.asal];
                    break;
                case 'tujuan':
                    [propertyA, propertyB] = [a.tujuan, b.tujuan];
                    break;
                case 'tanggal':
                    [propertyA, propertyB] = [a.tanggal, b.tanggal];
                    break;
                case 'jenis':
                    [propertyA, propertyB] = [a.jenis, b.jenis];
                    break;
                case 'kuota_motor':
                    [propertyA, propertyB] = [a.kouta_motor, b.kouta_motor];
                    break;
                case 'status':
                    [propertyA, propertyB] = [a.status, b.status];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
        });
    }

    disconnect()
    {
    }
}
