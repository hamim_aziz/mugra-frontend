import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { DaftarRuteComponent } from './laporan.component';
import { DaftarRuteService } from './laporan.service';
import { FuseConfirmDialogComponent } from './../../../../core/components/confirm-dialog/confirm-dialog.component';
import { AgmCoreModule } from '@agm/core';
import { CdkTableModule } from '@angular/cdk/table';
import { DragDropSortableService, DndModule, DragDropService, DragDropConfig } from 'ng2-dnd';
import { AuthGuard } from './../auth.guard';
import { MatTableModule } from '@angular/material/table';

const routes: Routes = [
    {
        path     : '',
        component: DaftarRuteComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DaftarRuteService
        }
    }
];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        CdkTableModule,
        DndModule,
        MatTableModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    exports : [
        CdkTableModule
    ],
    entryComponents: [
        DaftarRuteComponent,
        FuseConfirmDialogComponent
    ],
    declarations: [
        DaftarRuteComponent
    ],
    providers   : [
        DaftarRuteService,
        DragDropSortableService, 
        DragDropService, 
        DragDropConfig
    ]
})
export class LaporanModule
{
}
