import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { DaftarPemudikComponent } from './daftarPemudik/daftarPemudik.component';
import { DaftarPemudikService } from './daftarPemudik/daftarPemudik.service';
import { FuseConfirmDialogComponent } from './../../../../core/components/confirm-dialog/confirm-dialog.component';
import { AgmCoreModule } from '@agm/core';
import { AuthGuard } from './../auth.guard';

const routes: Routes = [
    {
        path     : 'daftarPemudik',
        component: DaftarPemudikComponent,
        canActivate : [AuthGuard]
    }

];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    entryComponents: [
        DaftarPemudikComponent,
        FuseConfirmDialogComponent
    ],
    declarations: [
        DaftarPemudikComponent
    ],
    providers   : [
        DaftarPemudikService
    ]
})
export class PemudikModule
{
}
