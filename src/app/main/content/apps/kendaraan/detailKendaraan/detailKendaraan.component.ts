import { Component, OnDestroy, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { DetailKendaraanService } from './detailKendaraan.service';
import { fuseAnimations } from '../../../../../core/animations';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Product } from './detailKendaraan.model';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from './../../../../../../environments/environment';
import { FuseConfirmDialogComponent } from './../../../../../core/components/confirm-dialog/confirm-dialog.component';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { SessionService } from './../../session.service';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import swal from 'sweetalert2';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Component({
    selector     : 'fuse-e-commerce-product',
    templateUrl  : './detailKendaraan.component.html',
    styleUrls    : ['./detailKendaraan.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class DetailKendaraanComponent implements OnInit, OnDestroy
{
    product = new Product();
    onProductChanged: Subscription;
    pageType: string;
    productForm: FormGroup;
    rute: any;
    status: boolean;
    token = this._sessionService.get().token;
    i = 0;
    index = [];
    role = this._sessionService.get().role;
    kuota: any;
    showLoadingBar = true;
    onProcess = false;

    constructor(
        private productService: DetailKendaraanService,
        private formBuilder: FormBuilder,
        public snackBar: MatSnackBar,
        private router: Router,
        private http: Http,
        private dialog: MatDialog,
        private location: Location,
        private _sessionService: SessionService
    )
    {

    }

    ngOnInit()
    {
        // Subscribe to update product on changes
        this.onProductChanged =
            this.productService.onProductChanged
                .subscribe(product => {

                    if ( product )
                    {
                        this.product = new Product(product);
                        this.pageType = 'edit';
                        for (var i = 0; i < this.product.baris-2; ++i) {
                            this.index.push(i);
                        }
                        this.kuota = this.product.kuota;
                    }
                    else
                    {
                        this.pageType = 'new';
                        this.product = new Product();
                    }

                    this.productForm = this.createProductForm();
                    this.status = this.product.status == 1 ? true : false;
                    this.showLoadingBar = false;
                });
        this.getRute();
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }

    getRute(){
        let options = new RequestOptions();
        this.setHeader(options);
        this.http.get(environment.setting.base_url + "/admin/rute", options)
            .map((res: Response) => res.json())
            .subscribe((response: any) => {
                this.rute = response['data'];
            })
    }

    updateKursi(id, index){
        if (Number(this.role) > 3) {
            if (this.product.kursis[index]['status'] == 1) {
                var raw = {
                    "status" : 0
                }
                let options = new RequestOptions();
                this.setHeader(options);
                this.http.put(environment.setting.base_url + '/admin/kursi/' + id, raw, options)
                    .map((res: Response) => res.json())
                    .subscribe(data => {
                        if (data['status'] == true) {
                            this.snackBar.open('Update Kursi Sukses', 'OK', {
                                verticalPosition: 'top',
                                duration        : 5000
                            });
                            this.product.kursis[index]['status'] = 0;
                            this.kuota--;
                        }
                    }, (err) => {
                        console.log(err);
                    }
                )
            } else {
                var raw = {
                    "status" : 1
                }
                let options = new RequestOptions();
                this.setHeader(options);
                this.http.put(environment.setting.base_url + '/admin/kursi/' + id, raw, options)
                    .map((res: Response) => res.json())
                    .subscribe((response: any) => {
                        if (response['status'] == true) {
                            this.snackBar.open('Update Kursi Sukses', 'OK', {
                                verticalPosition: 'top',
                                duration        : 5000
                            });
                            this.product.kursis[index]['status'] = 1;
                            this.kuota++;
                        }
                    }, (err) => {
                        console.log(err);
                    }
                )
            }
        }
    }

    createProductForm()
    {
        if (Number(this.role) < 1) {
            return this.formBuilder.group({
                rute_id            : new FormControl({value: '', disabled: true}, [Validators.required]),
                jenis              : new FormControl({value: this.product.jenis, disabled: true}),
                nomor              : new FormControl({value: this.product.nomor, disabled: true}),
                status             : new FormControl({value: this.product.status, disabled: true}),
                baris              : new FormControl({value: this.product.baris, disabled: true}, [Validators.required]),
                kolom              : new FormControl({value: this.product.kolom, disabled: true}, [Validators.required]),
                jumlah             : new FormControl({value: this.product.jumlah, disabled: true}, [Validators.required]),
                kapasitas          : new FormControl({value: this.product.kapasitas, disabled: true}),
                rute               : new FormControl({value: this.product.rute, disabled: true}),
                kursis             : new FormControl({value: this.product.kursis, disabled: true})
            });
        }
        else {
            return this.formBuilder.group({
                rute_id            : ['', [Validators.required]],
                jenis              : [this.product.jenis],
                nomor              : [{value : this.product.nomor, disabled: true}],
                status             : [this.product.status == 1 ? true:false],
                baris              : [this.product.baris, [Validators.required]],
                kolom              : [this.product.kolom, [Validators.required]],
                jumlah             : [this.product.jumlah, [Validators.required]],
                kapasitas          : [this.product.kapasitas],
                rute               : [this.product.rute],
                kursis             : [this.product.kursis]
            });
        }
    }

    saveProduct()
    {
        const data = this.productForm.getRawValue();
        this.showLoadingBar = true;
        this.onProcess = true;
        this.productService.saveProduct(data)
            .then(() => {

                // Trigger the subscription with new data
                this.productService.onProductChanged.next(data);

                // Show the success message
                this.snackBar.open('Update Kendaraan Sukses', 'OK', {
                    verticalPosition: 'top',
                    duration        : 5000
                });

                this.showLoadingBar = false;
                this.onProcess = false;

                // Change the location with new one
                this.back();
            });
    }

    salinProduct()
    {
        this.pageType = "salin";
    }

    addProduct()
    {
        this.showLoadingBar = true;
        this.onProcess = true;
        const data = this.productForm.getRawValue();
        this.productService.addProduct(data)
            .then(() => {

                // Trigger the subscription with new data
                // this.productService.onProductChanged.next(data);
                this.showLoadingBar = false;
                this.onProcess = false;

                // Show the success message
                this.snackBar.open('Tambah Kendaraan Sukses', 'OK', {
                    verticalPosition: 'top',
                    duration        : 5000
                });

                // Change the location with new one
                this.back();
            });
    }

    changeStatus(){
        var pesan = "Aktifkan Kendaraan dari Rute ";
        var url = "/admin/kendaraan/status/"
        var navigate = '/apps/kendaraan/daftarKendaraan';
        let confirmDialog = this.dialog.open(FuseConfirmDialog, {
            width: '250',
            data: { kendaraan: this.product, status: this.status, pesan: pesan, url: url, navigate: navigate}
        });
        confirmDialog.afterClosed().subscribe(result => {
            this.status = result;
        })
    }

    ngOnDestroy()
    {
        this.onProductChanged.unsubscribe();
    }

    hapus(): void{
        // var pesan = "Hapus Kendaraan dari Rute ";
        // var url = "/admin/kendaraan/"
        // var navigate = '/apps/kendaraan/daftarKendaraan';
        // let deleteDialog = this.dialog.open(FuseConfirmDialogComponent, {
        //     width: '250',
        //     data: { id: this.product.id, rute: this.product.rute, pesan: pesan, url: url, navigate: navigate}
        // });
        swal({
            title: 'Hapus Kendaraan dari Rute' + this.product.rute['kota']['asal'] + " - " + this.product.rute['kota']['tujuan'],
            text: "Kendaraan yang telah dihapus tidak bisa dikembalikan lagi",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus !',
            cancelButtonText: 'Batal!',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                this.productService.hapus()
                    .then((response: any) => {
                        if (response.status) {
                            swal({
                                title: 'Hapus Kendaraan Berhasil!',
                                text: 'Kendaraan ' + this.product.rute['kota']['asal'] + ' - ' + this.product.rute['kota']['tujuan'] + ' Telah Dihapus',
                                type: 'success',
                                timer: 2000
                            });
                            this.back();
                        } else {
                            swal(
                                'Hapus Kendaraan Gagal!',
                                response.message,
                                'error'
                            );
                        }
                    })
                    .catch((error) => {
                        console.log(error);
                        swal(
                            'Hapus Kendaraan Gagal!',
                            'Internal server error',
                            'error'
                        )
                    })
            }
        })
    }

    bulan(date){
        if (date.substring(5, 7) === '01') {
            return "Januari";
        } else if (date.substring(5, 7) === '02') {
            return "Februari";
        } else if (date.substring(5, 7) === '03') {
            return "Maret";
        } else if (date.substring(5, 7) === '04') {
            return "April";
        } else if (date.substring(5, 7) === '05') {
            return "Mei";
        } else if (date.substring(5, 7) === '06') {
            return "Juni";
        } else if (date.substring(5, 7) === '07') {
            return "Juli";
        } else if (date.substring(5, 7) === '08') {
            return "Agustus";
        } else if (date.substring(5, 7) === '09') {
            return "September";
        } else if (date.substring(5, 7) === '10') {
            return "Oktober";
        } else if (date.substring(5, 7) === '11') {
            return "November";
        } else if (date.substring(5, 7) === '12') {
            return "Desember";
        }
    }

    back(){
        this.router.navigate(['/apps/kendaraan/daftarKendaraan']);
    }

    detailTiket(tiket) {
        this.router.navigate(['/apps/tiket/detailTiket/' + tiket]);
    }

    print() {
        this.productService.printKendaraan()
            .then((response: any) => {
                if (response.status) {
                    swal({
                        title: 'Berhasil Download Tiket.',
                        text: `Berhasil mendownload denah kendaraan`,
                        type: 'success'
                    })
                }
            })
            .catch((error) => {
                swal({
                    title: 'Internal Server Error',
                    text: error.message,
                    type: 'error'
                })
            })
    }
}

@Component({
    selector   : 'fuse-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
    styleUrls  : ['./detailKendaraan.component.scss']
})
export class FuseConfirmDialog implements OnInit
{
    public confirmMessage: string;
    token = this._sessionService.get().token;

    constructor(
        public dialogRef: MatDialogRef<FuseConfirmDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private http: Http,
        private snackBar: MatSnackBar,
        private router: Router,
        private location: Location,
        private _sessionService: SessionService
        )
    {
    }

    ngOnInit()
    {
    }

    confirm(): void{
        var st : any;
        if (this.data.status == 1) {
            st = {
                "status" : 0
            }
        } else {
            st = {
                "status" : 1
            }
        }
        let options = new RequestOptions();
        this.setHeader(options);
        this.http.put(environment.setting.base_url + "/admin/kendaraan/status/" + this.data.kendaraan.id, st, options)
            .map((res: Response) => res.json())
            .subscribe((data: any) => {
                if (data['status']) {
                    this.snackBar.open('Update status sukses', 'OK', {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                    if (this.data.status == 1) {
                        this.dialogRef.close(false);
                    } else {
                        this.dialogRef.close(true);
                    }
                } else {
                    this.dialogRef.close();
                    this.snackBar.open(data["message"], 'OK', {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                }
            });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.token);
        options.headers = headers;
    }

}
