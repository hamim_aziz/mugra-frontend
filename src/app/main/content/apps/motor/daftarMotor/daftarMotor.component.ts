import {Component, AfterViewInit, ViewChild} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {merge} from 'rxjs/observable/merge';
import {of as observableOf} from 'rxjs/observable/of';
import {catchError} from 'rxjs/operators/catchError';
import {map} from 'rxjs/operators/map';
import {startWith} from 'rxjs/operators/startWith';
import {switchMap} from 'rxjs/operators/switchMap';
import { fuseAnimations } from '../../../../../core/animations';
import { environment } from './../../../../../../environments/environment';
import { Router } from '@angular/router';

@Component({
    selector   : 'fuse-e-commerce-products',
    templateUrl: './daftarMotor.component.html',
    styleUrls  : ['./daftarMotor.component.scss'],
    animations : fuseAnimations
})
export class DaftarMotorComponent implements AfterViewInit
{
    displayedColumns = ['id', 'kode_tiket', 'nama', 'nik', 'jenis_kelamin', 'rute', 'kendaraan', 'kursi', 'status'];
    exampleDatabase: ExampleHttpDao | null;
    dataSource = new MatTableDataSource();

    resultsLength = 0;
    isLoadingResults = false;
    isRateLimitReached = false;

    search_query: string;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private http: HttpClient, private router: Router) {}

    ngAfterViewInit() {
        if (localStorage.getItem('motor-page')) {
            this.paginator.pageIndex = Number(localStorage.getItem('motor-page'));
        }
        this.exampleDatabase = new ExampleHttpDao(this.http);
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    this.isLoadingResults = true;
                    return this.exampleDatabase!.getPemudik(
                        this.sort.active, this.sort.direction, this.paginator.pageIndex);
                }),
                map(data => {
                    this.isLoadingResults = false;
                    this.isRateLimitReached = false;
                    this.resultsLength = data.data['total'];

                    return data.data;
                }),
                catchError(() => {
                    this.isLoadingResults = false;
                    this.isRateLimitReached = true;
                    return observableOf([]);
                })
            ).subscribe(data => {
                this.dataSource.data = data['data'];
            });
    }

    clickTiket(nomor_tiket, status): void{
        if (status != 2 && status != 4) {
            localStorage.setItem('motor-page', String(this.paginator.pageIndex));
            localStorage.removeItem('pemudik-item');
            localStorage.removeItem('tiket-item');
            var url = '/apps/tiket/detailTiket/' + nomor_tiket;
            this.router.navigate([url]);
        }
    }

    clear(){
        this.paginator.pageIndex = 0;
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    this.isLoadingResults = true;
                    return this.exampleDatabase!.getPemudik(
                        this.sort.active, this.sort.direction, this.paginator.pageIndex);
                }),
                map(data => {
                    this.isLoadingResults = false;
                    this.isRateLimitReached = false;
                    this.resultsLength = data.data['total'];
                    return data.data;
                }),
                catchError(() => {
                    this.isLoadingResults = false;
                    this.isRateLimitReached = true;
                    return observableOf([]);
                })
            ).subscribe(data => {
                this.dataSource.data = data['data'];
            });
    }

    search(){
        let search = "";
        if (this.search_query) {
          search = this.search_query;
        } else {
          search = "";
        }
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    this.isLoadingResults = true;
                    return this.exampleDatabase!.getPemudikSearch(
                      this.sort.active, this.sort.direction, search);
                }),
                map(data => {
                    this.isLoadingResults = false;
                    this.isRateLimitReached = false;
                    this.resultsLength = data.data['total'];
                    return data.data;
                }),
                catchError(() => {
                    this.isLoadingResults = false;
                    this.isRateLimitReached = true;
                    return observableOf([]);
                })
            ).subscribe(data => {
                this.dataSource.data = data['data'];
            });
    }

    filterButton(filter: any){
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    this.isLoadingResults = true;
                    return this.exampleDatabase!.getPemudikSearch(
                        this.sort.active, this.sort.direction, filter);
                }),
                map(data => {
                    this.isLoadingResults = false;
                    this.isRateLimitReached = false;
                    this.resultsLength = data.data['total'];
                    return data.data;
                }),
                catchError(() => {
                    this.isLoadingResults = false;
                    this.isRateLimitReached = true;
                    return observableOf([]);
                })
            ).subscribe(data => {
                this.dataSource.data = data['data'];
            });
    }

}

export interface pemudikApi {
    data: pemudikApiData[];
    total: number;
}

export interface pemudikApiData {
    data: any;
    total: number;
}

export class ExampleHttpDao {

    constructor(private http: HttpClient) {}

    getPemudik(sort: string, order: string, page: number): Observable<pemudikApi> {
        const href = environment.setting.base_url + '/admin/pemudik/motor';
        const requestUrl =
            `${href}?page=${page + 1}`;

        return this.http.get<pemudikApi>(requestUrl, httpOptions);
    }

    getPemudikSearch(sort: string, order: string, search: string): Observable<pemudikApi> {
        const href = environment.setting.base_url + '/admin/pemudik/motor/search/'+search;

        return this.http.get<pemudikApi>(href, httpOptions);
    }

}

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': "Bearer " + JSON.parse(localStorage.getItem('currentUser')).token
    })
};
