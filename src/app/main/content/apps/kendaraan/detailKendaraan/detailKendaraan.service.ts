import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import 'rxjs/add/operator/map';
import { MatSnackBar } from '@angular/material';
import { SessionService } from './../../session.service';

@Injectable()
export class DetailKendaraanService implements Resolve<any>
{
    routeParams: any;
    product: any;
    onProductChanged: BehaviorSubject<any> = new BehaviorSubject({});
    token = this._sessionService.get().token;

    constructor(
        private http: Http,
        private snackBar: MatSnackBar,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProduct()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getProduct(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            if ( this.routeParams.id === 'new' )
            {
                this.onProductChanged.next(false);
                resolve(false);
            }
            else
            {
                let options = new RequestOptions();
                this.setHeader(options);
                this.http.get(environment.setting.base_url + '/admin/kendaraan/' + this.routeParams.id, options)
                    .map((res: Response) => res.json())
                    .subscribe((response: any) => {
                        this.product = response['data'];
                        this.onProductChanged.next(this.product);
                        resolve(response);
                    }, (err) => {
                        let eror = JSON.parse(err._body);
                        this.snackBar.open(eror.message, 'Error !', {
                            verticalPosition: 'top',
                            duration        : 5000
                        });
                        reject(err);
                    });
            }
        });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }

    saveProduct(product)
    {
        var object = {
            rute_id: product.rute_id,
            jenis: product.jenis,
            nomor: product.nomor,
            nama: product.nama,
            baris: product.baris,
            kolom: product.kolom
        };
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/kendaraan/' + this.routeParams.id, object, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (err) => {
                    let eror = JSON.parse(err._body);
                    this.snackBar.open(eror.message, 'Error !', {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                    reject(err);
                });
        });
    }

    addProduct(product)
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.post(environment.setting.base_url + '/admin/kendaraan', product, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (err) => {
                    let eror = JSON.parse(err._body);
                    this.snackBar.open(eror.message, 'Error !', {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                    reject(err);
                });
        });
    }

    hapus()
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.delete(environment.setting.base_url + '/admin/kendaraan/' + this.routeParams.id, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (err) => {
                    reject(err);
                });
        });
    }

    printKendaraan(): Promise<any> {
        let options = new RequestOptions();
        let header = new Headers();
        header.append('Authorization', `Bearer ${this.token}`);
        header.append('Content-Type', 'application/json');
        header.append('Accept', 'application/pdf');
        options.headers = header;

        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url + "/admin/kendaraan/printDenah/" + this.routeParams.id, {
                    headers: header, responseType: ResponseContentType.Blob
                })
                .map((res) => {
                    return {
                        filename: `kendaraan-${this.product.nomor}.pdf`,
                        data: res.blob(),
                        success: true
                    };
                })
                .subscribe((res: any) => {
                    var url = window.URL.createObjectURL(res.data);
                    var a = document.createElement('a');
                    document.body.appendChild(a);
                    a.setAttribute('style', 'display: none');
                    a.href = url;
                    a.download = res.filename;
                    a.click();
                    window.URL.revokeObjectURL(url);
                    a.remove(); // remove the element
                    resolve(res);
                }, (err) => {
                    reject(err);
                    console.log('download error:', JSON.stringify(err));
                }, () => {
                    
                })
        });
    }
}
