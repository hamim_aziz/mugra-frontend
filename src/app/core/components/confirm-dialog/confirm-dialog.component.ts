import { Component, OnInit, Inject } from '@angular/core';
import { Location } from '@angular/common';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { environment } from './../../../../environments/environment';
import { Router } from '@angular/router';

@Component({
    selector   : 'fuse-confirm-dialog',
    templateUrl: './confirm-dialog.component.html',
    styleUrls  : ['./confirm-dialog.component.scss']
})
export class FuseConfirmDialogComponent implements OnInit
{
    public confirmMessage: string;
    token = sessionStorage.getItem('token');

    constructor(
        public dialogRef: MatDialogRef<FuseConfirmDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private http: Http,
        private snackBar: MatSnackBar,
        private router: Router,
        private location: Location
        )
    {
    }

    ngOnInit()
    {
    }

    confirm(id): void{
        let options = new RequestOptions();
        this.setHeader(options);
        // console.log(options);
        this.http.delete(environment.setting.base_url + this.data.url + id, options)
            .map((res: Response) => res.json())
            .subscribe((response: any) => {
                if (!response) {
                    this.dialogRef.close();
                    this.snackBar.open(this.data.pesan + " " + this.data.username, 'Sukses !', {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                    this.location.back();
                } else {
                    if (response['status'] == false) {
                        this.dialogRef.close();
                        this.snackBar.open(response['message'], 'Eror !', {
                            verticalPosition: 'top',
                            duration        : 5000
                        });
                    } else {
                        this.dialogRef.close();
                        this.snackBar.open(response['message'], 'Ok !', {
                            verticalPosition: 'top',
                            duration        : 5000
                        });
                        this.location.back();
                    }
                }
            }, err => {
                this.dialogRef.close();
                console.log(err);
                this.snackBar.open(err.statusText, 'Error !', {
                    verticalPosition: 'top',
                    duration        : 5000
                });
            })
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        // console.log(this.token);
        headers.append('Authorization', 'Bearer ' + this.token);
        options.headers = headers;
    }

}
