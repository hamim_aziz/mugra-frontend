import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { MatSnackBar } from '@angular/material';
import { SessionService } from './../../session.service';

@Injectable()
export class DaftarKendaraanService implements Resolve<any>
{
    products: any[];
    kota: any[];
    onProductsChanged: BehaviorSubject<any> = new BehaviorSubject({});
    token = this._sessionService.get().token;

    constructor(
        private http: Http,
        private snackBar: MatSnackBar,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProducts()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }

    getProducts(): Promise<any>
    {
        let options = new RequestOptions();
        this.setHeader(options);
        var role = Number(this._sessionService.get().role);
        var name = this._sessionService.get().email.toLowerCase();
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url+'/admin/kendaraan', options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    this.products = response['data'];
                    for (var i = 0; i < this.products.length; ++i) {
                        var ass = this.products[i]['asal'].toLowerCase();
                        var tu = this.products[i]['tujuan'].toLowerCase();
                        this.products[i]['asal'] = this.products[i]['asal'] + " - " + this.products[i]['tujuan'];
                        this.products[i]['jenis_rute'] == 1 ?
                        this.products[i]['jenis_rute'] = 'Mudik' :
                        this.products[i]['jenis_rute'] = 'Balik';
                        if (this.products[i].status == 1) {
                            this.products[i].status = "Aktif";
                        } else {
                            this.products[i].status = "Tidak Aktif";
                        }
                        if (role == 1) {
                            if (name.search(ass) == -1 && name.search(tu) == -1) {
                                this.products.splice(i, 1);
                                i--;
                            }
                        }
                    }
                    this.onProductsChanged.next(this.products);
                    resolve(response);
                }, (reject) => {
                    let eror = JSON.parse(reject._body);
                    this.snackBar.open(eror.message, 'Error !', {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                });
        });
    }
}
