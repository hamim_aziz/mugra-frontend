import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { DaftarKendaraanComponent } from './daftarKendaraan/daftarKendaraan.component';
import { DaftarKendaraanService } from './daftarKendaraan/daftarKendaraan.service';
import { DetailKendaraanComponent, FuseConfirmDialog } from './detailKendaraan/detailKendaraan.component';
import { DetailKendaraanService } from './detailKendaraan/detailKendaraan.service';
import { FuseConfirmDialogComponent } from './../../../../core/components/confirm-dialog/confirm-dialog.component';
import { AgmCoreModule } from '@agm/core';
import { AuthGuard } from './../auth.guard';

const routes: Routes = [
    {
        path     : 'daftarKendaraan',
        component: DaftarKendaraanComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DaftarKendaraanService
        }
    },
    {
        path     : 'detailKendaraan/:id',
        component: DetailKendaraanComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DetailKendaraanService
        }
    }

];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    entryComponents: [
        DaftarKendaraanComponent,
        FuseConfirmDialogComponent,
        FuseConfirmDialog
    ],
    declarations: [
        DaftarKendaraanComponent,
        DetailKendaraanComponent,
        FuseConfirmDialog
    ],
    providers   : [
        DaftarKendaraanService,
        DetailKendaraanService
    ]
})
export class KendaraanModule
{
}
