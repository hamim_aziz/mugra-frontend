import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { DetailFeedbackService } from './detailFeedback.service';
import { fuseAnimations } from '../../../../../core/animations';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Product } from './detailFeedback.model';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from './../../session.service';

@Component({
    selector     : 'fuse-e-commerce-product',
    templateUrl  : './detailFeedback.component.html',
    styleUrls    : ['./detailFeedback.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class DetailFeedbackComponent implements OnInit, OnDestroy
{
    product = new Product();
    onProductChanged: Subscription;
    pageType: string;
    productForm: FormGroup;
    token = this._sessionService.get().token;

    constructor(
        private productService: DetailFeedbackService,
        private formBuilder: FormBuilder,
        public snackBar: MatSnackBar,
        private location: Location,
        private http: Http,
        private route: ActivatedRoute,
        private _sessionService: SessionService
    )
    {

    }

    ngOnInit()
    {
        // Subscribe to update product on changes
        this.onProductChanged =
            this.productService.onProductChanged
                .subscribe(product => {

                    if ( product )
                    {
                        this.product = new Product(product);
                        this.pageType = 'edit';
                    }
                    else
                    {
                        this.pageType = 'new';
                        this.product = new Product();
                    }

                    this.productForm = this.createProductForm();
                });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }

    createProductForm()
    {
        return this.formBuilder.group({
            id               : new FormControl({value: this.product.id, disabled: true}, [Validators.required]),
            nik              : new FormControl({value: this.product.nik, disabled: true}, [Validators.required]),
            no_kk            : new FormControl({value: this.product.no_kk, disabled: true}, [Validators.required]),
            comment          : new FormControl(this.product.comment, [Validators.required]),
            nilai            : new FormControl({value: this.product.nilai, disabled: true}, [Validators.required]),
            status           : new FormControl(this.product.status == 0 ? false : true, [Validators.required])
        });
    }

    saveProduct()
    {
        if (this.productForm.controls.status.value) {
            this.productForm.controls.status.setValue(1);
        } else {
            this.productForm.controls.status.setValue(0);
        }
        const data = this.productForm.getRawValue();
        this.productService.saveProduct(data)
            .then(() => {

                // Trigger the subscription with new data
                this.productService.onProductChanged.next(data);

                // Show the success message
                this.snackBar.open('Update Feedback Sukses', 'OK', {
                    verticalPosition: 'top',
                    duration        : 2000
                });

                this.location.back();
            });
    }

    addProduct()
    {
        if (this.productForm.controls.status.value) {
            this.productForm.controls.status.setValue(1);
        } else {
            this.productForm.controls.status.setValue(0);
        }
        const data = this.productForm.getRawValue();
        this.productService.addProduct(data)
            .then(() => {

                // Trigger the subscription with new data
                this.productService.onProductChanged.next(data);

                // Show the success message
                this.snackBar.open('Tambah Feedback Sukses', 'OK', {
                    verticalPosition: 'top',
                    duration        : 2000
                });

                // Change the location with new one
                this.location.back();
            });
    }

    ngOnDestroy()
    {
        this.onProductChanged.unsubscribe();
    }
}
