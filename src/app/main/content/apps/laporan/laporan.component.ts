import { Component, ElementRef, OnInit, ViewChild, Injectable } from '@angular/core';
import { DaftarRuteService } from './laporan.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { fuseAnimations } from '../../../../core/animations';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from '@angular/router';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { FuseUtils } from '../../../../core/fuseUtils';
import { FuseConfirmDialogComponent } from './../../../../core/components/confirm-dialog/confirm-dialog.component';
import { environment } from './../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { SessionService } from './../session.service';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Component({
    selector   : 'fuse-e-commerce-products',
    templateUrl: './laporan.component.html',
    styleUrls  : ['./laporan.component.scss'],
    animations : fuseAnimations
})
@Injectable()
export class DaftarRuteComponent implements OnInit
{
    role = Number(this._sessionService.get().role);
    token = this._sessionService.get().token;
    report: any;
    reportMotor: any;
    rute: any;
    arrayMudik = [];
    arrayBalik = [];
    arrayMudikMotor = [];
    arrayBalikMotor = [];

    jumlah_mudik = [];
    jumlah_balik = [];
    jumlah_mudik_motor = [];
    jumlah_balik_motor = [];


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private productsService: DaftarRuteService,
        private dialog: MatDialog,
        private router: Router,
        private http: Http,
        private _sessionService: SessionService
    )
    {
    }

    ngOnInit()
    {
        for (let i = 0; i < 40; ++i) {
            this.jumlah_mudik.push(i);
        }
        for (let i = 0; i < 37; ++i) {
            this.jumlah_balik.push(i);
        }
        for (let i = 0; i < 20; ++i) {
            this.jumlah_mudik_motor.push(i);
        }
        for (let i = 0; i < 10; ++i) {
            this.jumlah_balik_motor.push(i);
        }
        this.report = this.productsService.products;
        this.reportMotor = this.productsService.reportTitipMotor;
    }

    public printLaporan(): void {
        this.arrayMudik = [];
        this.arrayBalik = [];
        this.arrayMudikMotor = [];
        this.arrayBalikMotor = [];
        for (const index in this.report.mudik_biasa) {
            const arrMudik = {
                'No.'                : (Number(index) + 1),
                'Rute'               : this.report.mudik_biasa[index].rute,
                'Jumlah Kendaraan'   : this.report.mudik_biasa[index].kendaraaan,
                'Total Kuota Mudik'  : this.report.mudik_biasa[index].total,

                'Kuota Online'       : this.report.mudik_biasa[index].online.sisa + this.report.mudik_biasa[index].online.booking + this.report.mudik_biasa[index].online.terdaftar + this.report.mudik_biasa[index].online.verified,
                'Booking Online'     : this.report.mudik_biasa[index].online.terdaftar,
                'Approved Online'    : this.report.mudik_biasa[index].online.verified,
                'Sisa Online'        : this.report.mudik_biasa[index].online.sisa,

                'Kuota Operator'     : this.report.mudik_biasa[index].offline.sisa + this.report.mudik_biasa[index].offline.booking + this.report.mudik_biasa[index].offline.terdaftar + this.report.mudik_biasa[index].offline.verified,
                'Approved Operator'  : this.report.mudik_biasa[index].offline.verified,
                'Sisa Operator'      : this.report.mudik_biasa[index].offline.sisa,

                'Kuota Cadangan'     : this.report.mudik_biasa[index].online.disable + this.report.mudik_biasa[index].offline.disable,
                'Tanggal'            : this.report.mudik_biasa[index].tanggal
            };
            this.arrayMudik.push(arrMudik);
        }
        for (const index in this.report.balik_biasa) {
            const arrBalik = {
                'No.'                : (Number(index) + 1),
                'Rute'               : this.report.balik_biasa[index].rute,
                'Jumlah Kendaraan'   : this.report.balik_biasa[index].kendaraaan,
                'Total Kuota Mudik'  : this.report.balik_biasa[index].total,

                'Kuota Online'       : this.report.balik_biasa[index].online.sisa + this.report.balik_biasa[index].online.booking + this.report.balik_biasa[index].online.terdaftar + this.report.balik_biasa[index].online.verified,
                'Booking Online'     : this.report.balik_biasa[index].online.terdaftar,
                'Approved Online'    : this.report.balik_biasa[index].online.verified,
                'Sisa Online'        : this.report.balik_biasa[index].online.sisa,

                'Kuota Operator'     : this.report.balik_biasa[index].offline.sisa + this.report.balik_biasa[index].offline.booking + this.report.balik_biasa[index].offline.terdaftar + this.report.balik_biasa[index].offline.verified,
                'Approved Operator'  : this.report.balik_biasa[index].offline.verified,
                'Sisa Operator'      : this.report.balik_biasa[index].offline.sisa,

                'Kuota Cadangan'     : this.report.balik_biasa[index].online.disable + this.report.balik_biasa[index].offline.disable,
                'Tanggal'            : this.report.balik_biasa[index].tanggal
            };
            this.arrayBalik.push(arrBalik);
        }
        for (const index in this.reportMotor.mudik) {
            const arrMudikMotor = {
                'No.'                : (Number(index) + 1),
                'Rute'               : this.reportMotor.mudik[index].kota,
                'Jumlah Motor'       : this.reportMotor.mudik[index].total_motor
            };
            this.arrayMudikMotor.push(arrMudikMotor);
        }
        for (const index in this.reportMotor.balik) {
            const arrBalikMotor = {
                'No.'                : (Number(index) + 1),
                'Rute'               : this.reportMotor.balik[index].kota,
                'Jumlah Motor'       : this.reportMotor.balik[index].total_motor
            };
            this.arrayBalikMotor.push(arrBalikMotor);
        }
        const statistik = [];
        statistik.push({'kriteria' : 'Laki-laki', 'nilai': this.report.pemudik.laki_laki});
        statistik.push({'kriteria' : 'Perempuan', 'nilai': this.report.pemudik.wanita});
        statistik.push({'kriteria' : '', 'nilai': ''});
        if (this.report.pemudik.pekerjaan) {
            statistik.push({'kriteria' : 'PNS / TNI / Polri / Pensiunan', 'nilai': this.report.pemudik.pekerjaan['PNS / TNI / Polri / Pensiunan']});
            statistik.push({'kriteria' : 'Wiraswasta', 'nilai': this.report.pemudik.pekerjaan['Wiraswasta']});
            statistik.push({'kriteria' : 'Karyawan Swasta / Buruh', 'nilai': this.report.pemudik.pekerjaan['Karyawan Swasta / Buruh']});
            statistik.push({'kriteria' : 'Pelajar / Mahasiswa', 'nilai': this.report.pemudik.pekerjaan['Pelajar / Mahasiswa']});
            statistik.push({'kriteria' : 'Pembantu Rumah Tangga', 'nilai': this.report.pemudik.pekerjaan['Pembantu Rumah Tangga']});
            statistik.push({'kriteria' : 'Dokter / Pengacara / Profesional', 'nilai': this.report.pemudik.pekerjaan['Dokter / Pengacara / Professional']});
            statistik.push({'kriteria' : 'Tidak Bekerja / Ibu Rumah Tangga / Lainnya', 'nilai': this.report.pemudik.pekerjaan['Tidak Bekerja / Ibu Rumah Tangga / Lainnya']});
        }
        const persentase = [];
        persentase.push({
            'Tipe'                         : 'Mudik',
            'Total'                        : this.report.persentase.mudik.total,
            'Pendaftar Online'             : this.report.persentase.mudik.pendaftar_online,
            'Persentase Pendaftar Online'  : this.report.persentase.mudik.persentase_pendaftar_online,
            'Pendaftar Offline'            : this.report.persentase.mudik.pendaftar_offline,
            'Persentase Pendaftar Offline' : this.report.persentase.mudik.persentase_pendaftar_offline,
            'Sisa Kuota'                   : this.report.persentase.mudik.sisa_kuota,
            'Persentase Sisa Kuota'        : this.report.persentase.mudik.persentase_sisa_kuota,
            'Grand Total'                  : this.report.persentase.mudik.grand_total,
            'Persentase Grand Total'       : this.report.persentase.mudik.persentase_grand_total
        });
        persentase.push({
            'Tipe'                         : 'Balik',
            'Total'                        : this.report.persentase.balik.total,
            'Pendaftar Online'             : this.report.persentase.balik.pendaftar_online,
            'Persentase Pendaftar Online'  : this.report.persentase.balik.persentase_pendaftar_online,
            'Pendaftar Offline'            : this.report.persentase.balik.pendaftar_offline,
            'Persentase Pendaftar Offline' : this.report.persentase.balik.persentase_pendaftar_offline,
            'Sisa Kuota'                   : this.report.persentase.balik.sisa_kuota,
            'Persentase Sisa Kuota'        : this.report.persentase.balik.persentase_sisa_kuota,
            'Grand Total'                  : this.report.persentase.balik.grand_total,
            'Persentase Grand Total'       : this.report.persentase.balik.persentase_grand_total
        });
        const jsonMudik = this.arrayMudik;
        const jsonBalik = this.arrayBalik;
        const jsonMudikMotor = this.arrayMudikMotor;
        const jsonBalikMotor = this.arrayBalikMotor;
        const excelFileName = 'Laporan Mudik dan Balik Gratis ';
        const worksheetMudik: XLSX.WorkSheet = XLSX.utils.json_to_sheet(jsonMudik);
        const worksheetBalik: XLSX.WorkSheet = XLSX.utils.json_to_sheet(jsonBalik);
        const worksheetMudikMotor: XLSX.WorkSheet = XLSX.utils.json_to_sheet(jsonMudikMotor);
        const worksheetBalikMotor: XLSX.WorkSheet = XLSX.utils.json_to_sheet(jsonBalikMotor);
        const worksheetStatistik: XLSX.WorkSheet = XLSX.utils.json_to_sheet(statistik);
        const worksheetPersentase: XLSX.WorkSheet = XLSX.utils.json_to_sheet(persentase);
        const workbook: XLSX.WorkBook = { 
            Sheets: { 
                'Mudik': worksheetMudik, 
                'Balik': worksheetBalik,
                'Mudik Motor': worksheetMudikMotor,
                'Balik Motor': worksheetBalikMotor,
                'Statistik': worksheetStatistik,
                'Persentase': worksheetPersentase
            }, 
            SheetNames: [
                'Mudik', 
                'Balik',
                'Mudik Motor',
                'Balik Motor',
                'Statistik',
                'Persentase'
            ] 
        };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        let d = new Date();
        const data: Blob = new Blob([buffer], {
          type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + 'Tanggal ' + d.getDate() + '.' + (d.getMonth() + 1) + '.' + d.getFullYear() + ' Jam ' + d.getHours() + '.' + d.getMinutes() + EXCEL_EXTENSION);
    }

    setHeader(options: RequestOptions) {
        const headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }
}
