import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {
    set(data) {
        localStorage.setItem('currentUser', JSON.stringify(data));
        const user = localStorage.getItem('currentUser');
    }

    get() {
        const user = localStorage.getItem('currentUser');
        return user ? JSON.parse(user) : null;
    }

    patch(data) {
        const user = this.get();
        this.set({ ...user, ...data });
    }

    clear() {
        localStorage.removeItem('currentUser');
    }
}
