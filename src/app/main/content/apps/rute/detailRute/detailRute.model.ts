import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatChipInputEvent } from '@angular/material';

export class Product
{
    id: number;
    jenis: any;
    keterangan: string;
    tanggal: string;
    asal: string;
    tujuan: string;
    kouta_motor: number;
    contact_person: string;
    tempat_berangkat: string;
    tempat_ambil_tiket: string;
    status: string;
    jam: string;
    is_titip_motor: number;
    is_titip_motor_balik: number;
    kode: number;
    kendaraans:{
        id: number,
        jenis: number,
        status: number,
        nomor: string,
        baris: string,
        kolom: string
    }[];
    subkotas: {
        id: number,
        nama: string,
        kota_id: number
    }[];

    constructor(product?)
    {
        if (product) {
            product = product;
            this.id = product.id;
            this.jenis = product.jenis;
            this.keterangan = product.keterangan;
            this.tanggal = product.tanggal;
            this.jam = product.jam;
            if (product.asal) {
                this.asal = product.asal;
                this.tujuan = product.tujuan;
            } else {
                this.asal = product.kota.asal;
                this.tujuan = product.kota.tujuan;
            }
            this.status = product.status;
            this.kouta_motor = product.kouta_motor;
            this.contact_person = product.contact_person;
            this.tempat_berangkat = product.tempat_berangkat;
            this.tempat_ambil_tiket = product.tempat_ambil_tiket;
            this.kendaraans = product.kendaraans;
            this.subkotas = product.subkotas;
            this.is_titip_motor = product.is_titip_motor;
            this.is_titip_motor_balik = product.is_titip_motor_balik;
            this.kode = product.kode;
        } else {
            product = {};
            this.id = 0;
            this.jenis = "1";
            this.keterangan = '';
            this.tanggal = '';
            this.jam = '';
            this.asal = '';
            this.tujuan = '';
            this.kouta_motor = 0;
            this.is_titip_motor = 0;
            this.is_titip_motor_balik = 0;
            this.contact_person = '031-8294459';
            this.tempat_berangkat = 'Dinas Perhubungan Provinsi Jawa Timur, Jl Ahmad Yani 268 Surabaya';
            this.tempat_ambil_tiket = 'Dishub Jatim Jl A Yani 268 Sby';
            this.status = '';
            this.kendaraans = [];
            this.subkotas = [];
            this.kode = 0;
        }
    }

    // addCategory(event: MatChipInputEvent): void
    // {
    //     const input = event.input;
    //     const value = event.value;

    //     // Add category
    //     if ( value )
    //     {
    //         this.categories.push(value);
    //     }

    //     // Reset the input value
    //     if ( input )
    //     {
    //         input.value = '';
    //     }
    // }

    // removeCategory(category)
    // {
    //     const index = this.categories.indexOf(category);

    //     if ( index >= 0 )
    //     {
    //         this.categories.splice(index, 1);
    //     }
    // }

    // addTag(event: MatChipInputEvent): void
    // {
    //     const input = event.input;
    //     const value = event.value;

    //     // Add tag
    //     if ( value )
    //     {
    //         this.tags.push(value);
    //     }

    //     // Reset the input value
    //     if ( input )
    //     {
    //         input.value = '';
    //     }
    // }

    // removeTag(tag)
    // {
    //     const index = this.tags.indexOf(tag);

    //     if ( index >= 0 )
    //     {
    //         this.tags.splice(index, 1);
    //     }
    // }
}
