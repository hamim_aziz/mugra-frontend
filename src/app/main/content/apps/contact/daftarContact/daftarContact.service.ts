import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from './../../session.service';

@Injectable()
export class DaftarContactService implements Resolve<any>
{
    contacts: any[];
    onContactsChanged: BehaviorSubject<any> = new BehaviorSubject({});
    token = this._sessionService.get().token;

    constructor(
        private http: Http,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getContacts()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }

    getContacts(): Promise<any>
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url+'/admin/cps', options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    console.log(response);
                    this.contacts = response['data'];
                    this.onContactsChanged.next(this.contacts);
                    resolve(response);
                }, reject);
        });
    }

    updateContact(contact): Promise<any>
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/cps/' + contact.id, contact, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response)
                }, (error) => {
                    reject(error);
                })
        });
    }

    deleteContact(contact): Promise<any>
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/cps/' + contact.id, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response)
                }, (error) => {
                    reject(error);
                })
        });
    }
}
