import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { MatSnackBar } from '@angular/material';
import 'rxjs/add/operator/map';
import { SessionService } from './../../session.service';

@Injectable()
export class DetailTiketService implements Resolve<any>
{
    routeParams: any;
    product: any;
    token = this._sessionService.get().token;
    onProductChanged: BehaviorSubject<any> = new BehaviorSubject({});
    asalMudik = [];
    asalBalik = [];
    listKotaMudik: any;
    listKotaBalik: any;
    list_kursi: any = [];

    constructor(
        private http: Http,
        private snackBar: MatSnackBar,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProduct(),
                this.getAsalBalik(),
                this.getAsalMudik(),
            ]).then(
                () => {
                    resolve();
                },
                reject
            ).catch((error) => {
                console.log(error);
            });
        });
    }

    getProduct(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            if ( this.routeParams.id === 'daftarMudik' )
            {
                this.onProductChanged.next(false);
                resolve(false);
            }
            else
            {
                this.http.get(environment.setting.base_url + '/pemudik/booking/detail/' + this.routeParams.id, this.setHeader())
                    .map((res: Response) => res.json())
                    .subscribe((response: any) => {
                        console.log(response);
                        this.getKursiKosong(response.data.kendaraan.id)
                            .then((result: any) => {
                                this.list_kursi = [];
                                for (let i = 0; i < result.data.length; ++i) {
                                    const kursi = result.data[i];
                                    this.list_kursi.push(kursi);
                                }
                                this.product = response['data'];
                                this.onProductChanged.next(this.product);
                                resolve(response);
                            });
                    }, (error) => {
                        reject(error);
                    });
            }
        });
    }

    getAsalMudik(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url + '/admin/pemudik/kota/mudik', this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    for (let i = 0; i < response.data.length; ++i) {
                        if (!this.asalMudik.includes(response.data[i].asal)) {
                            this.asalMudik.push(response.data[i].asal);
                        }
                    }
                    this.listKotaMudik = response.data;
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    getAsalBalik(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url + '/admin/pemudik/kota/balik', this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    for (let i = 0; i < response.data.length; ++i) {
                        if (!this.asalBalik.includes(response.data[i].asal)) {
                            this.asalBalik.push(response.data[i].asal);
                        }
                    }
                    this.listKotaBalik = response.data;
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    getRuteByAsalTujuan(asal, tujuan){
        const url = environment.setting.base_url + '/pemudik/rute/'
                + asal + '/' 
                + tujuan;
        return this.http.get(url, this.setHeader());
    }

    booking(data){
        return new Promise((resolve, reject) => {
            this.http.post(environment.setting.base_url + '/admin/pemudik/biasa/booking', data, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    bookingMotor(data){
        return new Promise((resolve, reject) => {
            this.http.post(environment.setting.base_url + '/admin/pemudik/motor/booking', data, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    addPemudik(data){
        return new Promise((resolve, reject) => {
            this.http.post(environment.setting.base_url + '/admin/pemudik/biasa/create', data, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (err) => {
                    reject(err);
                });
        });
    }

    addPemudikMotor(data){
        return new Promise((resolve, reject) => {
            this.http.post(environment.setting.base_url + '/admin/pemudik/motor/create', data, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    approve(status): Promise<any>{
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/pemudik/approve/' + this.routeParams.id, {'status': status}, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    decline(status): Promise<any>{
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/pemudik/decline/' + this.routeParams.id, {'status': status}, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    save(data, id): Promise<any>{
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/tiket/' + id, data, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    savePemudik(data, id): Promise<any>{
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/pemudik/' + id, data, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    pindahKursi(data): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.post(environment.setting.base_url + '/admin/pemudik/pindah/kursi', data, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    hapusPemudik(id): Promise<any>{
        return new Promise((resolve, reject) => {
            this.http.delete(environment.setting.base_url + '/admin/pemudik/delete/' + id, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    hapusTiket(): Promise<any>{
        return new Promise((resolve, reject) => {
            this.http.delete(environment.setting.base_url + '/admin/pemudik/biasa/cancel/' + this.routeParams.id, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    batalTiket(tiket): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.delete(environment.setting.base_url + '/admin/pemudik/tiket/' + tiket, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (err) => {
                    reject(err);
                });
        });
    }

    // printTiket(tiket): Promise<any> {
        // const options = new RequestOptions();
        // const header = new Headers();
        // header.append('Authorization', `Bearer ${this.token}`);
        // header.append('Content-Type', 'application/json');
        // header.append('Accept', 'application/pdf');
        // options.headers = header;

        // return new Promise((resolve, reject) => {
        //     this.http.get(environment.setting.base_url + '/admin/tiket/' + tiket + '/download', {
        //             headers: header, responseType: ResponseContentType.Blob
        //         })
        //         .map((res) => {
        //             return {
        //                 filename: `tiket-${tiket}.pdf`,
        //                 data: res.blob(),
        //                 success: true
        //             };
        //         })
        //         .subscribe((res: any) => {
        //             const url = window.URL.createObjectURL(res.data);
        //             const a = document.createElement('a');
        //             document.body.appendChild(a);
        //             a.setAttribute('style', 'display: none');
        //             a.href = url;
        //             a.download = res.filename;
        //             a.click();
        //             window.URL.revokeObjectURL(url);
        //             a.remove(); // remove the element
        //             resolve(res);
        //         }, (err) => {
        //             console.log('download error:', JSON.stringify(err));
        //             reject(err);
        //         }, () => {
                    
        //         });
        // });
    // }

    getKursiKosong(id): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url + '/admin/kendaraan/getListKursiKosong/' + id, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    console.log(response);
                    resolve(response);
                }, (err) => {
                    reject(err);
                });
        });
    }

    getKendaraan(id): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url + '/admin/kendaraan/' + id, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (err) => {
                    reject(err);
                });
        });
    }

    checkNIK(tiket, nik): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url + '/pemudik/checkNik/' + tiket + '/' + nik)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (err) => {
                    reject(err);
                });
        });
    }

    checkEmailNotTaken(tiket: string, nik: string) {
        return this.http.get(environment.setting.base_url + '/pemudik/checkNik/' + tiket + '/' + nik)
                        .map(res => res.json());
      }

    setHeader() {
        const options = new RequestOptions();
        const headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        options.headers = headers;
        return options;
    }

}
