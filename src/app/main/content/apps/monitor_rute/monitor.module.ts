import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { MonitorRuteComponent } from './monitorRute/monitorRute.component';
import { MonitorRuteService } from './monitorRute/monitorRute.service';
import { FuseConfirmDialogComponent } from './../../../../core/components/confirm-dialog/confirm-dialog.component';
import { AgmCoreModule } from '@agm/core';
import { CdkTableModule } from '@angular/cdk/table';
import { DragDropSortableService, DndModule, DragDropService, DragDropConfig } from 'ng2-dnd';
import { AuthGuard } from './../auth.guard';

const routes: Routes = [
    {
        path     : 'daftarRute',
        component: MonitorRuteComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: MonitorRuteService
        }
    }
];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        CdkTableModule,
        DndModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    exports : [
        CdkTableModule
    ],
    entryComponents: [
        MonitorRuteComponent,
        FuseConfirmDialogComponent
    ],
    declarations: [
        MonitorRuteComponent
    ],
    providers   : [
        MonitorRuteService,
        DragDropSortableService, 
        DragDropService, 
        DragDropConfig
    ]
})
export class MonitorModule
{
}
