webpackJsonp(["user.module"],{

/***/ "../../../../../src/app/main/content/apps/user/daftarUser/daftarUser.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"products\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\"\r\n             fxLayout=\"column\" fxLayoutAlign=\"center center\"\r\n             fxLayout.gt-xs=\"row\" fxLayoutAlign.gt-xs=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div class=\"logo my-12 m-sm-0\"\r\n                 fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <mat-icon class=\"logo-icon mr-16\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'50ms',scale:'0.2'}}\">account_box</mat-icon>\r\n                <span class=\"logo-text h1\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">Daftar User</span>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <!-- SEARCH -->\r\n            <div class=\"search-input-wrapper mx-12 m-md-0\"\r\n                 fxFlex=\"1 0 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <label for=\"search\" class=\"mr-8\">\r\n                    <mat-icon class=\"secondary-text\">search</mat-icon>\r\n                </label>\r\n                <mat-form-field floatPlaceholder=\"never\" fxFlex=\"1 0 auto\">\r\n                    <input id=\"search\" matInput #filter placeholder=\"Search\">\r\n                </mat-form-field>\r\n            </div>\r\n            <!-- / SEARCH -->\r\n\r\n            <button mat-raised-button\r\n                    [routerLink]=\"'/apps/user/detailUser/new'\"\r\n                    class=\"add-product-button my-12 m-sm-0\"\r\n                    [disabled]=\"role != '4'\">\r\n                <span>Tambah User</span>\r\n            </button>\r\n\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <mat-table class=\"products-table\"\r\n                       #table [dataSource]=\"dataSource\"\r\n                       matSort\r\n                       [@animateStagger]=\"{value:'50'}\"\r\n                       fusePerfectScrollbar>\r\n\r\n                <!-- Index Column -->\r\n                <ng-container cdkColumnDef=\"index\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>No. </mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product; let i = index\">\r\n                        <p class=\"text-truncate\">{{ (paginator.pageIndex * paginator.pageSize) + (i + 1) }}.</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Role Column -->\r\n                <ng-container cdkColumnDef=\"role\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Role</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"text-truncate\" *ngIf=\"product.group == 4\">Super Admin</p>\r\n                        <p class=\"text-truncate\" *ngIf=\"product.group == 3\">Admin</p>\r\n                        <p class=\"text-truncate\" *ngIf=\"product.group == 2\">Operator Pusat</p>\r\n                        <p class=\"text-truncate\" *ngIf=\"product.group == 1\">Operator Daerah</p>\r\n                        <p class=\"text-truncate\" *ngIf=\"product.group == 0\">No Role</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Name Column -->\r\n                <ng-container cdkColumnDef=\"name\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Name</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"text-truncate\">{{product.name}}</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Username Column -->\r\n                <ng-container cdkColumnDef=\"username\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Username</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"category text-truncate\"> {{product.username}} </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Email Column -->\r\n                <ng-container cdkColumnDef=\"email\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Email</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"price text-truncate\">\r\n                            {{product.email}}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <mat-header-row *cdkHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n\r\n                <mat-row *cdkRowDef=\"let product; columns: displayedColumns;\"\r\n                         class=\"product\"\r\n                         matRipple\r\n                         (click)=\"clickRute(product.id)\">\r\n                </mat-row>\r\n\r\n            </mat-table>\r\n\r\n            <mat-paginator #paginator\r\n                           [length]=\"dataSource.filteredData.length\"\r\n                           [pageIndex]=\"0\"\r\n                           [pageSize]=\"25\"\r\n                           [pageSizeOptions]=\"[5, 10, 25, 100]\">\r\n            </mat-paginator>\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/user/daftarUser/daftarUser.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n:host .header .search-input-wrapper {\n  max-width: 480px; }\n:host .header .add-product-button {\n  background-color: #F96D01; }\n@media (max-width: 599px) {\n  :host .header {\n    height: 176px !important;\n    min-height: 176px !important;\n    max-height: 176px !important; } }\n@media (max-width: 599px) {\n  :host .top-bg {\n    height: 240px; } }\n:host .products-table {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  border-bottom: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-header-row {\n    min-width: 500px;\n    min-height: 35px;\n    background-color: rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-row {\n    min-width: 500px; }\n:host .products-table .product {\n    position: relative;\n    cursor: pointer;\n    height: 60px; }\n:host .products-table .mat-cell {\n    min-width: 0;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n:host .products-table .mat-column-role {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center !important;\n        -ms-flex-pack: center !important;\n            justify-content: center !important; }\n:host .products-table .mat-column-index {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 50px;\n            flex: 0 1 50px; }\n:host .products-table .mat-column-image {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 84px;\n            flex: 0 1 84px; }\n:host .products-table .mat-column-image .product-image {\n      width: 52px;\n      height: 52px;\n      border: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-column-buttons {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 80px;\n            flex: 0 1 80px; }\n:host .products-table .quantity-indicator {\n    display: inline-block;\n    vertical-align: middle;\n    width: 8px;\n    height: 8px;\n    border-radius: 4px;\n    margin-right: 8px; }\n:host .products-table .quantity-indicator + span {\n      display: inline-block;\n      vertical-align: middle; }\n:host .products-table .active-icon {\n    border-radius: 50%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/user/daftarUser/daftarUser.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarUserComponent; });
/* unused harmony export FilesDataSource */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__daftarUser_service__ = __webpack_require__("../../../../../src/app/main/content/apps/user/daftarUser/daftarUser.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_cdk_collections__ = __webpack_require__("../../../cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs_add_observable_fromEvent__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/fromEvent.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__core_fuseUtils__ = __webpack_require__("../../../../../src/app/core/fuseUtils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__core_components_confirm_dialog_confirm_dialog_component__ = __webpack_require__("../../../../../src/app/core/components/confirm-dialog/confirm-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var DaftarUserComponent = /** @class */ (function () {
    function DaftarUserComponent(productsService, dialog, router, _sessionService) {
        this.productsService = productsService;
        this.dialog = dialog;
        this.router = router;
        this._sessionService = _sessionService;
        this.hapus = false;
        this.displayedColumns = ['index', 'role', 'name', 'username', 'email'];
        this.role = this._sessionService.get().role;
        this.id = this._sessionService.get().token;
    }
    DaftarUserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataSource = new FilesDataSource(this.productsService, this.paginator, this.sort);
        __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].fromEvent(this.filter.nativeElement, 'keyup')
            .debounceTime(150)
            .distinctUntilChanged()
            .subscribe(function () {
            if (!_this.dataSource) {
                return;
            }
            _this.dataSource.filter = _this.filter.nativeElement.value;
        });
    };
    DaftarUserComponent.prototype.toggleHapus = function () {
        this.hapus == true ? this.hapus = false : this.hapus = true;
    };
    DaftarUserComponent.prototype.hapusDialog = function (id, asal, tujuan, navigate) {
        var _this = this;
        var pesan = "Hapus Rute ";
        var deleteDialog = this.dialog.open(__WEBPACK_IMPORTED_MODULE_15__core_components_confirm_dialog_confirm_dialog_component__["a" /* FuseConfirmDialogComponent */], {
            width: '250',
            data: { id: id, asal: asal, tujuan: tujuan, url: "/admin/rute/", navigate: navigate }
        });
        deleteDialog.afterClosed().subscribe(function () {
            _this.dataSource = new FilesDataSource(_this.productsService, _this.paginator, _this.sort);
            __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].fromEvent(_this.filter.nativeElement, 'keyup')
                .debounceTime(150)
                .distinctUntilChanged()
                .subscribe(function () {
                if (!_this.dataSource) {
                    return;
                }
                _this.dataSource.filter = _this.filter.nativeElement.value;
            });
        });
    };
    DaftarUserComponent.prototype.clickRute = function (id) {
        var url = '/apps/user/detailUser/' + id;
        if (this.role == "4") {
            this.router.navigate([url]);
        }
        if (this.id == id) {
            this.router.navigate([url]);
            ;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__angular_material__["z" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__angular_material__["z" /* MatPaginator */])
    ], DaftarUserComponent.prototype, "paginator", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('filter'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], DaftarUserComponent.prototype, "filter", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__angular_material__["M" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__angular_material__["M" /* MatSort */])
    ], DaftarUserComponent.prototype, "sort", void 0);
    DaftarUserComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-products',
            template: __webpack_require__("../../../../../src/app/main/content/apps/user/daftarUser/daftarUser.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/user/daftarUser/daftarUser.component.scss")],
            animations: __WEBPACK_IMPORTED_MODULE_4__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__daftarUser_service__["a" /* DaftarUserService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_material__["m" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_7__angular_router__["f" /* Router */],
            __WEBPACK_IMPORTED_MODULE_16__session_service__["a" /* SessionService */]])
    ], DaftarUserComponent);
    return DaftarUserComponent;
}());

var FilesDataSource = /** @class */ (function (_super) {
    __extends(FilesDataSource, _super);
    function FilesDataSource(productsService, _paginator, _sort) {
        var _this = _super.call(this) || this;
        _this.productsService = productsService;
        _this._paginator = _paginator;
        _this._sort = _sort;
        _this._filterChange = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        _this._filteredDataChange = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        _this.filteredData = _this.productsService.products;
        return _this;
    }
    Object.defineProperty(FilesDataSource.prototype, "filteredData", {
        get: function () {
            return this._filteredDataChange.value;
        },
        set: function (value) {
            this._filteredDataChange.next(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FilesDataSource.prototype, "filter", {
        get: function () {
            return this._filterChange.value;
        },
        set: function (filter) {
            this._filterChange.next(filter);
        },
        enumerable: true,
        configurable: true
    });
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    FilesDataSource.prototype.connect = function () {
        var _this = this;
        var displayDataChanges = [
            this.productsService.onProductsChanged,
            this._paginator.page,
            this._filterChange,
            this._sort.sortChange
        ];
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].merge.apply(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"], displayDataChanges).map(function () {
            var data = _this.productsService.products.slice();
            data = _this.filterData(data);
            _this.filteredData = data.slice();
            data = _this.sortData(data);
            // Grab the page's slice of data.
            var startIndex = _this._paginator.pageIndex * _this._paginator.pageSize;
            return data.splice(startIndex, _this._paginator.pageSize);
        });
    };
    FilesDataSource.prototype.filterData = function (data) {
        if (!this.filter) {
            return data;
        }
        return __WEBPACK_IMPORTED_MODULE_14__core_fuseUtils__["a" /* FuseUtils */].filterArrayByString(data, this.filter);
    };
    FilesDataSource.prototype.sortData = function (data) {
        var _this = this;
        if (!this._sort.active || this._sort.direction === '') {
            return data;
        }
        return data.sort(function (a, b) {
            var propertyA = '';
            var propertyB = '';
            switch (_this._sort.active) {
                case 'id':
                    _a = [a.id, b.id], propertyA = _a[0], propertyB = _a[1];
                    break;
                case 'asal':
                    _b = [a.asal, b.asal], propertyA = _b[0], propertyB = _b[1];
                    break;
                case 'tujuan':
                    _c = [a.tujuan, b.tujuan], propertyA = _c[0], propertyB = _c[1];
                    break;
                case 'tanggal':
                    _d = [a.tanggal, b.tanggal], propertyA = _d[0], propertyB = _d[1];
                    break;
                case 'jenis':
                    _e = [a.jenis, b.jenis], propertyA = _e[0], propertyB = _e[1];
                    break;
                case 'kuota_motor':
                    _f = [a.kouta_motor, b.kouta_motor], propertyA = _f[0], propertyB = _f[1];
                    break;
                case 'status':
                    _g = [a.status, b.status], propertyA = _g[0], propertyB = _g[1];
                    break;
            }
            var valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            var valueB = isNaN(+propertyB) ? propertyB : +propertyB;
            return (valueA < valueB ? -1 : 1) * (_this._sort.direction === 'asc' ? 1 : -1);
            var _a, _b, _c, _d, _e, _f, _g;
        });
    };
    FilesDataSource.prototype.disconnect = function () {
    };
    return FilesDataSource;
}(__WEBPACK_IMPORTED_MODULE_2__angular_cdk_collections__["a" /* DataSource */]));



/***/ }),

/***/ "../../../../../src/app/main/content/apps/user/daftarUser/daftarUser.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarUserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var DaftarUserService = /** @class */ (function () {
    function DaftarUserService(http, router, location, snackBar, _sessionService) {
        this.http = http;
        this.router = router;
        this.location = location;
        this.snackBar = snackBar;
        this._sessionService = _sessionService;
        this.onProductsChanged = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["BehaviorSubject"]({});
        this.token = this._sessionService.get().token;
        this.role = this._sessionService.get().role;
        this.id = this._sessionService.get().role;
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DaftarUserService.prototype.resolve = function (route, state) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getProducts()
            ]).then(function () {
                resolve();
            }, reject).catch(function (error) {
                console.log(error);
            });
        });
    };
    DaftarUserService.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        options.headers = headers;
    };
    DaftarUserService.prototype.getProducts = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].setting.base_url + '/admin/users', options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                _this.products = response.data;
                if (_this.role < 4) {
                    _this.products = [];
                    _this.snackBar.open(response.message, response.status, {
                        verticalPosition: 'top',
                        duration: 5000
                    });
                }
                _this.onProductsChanged.next(_this.products);
                resolve(response);
            }, function (error) {
                // this.router.navigate(["/apps/login"]);
                console.log(error);
                reject(error);
            });
        });
    };
    DaftarUserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["f" /* Router */],
            __WEBPACK_IMPORTED_MODULE_5__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_8__session_service__["a" /* SessionService */]])
    ], DaftarUserService);
    return DaftarUserService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/user/detailUser/detailUser.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"product\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"start center\" style=\"width: 80%\">\r\n\r\n                <button class=\"mr-0 mr-sm-16\" mat-icon-button [routerLink]=\"'/apps/user/daftarUser'\">\r\n                    <mat-icon>arrow_back</mat-icon>\r\n                </button>\r\n\r\n                <div fxLayout=\"column\" fxLayoutAlign=\"start start\"\r\n                     *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">\r\n                    <div class=\"h2\" *ngIf=\"pageType ==='edit'\">\r\n                        {{product.name}}\r\n                    </div>\r\n                    <div class=\"h2\" *ngIf=\"pageType ==='new'\">\r\n                        Tambah User\r\n                    </div>\r\n                    <div class=\"subtitle secondary-text\">\r\n                        <span>Detail User</span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <button mat-raised-button fxHide.xs\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"productForm.invalid\"\r\n                    *ngIf=\"pageType ==='new'\" (click)=\"addProduct()\">\r\n                <span>Simpan</span>\r\n            </button>\r\n\r\n            <button mat-raised-button fxHide.xs\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"productForm.invalid || productForm.pristine\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"saveProduct()\">\r\n                <span>Simpan</span>\r\n            </button>\r\n\r\n            <button mat-raised-button fxHide.xs\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    *ngIf=\"pageType ==='edit' && !editPassword\" (click)=\"hapus()\">\r\n                <span>Hapus</span>\r\n            </button>\r\n\r\n            <button mat-raised-button fxHide.xs\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    *ngIf=\"pageType ==='edit' && !editPassword\" (click)=\"ubahPassword()\">\r\n                <span>Ubah Password</span>\r\n            </button>\r\n\r\n            <button mat-raised-button fxHide.xs\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    *ngIf=\"pageType ==='edit' && editPassword\" (click)=\"batalUbahPassword()\">\r\n                <span>Batalkan</span>\r\n            </button>\r\n\r\n            <button mat-icon-button fxHide fxShow.xs [matMenuTriggerFor]=\"menu\">\r\n              <mat-icon>more_vert</mat-icon>\r\n            </button>\r\n\r\n            <mat-menu #menu=\"matMenu\">\r\n              <button mat-menu-item [disabled]=\"productForm.invalid\"\r\n                    *ngIf=\"pageType ==='new'\" (click)=\"addProduct()\">\r\n                <mat-icon>save_alt</mat-icon>\r\n                <span>Simpan</span>\r\n              </button>\r\n              <button mat-menu-item [disabled]=\"productForm.invalid || productForm.pristine\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"saveProduct()\">\r\n                <mat-icon>save_alt</mat-icon>\r\n                <span>Simpan</span>\r\n              </button>\r\n              <button mat-menu-item *ngIf=\"pageType ==='edit' && !editPassword\" (click)=\"hapus()\">\r\n                <mat-icon>delete_forever</mat-icon>\r\n                <span>Hapus</span>\r\n              </button>\r\n              <button mat-menu-item *ngIf=\"pageType ==='edit' && !editPassword\" (click)=\"ubahPassword()\">\r\n                <mat-icon>vpn_key</mat-icon>\r\n                <span>Ubah Password</span>\r\n              </button>\r\n              <button mat-menu-item *ngIf=\"pageType ==='edit' && editPassword\" (click)=\"batalUbahPassword()\">\r\n                <mat-icon>cancel</mat-icon>\r\n                <span>Batalkan</span>\r\n              </button>\r\n            </mat-menu>\r\n\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <!-- CONTENT -->\r\n            <div class=\"content\">\r\n\r\n                <form name=\"productForm\" [formGroup]=\"productForm\" class=\"product w-100-p\" fxLayout=\"column\" fxFlex>\r\n\r\n                    <div class=\"tab-content p-24\" fusePerfectScrollbar>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                            <input matInput\r\n                                   name=\"username\"\r\n                                   formControlName=\"username\"\r\n                                   placeholder=\"Username\"\r\n                                   required>\r\n                              <mat-icon matPrefix> account_circle </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                            <input matInput\r\n                                   type=\"email\" \r\n                                   name=\"email\"\r\n                                   formControlName=\"email\"\r\n                                   placeholder=\"Email\"\r\n                                   required>\r\n                            <mat-icon matPrefix> email </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                            <input matInput\r\n                                   name=\"name\"\r\n                                   formControlName=\"name\"\r\n                                   placeholder=\"Name\"\r\n                                   required>\r\n                            <mat-icon matPrefix> account_circle </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                            <mat-select formControlName=\"group\" placeholder=\"Role\" (change)=\"enableRute()\">\r\n                                <mat-option [value]=\"4\"> Super Admin </mat-option>\r\n                                <mat-option [value]=\"3\"> Admin </mat-option>\r\n                                <mat-option [value]=\"2\"> Operator Pusat </mat-option>\r\n                                <mat-option [value]=\"1\"> Operator Daerah </mat-option>\r\n                                <mat-option [value]=\"0\"> No Role </mat-option>\r\n                            </mat-select>\r\n                            <mat-icon matPrefix> verified_user </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                            <mat-select formControlName=\"rute_id\" placeholder=\"Rute\" multiple>\r\n                                    <mat-option *ngFor=\"let rute of rutes\" [value]=\"rute.id\"> \r\n                                        {{ rute.kota.asal }} - {{ rute.kota.tujuan }} - {{ rute.keterangan }}\r\n                                    </mat-option>\r\n                                </mat-select>\r\n                            <mat-icon matPrefix> navigation </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <mat-form-field class=\"w-100-p\" *ngIf=\"editPassword || pageType ==='new'\">\r\n                            <input matInput\r\n                                   type=\"password\"\r\n                                   name=\"password\"\r\n                                   formControlName=\"password\"\r\n                                   placeholder=\"Password\">\r\n                            <mat-icon matPrefix> vpn_key </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <mat-form-field class=\"w-100-p\" *ngIf=\"editPassword || pageType ==='new'\">\r\n                            <input matInput\r\n                                   type=\"password\"\r\n                                   name=\"password_confirmation\"\r\n                                   formControlName=\"password_confirmation\"\r\n                                   placeholder=\"Confirm Password\">\r\n                            <mat-icon matPrefix> vpn_key </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                    </div>\r\n                </form>\r\n\r\n            </div>\r\n            <!-- / CONTENT -->\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/user/detailUser/detailUser.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#product .header .product-image {\n  overflow: hidden;\n  width: 56px;\n  height: 56px;\n  border: 3px solid rgba(0, 0, 0, 0.12); }\n  #product .header .product-image img {\n    height: 100%;\n    width: auto;\n    max-width: none; }\n  #product .header .save-product-button {\n  background-color: #F96D01;\n  margin: 0px 20px; }\n  #product .header .subtitle {\n  margin: 6px 0 0 0; }\n  #product .content table, #product .content th, #product .content td {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  border-collapse: collapse; }\n  #product .content th, #product .content td {\n  padding: 10px;\n  text-align: center;\n  font-weight: normal !important;\n  font-size: 12px; }\n  #product .content th {\n  background-color: rgba(0, 0, 0, 0.12); }\n  #product .content .example-container {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  max-height: 500px;\n  min-width: 300px; }\n  #product .content .example-chip-list {\n  width: 100%; }\n  #product .content .mat-table {\n  overflow: auto;\n  max-height: 500px; }\n  #product .content .mat-form-field-prefix .mat-icon, #product .content .mat-form-field-suffix .mat-icon {\n  margin-right: 10px; }\n  #product .content .mat-tab-group,\n#product .content .mat-tab-body-wrapper,\n#product .content .tab-content {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  max-width: 100%; }\n  #product .content .mat-tab-body-content {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n  #product .content .mat-tab-label {\n  height: 64px; }\n  #product .content .product-image {\n  overflow: hidden;\n  width: 128px;\n  height: 128px;\n  margin-right: 16px;\n  margin-bottom: 16px;\n  border: 3px solid rgba(0, 0, 0, 0.12); }\n  #product .content .product-image img {\n    height: 100%;\n    width: auto;\n    max-width: none; }\n  #product .content .list-kota {\n  border-bottom: 1px solid; }\n  #product .content .mat-card .mat-divider.mat-divider-inset {\n  position: absolute !important; }\n  #product .content .mat-list .mat-list-item .mat-divider.mat-divider-inset {\n  left: 0 !important;\n  width: 100% !important; }\n  #product .content .mat-grid-tile .mat-figure {\n  -webkit-box-pack: left !important;\n      -ms-flex-pack: left !important;\n          justify-content: left !important; }\n  #product .content .mat-list .mat-list-item {\n  height: unset !important; }\n  #product .content .mat-list .mat-list-item {\n  font-size: 13px !important; }\n  #product .content .mat-card-title {\n  font-size: 15px !important; }\n  #product .content .mat-card-actions, #product .content .mat-card-content, #product .content .mat-card-subtitle, #product .content .mat-card-title {\n  padding-bottom: 10px !important; }\n  #product .content .mat-card {\n  padding: 10px !important; }\n  #product .content .mat-card-subtitle, #product .content .mat-card-content, #product .content .mat-card-header .mat-card-title {\n  font-size: 12px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/user/detailUser/detailUser.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailUserComponent; });
/* unused harmony export determineId */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__detailUser_service__ = __webpack_require__("../../../../../src/app/main/content/apps/user/detailUser/detailUser.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_observable_fromEvent__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/fromEvent.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__detailUser_model__ = __webpack_require__("../../../../../src/app/main/content/apps/user/detailUser/detailUser.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_sweetalert2__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


















var DetailUserComponent = /** @class */ (function () {
    function DetailUserComponent(productService, formBuilder, snackBar, location, http, dialog, route, _sessionService) {
        this.productService = productService;
        this.formBuilder = formBuilder;
        this.snackBar = snackBar;
        this.location = location;
        this.http = http;
        this.dialog = dialog;
        this.route = route;
        this._sessionService = _sessionService;
        this.product = new __WEBPACK_IMPORTED_MODULE_9__detailUser_model__["a" /* Product */]();
        this.editPassword = false;
        this.token = this._sessionService.get().token;
        this.rute_id = [];
    }
    DetailUserComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Subscribe to update product on changes
        this.onProductChanged =
            this.productService.onProductChanged
                .subscribe(function (product) {
                if (product) {
                    _this.product = new __WEBPACK_IMPORTED_MODULE_9__detailUser_model__["a" /* Product */](product);
                    _this.pageType = 'edit';
                    if (_this.product.rute_id) {
                        var id = _this.product.rute_id.slice(1, (_this.product.rute_id.length - 1)).split(',');
                        for (var i = 0; i < id.length; ++i) {
                            _this.rute_id.push(Number(id[i]));
                        }
                    }
                }
                else {
                    _this.pageType = 'new';
                    _this.product = new __WEBPACK_IMPORTED_MODULE_9__detailUser_model__["a" /* Product */]();
                }
                _this.productForm = _this.createProductForm();
            });
        this.productService.getRute()
            .then(function (response) {
            _this.rutes = response.data;
        })
            .catch(function (error) {
            var eror = JSON.parse(error._body);
            _this.snackBar.open(eror.message, eror.errors.email ? eror.errors.email : eror.errors.password, {
                verticalPosition: 'top',
                duration: 5000
            });
        });
    };
    DetailUserComponent.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_14__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        options.headers = headers;
    };
    DetailUserComponent.prototype.createProductForm = function () {
        return this.formBuilder.group({
            "username": new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.username, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            "email": new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.email, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].email]),
            "name": new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.name, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            "group": new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.group, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            "password": new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.password),
            "password_confirmation": new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](''),
            "rute_id": new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: this.rute_id, disabled: (this.product.group == 1 ? false : true) })
        });
    };
    DetailUserComponent.prototype.compareIds = function (id1, id2) {
        var a1 = determineId(id1);
        var a2 = determineId(id2);
        return a1 === a2;
    };
    DetailUserComponent.prototype.saveProduct = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_14__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        var data = this.productForm.getRawValue();
        this.productService.saveProduct(data)
            .then(function () {
            // Show the success message
            _this.snackBar.open('Update User Sukses', 'OK', {
                verticalPosition: 'top',
                duration: 2000
            });
            // Change the location with new one
            _this.route.navigate(['apps/user/daftarUser/']);
        })
            .catch(function (error) {
            var eror = JSON.parse(error._body);
            _this.snackBar.open(eror.message, eror.errors.email ? eror.errors.email : eror.errors.password, {
                verticalPosition: 'top',
                duration: 5000
            });
        });
    };
    DetailUserComponent.prototype.addProduct = function () {
        var _this = this;
        var data = this.productForm.getRawValue();
        this.productService.addProduct(data)
            .then(function (response) {
            _this.snackBar.open(response.message, response.status, {
                verticalPosition: 'top',
                duration: 2000
            });
            if (response.status) {
                _this.route.navigate(['apps/user/daftarUser/']);
            }
        })
            .catch(function (error) {
            var eror = JSON.parse(error._body);
            _this.snackBar.open(eror.message, eror.errors.email ? eror.errors.email : eror.errors.password, {
                verticalPosition: 'top',
                duration: 5000
            });
        });
        ;
    };
    DetailUserComponent.prototype.hapus = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
            title: 'Hapus User' + this.product.name,
            text: "User yang telah dihapus tidak bisa dikembalikan lagi",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus !',
            cancelButtonText: 'Batal!',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                _this.productService.hapus(_this.product)
                    .then(function (response) {
                    if (response.status) {
                        _this.back();
                        __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                            title: 'Hapus User Berhasil!',
                            text: 'User' + _this.product.username + ' Telah Dihapus',
                            type: 'success',
                            timer: 2000
                        });
                    }
                    else {
                        __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()('Hapus Rute Gagal!', response.message, 'error');
                    }
                })
                    .catch(function (error) {
                    var eror = JSON.parse(error._body);
                    _this.snackBar.open(eror.message, eror.errors.email ? eror.errors.email : eror.errors.password, {
                        verticalPosition: 'top',
                        duration: 5000
                    });
                });
            }
        });
    };
    DetailUserComponent.prototype.ubahPassword = function () {
        this.editPassword = true;
        this.productForm.controls.password.setValidators([__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].minLength(8)]);
        this.productForm.controls.password_confirmation.setValidators([__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]);
    };
    DetailUserComponent.prototype.batalUbahPassword = function () {
        this.editPassword = false;
        this.productForm.controls.password.setValidators([]);
        this.productForm.controls.password_confirmation.setValidators([]);
    };
    DetailUserComponent.prototype.ngOnDestroy = function () {
        this.onProductChanged.unsubscribe();
    };
    DetailUserComponent.prototype.enableRute = function () {
        if (this.productForm.controls.group.value == 1) {
            this.productForm.controls.rute_id.enable();
        }
        else {
            this.productForm.controls.rute_id.disable();
        }
    };
    DetailUserComponent.prototype.back = function () {
        this.location.back();
    };
    DetailUserComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-product',
            template: __webpack_require__("../../../../../src/app/main/content/apps/user/detailUser/detailUser.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/user/detailUser/detailUser.component.scss")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
            animations: __WEBPACK_IMPORTED_MODULE_2__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__detailUser_service__["a" /* DetailUserService */],
            __WEBPACK_IMPORTED_MODULE_10__angular_forms__["c" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_11__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_12__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_14__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_11__angular_material__["m" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_13__angular_router__["f" /* Router */],
            __WEBPACK_IMPORTED_MODULE_15__session_service__["a" /* SessionService */]])
    ], DetailUserComponent);
    return DetailUserComponent;
}());

function determineId(id) {
    if (id.constructor.name === 'array' && id.length > 0) {
        return '' + id[0];
    }
    return '' + id;
}


/***/ }),

/***/ "../../../../../src/app/main/content/apps/user/detailUser/detailUser.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Product; });
var Product = /** @class */ (function () {
    function Product(product) {
        if (product) {
            product = product;
            this.id = product.id;
            this.username = product.username;
            this.email = product.email;
            this.name = product.name;
            this.group = product.group;
            this.password = product.password;
            this.rute_id = product.rute_id;
        }
        else {
            product = {};
            this.username = "";
            this.email = "";
            this.name = "";
            this.group = 3;
            this.password = "";
            this.rute_id = [];
        }
    }
    return Product;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/user/detailUser/detailUser.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailUserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DetailUserService = /** @class */ (function () {
    function DetailUserService(http, snackBar, _sessionService) {
        this.http = http;
        this.snackBar = snackBar;
        this._sessionService = _sessionService;
        this.onProductChanged = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["BehaviorSubject"]({});
        this.token = this._sessionService.get().token;
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DetailUserService.prototype.resolve = function (route, state) {
        var _this = this;
        this.routeParams = route.params;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getProduct(),
                _this.getRute()
            ]).then(function () {
                resolve();
            }, reject).catch(function (error) {
                var eror = JSON.parse(error._body);
                _this.snackBar.open(eror.message, eror.errors.email ? eror.errors.email : eror.errors.password, {
                    verticalPosition: 'top',
                    duration: 5000
                });
            });
        });
    };
    DetailUserService.prototype.getRute = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].setting.base_url + '/admin/rute', _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailUserService.prototype.getProduct = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.routeParams.id === 'new') {
                _this.onProductChanged.next(false);
                resolve(false);
            }
            else {
                _this.http.get(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].setting.base_url + '/admin/users/' + _this.routeParams.id, _this.setHeader())
                    .map(function (res) { return res.json(); })
                    .subscribe(function (response) {
                    _this.product = response.data;
                    _this.onProductChanged.next(_this.product);
                    resolve(response);
                }, function (error) {
                    reject(error);
                });
            }
        });
    };
    DetailUserService.prototype.setHeader = function () {
        var options = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["g" /* RequestOptions */]();
        var headers = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', "Bearer " + this._sessionService.get().token);
        options.headers = headers;
        return options;
    };
    DetailUserService.prototype.saveProduct = function (product) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.put(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].setting.base_url + '/admin/users/' + _this.routeParams.id, product, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                var obj = _this._sessionService.get();
                obj.email = product.name;
                _this._sessionService.set(obj);
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailUserService.prototype.addProduct = function (product) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].setting.base_url + '/admin/users', product, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailUserService.prototype.hapus = function (product) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].setting.base_url + '/admin/users/' + product.id;
        return new Promise(function (resolve, reject) {
            _this.http.delete(url, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailUserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_1__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_6__session_service__["a" /* SessionService */]])
    ], DetailUserService);
    return DetailUserService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/user/user.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserModule", function() { return UserModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__ = __webpack_require__("../../../../@swimlane/ngx-charts/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__ = __webpack_require__("../../../../../src/app/core/modules/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__ = __webpack_require__("../../../../../src/app/core/components/widget/widget.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__daftarUser_daftarUser_component__ = __webpack_require__("../../../../../src/app/main/content/apps/user/daftarUser/daftarUser.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__daftarUser_daftarUser_service__ = __webpack_require__("../../../../../src/app/main/content/apps/user/daftarUser/daftarUser.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__detailUser_detailUser_component__ = __webpack_require__("../../../../../src/app/main/content/apps/user/detailUser/detailUser.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__detailUser_detailUser_service__ = __webpack_require__("../../../../../src/app/main/content/apps/user/detailUser/detailUser.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__core_components_confirm_dialog_confirm_dialog_component__ = __webpack_require__("../../../../../src/app/core/components/confirm-dialog/confirm-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__ = __webpack_require__("../../../cdk/esm5/table.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__ = __webpack_require__("../../../../ng2-dnd/ng2-dnd.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__auth_guard__ = __webpack_require__("../../../../../src/app/main/content/apps/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_material_table__ = __webpack_require__("../../../material/esm5/table.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var routes = [
    {
        path: 'daftarUser',
        component: __WEBPACK_IMPORTED_MODULE_5__daftarUser_daftarUser_component__["a" /* DaftarUserComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_13__auth_guard__["a" /* AuthGuard */]],
        resolve: {
            data: __WEBPACK_IMPORTED_MODULE_6__daftarUser_daftarUser_service__["a" /* DaftarUserService */]
        }
    },
    {
        path: 'detailUser/:id',
        component: __WEBPACK_IMPORTED_MODULE_7__detailUser_detailUser_component__["a" /* DetailUserComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_13__auth_guard__["a" /* AuthGuard */]],
        resolve: {
            data: __WEBPACK_IMPORTED_MODULE_8__detailUser_detailUser_service__["a" /* DetailUserService */]
        }
    }
];
var UserModule = /** @class */ (function () {
    function UserModule() {
    }
    UserModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__["a" /* FuseWidgetModule */],
                __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__["NgxChartsModule"],
                __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__["m" /* CdkTableModule */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["a" /* DndModule */],
                __WEBPACK_IMPORTED_MODULE_14__angular_material_table__["b" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_10__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
                })
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__["m" /* CdkTableModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__daftarUser_daftarUser_component__["a" /* DaftarUserComponent */],
                __WEBPACK_IMPORTED_MODULE_9__core_components_confirm_dialog_confirm_dialog_component__["a" /* FuseConfirmDialogComponent */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__daftarUser_daftarUser_component__["a" /* DaftarUserComponent */],
                __WEBPACK_IMPORTED_MODULE_7__detailUser_detailUser_component__["a" /* DetailUserComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__daftarUser_daftarUser_service__["a" /* DaftarUserService */],
                __WEBPACK_IMPORTED_MODULE_8__detailUser_detailUser_service__["a" /* DetailUserService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["d" /* DragDropSortableService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["c" /* DragDropService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["b" /* DragDropConfig */]
            ]
        })
    ], UserModule);
    return UserModule;
}());



/***/ })

});
//# sourceMappingURL=user.module.chunk.js.map