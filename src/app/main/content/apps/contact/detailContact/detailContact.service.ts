import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { MatSnackBar } from '@angular/material';
import swal from 'sweetalert2';
import { SessionService } from './../../session.service';

@Injectable()
export class DetailContactService implements Resolve<any>
{
    routeParams: any;
    contact: any;
    onContactChanged: BehaviorSubject<any> = new BehaviorSubject({});
    token = this._sessionService.get().token;

    constructor(
        private http: Http,
        private snackBar: MatSnackBar,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getContact()
                    .then()
                    .catch((error)=> {
                        console.log(error);
                    })
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getContact(): Promise<any>
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            if ( this.routeParams.id === 'new' )
            {
                this.onContactChanged.next(false);
                resolve(false);
            }
            else
            {
                this.http.get(environment.setting.base_url + '/admin/cps/' + this.routeParams.id, options)
                    // .map((res: Response) => res.json())
                    .subscribe((response: any) => {
                        this.contact = response['data'];
                        this.onContactChanged.next(this.contact);
                        resolve(response);
                    }, (error) => {
                        reject(error);
                    });
            }
        });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        headers.append('Contact-Type', 'application/x-www-form-urlencoded');
        options.headers = headers;
    }

    saveContact(contact)
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/cps/' + this.routeParams.id, contact, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (reject) => {
                    let eror = JSON.parse(reject._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    })
                });
        });
    }

    addContact(contact)
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.post(environment.setting.base_url + '/admin/cps/', contact, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    hapus(contact){
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.delete(environment.setting.base_url + '/admin/cps/' + this.routeParams.id, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (reject) => {
                    let eror = JSON.parse(reject._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    })
                });
        });
    }
}
