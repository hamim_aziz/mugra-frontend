import { FuseNavigationModelInterface } from '../core/components/navigation/navigation.model';

export class FuseNavigationModel implements FuseNavigationModelInterface
{
    public model: any[];

    constructor()
    {
        this.model = [
            {
                'id'       : 'logout',
                'title'    : 'Logout',
                'translate': 'Logout',
                'type'     : 'item',
                'icon'     : 'exit_to_app',
                'function' : 'logout()'
            },
            {
                'id'       : 'pendaftaran_mudik',
                'title'    : 'pendaftaran_mudik',
                'translate': 'Pendaftaran Mudik',
                'type'     : 'item',
                'icon'     : 'local_play',
                'url'      : '/apps/tiket/detailTiket/daftarMudik'
            },
            {
                'id'       : 'pendaftaran_titip_motor',
                'title'    : 'pendaftaran_titip_motor',
                'translate': 'Pendaftaran Titip Motor',
                'type'     : 'item',
                'icon'     : 'motorcycle',
                'url'      : '/apps/titipMotor/detailTitipMotor/new'
            },
            {
                'id'       : 'applications',
                'title'    : 'Admin',
                'translate': 'Admin',
                'type'     : 'group',
                'icon'     : 'apps',
                'children' : [
                    {
                        'id'       : 'dashboards',
                        'title'    : 'Dashboards',
                        'translate': 'NAV.DASHBOARDS',
                        'type'     : 'item',
                        'icon'     : 'dashboard',
                        'url'      : '/apps/dashboards/project'
                    },
                    {
                        'id'       : 'user',
                        'title'    : 'User',
                        'translate': 'User',
                        'type'     : 'item',
                        'icon'     : 'account_box',
                        'url'      : '/apps/user/daftarUser'
                    },
                    {
                        'id'       : 'rute',
                        'title'    : 'Rute',
                        'translate': 'Rute',
                        'type'     : 'item',
                        'icon'     : 'navigation',
                        'url'      : '/apps/rute/daftarRute'
                    },
                    {
                        'id'       : 'kendaraan',
                        'title'    : 'Kendaraan',
                        'translate': 'Kendaraan',
                        'type'     : 'item',
                        'icon'     : 'directions_bus',
                        'url'      : '/apps/kendaraan/daftarKendaraan'
                    },
                    {
                        'id'       : 'tiket',
                        'title'    : 'tiket',
                        'translate': 'Tiket',
                        'type'     : 'item',
                        'icon'     : 'local_play',
                        'url'      : '/apps/tiket/daftarTiket'
                    },
                    {
                        'id'        : 'pemudik',
                        'title'     : 'pemudik',
                        'translate' : 'Pemudik',
                        'type'      : 'item',
                        'icon'      : 'people',
                        'url'       : '/apps/pemudik/daftarPemudik',
                        'exactMatch': true
                    },
                    {
                        'id'        : 'titip_motor',
                        'title'     : 'titip_motor',
                        'translate' : 'Titip Motor',
                        'type'      : 'item',
                        'icon'      : 'motorcycle',
                        'url'       : '/apps/titipMotor/daftarTitipMotor',
                        'exactMatch': true
                    },
                    {
                        'id'        : 'print-tiket',
                        'title'     : 'Print Tiket',
                        'translate' : 'Print Tiket',
                        'type'      : 'item',
                        'icon'      : 'print',
                        'url'       : '/apps/print_tiket/daftarTiket',
                        'exactMatch': true
                    },
                    {
                        'id'        : 'feedback',
                        'title'     : 'Feedback',
                        'translate' : 'Feedback',
                        'type'      : 'item',
                        'icon'      : 'question_answer',
                        'url'       : '/apps/feedback/daftarFeedback',
                        'exactMatch': true
                    },
                    {
                        'id'        : 'laporan',
                        'title'     : 'laporan',
                        'translate' : 'Laporan',
                        'type'      : 'item',
                        'icon'      : 'assignment',
                        'url'       : '/apps/laporan/',
                        'exactMatch': true
                    },
                    {
                        'id'       : 'content',
                        'title'    : 'Content',
                        'translate': 'Content',
                        'type'     : 'item',
                        'icon'     : 'ballot',
                        'url'      : '/apps/content/daftarContent'
                    },
                    {
                        'id'       : 'contact',
                        'title'    : 'Contact',
                        'translate': 'Contact',
                        'type'     : 'item',
                        'icon'     : 'contact_phone',
                        'url'      : '/apps/contact/daftarContact'
                    },
                    {
                        'id'       : 'sms',
                        'title'    : 'Sms',
                        'translate': 'Sms',
                        'type'     : 'item',
                        'icon'     : 'message',
                        'url'      : '/apps/sms/daftarSms'
                    },
                    // {
                    //     'id'       : 'monitor_rute',
                    //     'title'    : 'Monitor Rute',
                    //     'translate': 'Monitor Rute',
                    //     'type'     : 'item',
                    //     'icon'     : 'navigation',
                    //     'url'      : '/apps/monitor_rute/daftarRute'
                    // },
                    {
                        'url'       : '/apps/login',
                        'exactMatch': true
                    },
                ]
            }
        ];
    }
}

