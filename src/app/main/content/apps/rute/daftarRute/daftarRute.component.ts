import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DaftarRuteService } from './daftarRute.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { fuseAnimations } from '../../../../../core/animations';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from '@angular/router';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { FuseConfirmDialogComponent } from './../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import swal from 'sweetalert2';
import { SessionService } from './../../session.service';

@Component({
    selector   : 'fuse-e-commerce-products',
    templateUrl: './daftarRute.component.html',
    styleUrls  : ['./daftarRute.component.scss'],
    animations : fuseAnimations
})
export class DaftarRuteComponent implements OnInit
{
    hapus = false;
    dataSource: FilesDataSource | null;
    displayedColumns = ['id', 'jenis', 'asal', 'tujuan', 'tanggal', 'kuota_motor', 'keterangan', 'status'];
    role = this._sessionService.get().role;
    rutes = this.productsService.products;
    date = [];
    dateIndo = [];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private productsService: DaftarRuteService,
        private dialog: MatDialog,
        private router: Router,
        private _sessionService: SessionService
    )
    {
    }

    ngOnInit()
    {
        this._tanggal();

        this.dataSource = new FilesDataSource(this.productsService, this.paginator, this.sort);
        Observable.fromEvent(this.filter.nativeElement, 'keyup')
                  .debounceTime(150)
                  .distinctUntilChanged()
                  .subscribe(() => {
                      if ( !this.dataSource )
                      {
                          return;
                      }

                      this.dataSource.filter = this.filter.nativeElement.value;
                  });
    }

    _tanggal(){
        for (let i = 0; i < this.rutes.length; ++i) {
            if (this.date.indexOf(this.rutes[i]['tanggal']) === -1) {
                this.dateIndo.push([this.rutes[i]['tanggal'], this.convertTanggal(this.rutes[i]['tanggal'])]);
                this.date.push(this.rutes[i]['tanggal']);
            }
        }
    }

    convertTanggal(tgl){
        const hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
        const bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
        const date = new Date(tgl);
        const day = date.getDay();
        const month = date.getMonth();
        return hari[day] + ', ' + date.getDate() + ' ' + bulan[month] + ' ' + date.getFullYear();
    }

    cobaSwal(){
        swal({
          title: 'Are you sure?',
          text: 'You won\'t be able to revert this!',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, cancel!',
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          reverseButtons: true
        }).then((result) => {
          if (result.value) {
            swal(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            );
          } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
          ) {
            swal(
              'Cancelled',
              'Your imaginary file is safe :)',
              'error'
            );
          }
        });
    }

    filterButton(filter){
        this.dataSource.filter = filter;
    }

    semua(){
        this.dataSource.filter = '';
    }

    toggleHapus(){
        this.hapus === true ? this.hapus = false : this.hapus = true;
    }

    clickRute(id): void{
        const url = '/apps/rute/detailRute/' + id;
        if (this.role >= 1) {
            this.router.navigate([url]);
        }
    }

    bulan(date){
        if (date.substring(5, 7) === '01') {
            return 'Januari';
        } else if (date.substring(5, 7) === '02') {
            return 'Februari';
        } else if (date.substring(5, 7) === '03') {
            return 'Maret';
        } else if (date.substring(5, 7) === '04') {
            return 'April';
        } else if (date.substring(5, 7) === '05') {
            return 'Mei';
        } else if (date.substring(5, 7) === '06') {
            return 'Juni';
        } else if (date.substring(5, 7) === '07') {
            return 'Juli';
        } else if (date.substring(5, 7) === '08') {
            return 'Agustus';
        } else if (date.substring(5, 7) === '09') {
            return 'September';
        } else if (date.substring(5, 7) === '10') {
            return 'Oktober';
        } else if (date.substring(5, 7) === '11') {
            return 'November';
        } else if (date.substring(5, 7) === '12') {
            return 'Desember';
        }
    }
}

export class FilesDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }

    constructor(
        private productsService: DaftarRuteService,
        private _paginator: MatPaginator,
        private _sort: MatSort
    )
    {
        super();
        this.filteredData = this.productsService.products;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this.productsService.onProductsChanged,
            this._paginator.page,
            this._filterChange,
            this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.productsService.products.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            data = this.sortData(data);

            // Grab the page's slice of data.
            // if (!this.filter) {
            //     sessionStorage.setItem("pageIndexRute", String(this._paginator.pageIndex));
            // }
            
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data)
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    sortData(data): any[]
    {
        if ( !this._sort.active || this._sort.direction === '' )
        {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch ( this._sort.active )
            {
                case 'id':
                    [propertyA, propertyB] = [a.id, b.id];
                    break;
                case 'asal':
                    [propertyA, propertyB] = [a.asal, b.asal];
                    break;
                case 'tujuan':
                    [propertyA, propertyB] = [a.tujuan, b.tujuan];
                    break;
                case 'tanggal':
                    [propertyA, propertyB] = [a.tanggal, b.tanggal];
                    break;
                case 'jenis':
                    [propertyA, propertyB] = [a.jenis, b.jenis];
                    break;
                case 'kuota_motor':
                    [propertyA, propertyB] = [a.kouta_motor, b.kouta_motor];
                    break;
                case 'status':
                    [propertyA, propertyB] = [a.status, b.status];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
        });
    }

    disconnect()
    {
    }
}
