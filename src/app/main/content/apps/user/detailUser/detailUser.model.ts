import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatChipInputEvent } from '@angular/material';

export class Product
{
    username: string;
    email: string;
    name: string;
    group: number;
    password: string;
    id: number;
    rute_id: any;

    constructor(product?)
    {
        if (product) {
            product = product;
            this.id = product.id;
            this.username = product.username;
            this.email = product.email;
            this.name = product.name;
            this.group = product.group;
            this.password = product.password;
            this.rute_id = product.rute_id;
        } else {
            product = {};
            this.username = "";
            this.email = "";
            this.name = "";
            this.group = 3;
            this.password = "";
            this.rute_id = [];
        }
    }

}
