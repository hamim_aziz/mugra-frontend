import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { DetailUserService } from './detailUser.service';
import { fuseAnimations } from '../../../../../core/animations';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Product } from './detailUser.model';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../../../../environments/environment';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { FuseConfirmDialogComponent } from './../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { SessionService } from './../../session.service';
import swal from 'sweetalert2';

@Component({
    selector     : 'fuse-e-commerce-product',
    templateUrl  : './detailUser.component.html',
    styleUrls    : ['./detailUser.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})

export class DetailUserComponent implements OnInit, OnDestroy
{
    product = new Product();
    onProductChanged: Subscription;
    pageType: string;
    productForm: FormGroup;
    editPassword = false;
    token = this._sessionService.get().token;
    rutes: any;
    rute_id = [];

    constructor(
        private productService: DetailUserService,
        private formBuilder: FormBuilder,
        public snackBar: MatSnackBar,
        private location: Location,
        private http: Http,
        private dialog: MatDialog,
        private route: Router,
        private _sessionService: SessionService
    )
    {

    }

    ngOnInit()
    {
        // Subscribe to update product on changes
        this.onProductChanged =
            this.productService.onProductChanged
                .subscribe(product => {

                    if ( product )
                    {
                        this.product = new Product(product);
                        this.pageType = 'edit';
                        if (this.product.rute_id) {
                            let id = this.product.rute_id.slice(1,(this.product.rute_id.length-1)).split(',');
                            for (var i = 0; i < id.length; ++i) {
                                this.rute_id.push(Number(id[i]));
                            }
                        }
                    }
                    else
                    {
                        this.pageType = 'new';
                        this.product = new Product();
                    }

                    this.productForm = this.createProductForm();
                });
        this.productService.getRute()
            .then(
                (response) => {
                    this.rutes = response.data;
                }
            )
            .catch(
                (error) => {
                    let eror = JSON.parse(error._body);
                    this.snackBar.open(eror.message, eror.errors.email ? eror.errors.email : eror.errors.password, {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                }
            );
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        options.headers = headers;
    }

    createProductForm()
    {
        return this.formBuilder.group({
            "username"              : new FormControl(this.product.username, [Validators.required]),
            "email"                 : new FormControl(this.product.email, [Validators.required, Validators.email]),
            "name"                  : new FormControl(this.product.name, [Validators.required]),
            "group"                 : new FormControl(this.product.group, [Validators.required]),
            "password"              : new FormControl(this.product.password),
            "password_confirmation" : new FormControl(''),
            "rute_id"               : new FormControl({value: this.rute_id, disabled: (this.product.group == 1 ? false : true)})
        });
    }

    compareIds(id1: any, id2: any): boolean {
        const a1 = determineId(id1);
        const a2 = determineId(id2);
        return a1 === a2;
    }

    saveProduct()
    {
        let options = new RequestOptions();
        this.setHeader(options);
        const data = this.productForm.getRawValue();
        this.productService.saveProduct(data)
            .then(() => {

                // Show the success message
                this.snackBar.open('Update User Sukses', 'OK', {
                    verticalPosition: 'top',
                    duration        : 2000
                });

                // Change the location with new one
                this.route.navigate(['apps/user/daftarUser/'])
            })
            .catch((error) => {
                let eror = JSON.parse(error._body);
                this.snackBar.open(eror.message, eror.errors.email ? eror.errors.email : eror.errors.password, {
                    verticalPosition: 'top',
                    duration        : 5000
                });
            });
    }

    addProduct()
    {
        const data = this.productForm.getRawValue();
        this.productService.addProduct(data)
            .then((response: any) => {
                this.snackBar.open(response.message, response.status, {
                    verticalPosition: 'top',
                    duration        : 2000
                });
                
                if (response.status) {
                    this.route.navigate(['apps/user/daftarUser/']);
                }
            })
            .catch((error) => {
                let eror = JSON.parse(error._body);
                this.snackBar.open(eror.message, eror.errors.email ? eror.errors.email : eror.errors.password, {
                    verticalPosition: 'top',
                    duration        : 5000
                });
            });;
    }

    hapus(): void{
        swal({
            title: 'Hapus User' + this.product.name,
            text: "User yang telah dihapus tidak bisa dikembalikan lagi",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus !',
            cancelButtonText: 'Batal!',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                this.productService.hapus(this.product)
                    .then((response: any) => {
                        if (response.status) {
                            this.back();
                            swal({
                                title: 'Hapus User Berhasil!',
                                text: 'User' + this.product.username + ' Telah Dihapus',
                                type: 'success',
                                timer: 2000
                            })
                        } else {
                            swal(
                                'Hapus Rute Gagal!',
                                response.message,
                                'error'
                            )
                        }
                    })
                    .catch((error) => {
                        let eror = JSON.parse(error._body);
                        this.snackBar.open(eror.message, eror.errors.email ? eror.errors.email : eror.errors.password, {
                            verticalPosition: 'top',
                            duration        : 5000
                        });
                    });
            }
        })
    }

    ubahPassword() {
        this.editPassword = true;
        this.productForm.controls.password.setValidators([Validators.required, Validators.minLength(8)]);
        this.productForm.controls.password_confirmation.setValidators([Validators.required]);
    }

    batalUbahPassword() {
        this.editPassword = false;
        this.productForm.controls.password.setValidators([]);
        this.productForm.controls.password_confirmation.setValidators([]);
    }

    ngOnDestroy()
    {
        this.onProductChanged.unsubscribe();
    }

    enableRute(){
        if (this.productForm.controls.group.value == 1) {
            this.productForm.controls.rute_id.enable();
        } else {
            this.productForm.controls.rute_id.disable();
        }
    }

    back(){
        this.location.back();
    }

}

export function determineId(id: any): string {
    if (id.constructor.name === 'array' && id.length > 0) {
       return '' + id[0];
    }
    return '' + id;
}
