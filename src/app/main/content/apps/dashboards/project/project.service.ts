import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from './../../session.service';

@Injectable()
export class ProjectDashboardService implements Resolve<any>
{
    projects: any[];
    widgets: any[];
    token = this._sessionService.get().token;
    dashboard: any;

    constructor(
        private http: HttpClient,
        private _http: Http,
        private router: Router,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getWidgets(),
                this.getDashboard()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getWidgets(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get('api/project-dashboard-widgets')
                .subscribe((response: any) => {
                    this.widgets = response;
                    resolve(response);
                }, (err) => {
                    reject(err);
                });
        });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this._sessionService.get().token);
        options.headers = headers;
    }

    getDashboard(): Promise<any> {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this._http.get(environment.setting.base_url + '/admin/dashboard', options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    this.dashboard = response.data;
                    resolve(response);
                }, (error) => {
                    let err = JSON.parse(error._body);
                    if (err.message == "Unauthenticated.") {
                        this.router.navigateByUrl('/apps/login');
                    }
                    reject(error);
                });
        });
    }

}
