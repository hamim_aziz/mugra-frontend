webpackJsonp(["feedback.module"],{

/***/ "../../../../../src/app/main/content/apps/feedback/daftarFeedback/daftarFeedback.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"products\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\"\r\n             fxLayout=\"column\" fxLayoutAlign=\"center center\"\r\n             fxLayout.gt-xs=\"row\" fxLayoutAlign.gt-xs=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div class=\"logo my-12 m-sm-0\"\r\n                 fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <mat-icon class=\"logo-icon mr-16\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'50ms',scale:'0.2'}}\">navigation</mat-icon>\r\n                <span class=\"logo-text h1\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">Daftar Komentar</span>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <!-- SEARCH -->\r\n            <div class=\"search-input-wrapper mx-12 m-md-0\"\r\n                 fxFlex=\"1 0 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <label for=\"search\" class=\"mr-8\">\r\n                    <mat-icon class=\"secondary-text\">search</mat-icon>\r\n                </label>\r\n                <mat-form-field floatPlaceholder=\"never\" fxFlex=\"1 0 auto\">\r\n                    <input id=\"search\" matInput #filter placeholder=\"Search\">\r\n                </mat-form-field>\r\n            </div>\r\n            <!-- / SEARCH -->\r\n\r\n            <!-- <button mat-raised-button\r\n                    [routerLink]=\"'/apps/feedback/detailFeedback/new'\"\r\n                    class=\"add-product-button mat-white-bg my-12 mt-sm-0\">\r\n                <span>Tambah Komentar</span>\r\n            </button> -->\r\n\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <mat-table class=\"products-table\"\r\n                       #table [dataSource]=\"dataSource\"\r\n                       matSort\r\n                       [@animateStagger]=\"{value:'50'}\"\r\n                       fusePerfectScrollbar>\r\n\r\n                <!-- ID Column -->\r\n                <ng-container cdkColumnDef=\"id\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>No.</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"text-truncate\">{{product.id}}</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Tujuan Column -->\r\n                <ng-container cdkColumnDef=\"nilai\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxShow.gt-md>Nilai</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxShow.gt-md>\r\n                        <p class=\"category text-truncate\"> {{product.nilai}} </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Asal Column -->\r\n                <ng-container cdkColumnDef=\"nik\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>NIK</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"text-truncate\">{{product.nik}}</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Tanggal Column -->\r\n                <ng-container cdkColumnDef=\"comment\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxHide fxShow.gt-xs>Komentar</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxHide fxShow.gt-xs>\r\n                        <p class=\"price text-truncate\">\r\n                            {{product.comment}}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <mat-header-row *cdkHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n\r\n                <mat-row *cdkRowDef=\"let product; columns: displayedColumns;\"\r\n                         class=\"product\" (click)=\"clickFeedback(product.id)\"\r\n                         matRipple>\r\n                </mat-row>\r\n\r\n            </mat-table>\r\n\r\n            <mat-paginator #paginator\r\n                           [length]=\"dataSource.filteredData.length\"\r\n                           [pageIndex]=\"0\"\r\n                           [pageSize]=\"10\"\r\n                           [pageSizeOptions]=\"[5, 10, 25, 100]\">\r\n            </mat-paginator>\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/feedback/daftarFeedback/daftarFeedback.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n:host .header .search-input-wrapper {\n  max-width: 480px; }\n@media (max-width: 599px) {\n  :host .header {\n    height: 176px !important;\n    min-height: 176px !important;\n    max-height: 176px !important; } }\n@media (max-width: 599px) {\n  :host .top-bg {\n    height: 240px; } }\n:host .products-table {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  border-bottom: 1px solid rgba(0, 0, 0, 0.12);\n  overflow: auto; }\n:host .products-table .mat-header-row {\n    min-height: 35px; }\n:host .products-table .product {\n    position: relative;\n    cursor: pointer;\n    height: 60px; }\n:host .products-table .mat-cell {\n    min-width: 0;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n:host .products-table .mat-column-id {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 5%;\n            flex: 0 1 5%; }\n:host .products-table .mat-column-comment {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 60%;\n            flex: 0 1 60%; }\n:host .products-table .mat-column-image {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 84px;\n            flex: 0 1 84px; }\n:host .products-table .mat-column-image .product-image {\n      width: 52px;\n      height: 52px;\n      border: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-column-buttons {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 80px;\n            flex: 0 1 80px; }\n:host .products-table .quantity-indicator {\n    display: inline-block;\n    vertical-align: middle;\n    width: 8px;\n    height: 8px;\n    border-radius: 4px;\n    margin-right: 8px; }\n:host .products-table .quantity-indicator + span {\n      display: inline-block;\n      vertical-align: middle; }\n:host .products-table .active-icon {\n    border-radius: 50%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/feedback/daftarFeedback/daftarFeedback.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarFeedbackComponent; });
/* unused harmony export FilesDataSource */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__daftarFeedback_service__ = __webpack_require__("../../../../../src/app/main/content/apps/feedback/daftarFeedback/daftarFeedback.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_cdk_collections__ = __webpack_require__("../../../cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs_add_observable_fromEvent__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/fromEvent.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__core_fuseUtils__ = __webpack_require__("../../../../../src/app/core/fuseUtils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__core_components_confirm_dialog_confirm_dialog_component__ = __webpack_require__("../../../../../src/app/core/components/confirm-dialog/confirm-dialog.component.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var DaftarFeedbackComponent = /** @class */ (function () {
    function DaftarFeedbackComponent(productsService, dialog, router) {
        this.productsService = productsService;
        this.dialog = dialog;
        this.router = router;
        this.hapus = false;
        this.displayedColumns = ['id', 'nik', 'nilai', 'comment'];
    }
    DaftarFeedbackComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataSource = new FilesDataSource(this.productsService, this.paginator, this.sort);
        __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].fromEvent(this.filter.nativeElement, 'keyup')
            .debounceTime(150)
            .distinctUntilChanged()
            .subscribe(function () {
            if (!_this.dataSource) {
                return;
            }
            _this.dataSource.filter = _this.filter.nativeElement.value;
        });
    };
    DaftarFeedbackComponent.prototype.toggleHapus = function () {
        this.hapus == true ? this.hapus = false : this.hapus = true;
    };
    DaftarFeedbackComponent.prototype.hapusDialog = function (id, asal, tujuan, navigate) {
        var _this = this;
        var pesan = "Hapus Rute ";
        var deleteDialog = this.dialog.open(__WEBPACK_IMPORTED_MODULE_15__core_components_confirm_dialog_confirm_dialog_component__["a" /* FuseConfirmDialogComponent */], {
            width: '250',
            data: { id: id, asal: asal, tujuan: tujuan, url: "/admin/rute/", navigate: navigate }
        });
        deleteDialog.afterClosed().subscribe(function () {
            _this.dataSource = new FilesDataSource(_this.productsService, _this.paginator, _this.sort);
            __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].fromEvent(_this.filter.nativeElement, 'keyup')
                .debounceTime(150)
                .distinctUntilChanged()
                .subscribe(function () {
                if (!_this.dataSource) {
                    return;
                }
                _this.dataSource.filter = _this.filter.nativeElement.value;
            });
        });
    };
    DaftarFeedbackComponent.prototype.clickFeedback = function (id) {
        var url = '/apps/feedback/detailFeedback/' + id;
        var navigate = '/apps/rute/daftarRute';
        if (!this.hapus) {
            this.router.navigate([url]);
        }
        else {
            // this.hapusDialog(id, navigate);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__angular_material__["z" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__angular_material__["z" /* MatPaginator */])
    ], DaftarFeedbackComponent.prototype, "paginator", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('filter'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], DaftarFeedbackComponent.prototype, "filter", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__angular_material__["M" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__angular_material__["M" /* MatSort */])
    ], DaftarFeedbackComponent.prototype, "sort", void 0);
    DaftarFeedbackComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-products',
            template: __webpack_require__("../../../../../src/app/main/content/apps/feedback/daftarFeedback/daftarFeedback.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/feedback/daftarFeedback/daftarFeedback.component.scss")],
            animations: __WEBPACK_IMPORTED_MODULE_4__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__daftarFeedback_service__["a" /* DaftarFeedbackService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_material__["m" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_7__angular_router__["f" /* Router */]])
    ], DaftarFeedbackComponent);
    return DaftarFeedbackComponent;
}());

var FilesDataSource = /** @class */ (function (_super) {
    __extends(FilesDataSource, _super);
    function FilesDataSource(productsService, _paginator, _sort) {
        var _this = _super.call(this) || this;
        _this.productsService = productsService;
        _this._paginator = _paginator;
        _this._sort = _sort;
        _this._filterChange = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        _this._filteredDataChange = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        _this.filteredData = _this.productsService.products;
        return _this;
    }
    Object.defineProperty(FilesDataSource.prototype, "filteredData", {
        get: function () {
            return this._filteredDataChange.value;
        },
        set: function (value) {
            this._filteredDataChange.next(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FilesDataSource.prototype, "filter", {
        get: function () {
            return this._filterChange.value;
        },
        set: function (filter) {
            this._filterChange.next(filter);
        },
        enumerable: true,
        configurable: true
    });
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    FilesDataSource.prototype.connect = function () {
        var _this = this;
        var displayDataChanges = [
            this.productsService.onProductsChanged,
            this._paginator.page,
            this._filterChange,
            this._sort.sortChange
        ];
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].merge.apply(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"], displayDataChanges).map(function () {
            var data = _this.productsService.products.slice();
            data = _this.filterData(data);
            _this.filteredData = data.slice();
            data = _this.sortData(data);
            // Grab the page's slice of data.
            var startIndex = _this._paginator.pageIndex * _this._paginator.pageSize;
            return data.splice(startIndex, _this._paginator.pageSize);
        });
    };
    FilesDataSource.prototype.filterData = function (data) {
        if (!this.filter) {
            return data;
        }
        return __WEBPACK_IMPORTED_MODULE_14__core_fuseUtils__["a" /* FuseUtils */].filterArrayByString(data, this.filter);
    };
    FilesDataSource.prototype.sortData = function (data) {
        var _this = this;
        if (!this._sort.active || this._sort.direction === '') {
            return data;
        }
        return data.sort(function (a, b) {
            var propertyA = '';
            var propertyB = '';
            switch (_this._sort.active) {
                case 'id':
                    _a = [a.id, b.id], propertyA = _a[0], propertyB = _a[1];
                    break;
                case 'asal':
                    _b = [a.asal, b.asal], propertyA = _b[0], propertyB = _b[1];
                    break;
                case 'tujuan':
                    _c = [a.tujuan, b.tujuan], propertyA = _c[0], propertyB = _c[1];
                    break;
                case 'tanggal':
                    _d = [a.tanggal, b.tanggal], propertyA = _d[0], propertyB = _d[1];
                    break;
                case 'jenis':
                    _e = [a.jenis, b.jenis], propertyA = _e[0], propertyB = _e[1];
                    break;
                case 'kuota_motor':
                    _f = [a.kouta_motor, b.kouta_motor], propertyA = _f[0], propertyB = _f[1];
                    break;
                case 'status':
                    _g = [a.status, b.status], propertyA = _g[0], propertyB = _g[1];
                    break;
            }
            var valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            var valueB = isNaN(+propertyB) ? propertyB : +propertyB;
            return (valueA < valueB ? -1 : 1) * (_this._sort.direction === 'asc' ? 1 : -1);
            var _a, _b, _c, _d, _e, _f, _g;
        });
    };
    FilesDataSource.prototype.disconnect = function () {
    };
    return FilesDataSource;
}(__WEBPACK_IMPORTED_MODULE_2__angular_cdk_collections__["a" /* DataSource */]));



/***/ }),

/***/ "../../../../../src/app/main/content/apps/feedback/daftarFeedback/daftarFeedback.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarFeedbackService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DaftarFeedbackService = /** @class */ (function () {
    function DaftarFeedbackService(http, _sessionService) {
        this.http = http;
        this._sessionService = _sessionService;
        this.onProductsChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({});
        this.token = this._sessionService.get().token;
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DaftarFeedbackService.prototype.resolve = function (route, state) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getProducts()
            ]).then(function () {
                resolve();
            }, reject);
        });
    };
    DaftarFeedbackService.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        options.headers = headers;
    };
    DaftarFeedbackService.prototype.getProducts = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/feedbacks', options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                _this.products = response.feedbacks.data;
                _this.onProductsChanged.next(_this.products);
                resolve(response);
            }, reject);
        });
    };
    DaftarFeedbackService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__session_service__["a" /* SessionService */]])
    ], DaftarFeedbackService);
    return DaftarFeedbackService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/feedback/detailFeedback/detailFeedback.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"product\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"start center\" style=\"width: 80%\">\r\n\r\n                <button class=\"mr-0 mr-sm-16\" mat-icon-button [routerLink]=\"'/apps/feedback/daftarFeedback'\">\r\n                    <mat-icon>arrow_back</mat-icon>\r\n                </button>\r\n\r\n                <div fxLayout=\"column\" fxLayoutAlign=\"start start\"\r\n                     *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">\r\n                    <div class=\"h2\" *ngIf=\"pageType ==='edit'\">\r\n                        {{product.id}}\r\n                    </div>\r\n                    <div class=\"h2\" *ngIf=\"pageType ==='new'\">\r\n                        Komentar Baru\r\n                    </div>\r\n                    <div class=\"subtitle secondary-text\">\r\n                        <span>Detail Komentar</span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <!-- <button mat-raised-button\r\n                    class=\"save-product-button mat-orange-bg mt-16 mt-sm-0\"\r\n                    [disabled]=\"productForm.invalid\"\r\n                    *ngIf=\"pageType ==='new'\" (click)=\"addProduct()\">\r\n                <span>Tambah Komentar</span>\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mat-orange-bg mt-16 mt-sm-0\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"saveProduct()\">\r\n                <span>Simpan</span>\r\n            </button> -->\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <!-- CONTENT -->\r\n            <div class=\"content\">\r\n\r\n                <form name=\"productForm\" [formGroup]=\"productForm\" class=\"product w-100-p\" fxLayout=\"column\" fxFlex>\r\n\r\n                    <mat-tab-group>\r\n\r\n                        <mat-tab label=\"Komentar\">\r\n                            <div class=\"tab-content p-24\" fusePerfectScrollbar>\r\n\r\n                                <!-- <mat-form-field class=\"w-100-p\" *ngIf=\"pageType ==='new'\">\r\n                                    <input matInput\r\n                                           name=\"id\"\r\n                                           formControlName=\"id\"\r\n                                           placeholder=\"ID Komentar\"\r\n                                           required>\r\n                                    <mat-error *ngIf=\"productForm.controls.id.invalid\">\r\n                                         {{productForm.controls.id.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                    </mat-error>\r\n                                </mat-form-field> -->\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                    <input matInput\r\n                                           name=\"nik\"\r\n                                           formControlName=\"nik\"\r\n                                           placeholder=\"NIK\"\r\n                                           required>\r\n                                    <mat-error *ngIf=\"productForm.controls.nik.invalid\">\r\n                                         {{productForm.controls.nik.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                    </mat-error>\r\n                                </mat-form-field>\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                    <input matInput\r\n                                           name=\"no_kk\"\r\n                                           formControlName=\"no_kk\"\r\n                                           placeholder=\"Nomor KK\"\r\n                                           required>\r\n                                    <mat-error *ngIf=\"productForm.controls.no_kk.invalid\">\r\n                                         {{productForm.controls.no_kk.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                    </mat-error>\r\n                                </mat-form-field>\r\n\r\n                                <!-- <mat-form-field class=\"w-100-p\">\r\n                                    <input matInput\r\n                                           name=\"comment\"\r\n                                           formControlName=\"comment\"\r\n                                           placeholder=\"Komentar\"\r\n                                           required>\r\n                                    <mat-error *ngIf=\"productForm.controls.comment.invalid\">\r\n                                         {{productForm.controls.comment.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                    </mat-error>\r\n                                </mat-form-field> -->\r\n\r\n                                <p>\r\n                                    {{ product.comment }}\r\n                                </p>\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                    <input matInput\r\n                                           name=\"nilai\"\r\n                                           formControlName=\"nilai\"\r\n                                           placeholder=\"Nilai\"\r\n                                           min=\"1\"\r\n                                           required>\r\n                                    <mat-error *ngIf=\"productForm.controls.nilai.invalid\">\r\n                                         {{productForm.controls.nilai.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                    </mat-error>\r\n                                </mat-form-field>\r\n\r\n                                <!-- <mat-slide-toggle matInput\r\n                                                  formControlName=\"status\"\r\n                                                   *ngIf=\"pageType !=='new'\">\r\n                                  Aktif\r\n                                </mat-slide-toggle> -->\r\n                            </div>\r\n                        </mat-tab>\r\n\r\n                    </mat-tab-group>\r\n                </form>\r\n\r\n            </div>\r\n            <!-- / CONTENT -->\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/feedback/detailFeedback/detailFeedback.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#product .header .product-image {\n  overflow: hidden;\n  width: 56px;\n  height: 56px;\n  border: 3px solid rgba(0, 0, 0, 0.12); }\n  #product .header .product-image img {\n    height: 100%;\n    width: auto;\n    max-width: none; }\n  #product .header .subtitle {\n  margin: 6px 0 0 0; }\n  #product .content table, #product .content th, #product .content td {\n  border: 1px solid black;\n  border-collapse: collapse; }\n  #product .content th, #product .content td {\n  padding: 15px; }\n  #product .content .example-container {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  max-height: 500px;\n  min-width: 300px; }\n  #product .content .mat-table {\n  overflow: auto;\n  max-height: 500px; }\n  #product .content .mat-tab-group,\n#product .content .mat-tab-body-wrapper,\n#product .content .tab-content {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  max-width: 100%; }\n  #product .content .mat-tab-body-content {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n  #product .content .mat-tab-label {\n  height: 64px; }\n  #product .content .product-image {\n  overflow: hidden;\n  width: 128px;\n  height: 128px;\n  margin-right: 16px;\n  margin-bottom: 16px;\n  border: 3px solid rgba(0, 0, 0, 0.12); }\n  #product .content .product-image img {\n    height: 100%;\n    width: auto;\n    max-width: none; }\n  #product .content .list-kota {\n  border-bottom: 1px solid; }\n  #product .content .mat-card .mat-divider.mat-divider-inset {\n  position: absolute !important; }\n  #product .content .mat-list .mat-list-item .mat-divider.mat-divider-inset {\n  left: 0 !important;\n  width: 100% !important; }\n  #product .content .mat-grid-tile .mat-figure {\n  -webkit-box-pack: left !important;\n      -ms-flex-pack: left !important;\n          justify-content: left !important; }\n  #product .content .mat-list .mat-list-item {\n  height: unset !important; }\n  #product .content .mat-list .mat-list-item {\n  font-size: 13px !important; }\n  #product .content .mat-card-title {\n  font-size: 15px !important; }\n  #product .content .mat-card-actions, #product .content .mat-card-content, #product .content .mat-card-subtitle, #product .content .mat-card-title {\n  padding-bottom: 10px !important; }\n  #product .content .mat-card {\n  padding: 10px !important; }\n  #product .content .mat-card-subtitle, #product .content .mat-card-content, #product .content .mat-card-header .mat-card-title {\n  font-size: 12px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/feedback/detailFeedback/detailFeedback.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailFeedbackComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__detailFeedback_service__ = __webpack_require__("../../../../../src/app/main/content/apps/feedback/detailFeedback/detailFeedback.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_observable_fromEvent__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/fromEvent.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__detailFeedback_model__ = __webpack_require__("../../../../../src/app/main/content/apps/feedback/detailFeedback/detailFeedback.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var DetailFeedbackComponent = /** @class */ (function () {
    function DetailFeedbackComponent(productService, formBuilder, snackBar, location, http, route, _sessionService) {
        this.productService = productService;
        this.formBuilder = formBuilder;
        this.snackBar = snackBar;
        this.location = location;
        this.http = http;
        this.route = route;
        this._sessionService = _sessionService;
        this.product = new __WEBPACK_IMPORTED_MODULE_9__detailFeedback_model__["a" /* Product */]();
        this.token = this._sessionService.get().token;
    }
    DetailFeedbackComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Subscribe to update product on changes
        this.onProductChanged =
            this.productService.onProductChanged
                .subscribe(function (product) {
                if (product) {
                    _this.product = new __WEBPACK_IMPORTED_MODULE_9__detailFeedback_model__["a" /* Product */](product);
                    _this.pageType = 'edit';
                }
                else {
                    _this.pageType = 'new';
                    _this.product = new __WEBPACK_IMPORTED_MODULE_9__detailFeedback_model__["a" /* Product */]();
                }
                _this.productForm = _this.createProductForm();
            });
    };
    DetailFeedbackComponent.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_14__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        options.headers = headers;
    };
    DetailFeedbackComponent.prototype.createProductForm = function () {
        return this.formBuilder.group({
            id: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: this.product.id, disabled: true }, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            nik: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: this.product.nik, disabled: true }, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            no_kk: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: this.product.no_kk, disabled: true }, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            comment: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.comment, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            nilai: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: this.product.nilai, disabled: true }, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            status: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.status == 0 ? false : true, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required])
        });
    };
    DetailFeedbackComponent.prototype.saveProduct = function () {
        var _this = this;
        if (this.productForm.controls.status.value) {
            this.productForm.controls.status.setValue(1);
        }
        else {
            this.productForm.controls.status.setValue(0);
        }
        var data = this.productForm.getRawValue();
        this.productService.saveProduct(data)
            .then(function () {
            // Trigger the subscription with new data
            _this.productService.onProductChanged.next(data);
            // Show the success message
            _this.snackBar.open('Update Feedback Sukses', 'OK', {
                verticalPosition: 'top',
                duration: 2000
            });
            _this.location.back();
        });
    };
    DetailFeedbackComponent.prototype.addProduct = function () {
        var _this = this;
        if (this.productForm.controls.status.value) {
            this.productForm.controls.status.setValue(1);
        }
        else {
            this.productForm.controls.status.setValue(0);
        }
        var data = this.productForm.getRawValue();
        this.productService.addProduct(data)
            .then(function () {
            // Trigger the subscription with new data
            _this.productService.onProductChanged.next(data);
            // Show the success message
            _this.snackBar.open('Tambah Feedback Sukses', 'OK', {
                verticalPosition: 'top',
                duration: 2000
            });
            // Change the location with new one
            _this.location.back();
        });
    };
    DetailFeedbackComponent.prototype.ngOnDestroy = function () {
        this.onProductChanged.unsubscribe();
    };
    DetailFeedbackComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-product',
            template: __webpack_require__("../../../../../src/app/main/content/apps/feedback/detailFeedback/detailFeedback.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/feedback/detailFeedback/detailFeedback.component.scss")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
            animations: __WEBPACK_IMPORTED_MODULE_2__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__detailFeedback_service__["a" /* DetailFeedbackService */],
            __WEBPACK_IMPORTED_MODULE_10__angular_forms__["c" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_11__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_12__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_14__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_13__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_15__session_service__["a" /* SessionService */]])
    ], DetailFeedbackComponent);
    return DetailFeedbackComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/feedback/detailFeedback/detailFeedback.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Product; });
var Product = /** @class */ (function () {
    function Product(product) {
        if (product) {
            product = product;
            this.id = product.id;
            this.nik = product.nik;
            this.no_kk = product.no_kk;
            this.comment = product.comment;
            this.nilai = product.nilai;
            this.status = product.status;
        }
        else {
            product = {};
            this.id = 0;
            this.nik = '';
            this.no_kk = '';
            this.comment = '';
            this.nilai = 0;
            this.status = 0;
        }
    }
    return Product;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/feedback/detailFeedback/detailFeedback.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailFeedbackService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DetailFeedbackService = /** @class */ (function () {
    function DetailFeedbackService(http, snackBar, _sessionService) {
        this.http = http;
        this.snackBar = snackBar;
        this._sessionService = _sessionService;
        this.onProductChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({});
        this.token = this._sessionService.get().token;
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DetailFeedbackService.prototype.resolve = function (route, state) {
        var _this = this;
        this.routeParams = route.params;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getProduct()
            ]).then(function () {
                resolve();
            }, reject);
        });
    };
    DetailFeedbackService.prototype.getProduct = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            if (_this.routeParams.id === 'new') {
                _this.onProductChanged.next(false);
                resolve(false);
            }
            else {
                _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/feedbacks/' + _this.routeParams.id, options)
                    .map(function (res) { return res.json(); })
                    .subscribe(function (response) {
                    _this.product = response;
                    _this.onProductChanged.next(_this.product);
                    resolve(response);
                }, reject);
            }
        });
    };
    DetailFeedbackService.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        options.headers = headers;
    };
    DetailFeedbackService.prototype.saveProduct = function (product) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.put(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/feedbacks/' + _this.routeParams.id, product, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (reject) {
                var eror = JSON.parse(reject._body);
                _this.snackBar.open(eror.message, 'Error !', {
                    verticalPosition: 'top',
                    duration: 5000
                });
            });
        });
    };
    DetailFeedbackService.prototype.addProduct = function (product) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        options.headers = headers;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/feedback', product, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (reject) {
                var eror = JSON.parse(reject._body);
                _this.snackBar.open(eror.message, 'Error !', {
                    verticalPosition: 'top',
                    duration: 5000
                });
            });
        });
    };
    DetailFeedbackService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_6__session_service__["a" /* SessionService */]])
    ], DetailFeedbackService);
    return DetailFeedbackService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/feedback/feedback.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeedbackModule", function() { return FeedbackModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__ = __webpack_require__("../../../../@swimlane/ngx-charts/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__ = __webpack_require__("../../../../../src/app/core/modules/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__ = __webpack_require__("../../../../../src/app/core/components/widget/widget.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__daftarFeedback_daftarFeedback_component__ = __webpack_require__("../../../../../src/app/main/content/apps/feedback/daftarFeedback/daftarFeedback.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__daftarFeedback_daftarFeedback_service__ = __webpack_require__("../../../../../src/app/main/content/apps/feedback/daftarFeedback/daftarFeedback.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__detailFeedback_detailFeedback_component__ = __webpack_require__("../../../../../src/app/main/content/apps/feedback/detailFeedback/detailFeedback.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__detailFeedback_detailFeedback_service__ = __webpack_require__("../../../../../src/app/main/content/apps/feedback/detailFeedback/detailFeedback.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__core_components_confirm_dialog_confirm_dialog_component__ = __webpack_require__("../../../../../src/app/core/components/confirm-dialog/confirm-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__ = __webpack_require__("../../../cdk/esm5/table.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__ = __webpack_require__("../../../../ng2-dnd/ng2-dnd.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__auth_guard__ = __webpack_require__("../../../../../src/app/main/content/apps/auth.guard.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var routes = [
    {
        path: 'daftarFeedback',
        component: __WEBPACK_IMPORTED_MODULE_5__daftarFeedback_daftarFeedback_component__["a" /* DaftarFeedbackComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_13__auth_guard__["a" /* AuthGuard */]],
        resolve: {
            data: __WEBPACK_IMPORTED_MODULE_6__daftarFeedback_daftarFeedback_service__["a" /* DaftarFeedbackService */]
        }
    },
    {
        path: 'detailFeedback/:id',
        component: __WEBPACK_IMPORTED_MODULE_7__detailFeedback_detailFeedback_component__["a" /* DetailFeedbackComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_13__auth_guard__["a" /* AuthGuard */]],
        resolve: {
            data: __WEBPACK_IMPORTED_MODULE_8__detailFeedback_detailFeedback_service__["a" /* DetailFeedbackService */]
        }
    }
];
var FeedbackModule = /** @class */ (function () {
    function FeedbackModule() {
    }
    FeedbackModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__["a" /* FuseWidgetModule */],
                __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__["NgxChartsModule"],
                __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__["m" /* CdkTableModule */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["a" /* DndModule */],
                __WEBPACK_IMPORTED_MODULE_10__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
                })
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__["m" /* CdkTableModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__daftarFeedback_daftarFeedback_component__["a" /* DaftarFeedbackComponent */],
                __WEBPACK_IMPORTED_MODULE_9__core_components_confirm_dialog_confirm_dialog_component__["a" /* FuseConfirmDialogComponent */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__daftarFeedback_daftarFeedback_component__["a" /* DaftarFeedbackComponent */],
                __WEBPACK_IMPORTED_MODULE_7__detailFeedback_detailFeedback_component__["a" /* DetailFeedbackComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__daftarFeedback_daftarFeedback_service__["a" /* DaftarFeedbackService */],
                __WEBPACK_IMPORTED_MODULE_8__detailFeedback_detailFeedback_service__["a" /* DetailFeedbackService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["d" /* DragDropSortableService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["c" /* DragDropService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["b" /* DragDropConfig */]
            ]
        })
    ], FeedbackModule);
    return FeedbackModule;
}());



/***/ })

});
//# sourceMappingURL=feedback.module.chunk.js.map