import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatChipInputEvent } from '@angular/material';

export class Product
{
    id: number;
    rute_id: number;
    jenis: number;
    nomor: string;
    nama: string;
    baris: number;
    kolom: number;
    jumlah: number;
    status: number;
    kuota: number;
    kapasitas:{
        id: number,
        tersedia: number,
        sisa: number
    }[];
    rute:{
        id: number,
        jenis: number,
        tanggal: string,
        keterangan: string,
        kota:{
            id: string,
            asal: string,
            tujuan: string
        }[]
    }[];
    kursis:{
        id: number,
        nama: string,
        status: number,
        nomor_tiket: string,
        pemudik: string
    }[];

    constructor(product?)
    {
        if (product) {
            product = product;
            this.id = product.id;
            this.rute_id = product.rute_id;
            this.jenis = product.jenis;
            this.nomor = product.nomor;
            this.nama = product.nama;
            this.baris = product.baris;
            this.kolom = product.kolom;
            this.status = product.status;
            this.jumlah = 0;
            this.kapasitas = product.kapasitas;
            this.rute = product.rute;
            this.kursis = product.kursis;
            this.kuota = product.kuota;
        } else {
            product = {};
            this.id = 0;
            this.rute_id = 0;
            this.jenis = 1;
            this.nomor = 'nomor';
            this.nama = 'nama';
            this.baris = 12;
            this.kolom = 5;
            this.jumlah = 1;
            this.status = 0;
            this.kapasitas = [];
            this.rute = [];
            this.kursis = [];
        }
    }
    
}
