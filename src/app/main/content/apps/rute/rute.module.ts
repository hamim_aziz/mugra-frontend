import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { DaftarRuteComponent } from './daftarRute/daftarRute.component';
import { DaftarRuteService } from './daftarRute/daftarRute.service';
import { DetailRuteComponent } from './detailRute/detailRute.component';
import { DetailRuteService } from './detailRute/detailRute.service';
import { FuseConfirmDialogComponent } from './../../../../core/components/confirm-dialog/confirm-dialog.component';
import { AgmCoreModule } from '@agm/core';
import { CdkTableModule } from '@angular/cdk/table';
import { DragDropSortableService, DndModule, DragDropService, DragDropConfig } from 'ng2-dnd';
import { AuthGuard } from './../auth.guard';
import { MatTableModule } from '@angular/material/table';

const routes: Routes = [
    {
        path     : 'daftarRute',
        component: DaftarRuteComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DaftarRuteService
        }
    },
    {
        path     : 'detailRute/:id',
        component: DetailRuteComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DetailRuteService
        }
    }
];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        CdkTableModule,
        DndModule,
        MatTableModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    exports : [
        CdkTableModule
    ],
    entryComponents: [
        DaftarRuteComponent,
        FuseConfirmDialogComponent
    ],
    declarations: [
        DaftarRuteComponent,
        DetailRuteComponent
    ],
    providers   : [
        DaftarRuteService,
        DetailRuteService,
        DragDropSortableService, 
        DragDropService, 
        DragDropConfig
    ]
})
export class RuteModule
{
}
