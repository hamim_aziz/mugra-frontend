import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { MatSnackBar } from '@angular/material';
import swal from 'sweetalert2';
import { SessionService } from './../../session.service';

@Injectable()
export class DetailContentService implements Resolve<any>
{
    routeParams: any;
    content: any;
    onContentChanged: BehaviorSubject<any> = new BehaviorSubject({});
    token = this._sessionService.get().token;

    constructor(
        private http: Http,
        private snackBar: MatSnackBar,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getContent()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getContent(): Promise<any>
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            if ( this.routeParams.id === 'new' )
            {
                this.onContentChanged.next(false);
                resolve(false);
            }
            else
            {
                this.http.get(environment.setting.base_url + '/admin/contents/' + this.routeParams.id, options)
                    .map((res: Response) => res.json())
                    .subscribe((response: any) => {
                        this.content = response['data'];
                        this.onContentChanged.next(this.content);
                        resolve(response);
                    }, (reject) => {
                        let eror = JSON.parse(reject._body);
                        this.snackBar.open(eror.message, 'Error !', {
                            verticalPosition: 'top',
                            duration        : 5000
                        });
                    });
            }
        });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        options.headers = headers;
    }

    saveContent(content)
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/contents/' + this.routeParams.id, content, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (reject) => {
                    let eror = JSON.parse(reject._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    })
                });
        });
    }

    addContent(content)
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.post(environment.setting.base_url + '/admin/contents/', content, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (reject) => {
                    let eror = JSON.parse(reject._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    })
                });
        });
    }

    hapus(content){
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.delete(environment.setting.base_url + '/admin/contents/' + this.routeParams.id, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (reject) => {
                    let eror = JSON.parse(reject._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    })
                });
        });
    }
}
