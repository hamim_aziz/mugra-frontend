import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DaftarContactService } from './daftarContact.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { fuseAnimations } from '../../../../../core/animations';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from '@angular/router';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { FuseConfirmDialogComponent } from './../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import swal from 'sweetalert2';
import { SessionService } from './../../session.service';

@Component({
    selector   : 'fuse-e-commerce-contacts',
    templateUrl: './daftarContact.component.html',
    styleUrls  : ['./daftarContact.component.scss'],
    animations : fuseAnimations
})
export class DaftarContactComponent implements OnInit
{
    hapus = false;
    dataSource: FilesDataSource | null;
    displayedColumns = ['id', 'name', 'phone', 'link'];
    role = this._sessionService.get().role;
    contacts = this.contactsService.contacts;
    date = [];
    dateIndo = [];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private contactsService: DaftarContactService,
        private dialog: MatDialog,
        private router: Router,
        private _sessionService: SessionService
    )
    {
    }

    ngOnInit()
    {
        this._tanggal();
        if (sessionStorage.getItem("pageIndexContact")) {
            this.paginator.pageIndex = Number(sessionStorage.getItem("pageIndexContact"));
        }

        this.dataSource = new FilesDataSource(this.contactsService, this.paginator, this.sort);
        Observable.fromEvent(this.filter.nativeElement, 'keyup')
                  .debounceTime(150)
                  .distinctUntilChanged()
                  .subscribe(() => {
                      if ( !this.dataSource )
                      {
                          return;
                      }
                      if (!this.filter.nativeElement.value) {
                          this.paginator.pageIndex = Number(sessionStorage.getItem("pageIndexContact"));
                      } else {
                          this.paginator.pageIndex = 0;
                      }
                      sessionStorage.setItem("filterContact", this.filter.nativeElement.value);
                      this.dataSource.filter = this.filter.nativeElement.value;
                  });

        Observable.fromEvent(this.filter.nativeElement, 'click')
                  .debounceTime(150)
                  .distinctUntilChanged()
                  .subscribe(() => {
                      if ( !this.dataSource )
                      {
                          return;
                      }
                      if (this.filter.nativeElement.value || sessionStorage.getItem("filterContact")) {
                          this.paginator.pageIndex = 0;
                      } else {
                          this.paginator.pageIndex = Number(sessionStorage.getItem("pageIndexContact"));
                      }
                      this.filter.nativeElement.value = sessionStorage.getItem("filterContact");
                      this.dataSource.filter = this.filter.nativeElement.value;
                  });
    }

    _tanggal(){
        for (var i = 0; i < this.contacts.length; ++i) {
            if (this.date.indexOf(this.contacts[i]["tanggal"]) == -1) {
                this.dateIndo.push([this.contacts[i]["tanggal"], this.convertTanggal(this.contacts[i]["tanggal"])]);
                this.date.push(this.contacts[i]["tanggal"]);
            }
        }
    }

    convertTanggal(tgl){
        let hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
        let bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
        let date = new Date(tgl);
        let day = date.getDay();
        let month = date.getMonth();
        return hari[day] + ", " + date.getDate() + " " + bulan[month] + " " + date.getFullYear();
    }

    cobaSwal(){
        swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, cancel!',
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          reverseButtons: true
        }).then((result) => {
          if (result.value) {
            swal(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
          } else if (
            // Read more about handling dismissals
            result.dismiss === swal.DismissReason.cancel
          ) {
            swal(
              'Cancelled',
              'Your imaginary file is safe :)',
              'error'
            )
          }
        })
    }

    filterButton(filter){
        this.dataSource.filter = filter;
    }

    semua(){
        this.dataSource.filter = "";
    }

    toggleHapus(){
        this.hapus == true ? this.hapus = false : this.hapus = true;
    }

    clickContact(contact): void{
        swal({
            title: 'Edit Contact',
            html:
                `<input id="swal-input1" class="swal2-input" value="${contact.name}" required>` +
                `<input id="swal-input2" class="swal2-input" value="${contact.phone}" required>`,
            focusConfirm: false,
            showCancelButton: true,
            showCloseButton: true,
            cancelButtonText: 'Delete',
            confirmButtonText: 'Save',
            preConfirm: () => {
                return [
                    (<HTMLInputElement>document.getElementById('swal-input1')).value,
                    (<HTMLInputElement>document.getElementById('swal-input2')).value
                ]
            }
        }).then((result) => {
            if (result.value) {
                let obj = {
                    name         : (<HTMLInputElement>document.getElementById('swal-input1')).value,
                    phone        : (<HTMLInputElement>document.getElementById('swal-input2')).value,
                    link         : "https://wa.me/" + (<HTMLInputElement>document.getElementById('swal-input2')).value,
                    id           : contact.id
                };
                this.contactsService.updateContact(obj)
                    .then((response: any) => {
                        if (response.status) {
                            console.log(this.dataSource);
                            swal(
                                'Updated!',
                                response.message,
                                'success'
                            )
                        }
                    })
                    .catch((error: any) => {
                        swal(
                            'Failed',
                            'Failed update data',
                            'error'
                        )
                    })
            }
            if (result.dismiss == swal.DismissReason.cancel) {
                swal(
                    'Apakah anda yakin ?',
                    'Kontak yang telah dihapus tidak bisa dikembalikan',
                    'question'
                ).then((result_hapus) => {
                    if (result_hapus.value) {
                        this.contactsService.deleteContact(contact)
                        .then((response: any) => {
                            if (response.status) {
                                console.log(this.dataSource);
                                swal(
                                    'Deleted!',
                                    response.message,
                                    'success'
                                )
                            }
                        })
                        .catch((error: any) => {
                            swal(
                                'Failed',
                                'Failed update data',
                                'error'
                            )
                        })
                    }
                })
            }
        });
    }

    bulan(date){
        if (date.substring(5, 7) === '01') {
            return "Januari";
        } else if (date.substring(5, 7) === '02') {
            return "Februari";
        } else if (date.substring(5, 7) === '03') {
            return "Maret";
        } else if (date.substring(5, 7) === '04') {
            return "April";
        } else if (date.substring(5, 7) === '05') {
            return "Mei";
        } else if (date.substring(5, 7) === '06') {
            return "Juni";
        } else if (date.substring(5, 7) === '07') {
            return "Juli";
        } else if (date.substring(5, 7) === '08') {
            return "Agustus";
        } else if (date.substring(5, 7) === '09') {
            return "September";
        } else if (date.substring(5, 7) === '10') {
            return "Oktober";
        } else if (date.substring(5, 7) === '11') {
            return "November";
        } else if (date.substring(5, 7) === '12') {
            return "Desember";
        }
    }
}

export class FilesDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }

    constructor(
        private contactsService: DaftarContactService,
        private _paginator: MatPaginator,
        private _sort: MatSort
    )
    {
        super();
        this.filteredData = this.contactsService.contacts;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this.contactsService.onContactsChanged,
            this._paginator.page,
            this._filterChange,
            this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.contactsService.contacts.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            data = this.sortData(data);

            // Grab the page's slice of data.
            if (!this.filter) {
                sessionStorage.setItem("pageIndexContact", String(this._paginator.pageIndex));
            }
            
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data)
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    sortData(data): any[]
    {
        if ( !this._sort.active || this._sort.direction === '' )
        {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch ( this._sort.active )
            {
                case 'id':
                    [propertyA, propertyB] = [a.id, b.id];
                    break;
                case 'asal':
                    [propertyA, propertyB] = [a.asal, b.asal];
                    break;
                case 'tujuan':
                    [propertyA, propertyB] = [a.tujuan, b.tujuan];
                    break;
                case 'tanggal':
                    [propertyA, propertyB] = [a.tanggal, b.tanggal];
                    break;
                case 'jenis':
                    [propertyA, propertyB] = [a.jenis, b.jenis];
                    break;
                case 'kuota_motor':
                    [propertyA, propertyB] = [a.kouta_motor, b.kouta_motor];
                    break;
                case 'status':
                    [propertyA, propertyB] = [a.status, b.status];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
        });
    }

    disconnect()
    {
    }
}
