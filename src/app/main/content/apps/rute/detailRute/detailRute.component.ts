import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { DetailRuteService } from './detailRute.service';
import { fuseAnimations } from '../../../../../core/animations';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Product } from './detailRute.model';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { FuseConfirmDialogComponent } from './../../../../../core/components/confirm-dialog/confirm-dialog.component';
import swal from 'sweetalert2';
import { SessionService } from './../../session.service';

@Component({
    selector     : 'fuse-e-commerce-product',
    templateUrl  : './detailRute.component.html',
    styleUrls    : ['./detailRute.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class DetailRuteComponent implements OnInit, OnDestroy
{
    product = new Product();
    onProductChanged: Subscription;
    pageType: string;
    productForm: FormGroup;
    kotaMaster: any;
    daftarKota: any;
    subkotaLama = [];
    subkota = [];
    daftarKotaSubkota = [];
    status: boolean;
    token = this._sessionService.get().token;
    role = this._sessionService.get().role;

    constructor(
        private productService: DetailRuteService,
        private formBuilder: FormBuilder,
        public snackBar: MatSnackBar,
        private location: Location,
        private http: Http,
        private route: ActivatedRoute,
        private dialog: MatDialog,
        private router: Router,
        private _sessionService: SessionService
    )
    {

    }

    ngOnInit()
    {
        // Subscribe to update product on changes
        this.onProductChanged =
            this.productService.onProductChanged
                .subscribe(product => {

                    if ( product )
                    {
                        this.product = new Product(product);
                        this.pageType = 'edit';
                        this.status = this.product.status == '1' ? true : false;
                    }
                    else
                    {
                        this.pageType = 'new';
                        this.product = new Product();
                    }

                    this.productForm = this.createProductForm();
                });
        this.getKota();
        this.getDaftarKota();
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }

    getDaftarKota(){
        let options = new RequestOptions();
        this.setHeader(options);
        this.http.get(environment.setting.base_url + "/admin/kota/master", options)
            .map((res: Response) => res.json())
            .subscribe((response: any) => {
                this.daftarKota = response['data'];
                if (this.pageType !== "new") {
                    this.getSubkota();
                }
            })
    }

    getKota(){
        let options = new RequestOptions();
        this.setHeader(options);
        this.http.get(environment.setting.base_url + "/admin/kota/master", options)
            .map((res: Response) => res.json())
            .subscribe((response: any) => {
                this.kotaMaster = response['data'];
            })
    }

    getSubkota(){
        let options = new RequestOptions();
        this.setHeader(options);
        this.http.get(environment.setting.base_url + "/admin/rute/subkota/" + this.route.snapshot.paramMap.get('id'), options)
            .map((res: Response) => res.json())
            .subscribe((response: any) => {
                this.subkotaLama = response['data'];
                if (response['data'].length == 0) {
                    for (var i = 0; i < this.daftarKota.length; ++i) {
                        this.daftarKotaSubkota.push(this.daftarKota[i]);
                    }
                } else {
                    for (var i = 0; i < this.daftarKota.length; ++i) {
                        for (var j = 0; j < response['data'].length; ++j) {
                            if (this.daftarKota[i]['nama'] == response['data'][j]['nama']) {
                                this.subkota.push(this.daftarKota[i]);
                                break;
                            } else {
                                if (j == response['data'].length - 1) {
                                    this.daftarKotaSubkota.push(this.daftarKota[i]);
                                }
                            }
                        }
                    }
                }
            })
    }

    createProductForm()
    {
        return this.formBuilder.group({
            jenis              : new FormControl(this.product.jenis, [Validators.required]),
            keterangan         : new FormControl(this.product.keterangan, [Validators.required]),
            tanggal            : new FormControl(this.product.tanggal + "T" + this.product.jam, [Validators.required]),
            jam                : new FormControl(this.product.jam),
            asal               : new FormControl(this.product.asal, [Validators.required]),
            tujuan             : new FormControl(this.product.tujuan, [Validators.required]),
            kouta_motor        : new FormControl(this.product.kouta_motor, [Validators.required]),
            contact_person     : new FormControl(this.product.contact_person, [Validators.required]),
            tempat_berangkat   : new FormControl(this.product.tempat_berangkat, [Validators.required]),
            tempat_ambil_tiket : new FormControl(this.product.tempat_ambil_tiket, [Validators.required]),
            status             : new FormControl(this.product.status),
            kendaraans         : new FormControl(this.product.kendaraans),
            subkotas           : new FormControl(this.product.subkotas),
            is_titip_motor     : new FormControl(this.product.is_titip_motor, [Validators.required]),
            is_titip_motor_balik: new FormControl(this.product.is_titip_motor_balik, [Validators.required]),
            kode               : new FormControl(this.product.kode, [Validators.required])
        });
    }

    saveProduct()
    {
        this.productForm.controls.jam.setValue(this.productForm.controls.tanggal.value.substring(11, 16));
        let options = new RequestOptions();
        this.setHeader(options);
        const data = this.productForm.getRawValue();
        this.productService.saveProduct(data)
            .then((response: any) => {

                // Trigger the subscription with new data
                this.productService.onProductChanged.next(data);
                for (var i = 0; i < this.subkotaLama.length; ++i) {
                    this.http.delete(environment.setting.base_url + "/admin/subkota/" + this.subkotaLama[i]['id'], options)
                        .map((res: Response) => res.json())
                        .subscribe((response: any) => {
                            if (response['status'] == false) {
                                this.snackBar.open(response['message'], 'OK', {
                                    verticalPosition: 'top',
                                    duration        : 2000
                                });
                            }
                        })
                }
                for (var i = 0; i < this.subkota.length; ++i) {
                    var url = {
                        rute_id: this.route.snapshot.paramMap.get('id'),
                        kota_id: this.subkota[i]['id']
                    }
                    this.http.post(environment.setting.base_url + "/admin/subkota", url, options)
                        .map((res: Response) => res.json())
                        .subscribe((response: any) => {
                            if (response['status'] == false) {
                                this.snackBar.open(response['message'], 'OK', {
                                    verticalPosition: 'top',
                                    duration        : 2000
                                });
                            } 
                        })
                }

                // Change the location with new one
                if (response.status) {
                    this.back();
                    swal({
                        title: 'Update Rute Berhasil!',
                        text: 'Rute ' + data.asal + ' - ' + data.tujuan + ' Telah Update',
                        type: 'success',
                        timer: 2000
                    })
                } else {
                    swal({
                        title: 'Edit Rute Gagal!',
                        text: response.message,
                        type: 'error'
                    })
                }
            });
    }

    salinProduct()
    {
        this.pageType = "salin";
        this.productForm.controls.jenis.disable();
        this.productForm.controls.tujuan.disable();
        this.productForm.controls.asal.disable();
    }

    simpanSalin()
    {
        this.productForm.controls.jam.setValue(this.productForm.controls.tanggal.value.substring(11, 16));
        const data = this.productForm.getRawValue();
        this.productService.salinProduct(data)
            .then((response: any) => {

                if (response.status) {
                    this.back();
                    swal({
                        title: 'Salin Rute Berhasil!',
                        text: 'Rute ' + data.asal + ' - ' + data.tujuan + ' Telah Disalin',
                        type: 'success',
                        timer: 2000
                    })
                } else {
                    swal({
                        title: 'Salin Rute Gagal!',
                        text: response.message,
                        type: 'error',
                        timer: 2000
                    })
                }

            });
    }

    addProduct()
    {
        this.productForm.controls.jam.setValue(this.productForm.controls.tanggal.value.substring(11, 16));
        const data = this.productForm.getRawValue();
        this.productService.addProduct(data)
            .then((response: any) => {

                this.productService.onProductChanged.next(data);

                if (response.status) {
                    this.back();
                    swal({
                        title: 'Tambah Rute Berhasil!',
                        text: 'Rute ' + data.asal + ' - ' + data.tujuan + ' Telah Ditambahkan',
                        type: 'success',
                        timer: 2000
                    })
                } else {
                    swal({
                        title: 'Tambah Rute Gagal!',
                        text: response.message,
                        type: 'error'
                    })
                }
            });
    }

    changeStatus(){
        var st : any;
        if (this.status) {
            st = {
                "status" : 0
            }
        } else {
            st = {
                "status" : 1
            }
        }
        this.productService.changeStatus(st)
            .then((response: any) => {
                if (response.status) {
                    var pesan = this.status ? ' Siap Mudik' : 'Belum Siap Mudik';
                    swal({
                        title: 'Ubah Status Rute Berhasil!',
                        text: 'Rute ' + this.product.asal + ' - ' + this.product.tujuan + " " + pesan,
                        type: 'success',
                        timer: 2000
                    })
                    this.status ? this.status = false : this.status = true;
                } else {
                    swal({
                        title: 'Ubah Status Rute Gagal!',
                        text: response.message,
                        type: 'error'
                    })
                }
            });
    }

    ngOnDestroy()
    {
        this.onProductChanged.unsubscribe();
    }

    pushSubkota(id){
        var index = this.daftarKotaSubkota.findIndex(item => item.id === id);
        this.subkota.push(this.daftarKotaSubkota[index]);
        this.daftarKotaSubkota.splice(index, 1);
    }

    popSubkota(id){
        var index = this.subkota.findIndex(item => item.id === id);
        this.daftarKotaSubkota.push(this.subkota[index]);
        this.subkota.splice(index, 1);
    }

    hapus(): void{
        swal({
            title: 'Hapus Rute' + this.product.asal + " - " + this.product.tujuan,
            text: "Rute yang telah dihapus tidak bisa dikembalikan lagi",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus !',
            cancelButtonText: 'Batal!',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                this.productService.hapus(this.productForm.getRawValue())
                    .then((response: any) => {
                        if (response.status) {
                            this.back();
                            swal({
                                title: 'Hapus Rute Berhasil!',
                                text: 'Rute' + this.product.asal + ' - ' + this.product.tujuan + ' Telah Dihapus',
                                type: 'success',
                                timer: 2000
                            })
                        } else {
                            swal(
                                'Hapus Rute Gagal!',
                                response.message,
                                'error'
                            )
                        }
                    })
            }
        })
    }

    clickKendaraan(id) {
        this.router.navigate(['/apps/kendaraan/detailKendaraan/' + id]);
    }

    back(){
        this.router.navigate(['/apps/rute/daftarRute']);
    }
}
