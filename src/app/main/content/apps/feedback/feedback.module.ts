import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { DaftarFeedbackComponent } from './daftarFeedback/daftarFeedback.component';
import { DaftarFeedbackService } from './daftarFeedback/daftarFeedback.service';
import { DetailFeedbackComponent } from './detailFeedback/detailFeedback.component';
import { DetailFeedbackService } from './detailFeedback/detailFeedback.service';
import { FuseConfirmDialogComponent } from './../../../../core/components/confirm-dialog/confirm-dialog.component';
import { AgmCoreModule } from '@agm/core';
import { CdkTableModule } from '@angular/cdk/table';
import { DragDropSortableService, DndModule, DragDropService, DragDropConfig } from 'ng2-dnd';
import { AuthGuard } from './../auth.guard';

const routes: Routes = [
    {
        path     : 'daftarFeedback',
        component: DaftarFeedbackComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DaftarFeedbackService
        }
    },
    {
        path     : 'detailFeedback/:id',
        component: DetailFeedbackComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DetailFeedbackService
        }
    }
];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        CdkTableModule,
        DndModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    exports : [
        CdkTableModule
    ],
    entryComponents: [
        DaftarFeedbackComponent,
        FuseConfirmDialogComponent
    ],
    declarations: [
        DaftarFeedbackComponent,
        DetailFeedbackComponent
    ],
    providers   : [
        DaftarFeedbackService,
        DetailFeedbackService,
        DragDropSortableService, 
        DragDropService, 
        DragDropConfig
    ]
})
export class FeedbackModule
{
}
