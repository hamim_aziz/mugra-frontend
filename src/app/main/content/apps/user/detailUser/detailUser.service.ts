import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from './../../session.service';

@Injectable()
export class DetailUserService implements Resolve<any>
{
    routeParams: any;
    product: any;
    onProductChanged: BehaviorSubject<any> = new BehaviorSubject({});
    token = this._sessionService.get().token;

    constructor(
        private http: Http,
        private snackBar: MatSnackBar,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProduct(),
                this.getRute()
            ]).then(
                () => {
                    resolve();
                },
                reject
            ).catch(
                (error) => {
                    let eror = JSON.parse(error._body);
                    this.snackBar.open(eror.message, eror.errors.email ? eror.errors.email : eror.errors.password, {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                }
            );
        });
    }

    getRute(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url+'/admin/rute', this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    getProduct(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            if ( this.routeParams.id === 'new' )
            {
                this.onProductChanged.next(false);
                resolve(false);
            }
            else
            {
                this.http.get(environment.setting.base_url + '/admin/users/' + this.routeParams.id, this.setHeader())
                    .map((res: Response) => res.json())
                    .subscribe((response: any) => {
                        this.product = response.data;
                        this.onProductChanged.next(this.product);
                        resolve(response);
                    }, (error) => {
                        reject(error);
                    });
            }
        });
    }

    setHeader() {
        let options = new RequestOptions();
        let headers = new Headers();
        headers.append('Content-Type', 'application/json')
        headers.append('Authorization', `Bearer ${this._sessionService.get().token}`);
        options.headers = headers;
        return options;
    }

    saveProduct(product)
    {
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/users/' + this.routeParams.id, product, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    let obj = this._sessionService.get();
                    obj.email = product.name;
                    this._sessionService.set(obj);
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    addProduct(product)
    {
        return new Promise((resolve, reject) => {
            this.http.post(environment.setting.base_url + '/admin/users', product, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    hapus(product){
        let url = environment.setting.base_url + '/admin/users/' + product.id;
        return new Promise((resolve, reject) => {
            this.http.delete(url, this.setHeader())
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }
}
