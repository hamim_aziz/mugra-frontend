import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DaftarPemudikService implements Resolve<any>
{
    products: any[];
    token = sessionStorage.getItem('token');
    onProductsChanged: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(
        private http: Http
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProducts()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        options.headers = headers;
    }

    getProducts(): Promise<any>
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url+'/admin/pemudik', options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    this.products = response['data']['data'];
                    for (var i = 0; i < this.products.length; ++i) {
                        if (this.products[i]['jenis'] == 1) {
                            this.products[i]['jenis'] = 'Mudik';
                        } else {
                            this.products[i]['jenis'] = 'Balik';
                        }
                    }
                    this.onProductsChanged.next(this.products);
                    resolve(response);
                }, (err) => {
                    reject(err);
                });
        });
    }
}
