webpackJsonp(["rute.module"],{

/***/ "../../../../../src/app/main/content/apps/rute/daftarRute/daftarRute.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"products\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\"\r\n             fxLayout=\"column\" fxLayoutAlign=\"center center\"\r\n             fxLayout.gt-xs=\"row\" fxLayoutAlign.gt-xs=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div class=\"logo my-12 m-sm-0\"\r\n                 fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <mat-icon class=\"logo-icon mr-16\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'50ms',scale:'0.2'}}\">navigation</mat-icon>\r\n                <span class=\"logo-text h1\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">Daftar Rute</span>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <!-- SEARCH -->\r\n            <div class=\"search-input-wrapper mx-12 m-md-0\"\r\n                 fxFlex=\"1 0 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n                <label for=\"search\" class=\"mr-8\">\r\n                    <mat-icon class=\"secondary-text\">search</mat-icon>\r\n                </label>\r\n                <mat-form-field floatPlaceholder=\"never\" fxFlex=\"1 0 auto\">\r\n                    <input id=\"search\" matInput #filter placeholder=\"Search\">\r\n                </mat-form-field>\r\n            </div>\r\n            <!-- / SEARCH -->\r\n\r\n            <div class=\"my-12\">\r\n                <button mat-raised-button class=\"add-product-button m-sm-0\" [matMenuTriggerFor]=\"filter\" fxHide.xs>\r\n                    <span>Filter</span>\r\n                </button>\r\n\r\n                <button mat-raised-button class=\"add-product-button m-sm-0\" [matMenuTriggerFor]=\"option\" fxHide fxShow.xs>\r\n                    <span>Option</span>\r\n                </button>\r\n\r\n                <mat-menu #option=\"matMenu\">\r\n                    <button mat-menu-item [matMenuTriggerFor]=\"filter\">Filter</button>\r\n                    <button mat-menu-item [routerLink]=\"'/apps/rute/detailRute/new'\" [disabled]=\"role < 3\">Tambah Pemudik</button>\r\n                </mat-menu>\r\n\r\n                <mat-menu #filter=\"matMenu\">\r\n                    <button mat-menu-item  (click)=\"filterButton('')\">Semua</button>\r\n                    <button mat-menu-item [matMenuTriggerFor]=\"status\">Status</button>\r\n                    <button mat-menu-item [matMenuTriggerFor]=\"tanggal\">Tanggal</button>\r\n                    <button mat-menu-item [matMenuTriggerFor]=\"jenis\">Jenis Mudik</button>\r\n                    <button mat-menu-item  (click)=\"filterButton('Mudik Motor')\">Mudik Motor</button>\r\n                </mat-menu>\r\n\r\n                <mat-menu #status=\"matMenu\">\r\n                    <button (click)=\"filterButton('Siap Mudik')\" mat-menu-item>Siap Mudik</button>\r\n                    <button (click)=\"filterButton('Belum Siap')\" mat-menu-item>Belum Siap</button>\r\n                </mat-menu>\r\n\r\n                <mat-menu #jenis=\"matMenu\">\r\n                    <button (click)=\"filterButton('Mudik')\" mat-menu-item>Mudik</button>\r\n                    <button (click)=\"filterButton('Balik')\" mat-menu-item>Balik</button>\r\n                </mat-menu>\r\n\r\n                <mat-menu #tanggal=\"matMenu\" fusePerfectScrollbar>\r\n                    <button *ngFor=\"let tgl of dateIndo\" (click)=\"filterButton(tgl[0])\" mat-menu-item> {{ tgl[1] }} </button>\r\n                </mat-menu>\r\n            </div>\r\n\r\n            <div class=\"my-12\" fxHide.xs>\r\n\r\n                <button mat-raised-button\r\n                        [routerLink]=\"'/apps/rute/detailRute/new'\"\r\n                        class=\"add-product-button m-sm-0\"\r\n                        [disabled]=\"role < 3\">\r\n                    <span>Tambah Rute</span>\r\n                </button>\r\n            </div>\r\n\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <mat-table class=\"products-table\"\r\n                       #table [dataSource]=\"dataSource\"\r\n                       matSort\r\n                       [@animateStagger]=\"{value:'50'}\"\r\n                       fusePerfectScrollbar>\r\n\r\n                <!-- ID Column -->\r\n                <ng-container cdkColumnDef=\"id\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>No.</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product; let i = index\">\r\n                        <p class=\"text-truncate\">{{ (paginator.pageIndex * paginator.pageSize) + (i + 1) }}.</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Jenis Column -->\r\n                <ng-container cdkColumnDef=\"jenis\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Jenis</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <img *ngIf=\"product.jenis === 'Mudik' && product.is_mudik_motor == 0\" src=\"assets/images/avatars/Slice_4.png\" style=\"width: 35px; margin-right: 8px\">\r\n                        <img *ngIf=\"product.jenis === 'Balik' && product.is_mudik_motor == 0\" src=\"assets/images/avatars/Slice_5.png\" style=\"width: 35px; margin-right: 8px\">\r\n                        <img *ngIf=\"product.jenis === 'Mudik' && product.is_mudik_motor == 1\" src=\"assets/images/avatars/motorcycle.png\" style=\"width: 35px; margin-right: 8px\">\r\n                        <img *ngIf=\"product.jenis === 'Balik' && product.is_mudik_motor == 1\" src=\"assets/images/avatars/motorcycle-flip.png\" style=\"width: 35px; margin-right: 8px\">\r\n                        <p class=\"price text-truncate\">\r\n                            {{product.jenis}} {{product.is_mudik_motor == 1 ? 'Motor' : ''}}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Asal Column -->\r\n                <ng-container cdkColumnDef=\"asal\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Asal</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"text-truncate\" *ngIf=\"product.kota\">{{product.kota.asal}}</p>\r\n                        <p class=\"text-truncate\"></p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Tujuan Column -->\r\n                <ng-container cdkColumnDef=\"tujuan\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Tujuan</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"text-truncate\" *ngIf=\"product.kota\"> {{product.kota.tujuan}} </p>\r\n                        <p class=\"text-truncate\"></p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Tanggal Column -->\r\n                <ng-container cdkColumnDef=\"tanggal\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Tanggal</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"price text-truncate\">\r\n                            {{ product.tanggal.substring(8, 11) }} {{ bulan(product.tanggal) }} {{ product.tanggal.substring(0, 4) }}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Kuota Motor -->\r\n                <ng-container cdkColumnDef=\"kuota_motor\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxHide fxShow.gt-sm>Kuota Motor</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxHide fxShow.gt-sm>\r\n                        <p class=\"price text-truncate\">\r\n                            {{product.kouta_motor}}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Keterangan -->\r\n                <ng-container cdkColumnDef=\"keterangan\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Keterangan</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"price text-truncate\">\r\n                            {{product.keterangan}}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Status Column -->\r\n                <ng-container cdkColumnDef=\"status\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxHide fxShow.gt-xs>Status</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxHide fxShow.gt-xs>\r\n                        <p class=\"price text-truncate\">\r\n                            {{product.status}}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <mat-header-row *cdkHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n\r\n                <mat-row *cdkRowDef=\"let product; columns: displayedColumns;\"\r\n                         class=\"product\"\r\n                         matRipple\r\n                         (click)=\"clickRute(product.id, product.asal, product.tujuan)\">\r\n                </mat-row>\r\n\r\n            </mat-table>\r\n\r\n            <mat-paginator #paginator\r\n                           [length]=\"dataSource.filteredData.length\"\r\n                           [pageSize]=\"100\"\r\n                           [pageSizeOptions]=\"[5, 10, 25, 100]\">\r\n            </mat-paginator>\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/rute/daftarRute/daftarRute.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n:host .header .search-input-wrapper {\n  max-width: 480px; }\n:host .header .add-product-button {\n  background-color: #F96D01; }\n@media (max-width: 599px) {\n  :host .header {\n    height: 176px !important;\n    min-height: 176px !important;\n    max-height: 176px !important; } }\n@media (max-width: 599px) {\n  :host .top-bg {\n    height: 240px; } }\n:host .products-table {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  border-bottom: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-header-row {\n    min-height: 35px;\n    min-width: 500px;\n    background-color: rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-row {\n    min-width: 500px; }\n:host .products-table .product {\n    position: relative;\n    cursor: pointer;\n    height: 60px; }\n:host .products-table .mat-cell {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n:host .products-table .mat-column-id {\n    min-width: 0;\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 30px;\n            flex: 0 1 30px; }\n:host .products-table .mat-column-jenis, :host .products-table .mat-column-kuota_motor, :host .products-table .mat-column-status {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center !important;\n        -ms-flex-pack: center !important;\n            justify-content: center !important; }\n:host .products-table .mat-column-keterangan {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 20%;\n            flex: 0 1 20%; }\n:host .products-table .mat-column-image {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 84px;\n            flex: 0 1 84px; }\n:host .products-table .mat-column-image .product-image {\n      width: 52px;\n      height: 52px;\n      border: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-column-buttons {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 80px;\n            flex: 0 1 80px; }\n:host .products-table .quantity-indicator {\n    display: inline-block;\n    vertical-align: middle;\n    width: 8px;\n    height: 8px;\n    border-radius: 4px;\n    margin-right: 8px; }\n:host .products-table .quantity-indicator + span {\n      display: inline-block;\n      vertical-align: middle; }\n:host .products-table .active-icon {\n    border-radius: 50%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/rute/daftarRute/daftarRute.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarRuteComponent; });
/* unused harmony export FilesDataSource */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__daftarRute_service__ = __webpack_require__("../../../../../src/app/main/content/apps/rute/daftarRute/daftarRute.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_cdk_collections__ = __webpack_require__("../../../cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs_add_observable_fromEvent__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/fromEvent.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__core_fuseUtils__ = __webpack_require__("../../../../../src/app/core/fuseUtils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var DaftarRuteComponent = /** @class */ (function () {
    function DaftarRuteComponent(productsService, dialog, router, _sessionService) {
        this.productsService = productsService;
        this.dialog = dialog;
        this.router = router;
        this._sessionService = _sessionService;
        this.hapus = false;
        this.displayedColumns = ['id', 'jenis', 'asal', 'tujuan', 'tanggal', 'kuota_motor', 'keterangan', 'status'];
        this.role = this._sessionService.get().role;
        this.rutes = this.productsService.products;
        this.date = [];
        this.dateIndo = [];
    }
    DaftarRuteComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._tanggal();
        this.dataSource = new FilesDataSource(this.productsService, this.paginator, this.sort);
        __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].fromEvent(this.filter.nativeElement, 'keyup')
            .debounceTime(150)
            .distinctUntilChanged()
            .subscribe(function () {
            if (!_this.dataSource) {
                return;
            }
            _this.dataSource.filter = _this.filter.nativeElement.value;
        });
    };
    DaftarRuteComponent.prototype._tanggal = function () {
        for (var i = 0; i < this.rutes.length; ++i) {
            if (this.date.indexOf(this.rutes[i]['tanggal']) === -1) {
                this.dateIndo.push([this.rutes[i]['tanggal'], this.convertTanggal(this.rutes[i]['tanggal'])]);
                this.date.push(this.rutes[i]['tanggal']);
            }
        }
    };
    DaftarRuteComponent.prototype.convertTanggal = function (tgl) {
        var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
        var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
        var date = new Date(tgl);
        var day = date.getDay();
        var month = date.getMonth();
        return hari[day] + ', ' + date.getDate() + ' ' + bulan[month] + ' ' + date.getFullYear();
    };
    DaftarRuteComponent.prototype.cobaSwal = function () {
        __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()({
            title: 'Are you sure?',
            text: 'You won\'t be able to revert this!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()('Deleted!', 'Your file has been deleted.', 'success');
            }
            else if (
            // Read more about handling dismissals
            result.dismiss === __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default.a.DismissReason.cancel) {
                __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()('Cancelled', 'Your imaginary file is safe :)', 'error');
            }
        });
    };
    DaftarRuteComponent.prototype.filterButton = function (filter) {
        this.dataSource.filter = filter;
    };
    DaftarRuteComponent.prototype.semua = function () {
        this.dataSource.filter = '';
    };
    DaftarRuteComponent.prototype.toggleHapus = function () {
        this.hapus === true ? this.hapus = false : this.hapus = true;
    };
    DaftarRuteComponent.prototype.clickRute = function (id) {
        var url = '/apps/rute/detailRute/' + id;
        if (this.role >= 1) {
            this.router.navigate([url]);
        }
    };
    DaftarRuteComponent.prototype.bulan = function (date) {
        if (date.substring(5, 7) === '01') {
            return 'Januari';
        }
        else if (date.substring(5, 7) === '02') {
            return 'Februari';
        }
        else if (date.substring(5, 7) === '03') {
            return 'Maret';
        }
        else if (date.substring(5, 7) === '04') {
            return 'April';
        }
        else if (date.substring(5, 7) === '05') {
            return 'Mei';
        }
        else if (date.substring(5, 7) === '06') {
            return 'Juni';
        }
        else if (date.substring(5, 7) === '07') {
            return 'Juli';
        }
        else if (date.substring(5, 7) === '08') {
            return 'Agustus';
        }
        else if (date.substring(5, 7) === '09') {
            return 'September';
        }
        else if (date.substring(5, 7) === '10') {
            return 'Oktober';
        }
        else if (date.substring(5, 7) === '11') {
            return 'November';
        }
        else if (date.substring(5, 7) === '12') {
            return 'Desember';
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__angular_material__["z" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__angular_material__["z" /* MatPaginator */])
    ], DaftarRuteComponent.prototype, "paginator", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('filter'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], DaftarRuteComponent.prototype, "filter", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__angular_material__["M" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__angular_material__["M" /* MatSort */])
    ], DaftarRuteComponent.prototype, "sort", void 0);
    DaftarRuteComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-products',
            template: __webpack_require__("../../../../../src/app/main/content/apps/rute/daftarRute/daftarRute.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/rute/daftarRute/daftarRute.component.scss")],
            animations: __WEBPACK_IMPORTED_MODULE_4__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__daftarRute_service__["a" /* DaftarRuteService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_material__["m" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_7__angular_router__["f" /* Router */],
            __WEBPACK_IMPORTED_MODULE_16__session_service__["a" /* SessionService */]])
    ], DaftarRuteComponent);
    return DaftarRuteComponent;
}());

var FilesDataSource = /** @class */ (function (_super) {
    __extends(FilesDataSource, _super);
    function FilesDataSource(productsService, _paginator, _sort) {
        var _this = _super.call(this) || this;
        _this.productsService = productsService;
        _this._paginator = _paginator;
        _this._sort = _sort;
        _this._filterChange = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        _this._filteredDataChange = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        _this.filteredData = _this.productsService.products;
        return _this;
    }
    Object.defineProperty(FilesDataSource.prototype, "filteredData", {
        get: function () {
            return this._filteredDataChange.value;
        },
        set: function (value) {
            this._filteredDataChange.next(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FilesDataSource.prototype, "filter", {
        get: function () {
            return this._filterChange.value;
        },
        set: function (filter) {
            this._filterChange.next(filter);
        },
        enumerable: true,
        configurable: true
    });
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    FilesDataSource.prototype.connect = function () {
        var _this = this;
        var displayDataChanges = [
            this.productsService.onProductsChanged,
            this._paginator.page,
            this._filterChange,
            this._sort.sortChange
        ];
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].merge.apply(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"], displayDataChanges).map(function () {
            var data = _this.productsService.products.slice();
            data = _this.filterData(data);
            _this.filteredData = data.slice();
            data = _this.sortData(data);
            // Grab the page's slice of data.
            // if (!this.filter) {
            //     sessionStorage.setItem("pageIndexRute", String(this._paginator.pageIndex));
            // }
            var startIndex = _this._paginator.pageIndex * _this._paginator.pageSize;
            return data.splice(startIndex, _this._paginator.pageSize);
        });
    };
    FilesDataSource.prototype.filterData = function (data) {
        if (!this.filter) {
            return data;
        }
        return __WEBPACK_IMPORTED_MODULE_14__core_fuseUtils__["a" /* FuseUtils */].filterArrayByString(data, this.filter);
    };
    FilesDataSource.prototype.sortData = function (data) {
        var _this = this;
        if (!this._sort.active || this._sort.direction === '') {
            return data;
        }
        return data.sort(function (a, b) {
            var propertyA = '';
            var propertyB = '';
            switch (_this._sort.active) {
                case 'id':
                    _a = [a.id, b.id], propertyA = _a[0], propertyB = _a[1];
                    break;
                case 'asal':
                    _b = [a.asal, b.asal], propertyA = _b[0], propertyB = _b[1];
                    break;
                case 'tujuan':
                    _c = [a.tujuan, b.tujuan], propertyA = _c[0], propertyB = _c[1];
                    break;
                case 'tanggal':
                    _d = [a.tanggal, b.tanggal], propertyA = _d[0], propertyB = _d[1];
                    break;
                case 'jenis':
                    _e = [a.jenis, b.jenis], propertyA = _e[0], propertyB = _e[1];
                    break;
                case 'kuota_motor':
                    _f = [a.kouta_motor, b.kouta_motor], propertyA = _f[0], propertyB = _f[1];
                    break;
                case 'status':
                    _g = [a.status, b.status], propertyA = _g[0], propertyB = _g[1];
                    break;
            }
            var valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            var valueB = isNaN(+propertyB) ? propertyB : +propertyB;
            return (valueA < valueB ? -1 : 1) * (_this._sort.direction === 'asc' ? 1 : -1);
            var _a, _b, _c, _d, _e, _f, _g;
        });
    };
    FilesDataSource.prototype.disconnect = function () {
    };
    return FilesDataSource;
}(__WEBPACK_IMPORTED_MODULE_2__angular_cdk_collections__["a" /* DataSource */]));



/***/ }),

/***/ "../../../../../src/app/main/content/apps/rute/daftarRute/daftarRute.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarRuteService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DaftarRuteService = /** @class */ (function () {
    function DaftarRuteService(http, _sessionService) {
        this.http = http;
        this._sessionService = _sessionService;
        this.onProductsChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({});
        this.token = this._sessionService.get().token;
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DaftarRuteService.prototype.resolve = function (route, state) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getProducts()
            ]).then(function () {
                resolve();
            }, reject);
        });
    };
    DaftarRuteService.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        options.headers = headers;
    };
    DaftarRuteService.prototype.getProducts = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/rute', options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                _this.products = response['data'];
                for (var i = 0; i < _this.products.length; ++i) {
                    if (_this.products[i]['jenis'] === 1) {
                        _this.products[i]['jenis'] = 'Mudik';
                    }
                    else {
                        _this.products[i]['jenis'] = 'Balik';
                    }
                    if (_this.products[i]['is_mudik_motor'] === 1) {
                        _this.products[i]['is_mudik_motor'] = 'Mudik Motor';
                    }
                    else {
                        _this.products[i]['is_mudik_motor'] = 'Mudik Biasa';
                    }
                    if (_this.products[i]['status'] === 1) {
                        _this.products[i]['status'] = 'Siap Mudik';
                    }
                    else {
                        _this.products[i]['status'] = 'Belum Siap';
                    }
                    if (_this.products[i].kota) {
                        _this.products[i].asal = _this.products[i].kota.asal + ' - ' + _this.products[i].kota.tujuan;
                    }
                }
                _this.onProductsChanged.next(_this.products);
                resolve(response);
            }, reject);
        });
    };
    DaftarRuteService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__session_service__["a" /* SessionService */]])
    ], DaftarRuteService);
    return DaftarRuteService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/rute/detailRute/detailRute.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"product\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"start center\" style=\"width: 80%\">\r\n\r\n                <button class=\"mr-0 mr-sm-16\" mat-icon-button (click)=\"back()\">\r\n                    <mat-icon>arrow_back</mat-icon>\r\n                </button>\r\n\r\n                <div fxLayout=\"column\" fxLayoutAlign=\"start start\"\r\n                     *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">\r\n                    <div class=\"h2\" *ngIf=\"pageType ==='edit' || pageType === 'salin'\">\r\n                        {{product.asal}} - {{product.tujuan}}\r\n                    </div>\r\n                    <div class=\"h2\" *ngIf=\"pageType ==='new'\">\r\n                        Tambah Rute\r\n                    </div>\r\n                    <div class=\"subtitle secondary-text\">\r\n                        <span>Detail Rute</span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"productForm.invalid || role < 3\"\r\n                    *ngIf=\"pageType ==='new'\" (click)=\"addProduct()\" fxHide.xs>\r\n                <span>Simpan</span>\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"saveProduct()\" fxHide.xs>\r\n                <span>Simpan</span>\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"productForm.invalid || role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"salinProduct()\" fxHide.xs>\r\n                <span>Salin</span>\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"hapus()\" fxHide.xs>\r\n                <span>Hapus</span>\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"productForm.invalid || role < 3\"\r\n                    *ngIf=\"pageType ==='salin'\" (click)=\"simpanSalin()\" fxHide.xs>\r\n                <span>Simpan Salin</span>\r\n            </button>\r\n\r\n            <button mat-icon-button fxHide fxShow.xs [matMenuTriggerFor]=\"menu\">\r\n              <mat-icon>more_vert</mat-icon>\r\n            </button>\r\n            \r\n            <mat-menu #menu=\"matMenu\">\r\n              <button mat-menu-item [disabled]=\"productForm.invalid || role < 3\"\r\n                    *ngIf=\"pageType ==='new'\" (click)=\"addProduct()\">\r\n                <mat-icon>save_alt</mat-icon>\r\n                <span>Simpan</span>\r\n              </button>\r\n              <button mat-menu-item [disabled]=\"role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"saveProduct()\">\r\n                <mat-icon>save_alt</mat-icon>\r\n                <span>Simpan</span>\r\n              </button>\r\n              <button mat-menu-item [disabled]=\"productForm.invalid || role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"salinProduct()\">\r\n                <mat-icon>file_copy</mat-icon>\r\n                <span>Salin</span>\r\n              </button>\r\n              <button mat-menu-item [disabled]=\"role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"hapus()\">\r\n                <mat-icon>delete_forever</mat-icon>\r\n                <span>Hapus</span>\r\n              </button>\r\n              <button mat-menu-item [disabled]=\"productForm.invalid || role < 3\"\r\n                    *ngIf=\"pageType ==='salin'\" (click)=\"simpanSalin()\">\r\n                <mat-icon>save</mat-icon>\r\n                <span>Simpan Salin</span>\r\n              </button>\r\n            </mat-menu>\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <!-- CONTENT -->\r\n            <div class=\"content\">\r\n\r\n                <form name=\"productForm\" [formGroup]=\"productForm\" class=\"product w-100-p\" fxLayout=\"column\" fxFlex>\r\n\r\n                    <mat-tab-group>\r\n\r\n                        <mat-tab label=\"Info Rute\">\r\n                            <div class=\"tab-content p-24\" fusePerfectScrollbar>\r\n\r\n                                <mat-radio-group formControlName=\"jenis\"  \r\n                                                 required>\r\n                                    <mat-radio-button class=\"mr-8 mb-8\" [value]=\"1\"> Mudik </mat-radio-button>\r\n                                    <mat-radio-button class=\"mr-8 mb-8\" [value]=\"2\"> Balik </mat-radio-button>\r\n                                </mat-radio-group>\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                  <mat-select matInput \r\n                                              formControlName=\"asal\" \r\n                                              placeholder=\"Asal\" \r\n                                              required>\r\n                                    <mat-option *ngFor=\"let kota of kotaMaster\" [value]=\"kota.nama\"> {{kota.nama}} </mat-option>\r\n                                    <mat-error *ngIf=\"productForm.controls.asal.invalid\">\r\n                                      {{productForm.controls.asal.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                    </mat-error>\r\n                                  </mat-select>\r\n                                  <mat-icon matPrefix> place </mat-icon>\r\n                                </mat-form-field>\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                  <mat-select matInput \r\n                                              formControlName=\"tujuan\" \r\n                                              placeholder=\"Tujuan\" \r\n                                              required>\r\n                                    <mat-option *ngFor=\"let kota of kotaMaster\" [value]=\"kota.nama\"> {{kota.nama}} </mat-option>\r\n                                  </mat-select>\r\n                                  <mat-icon matPrefix> place </mat-icon>\r\n                                </mat-form-field>\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                    <input matInput\r\n                                           type=\"datetime-local\" \r\n                                           name=\"tanggal\"\r\n                                           formControlName=\"tanggal\"\r\n                                           placeholder=\"Tanggal\"\r\n                                           required>\r\n                                    <mat-error *ngIf=\"productForm.controls.tanggal.invalid\">\r\n                                      {{productForm.controls.tanggal.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                    </mat-error>\r\n                                    <mat-icon matPrefix> date_range </mat-icon>\r\n                                </mat-form-field>\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                    <input matInput\r\n                                           name=\"kouta_motor\"\r\n                                           formControlName=\"kouta_motor\"\r\n                                           placeholder=\"Kuota Motor\"\r\n                                           required>\r\n                                    <mat-icon matPrefix> motorcycle </mat-icon>\r\n                                </mat-form-field>\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                    <input matInput\r\n                                           name=\"keterangan\"\r\n                                           formControlName=\"keterangan\"\r\n                                           placeholder=\"Keterangan\"\r\n                                           required>\r\n                                    <mat-error *ngIf=\"productForm.controls.keterangan.invalid\">\r\n                                      {{productForm.controls.keterangan.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                    </mat-error>\r\n                                    <mat-icon matPrefix> description </mat-icon>\r\n                                </mat-form-field>\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                    <input matInput\r\n                                           name=\"contact_person\"\r\n                                           formControlName=\"contact_person\"\r\n                                           placeholder=\"Contact Person\"\r\n                                           rquired>\r\n                                    <mat-icon matPrefix> contact_phone </mat-icon>\r\n                                </mat-form-field>\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                    <input matInput\r\n                                           name=\"tempat_berangkat\"\r\n                                           formControlName=\"tempat_berangkat\"\r\n                                           placeholder=\"Tempat Berangkat\"\r\n                                           required>\r\n                                    <mat-icon matPrefix> store_mall_directory </mat-icon>\r\n                                </mat-form-field>\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                    <input matInput\r\n                                           name=\"tempat_ambil_tiket\"\r\n                                           formControlName=\"tempat_ambil_tiket\"\r\n                                           placeholder=\"Tempat Ambil Tiket\"\r\n                                           required>\r\n                                    <mat-icon matPrefix> local_play </mat-icon>\r\n                                </mat-form-field>\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                    <input matInput\r\n                                           name=\"kode\"\r\n                                           formControlName=\"kode\"\r\n                                           placeholder=\"Kode\"\r\n                                           type=\"number\" \r\n                                           required>\r\n                                    <mat-icon matPrefix> local_play </mat-icon>\r\n                                </mat-form-field>\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                    <mat-label>Titip Motor</mat-label>\r\n                                    <mat-select matInput formControlName=\"is_titip_motor\" placeholder=\"Titip Motor\"\r\n                                                required>\r\n                                        <mat-option [value]=\"1\"> Ya </mat-option>\r\n                                        <mat-option [value]=\"0\"> Tidak </mat-option>\r\n                                    </mat-select>\r\n                                    <mat-icon matPrefix> motorcycle </mat-icon>\r\n                                </mat-form-field>\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                    <mat-label>Titip Motor Balik</mat-label>\r\n                                    <mat-select matInput formControlName=\"is_titip_motor_balik\" placeholder=\"Titip Motor Balik\" required>\r\n                                        <mat-option [value]=\"1\"> Ya </mat-option>\r\n                                        <mat-option [value]=\"0\"> Tidak </mat-option>\r\n                                    </mat-select>\r\n                                    <mat-icon matPrefix> motorcycle </mat-icon>\r\n                                </mat-form-field>\r\n\r\n                                <button mat-raised-button\r\n                                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                                    *ngIf=\"status && pageType !=='new' && pageType !=='salin'\"\r\n                                    (click)=\"changeStatus()\">\r\n                                  Non Aktifkan\r\n                                </button>\r\n\r\n                                <button mat-raised-button\r\n                                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                                    *ngIf=\"!status && pageType !=='new' && pageType !=='salin'\"\r\n                                    (click)=\"changeStatus()\">\r\n                                  Aktifkan\r\n                                </button>\r\n                            </div>\r\n                        </mat-tab>\r\n\r\n                        <mat-tab label=\"Kendaraan\" *ngIf=\"pageType !== 'new' && pageType !== 'salin'\">\r\n                            <div class=\"tab-content p-24\" fusePerfectScrollbar>\r\n                              <table class=\"table-rute\" style=\"width: 100%\">\r\n                                <tr>\r\n                                  <th class=\"th-rute\">No.</th>\r\n                                  <th class=\"th-rute\">Nomor Kendaraan</th>\r\n                                  <th class=\"th-rute\">Jenis</th>\r\n                                  <th class=\"th-rute\">Status</th>\r\n                                </tr>\r\n                                <tr class=\"tr\" *ngFor=\"let kendaraan of product.kendaraans; let i = index\" (click)=\"clickKendaraan(kendaraan.id)\">\r\n                                  <td class=\"td-rute\">{{i+1}}.</td>\r\n                                  <td class=\"td-rute\">{{kendaraan.nomor}}</td>\r\n                                  <td class=\"td-rute\">{{kendaraan.jenis == 1 ? 'Bis':'Kereta Api'}}</td>\r\n                                  <td class=\"td-rute\">{{kendaraan.status == 1 ? 'Aktif':'Tidak Aktif'}}</td>\r\n                                </tr>\r\n                              </table>\r\n                            </div>\r\n                        </mat-tab>\r\n\r\n                        <mat-tab label=\"Subkota\" *ngIf=\"pageType !== 'new' && pageType !== 'salin'\">\r\n                            <div class=\"tab-content p-24\" fusePerfectScrollbar>\r\n                                <mat-card>\r\n                                    <mat-card-title> Subkota </mat-card-title>\r\n                                    <mat-divider></mat-divider>\r\n                                    <mat-card-content>\r\n                                        <mat-list dnd-sortable-container [dropZones]=\"['boxers-zone']\" [sortableData]=\"subkota\">\r\n                                            <mat-list-item *ngFor=\"let item of subkota; let i = index; last as last\" dnd-sortable [sortableIndex]=\"i\">\r\n                                                <h4 mat-line>{{item.nama}}</h4>\r\n                                                <button mat-icon-button color=\"primary\" align=\"end\" (click)=\"popSubkota(item.id)\">\r\n                                                    <mat-icon>remove_circle</mat-icon>\r\n                                                </button>\r\n                                                <mat-divider [inset]=\"true\" *ngIf=\"!last\"></mat-divider>\r\n                                            </mat-list-item>\r\n                                        </mat-list>\r\n                                    </mat-card-content>\r\n                                </mat-card>\r\n                                <br><br>\r\n                                <mat-card>\r\n                                    <mat-card-title> Daftar Kota </mat-card-title>\r\n                                    <mat-divider></mat-divider>\r\n                                    <mat-card-content>\r\n                                        <div class=\"panel-body\" dnd-sortable-container [dropZones]=\"['boxers-zone']\" [sortableData]=\"daftarKotaSubkota\">\r\n                                            <mat-grid-list cols=\"5\" rowHeight=\"4:1\" class=\"list-group\" >\r\n                                                <mat-grid-tile *ngFor=\"let item of daftarKotaSubkota; let i = index\" class=\"list-kota\" dnd-sortable [sortableIndex]=\"i\">\r\n                                                    <button mat-icon-button color=\"primary\" (click)=\"pushSubkota(item.id)\">\r\n                                                        <mat-icon>add_circle</mat-icon>\r\n                                                    </button>\r\n                                                    <p>{{item.nama}}</p> \r\n                                                </mat-grid-tile>\r\n                                            </mat-grid-list>\r\n                                        </div>\r\n                                    </mat-card-content>\r\n                                </mat-card>\r\n                            </div>\r\n                        </mat-tab>\r\n\r\n                    </mat-tab-group>\r\n                </form>\r\n\r\n            </div>\r\n            <!-- / CONTENT -->\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/rute/detailRute/detailRute.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#product .header .product-image {\n  overflow: hidden;\n  width: 56px;\n  height: 56px;\n  border: 3px solid rgba(0, 0, 0, 0.12); }\n  #product .header .product-image img {\n    height: 100%;\n    width: auto;\n    max-width: none; }\n  #product .header .save-product-button {\n  background-color: #F96D01;\n  margin: 0px 20px; }\n  #product .header .subtitle {\n  margin: 6px 0 0 0; }\n  #product .content .table-rute, #product .content .th-rute, #product .content .td-rute {\n  border: 1px solid rgba(0, 0, 0, 0.12) !important;\n  border-collapse: collapse !important; }\n  #product .content .th-rute, #product .content .td-rute {\n  padding: 10px !important;\n  text-align: center !important;\n  font-weight: normal !important;\n  font-size: 12px !important; }\n  #product .content .th-rute {\n  background-color: rgba(0, 0, 0, 0.12); }\n  #product .content .tr:hover {\n  background-color: rgba(0, 0, 0, 0.03); }\n  #product .content .example-container {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  max-height: 500px;\n  min-width: 300px; }\n  #product .content .mat-table {\n  overflow: auto;\n  max-height: 500px; }\n  #product .content .w-100-p {\n  margin-bottom: 8px; }\n  #product .content .w-90-p {\n  margin-bottom: 8px; }\n  #product .content .mat-form-field-prefix .mat-icon, #product .content .mat-form-field-suffix .mat-icon {\n  margin-right: 10px; }\n  #product .content .mat-tab-group,\n#product .content .mat-tab-body-wrapper,\n#product .content .tab-content {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  max-width: 100%; }\n  #product .content .mat-tab-body-content {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n  #product .content .mat-tab-header {\n  height: 40px;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n  #product .content .mat-tab-label {\n  height: 64px; }\n  #product .content .product-image {\n  overflow: hidden;\n  width: 128px;\n  height: 128px;\n  margin-right: 16px;\n  margin-bottom: 16px;\n  border: 3px solid rgba(0, 0, 0, 0.12); }\n  #product .content .product-image img {\n    height: 100%;\n    width: auto;\n    max-width: none; }\n  #product .content .mat-card .mat-divider.mat-divider-inset {\n  position: absolute !important; }\n  #product .content .mat-list .mat-list-item .mat-divider.mat-divider-inset {\n  left: 0 !important;\n  width: 100% !important; }\n  #product .content .mat-grid-tile .mat-figure {\n  -webkit-box-pack: left !important;\n      -ms-flex-pack: left !important;\n          justify-content: left !important; }\n  #product .content .mat-list .mat-list-item {\n  height: unset !important; }\n  #product .content .mat-list .mat-list-item {\n  font-size: 13px !important; }\n  #product .content .mat-card-title {\n  font-size: 15px !important; }\n  #product .content .mat-card-actions, #product .content .mat-card-content, #product .content .mat-card-subtitle, #product .content .mat-card-title {\n  padding-bottom: 10px !important; }\n  #product .content .mat-card {\n  padding: 10px !important; }\n  #product .content .mat-card-subtitle, #product .content .mat-card-content, #product .content .mat-card-header .mat-card-title {\n  font-size: 12px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/rute/detailRute/detailRute.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailRuteComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__detailRute_service__ = __webpack_require__("../../../../../src/app/main/content/apps/rute/detailRute/detailRute.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_observable_fromEvent__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/fromEvent.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__detailRute_model__ = __webpack_require__("../../../../../src/app/main/content/apps/rute/detailRute/detailRute.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



















var DetailRuteComponent = /** @class */ (function () {
    function DetailRuteComponent(productService, formBuilder, snackBar, location, http, route, dialog, router, _sessionService) {
        this.productService = productService;
        this.formBuilder = formBuilder;
        this.snackBar = snackBar;
        this.location = location;
        this.http = http;
        this.route = route;
        this.dialog = dialog;
        this.router = router;
        this._sessionService = _sessionService;
        this.product = new __WEBPACK_IMPORTED_MODULE_9__detailRute_model__["a" /* Product */]();
        this.subkotaLama = [];
        this.subkota = [];
        this.daftarKotaSubkota = [];
        this.token = this._sessionService.get().token;
        this.role = this._sessionService.get().role;
    }
    DetailRuteComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Subscribe to update product on changes
        this.onProductChanged =
            this.productService.onProductChanged
                .subscribe(function (product) {
                if (product) {
                    _this.product = new __WEBPACK_IMPORTED_MODULE_9__detailRute_model__["a" /* Product */](product);
                    _this.pageType = 'edit';
                    _this.status = _this.product.status == '1' ? true : false;
                }
                else {
                    _this.pageType = 'new';
                    _this.product = new __WEBPACK_IMPORTED_MODULE_9__detailRute_model__["a" /* Product */]();
                }
                _this.productForm = _this.createProductForm();
            });
        this.getKota();
        this.getDaftarKota();
    };
    DetailRuteComponent.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_15__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        options.headers = headers;
    };
    DetailRuteComponent.prototype.getDaftarKota = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_15__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        this.http.get(__WEBPACK_IMPORTED_MODULE_13__environments_environment__["a" /* environment */].setting.base_url + "/admin/kota/master", options)
            .map(function (res) { return res.json(); })
            .subscribe(function (response) {
            _this.daftarKota = response['data'];
            if (_this.pageType !== "new") {
                _this.getSubkota();
            }
        });
    };
    DetailRuteComponent.prototype.getKota = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_15__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        this.http.get(__WEBPACK_IMPORTED_MODULE_13__environments_environment__["a" /* environment */].setting.base_url + "/admin/kota/master", options)
            .map(function (res) { return res.json(); })
            .subscribe(function (response) {
            _this.kotaMaster = response['data'];
        });
    };
    DetailRuteComponent.prototype.getSubkota = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_15__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        this.http.get(__WEBPACK_IMPORTED_MODULE_13__environments_environment__["a" /* environment */].setting.base_url + "/admin/rute/subkota/" + this.route.snapshot.paramMap.get('id'), options)
            .map(function (res) { return res.json(); })
            .subscribe(function (response) {
            _this.subkotaLama = response['data'];
            if (response['data'].length == 0) {
                for (var i = 0; i < _this.daftarKota.length; ++i) {
                    _this.daftarKotaSubkota.push(_this.daftarKota[i]);
                }
            }
            else {
                for (var i = 0; i < _this.daftarKota.length; ++i) {
                    for (var j = 0; j < response['data'].length; ++j) {
                        if (_this.daftarKota[i]['nama'] == response['data'][j]['nama']) {
                            _this.subkota.push(_this.daftarKota[i]);
                            break;
                        }
                        else {
                            if (j == response['data'].length - 1) {
                                _this.daftarKotaSubkota.push(_this.daftarKota[i]);
                            }
                        }
                    }
                }
            }
        });
    };
    DetailRuteComponent.prototype.createProductForm = function () {
        return this.formBuilder.group({
            jenis: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.jenis, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            keterangan: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.keterangan, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            tanggal: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.tanggal + "T" + this.product.jam, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            jam: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.jam),
            asal: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.asal, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            tujuan: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.tujuan, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            kouta_motor: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.kouta_motor, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            contact_person: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.contact_person, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            tempat_berangkat: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.tempat_berangkat, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            tempat_ambil_tiket: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.tempat_ambil_tiket, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            status: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.status),
            kendaraans: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.kendaraans),
            subkotas: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.subkotas),
            is_titip_motor: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.is_titip_motor, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            is_titip_motor_balik: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.is_titip_motor_balik, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            kode: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.kode, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required])
        });
    };
    DetailRuteComponent.prototype.saveProduct = function () {
        var _this = this;
        this.productForm.controls.jam.setValue(this.productForm.controls.tanggal.value.substring(11, 16));
        var options = new __WEBPACK_IMPORTED_MODULE_15__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        var data = this.productForm.getRawValue();
        this.productService.saveProduct(data)
            .then(function (response) {
            // Trigger the subscription with new data
            _this.productService.onProductChanged.next(data);
            for (var i = 0; i < _this.subkotaLama.length; ++i) {
                _this.http.delete(__WEBPACK_IMPORTED_MODULE_13__environments_environment__["a" /* environment */].setting.base_url + "/admin/subkota/" + _this.subkotaLama[i]['id'], options)
                    .map(function (res) { return res.json(); })
                    .subscribe(function (response) {
                    if (response['status'] == false) {
                        _this.snackBar.open(response['message'], 'OK', {
                            verticalPosition: 'top',
                            duration: 2000
                        });
                    }
                });
            }
            for (var i = 0; i < _this.subkota.length; ++i) {
                var url = {
                    rute_id: _this.route.snapshot.paramMap.get('id'),
                    kota_id: _this.subkota[i]['id']
                };
                _this.http.post(__WEBPACK_IMPORTED_MODULE_13__environments_environment__["a" /* environment */].setting.base_url + "/admin/subkota", url, options)
                    .map(function (res) { return res.json(); })
                    .subscribe(function (response) {
                    if (response['status'] == false) {
                        _this.snackBar.open(response['message'], 'OK', {
                            verticalPosition: 'top',
                            duration: 2000
                        });
                    }
                });
            }
            // Change the location with new one
            if (response.status) {
                _this.back();
                __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                    title: 'Update Rute Berhasil!',
                    text: 'Rute ' + data.asal + ' - ' + data.tujuan + ' Telah Update',
                    type: 'success',
                    timer: 2000
                });
            }
            else {
                __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                    title: 'Edit Rute Gagal!',
                    text: response.message,
                    type: 'error'
                });
            }
        });
    };
    DetailRuteComponent.prototype.salinProduct = function () {
        this.pageType = "salin";
        this.productForm.controls.jenis.disable();
        this.productForm.controls.tujuan.disable();
        this.productForm.controls.asal.disable();
    };
    DetailRuteComponent.prototype.simpanSalin = function () {
        var _this = this;
        this.productForm.controls.jam.setValue(this.productForm.controls.tanggal.value.substring(11, 16));
        var data = this.productForm.getRawValue();
        this.productService.salinProduct(data)
            .then(function (response) {
            if (response.status) {
                _this.back();
                __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                    title: 'Salin Rute Berhasil!',
                    text: 'Rute ' + data.asal + ' - ' + data.tujuan + ' Telah Disalin',
                    type: 'success',
                    timer: 2000
                });
            }
            else {
                __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                    title: 'Salin Rute Gagal!',
                    text: response.message,
                    type: 'error',
                    timer: 2000
                });
            }
        });
    };
    DetailRuteComponent.prototype.addProduct = function () {
        var _this = this;
        this.productForm.controls.jam.setValue(this.productForm.controls.tanggal.value.substring(11, 16));
        var data = this.productForm.getRawValue();
        this.productService.addProduct(data)
            .then(function (response) {
            _this.productService.onProductChanged.next(data);
            if (response.status) {
                _this.back();
                __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                    title: 'Tambah Rute Berhasil!',
                    text: 'Rute ' + data.asal + ' - ' + data.tujuan + ' Telah Ditambahkan',
                    type: 'success',
                    timer: 2000
                });
            }
            else {
                __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                    title: 'Tambah Rute Gagal!',
                    text: response.message,
                    type: 'error'
                });
            }
        });
    };
    DetailRuteComponent.prototype.changeStatus = function () {
        var _this = this;
        var st;
        if (this.status) {
            st = {
                "status": 0
            };
        }
        else {
            st = {
                "status": 1
            };
        }
        this.productService.changeStatus(st)
            .then(function (response) {
            if (response.status) {
                var pesan = _this.status ? ' Siap Mudik' : 'Belum Siap Mudik';
                __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                    title: 'Ubah Status Rute Berhasil!',
                    text: 'Rute ' + _this.product.asal + ' - ' + _this.product.tujuan + " " + pesan,
                    type: 'success',
                    timer: 2000
                });
                _this.status ? _this.status = false : _this.status = true;
            }
            else {
                __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                    title: 'Ubah Status Rute Gagal!',
                    text: response.message,
                    type: 'error'
                });
            }
        });
    };
    DetailRuteComponent.prototype.ngOnDestroy = function () {
        this.onProductChanged.unsubscribe();
    };
    DetailRuteComponent.prototype.pushSubkota = function (id) {
        var index = this.daftarKotaSubkota.findIndex(function (item) { return item.id === id; });
        this.subkota.push(this.daftarKotaSubkota[index]);
        this.daftarKotaSubkota.splice(index, 1);
    };
    DetailRuteComponent.prototype.popSubkota = function (id) {
        var index = this.subkota.findIndex(function (item) { return item.id === id; });
        this.daftarKotaSubkota.push(this.subkota[index]);
        this.subkota.splice(index, 1);
    };
    DetailRuteComponent.prototype.hapus = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
            title: 'Hapus Rute' + this.product.asal + " - " + this.product.tujuan,
            text: "Rute yang telah dihapus tidak bisa dikembalikan lagi",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus !',
            cancelButtonText: 'Batal!',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                _this.productService.hapus(_this.productForm.getRawValue())
                    .then(function (response) {
                    if (response.status) {
                        _this.back();
                        __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                            title: 'Hapus Rute Berhasil!',
                            text: 'Rute' + _this.product.asal + ' - ' + _this.product.tujuan + ' Telah Dihapus',
                            type: 'success',
                            timer: 2000
                        });
                    }
                    else {
                        __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()('Hapus Rute Gagal!', response.message, 'error');
                    }
                });
            }
        });
    };
    DetailRuteComponent.prototype.clickKendaraan = function (id) {
        this.router.navigate(['/apps/kendaraan/detailKendaraan/' + id]);
    };
    DetailRuteComponent.prototype.back = function () {
        this.router.navigate(['/apps/rute/daftarRute']);
    };
    DetailRuteComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-product',
            template: __webpack_require__("../../../../../src/app/main/content/apps/rute/detailRute/detailRute.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/rute/detailRute/detailRute.component.scss")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
            animations: __WEBPACK_IMPORTED_MODULE_2__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__detailRute_service__["a" /* DetailRuteService */],
            __WEBPACK_IMPORTED_MODULE_10__angular_forms__["c" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_11__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_12__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_15__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_14__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_11__angular_material__["m" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_14__angular_router__["f" /* Router */],
            __WEBPACK_IMPORTED_MODULE_17__session_service__["a" /* SessionService */]])
    ], DetailRuteComponent);
    return DetailRuteComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/rute/detailRute/detailRute.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Product; });
var Product = /** @class */ (function () {
    function Product(product) {
        if (product) {
            product = product;
            this.id = product.id;
            this.jenis = product.jenis;
            this.keterangan = product.keterangan;
            this.tanggal = product.tanggal;
            this.jam = product.jam;
            if (product.asal) {
                this.asal = product.asal;
                this.tujuan = product.tujuan;
            }
            else {
                this.asal = product.kota.asal;
                this.tujuan = product.kota.tujuan;
            }
            this.status = product.status;
            this.kouta_motor = product.kouta_motor;
            this.contact_person = product.contact_person;
            this.tempat_berangkat = product.tempat_berangkat;
            this.tempat_ambil_tiket = product.tempat_ambil_tiket;
            this.kendaraans = product.kendaraans;
            this.subkotas = product.subkotas;
            this.is_titip_motor = product.is_titip_motor;
            this.is_titip_motor_balik = product.is_titip_motor_balik;
            this.kode = product.kode;
        }
        else {
            product = {};
            this.id = 0;
            this.jenis = "1";
            this.keterangan = '';
            this.tanggal = '';
            this.jam = '';
            this.asal = '';
            this.tujuan = '';
            this.kouta_motor = 0;
            this.is_titip_motor = 0;
            this.is_titip_motor_balik = 0;
            this.contact_person = '031-8294459';
            this.tempat_berangkat = 'Dinas Perhubungan Provinsi Jawa Timur, Jl Ahmad Yani 268 Surabaya';
            this.tempat_ambil_tiket = 'Dishub Jatim Jl A Yani 268 Sby';
            this.status = '';
            this.kendaraans = [];
            this.subkotas = [];
            this.kode = 0;
        }
    }
    return Product;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/rute/detailRute/detailRute.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailRuteService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var DetailRuteService = /** @class */ (function () {
    function DetailRuteService(http, snackBar, _sessionService) {
        this.http = http;
        this.snackBar = snackBar;
        this._sessionService = _sessionService;
        this.onProductChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({});
        this.token = this._sessionService.get().token;
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DetailRuteService.prototype.resolve = function (route, state) {
        var _this = this;
        this.routeParams = route.params;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getProduct()
            ]).then(function () {
                resolve();
            }, reject);
        });
    };
    DetailRuteService.prototype.getProduct = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            if (_this.routeParams.id === 'new') {
                _this.onProductChanged.next(false);
                resolve(false);
            }
            else {
                _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/rute/' + _this.routeParams.id, options)
                    .map(function (res) { return res.json(); })
                    .subscribe(function (response) {
                    _this.product = response['data'];
                    _this.onProductChanged.next(_this.product);
                    resolve(response);
                }, function (err) {
                    var eror = JSON.parse(err._body);
                    _this.snackBar.open(eror.message, 'Error !', {
                        verticalPosition: 'top',
                        duration: 5000
                    });
                    reject(err);
                });
            }
        });
    };
    DetailRuteService.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        options.headers = headers;
    };
    DetailRuteService.prototype.saveProduct = function (product) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.put(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/rute/' + _this.routeParams.id, product, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (err) {
                var eror = JSON.parse(err._body);
                __WEBPACK_IMPORTED_MODULE_6_sweetalert2___default()({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
                reject(err);
            });
        });
    };
    DetailRuteService.prototype.salinProduct = function (product) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.put(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/rute/copy/' + _this.routeParams.id, product, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (err) {
                var eror = JSON.parse(err._body);
                __WEBPACK_IMPORTED_MODULE_6_sweetalert2___default()({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
                reject(err);
            });
        });
    };
    DetailRuteService.prototype.addProduct = function (product) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/rute/', product, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (err) {
                var eror = JSON.parse(err._body);
                __WEBPACK_IMPORTED_MODULE_6_sweetalert2___default()({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
                reject(err);
            });
        });
    };
    DetailRuteService.prototype.hapus = function (product) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.delete(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/rute/' + _this.routeParams.id, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (err) {
                var eror = JSON.parse(err._body);
                __WEBPACK_IMPORTED_MODULE_6_sweetalert2___default()({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
                reject(err);
            });
        });
    };
    DetailRuteService.prototype.changeStatus = function (st) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.put(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + "/admin/rute/status/" + _this.product.id, st, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (err) {
                var eror = JSON.parse(err._body);
                __WEBPACK_IMPORTED_MODULE_6_sweetalert2___default()({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
                reject(err);
            });
        });
    };
    DetailRuteService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_7__session_service__["a" /* SessionService */]])
    ], DetailRuteService);
    return DetailRuteService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/rute/rute.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RuteModule", function() { return RuteModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__ = __webpack_require__("../../../../@swimlane/ngx-charts/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__ = __webpack_require__("../../../../../src/app/core/modules/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__ = __webpack_require__("../../../../../src/app/core/components/widget/widget.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__daftarRute_daftarRute_component__ = __webpack_require__("../../../../../src/app/main/content/apps/rute/daftarRute/daftarRute.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__daftarRute_daftarRute_service__ = __webpack_require__("../../../../../src/app/main/content/apps/rute/daftarRute/daftarRute.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__detailRute_detailRute_component__ = __webpack_require__("../../../../../src/app/main/content/apps/rute/detailRute/detailRute.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__detailRute_detailRute_service__ = __webpack_require__("../../../../../src/app/main/content/apps/rute/detailRute/detailRute.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__core_components_confirm_dialog_confirm_dialog_component__ = __webpack_require__("../../../../../src/app/core/components/confirm-dialog/confirm-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__ = __webpack_require__("../../../cdk/esm5/table.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__ = __webpack_require__("../../../../ng2-dnd/ng2-dnd.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__auth_guard__ = __webpack_require__("../../../../../src/app/main/content/apps/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_material_table__ = __webpack_require__("../../../material/esm5/table.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var routes = [
    {
        path: 'daftarRute',
        component: __WEBPACK_IMPORTED_MODULE_5__daftarRute_daftarRute_component__["a" /* DaftarRuteComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_13__auth_guard__["a" /* AuthGuard */]],
        resolve: {
            data: __WEBPACK_IMPORTED_MODULE_6__daftarRute_daftarRute_service__["a" /* DaftarRuteService */]
        }
    },
    {
        path: 'detailRute/:id',
        component: __WEBPACK_IMPORTED_MODULE_7__detailRute_detailRute_component__["a" /* DetailRuteComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_13__auth_guard__["a" /* AuthGuard */]],
        resolve: {
            data: __WEBPACK_IMPORTED_MODULE_8__detailRute_detailRute_service__["a" /* DetailRuteService */]
        }
    }
];
var RuteModule = /** @class */ (function () {
    function RuteModule() {
    }
    RuteModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__["a" /* FuseWidgetModule */],
                __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__["NgxChartsModule"],
                __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__["m" /* CdkTableModule */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["a" /* DndModule */],
                __WEBPACK_IMPORTED_MODULE_14__angular_material_table__["b" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_10__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
                })
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__["m" /* CdkTableModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__daftarRute_daftarRute_component__["a" /* DaftarRuteComponent */],
                __WEBPACK_IMPORTED_MODULE_9__core_components_confirm_dialog_confirm_dialog_component__["a" /* FuseConfirmDialogComponent */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__daftarRute_daftarRute_component__["a" /* DaftarRuteComponent */],
                __WEBPACK_IMPORTED_MODULE_7__detailRute_detailRute_component__["a" /* DetailRuteComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__daftarRute_daftarRute_service__["a" /* DaftarRuteService */],
                __WEBPACK_IMPORTED_MODULE_8__detailRute_detailRute_service__["a" /* DetailRuteService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["d" /* DragDropSortableService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["c" /* DragDropService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["b" /* DragDropConfig */]
            ]
        })
    ], RuteModule);
    return RuteModule;
}());



/***/ })

});
//# sourceMappingURL=rute.module.chunk.js.map