import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { SessionService } from './../../../../../main/content/apps/session.service';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';

@Component({
    selector   : 'fuse-nav-vertical-item',
    templateUrl: './nav-vertical-item.component.html',
    styleUrls  : ['./nav-vertical-item.component.scss']
})
export class FuseNavVerticalItemComponent implements OnInit
{
    @HostBinding('class') classes = 'nav-item';
    @Input() item: any;

    constructor(private router: Router, private _sessionService: SessionService, private _http: Http)
    {
    }

    ngOnInit()
    {
    }

    logout(){
        this._http.post(environment.setting.base_url + '/logout', {}, this.setHeader())
            .subscribe(() => {
                // console.log('coba');
                this._sessionService.clear();
                this.router.navigate(['/apps/login']);
            })
    }

    setHeader() {
        let options = new RequestOptions();
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this._sessionService.get().token}`);
        options.headers = headers;
        return options;
    }
}
