import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from './../../session.service';

@Injectable()
export class DaftarSmsService implements Resolve<any>
{
    smss: any[];
    onSmssChanged: BehaviorSubject<any> = new BehaviorSubject({});
    token = this._sessionService.get().token;

    constructor(
        private http: Http,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getSmss()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }

    getSmss(): Promise<any>
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url+'/admin/sms', options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    this.smss = response['data'];
                    this.onSmssChanged.next(this.smss);
                    resolve(response);
                }, reject);
        });
    }

    kirimSms(pesan): Promise<any>
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.post(environment.setting.base_url+'/admin/kirim/pesan', pesan, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    updateSms(sms): Promise<any>
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/sms/' + sms.id, sms, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response)
                }, (error) => {
                    reject(error);
                })
        });
    }
}
