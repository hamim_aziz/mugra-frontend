import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { MatSnackBar } from '@angular/material';
import swal from 'sweetalert2';
import { SessionService } from './../../session.service';

@Injectable()
export class DetailRuteService implements Resolve<any>
{
    routeParams: any;
    product: any;
    onProductChanged: BehaviorSubject<any> = new BehaviorSubject({});
    token = this._sessionService.get().token;

    constructor(
        private http: Http,
        private snackBar: MatSnackBar,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProduct()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getProduct(): Promise<any>
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            if ( this.routeParams.id === 'new' )
            {
                this.onProductChanged.next(false);
                resolve(false);
            }
            else
            {
                this.http.get(environment.setting.base_url + '/admin/rute/' + this.routeParams.id, options)
                    .map((res: Response) => res.json())
                    .subscribe((response: any) => {
                        this.product = response['data'];
                        this.onProductChanged.next(this.product);
                        resolve(response);
                    }, (err) => {
                        let eror = JSON.parse(err._body);
                        this.snackBar.open(eror.message, 'Error !', {
                            verticalPosition: 'top',
                            duration        : 5000
                        });
                        reject(err);
                    });
            }
        });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        options.headers = headers;
    }

    saveProduct(product)
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/rute/' + this.routeParams.id, product, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (err) => {
                    let eror = JSON.parse(err._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    });
                    reject(err);
                });
        });
    }

    salinProduct(product)
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/rute/copy/' + this.routeParams.id, product, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (err) => {
                    let eror = JSON.parse(err._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    });
                    reject(err);
                });
        });
    }

    addProduct(product)
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.post(environment.setting.base_url + '/admin/rute/', product, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (err) => {
                    let eror = JSON.parse(err._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    });
                    reject(err);
                });
        });
    }

    hapus(product){
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.delete(environment.setting.base_url + '/admin/rute/' + this.routeParams.id, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (err) => {
                    let eror = JSON.parse(err._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    });
                    reject(err);
                });
        });
    }

    changeStatus(st){
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + "/admin/rute/status/" + this.product.id, st, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (err) => {
                    let eror = JSON.parse(err._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    });
                    reject(err);
                });
        });
    }
}
