import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DetailTiketService implements Resolve<any>
{
    routeParams: any;
    product: any;
    token = sessionStorage.getItem('token');
    onProductChanged: BehaviorSubject<any> = new BehaviorSubject({});

    constructor(
        private http: Http
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProduct()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    getProduct(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            if ( this.routeParams.id === 'new' )
            {
                this.onProductChanged.next(false);
                resolve(false);
            }
            else
            {
                let options = new RequestOptions();
                this.setHeader(options);
                this.http.get(environment.setting.base_url + '/pemudik/booking/' + this.routeParams.id, options)
                    .map((res: Response) => res.json())
                    .subscribe((response: any) => {
                        this.product = response['data'];
                        // this.product['rute']['tempat_berangkat'] = this.product['rute']['tempat_berangkat'].substring(0, 55);
                        // this.product['rute']['tempat_berangkat'] = this.product['rute']['tempat_berangkat'] + " . . .";
                        this.onProductChanged.next(this.product);
                        resolve(response);
                    }, reject);
            }
        });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        options.headers = headers;
    }
    
}
