import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import 'rxjs/add/operator/map';
import { MatSnackBar } from '@angular/material';
import swal from 'sweetalert2';
import { SessionService } from './../../session.service';

@Injectable()
export class DetailTitipMotorService implements Resolve<any>
{
    routeParams: any;
    product: any;
    onProductChanged: BehaviorSubject<any> = new BehaviorSubject({});
    token = this._sessionService.get().token;
    ruteAll: any = [];

    constructor(
        private http: Http,
        private snackBar: MatSnackBar,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProduct(),
                this.getRuteAllMudik(),
                this.getRuteAllBalik()
            ]).then(
                () => {
                    resolve();
                },
                reject
            )
            .catch(
                (error) => {
                    const eror = JSON.parse(error._body);
                    this.snackBar.open(eror.message, eror.errors.email ? eror.errors.email : eror.errors.password, {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                }
            );
        });
    }

    getRuteAllMudik(): Promise<any>
    {
        const options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url + '/admin/getRute/titipMotor', options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    this.ruteAll = response['data'];
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    getRuteAllBalik(): Promise<any>
    {
        const options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url + '/admin/getRuteBalik/titipMotor', options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    for (let i = 0; i < response.data.length; ++i) {
                        this.ruteAll.push(response.data[i]);
                    }
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    getProduct(): Promise<any>
    {
        const options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            if ( this.routeParams.id === 'new' )
            {
                this.onProductChanged.next(false);
                resolve(false);
            }
            else
            {
                this.http.get(environment.setting.base_url + '/admin/titipMotor/' + this.routeParams.id, options)
                    .map((res: Response) => res.json())
                    .subscribe((response: any) => {
                        this.product = response['data'];
                        this.onProductChanged.next(this.product);
                        resolve(response);
                    }, (reject) => {
                        const eror = JSON.parse(reject._body);
                        this.snackBar.open(eror.message, 'Error !', {
                            verticalPosition: 'top',
                            duration        : 5000
                        });
                    });
            }
        });
    }

    setHeader(options: RequestOptions) {
        const headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        options.headers = headers;
    }

    saveProduct(product)
    {
        const options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.put(environment.setting.base_url + '/admin/titipMotor/' + this.routeParams.id, product, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    addProduct(product)
    {
        const options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.post(environment.setting.base_url + '/admin/titipMotor/', product, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    hapus(product){
        const options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.delete(environment.setting.base_url + '/admin/titipMotor/' + this.routeParams.id, options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }

    printTiket(tiket): Promise<any> {
        const options = new RequestOptions();
        const header = new Headers();
        header.append('Authorization', `Bearer ${this.token}`);
        header.append('Content-Type', 'application/json');
        header.append('Accept', 'application/pdf');
        options.headers = header;

        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url + "/admin/titipMotor/" + tiket + "/download", {
                    headers: header, responseType: ResponseContentType.Blob
                })
                .map((res) => {
                    return {
                        filename: `tiket-${tiket}.pdf`,
                        data: res.blob(),
                        success: true
                    };
                })
                .subscribe((res: any) => {
                    console.log();
                    let url = window.URL.createObjectURL(res.data);
                    let a = document.createElement('a');
                    document.body.appendChild(a);
                    a.setAttribute('style', 'display: none');
                    a.href = url;
                    a.download = res.filename;
                    a.click();
                    window.URL.revokeObjectURL(url);
                    a.remove(); // remove the element
                    resolve(res);
                }, error => {
                    console.log('download error:', JSON.stringify(error));
                }, () => {
                    console.log('Completed file download.');
                });
        });
    }
}
