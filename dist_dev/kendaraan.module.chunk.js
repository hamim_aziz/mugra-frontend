webpackJsonp(["kendaraan.module"],{

/***/ "../../../../../src/app/main/content/apps/kendaraan/daftarKendaraan/daftarKendaraan.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"products\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\"\r\n             fxLayout=\"column\" fxLayoutAlign=\"center center\"\r\n             fxLayout.gt-xs=\"row\" fxLayoutAlign.gt-xs=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div class=\"logo my-12 m-sm-0\"\r\n                 fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <mat-icon class=\"logo-icon mr-16\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'50ms',scale:'0.2'}}\">directions_bus</mat-icon>\r\n                <span class=\"logo-text h1\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">Daftar Kendaraan</span>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <!-- SEARCH -->\r\n            <div class=\"search-input-wrapper mx-12 m-md-0\"\r\n                 fxFlex=\"1 0 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <label for=\"search\" class=\"mr-8\">\r\n                    <mat-icon class=\"secondary-text\">search</mat-icon>\r\n                </label>\r\n                <mat-form-field floatPlaceholder=\"never\" fxFlex=\"1 0 auto\">\r\n                    <input id=\"search\" matInput #filter placeholder=\"Search\">\r\n                </mat-form-field>\r\n            </div>\r\n            <!-- / SEARCH -->\r\n\r\n            <div class=\"my-12\">\r\n                <button fxHide.xs mat-raised-button class=\"add-product-button m-sm-0\" [matMenuTriggerFor]=\"filter\">\r\n                    <span>Filter</span>\r\n                </button>\r\n\r\n                <button mat-raised-button class=\"add-product-button m-sm-0\" [matMenuTriggerFor]=\"option\" fxHide fxShow.xs>\r\n                    <span>Option</span>\r\n                </button>\r\n\r\n                <mat-menu #option=\"matMenu\">\r\n                    <button mat-menu-item [matMenuTriggerFor]=\"filter\">Filter</button>\r\n                    <button mat-menu-item [routerLink]=\"'/apps/kendaraan/detailKendaraan/new'\">Tambah Kendaraan</button>\r\n                </mat-menu>\r\n\r\n                <mat-menu #filter=\"matMenu\">\r\n                    <button mat-menu-item  (click)=\"filterButton('')\">Semua</button>\r\n                    <button mat-menu-item [matMenuTriggerFor]=\"status\">Status</button>\r\n                    <button mat-menu-item [matMenuTriggerFor]=\"tanggal\">Tanggal</button>\r\n                    <button mat-menu-item [matMenuTriggerFor]=\"jenis\">Jenis Mudik</button>\r\n                    <button mat-menu-item  (click)=\"filterButton('Mudik Motor')\">Mudik Motor</button>\r\n                </mat-menu>\r\n\r\n                <mat-menu #status=\"matMenu\">\r\n                    <button (click)=\"filterButton('Aktif')\" mat-menu-item>Aktif</button>\r\n                    <button (click)=\"filterButton('Tidak Aktif')\" mat-menu-item>Tidak Aktif</button>\r\n                </mat-menu>\r\n\r\n                <mat-menu #jenis=\"matMenu\">\r\n                    <button (click)=\"filterButton('Mudik')\" mat-menu-item>Mudik</button>\r\n                    <button (click)=\"filterButton('Balik')\" mat-menu-item>Balik</button>\r\n                </mat-menu>\r\n\r\n                <mat-menu #tanggal=\"matMenu\" fusePerfectScrollbar>\r\n                    <button *ngFor=\"let tgl of dateIndo\" (click)=\"filterButton(tgl[0])\" mat-menu-item> {{ tgl[1] }} </button>\r\n                </mat-menu>\r\n            </div>\r\n\r\n            <button mat-raised-button fxHide.xs\r\n                    [routerLink]=\"'/apps/kendaraan/detailKendaraan/new'\"\r\n                    class=\"add-product-button my-12 m-sm-0\">\r\n                <span>Tambah Kendaraan</span>\r\n            </button>\r\n\r\n\r\n\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <mat-table class=\"products-table\"\r\n                       #table [dataSource]=\"dataSource\"\r\n                       matSort\r\n                       [@animateStagger]=\"{value:'50'}\"\r\n                       fusePerfectScrollbar>\r\n\r\n                <!-- ID Column -->\r\n                <ng-container cdkColumnDef=\"id\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>No.</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product; let i = index\">\r\n                        <p class=\"text-truncate\">{{ (paginator.pageIndex * paginator.pageSize) + (i + 1) }}.</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Rute Column -->\r\n                <ng-container cdkColumnDef=\"rute\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Rute</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <span>{{product.asal}}</span><br>\r\n                        <span>{{product.keterangan}}</span>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Tanggal Column -->\r\n                <ng-container cdkColumnDef=\"tanggal\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxLayoutAlign=\"start\">Tanggal</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxLayoutAlign=\"start\">\r\n                        <p>{{ product.tanggal.substring(8, 11) }} {{ bulan(product.tanggal) }} {{ product.tanggal.substring(0, 4) }}</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Jenis Rute Column -->\r\n                <ng-container cdkColumnDef=\"jenis_rute\">\r\n                    <mat-header-cell *cdkHeaderCellDef fxHide mat-sort-header fxShow.gt-md fxLayoutAlign=\"center\">Jenis</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxHide fxShow.gt-md fxLayoutAlign=\"center\">\r\n                        <p class=\"category text-truncate\"> {{product.jenis_rute}} </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Nomor Column -->\r\n                <ng-container cdkColumnDef=\"nomor\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxLayoutAlign=\"center\">Nomor Kendaraan</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxLayoutAlign=\"center\">\r\n                        <p class=\"price text-truncate\">\r\n                            {{product.nomor}}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Ukuran Column -->\r\n                <ng-container cdkColumnDef=\"baris_kolom\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxHide fxShow.gt-xs fxLayoutAlign=\"center\">Ukuran</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxHide fxShow.gt-xs fxLayoutAlign=\"center\">\r\n                        <p class=\"price text-truncate\">\r\n                            {{product.baris}} X {{product.kolom}}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Kapasitas Column -->\r\n                <ng-container cdkColumnDef=\"sisa\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxLayoutAlign=\"center\">Kuota Sisa</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxLayoutAlign=\"center\">\r\n                        <p class=\"price text-truncate\">\r\n                            {{product.kapasitas.sisa}}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container cdkColumnDef=\"kapasitas\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxHide fxShow.gt-xs fxLayoutAlign=\"center\">Total Kuota</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxHide fxShow.gt-xs fxLayoutAlign=\"center\">\r\n                        <p class=\"price text-truncate\">\r\n                            {{product.kuota}}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Status Column -->\r\n                <ng-container cdkColumnDef=\"status\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxLayoutAlign=\"center\">Status</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxLayoutAlign=\"center\">\r\n                        <!-- <mat-icon *ngIf=\"product.status == 1\" class=\"active-icon mat-green-600-bg s-16\">check</mat-icon>\r\n                        <mat-icon *ngIf=\"product.status == 0\" class=\"active-icon mat-red-500-bg s-16\">close</mat-icon> -->\r\n                        <p class=\"price text-truncate\"> {{ product.status }} </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <mat-header-row *cdkHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n\r\n                <mat-row *cdkRowDef=\"let product; columns: displayedColumns;\"\r\n                         class=\"product\"\r\n                         matRipple\r\n                         (click)=\"clickKendaraan(product.id)\">\r\n                </mat-row>\r\n\r\n            </mat-table>\r\n\r\n            <mat-paginator #paginator\r\n                           [length]=\"dataSource.filteredData.length\"\r\n                           [pageSize]=\"100\"\r\n                           [pageSizeOptions]=\"[5, 10, 25, 100]\">\r\n            </mat-paginator>\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/kendaraan/daftarKendaraan/daftarKendaraan.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n:host .header .search-input-wrapper {\n  max-width: 480px; }\n:host .header .add-product-button {\n  background-color: #F96D01; }\n@media (max-width: 599px) {\n  :host .header {\n    height: 176px !important;\n    min-height: 176px !important;\n    max-height: 176px !important; } }\n@media (max-width: 599px) {\n  :host .top-bg {\n    height: 240px; } }\n:host .products-table {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  border-bottom: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-header-row {\n    min-height: 35px;\n    background-color: rgba(0, 0, 0, 0.12);\n    min-width: 500px; }\n:host .products-table .mat-row {\n    min-width: 500px; }\n:host .products-table .product {\n    position: relative;\n    cursor: pointer;\n    height: 60px; }\n:host .products-table .mat-cell {\n    min-width: 0;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n:host .products-table .mat-column-id {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 5%;\n            flex: 0 1 5%; }\n:host .products-table .mat-column-tanggal {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 10%;\n            flex: 0 1 10%; }\n:host .products-table .mat-column-rute {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 23%;\n            flex: 0 1 23%;\n    display: block; }\n:host .products-table .mat-column-image {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 84px;\n            flex: 0 1 84px; }\n:host .products-table .mat-column-image .product-image {\n      width: 52px;\n      height: 52px;\n      border: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-column-buttons {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 80px;\n            flex: 0 1 80px; }\n:host .products-table .quantity-indicator {\n    display: inline-block;\n    vertical-align: middle;\n    width: 8px;\n    height: 8px;\n    border-radius: 4px;\n    margin-right: 8px; }\n:host .products-table .quantity-indicator + span {\n      display: inline-block;\n      vertical-align: middle; }\n:host .products-table .active-icon {\n    border-radius: 50%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/kendaraan/daftarKendaraan/daftarKendaraan.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarKendaraanComponent; });
/* unused harmony export FilesDataSource */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__daftarKendaraan_service__ = __webpack_require__("../../../../../src/app/main/content/apps/kendaraan/daftarKendaraan/daftarKendaraan.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_cdk_collections__ = __webpack_require__("../../../cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs_add_observable_fromEvent__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/fromEvent.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__core_fuseUtils__ = __webpack_require__("../../../../../src/app/core/fuseUtils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
var EXCEL_EXTENSION = '.xlsx';
var DaftarKendaraanComponent = /** @class */ (function () {
    function DaftarKendaraanComponent(productsService, dialog, router, _sessionService) {
        this.productsService = productsService;
        this.dialog = dialog;
        this.router = router;
        this._sessionService = _sessionService;
        this.hapus = false;
        this.displayedColumns = ['id', 'rute', 'tanggal', 'jenis_rute', 'nomor', 'baris_kolom', 'kapasitas', 'sisa', 'status'];
        this.role = Number(this._sessionService.get().role);
        this.name = this._sessionService.get().email;
        this.kendaraans = this.productsService.products;
        this.date = [];
        this.dateIndo = [];
    }
    DaftarKendaraanComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._tanggal();
        this.dataSource = new FilesDataSource(this.productsService, this.paginator, this.sort);
        __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].fromEvent(this.filter.nativeElement, 'keyup')
            .debounceTime(150)
            .distinctUntilChanged()
            .subscribe(function () {
            if (!_this.dataSource) {
                return;
            }
            _this.dataSource.filter = _this.filter.nativeElement.value;
        });
    };
    DaftarKendaraanComponent.prototype._tanggal = function () {
        for (var i = 0; i < this.kendaraans.length; ++i) {
            if (this.date.indexOf(this.kendaraans[i]["tanggal"]) == -1) {
                this.dateIndo.push([this.kendaraans[i]["tanggal"], this.convertTanggal(this.kendaraans[i]["tanggal"])]);
                this.date.push(this.kendaraans[i]["tanggal"]);
            }
        }
    };
    DaftarKendaraanComponent.prototype.convertTanggal = function (tgl) {
        var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
        var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
        var date = new Date(tgl);
        var day = date.getDay();
        var month = date.getMonth();
        return hari[day] + ", " + date.getDate() + " " + bulan[month] + " " + date.getFullYear();
    };
    DaftarKendaraanComponent.prototype.filterButton = function (filter) {
        this.dataSource.filter = filter;
    };
    DaftarKendaraanComponent.prototype.toggleHapus = function () {
        this.hapus == true ? this.hapus = false : this.hapus = true;
    };
    DaftarKendaraanComponent.prototype.clickKendaraan = function (id) {
        var url = '/apps/kendaraan/detailKendaraan/' + id;
        this.router.navigate([url]);
    };
    DaftarKendaraanComponent.prototype.bulan = function (date) {
        if (date.substring(5, 7) === '01') {
            return "Januari";
        }
        else if (date.substring(5, 7) === '02') {
            return "Februari";
        }
        else if (date.substring(5, 7) === '03') {
            return "Maret";
        }
        else if (date.substring(5, 7) === '04') {
            return "April";
        }
        else if (date.substring(5, 7) === '05') {
            return "Mei";
        }
        else if (date.substring(5, 7) === '06') {
            return "Juni";
        }
        else if (date.substring(5, 7) === '07') {
            return "Juli";
        }
        else if (date.substring(5, 7) === '08') {
            return "Agustus";
        }
        else if (date.substring(5, 7) === '09') {
            return "September";
        }
        else if (date.substring(5, 7) === '10') {
            return "Oktober";
        }
        else if (date.substring(5, 7) === '11') {
            return "November";
        }
        else if (date.substring(5, 7) === '12') {
            return "Desember";
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__angular_material__["z" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__angular_material__["z" /* MatPaginator */])
    ], DaftarKendaraanComponent.prototype, "paginator", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('filter'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], DaftarKendaraanComponent.prototype, "filter", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__angular_material__["M" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__angular_material__["M" /* MatSort */])
    ], DaftarKendaraanComponent.prototype, "sort", void 0);
    DaftarKendaraanComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-products',
            template: __webpack_require__("../../../../../src/app/main/content/apps/kendaraan/daftarKendaraan/daftarKendaraan.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/kendaraan/daftarKendaraan/daftarKendaraan.component.scss")],
            animations: __WEBPACK_IMPORTED_MODULE_4__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__daftarKendaraan_service__["a" /* DaftarKendaraanService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_material__["m" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_7__angular_router__["f" /* Router */],
            __WEBPACK_IMPORTED_MODULE_15__session_service__["a" /* SessionService */]])
    ], DaftarKendaraanComponent);
    return DaftarKendaraanComponent;
}());

var FilesDataSource = /** @class */ (function (_super) {
    __extends(FilesDataSource, _super);
    function FilesDataSource(productsService, _paginator, _sort) {
        var _this = _super.call(this) || this;
        _this.productsService = productsService;
        _this._paginator = _paginator;
        _this._sort = _sort;
        _this._filterChange = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        _this._filteredDataChange = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        _this.filteredData = _this.productsService.products;
        return _this;
    }
    Object.defineProperty(FilesDataSource.prototype, "filteredData", {
        get: function () {
            return this._filteredDataChange.value;
        },
        set: function (value) {
            this._filteredDataChange.next(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FilesDataSource.prototype, "filter", {
        get: function () {
            return this._filterChange.value;
        },
        set: function (filter) {
            this._filterChange.next(filter);
        },
        enumerable: true,
        configurable: true
    });
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    FilesDataSource.prototype.connect = function () {
        var _this = this;
        var displayDataChanges = [
            this.productsService.onProductsChanged,
            this._paginator.page,
            this._filterChange,
            this._sort.sortChange
        ];
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].merge.apply(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"], displayDataChanges).map(function () {
            var data = _this.productsService.products.slice();
            data = _this.filterData(data);
            _this.filteredData = data.slice();
            data = _this.sortData(data);
            // Grab the page's slice of data.
            // if (!this.filter) {
            //     sessionStorage.setItem("pageIndexKendaraan", String(this._paginator.pageIndex));
            // }
            var startIndex = _this._paginator.pageIndex * _this._paginator.pageSize;
            return data.splice(startIndex, _this._paginator.pageSize);
        });
    };
    FilesDataSource.prototype.filterData = function (data) {
        if (!this.filter) {
            return data;
        }
        return __WEBPACK_IMPORTED_MODULE_14__core_fuseUtils__["a" /* FuseUtils */].filterArrayByString(data, this.filter);
    };
    FilesDataSource.prototype.sortData = function (data) {
        var _this = this;
        if (!this._sort.active || this._sort.direction === '') {
            return data;
        }
        return data.sort(function (a, b) {
            var propertyA = '';
            var propertyB = '';
            switch (_this._sort.active) {
                case 'id':
                    _a = [a.id, b.id], propertyA = _a[0], propertyB = _a[1];
                    break;
                case 'asal':
                    _b = [a.asal, b.asal], propertyA = _b[0], propertyB = _b[1];
                    break;
                case 'tujuan':
                    _c = [a.tujuan, b.tujuan], propertyA = _c[0], propertyB = _c[1];
                    break;
                case 'tanggal':
                    _d = [a.tanggal, b.tanggal], propertyA = _d[0], propertyB = _d[1];
                    break;
                case 'jenis':
                    _e = [a.jenis, b.jenis], propertyA = _e[0], propertyB = _e[1];
                    break;
                case 'kuota_motor':
                    _f = [a.kouta_motor, b.kouta_motor], propertyA = _f[0], propertyB = _f[1];
                    break;
                case 'status':
                    _g = [a.status, b.status], propertyA = _g[0], propertyB = _g[1];
                    break;
            }
            var valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            var valueB = isNaN(+propertyB) ? propertyB : +propertyB;
            return (valueA < valueB ? -1 : 1) * (_this._sort.direction === 'asc' ? 1 : -1);
            var _a, _b, _c, _d, _e, _f, _g;
        });
    };
    FilesDataSource.prototype.disconnect = function () {
    };
    return FilesDataSource;
}(__WEBPACK_IMPORTED_MODULE_2__angular_cdk_collections__["a" /* DataSource */]));



/***/ }),

/***/ "../../../../../src/app/main/content/apps/kendaraan/daftarKendaraan/daftarKendaraan.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarKendaraanService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DaftarKendaraanService = /** @class */ (function () {
    function DaftarKendaraanService(http, snackBar, _sessionService) {
        this.http = http;
        this.snackBar = snackBar;
        this._sessionService = _sessionService;
        this.onProductsChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({});
        this.token = this._sessionService.get().token;
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DaftarKendaraanService.prototype.resolve = function (route, state) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getProducts()
            ]).then(function () {
                resolve();
            }, reject);
        });
    };
    DaftarKendaraanService.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        options.headers = headers;
    };
    DaftarKendaraanService.prototype.getProducts = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        var role = Number(this._sessionService.get().role);
        var name = this._sessionService.get().email.toLowerCase();
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/kendaraan', options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                _this.products = response['data'];
                for (var i = 0; i < _this.products.length; ++i) {
                    var ass = _this.products[i]['asal'].toLowerCase();
                    var tu = _this.products[i]['tujuan'].toLowerCase();
                    _this.products[i]['asal'] = _this.products[i]['asal'] + " - " + _this.products[i]['tujuan'];
                    _this.products[i]['jenis_rute'] == 1 ?
                        _this.products[i]['jenis_rute'] = 'Mudik' :
                        _this.products[i]['jenis_rute'] = 'Balik';
                    if (_this.products[i].status == 1) {
                        _this.products[i].status = "Aktif";
                    }
                    else {
                        _this.products[i].status = "Tidak Aktif";
                    }
                    if (role == 1) {
                        if (name.search(ass) == -1 && name.search(tu) == -1) {
                            _this.products.splice(i, 1);
                            i--;
                        }
                    }
                }
                _this.onProductsChanged.next(_this.products);
                resolve(response);
            }, function (reject) {
                var eror = JSON.parse(reject._body);
                _this.snackBar.open(eror.message, 'Error !', {
                    verticalPosition: 'top',
                    duration: 5000
                });
            });
        });
    };
    DaftarKendaraanService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_6__session_service__["a" /* SessionService */]])
    ], DaftarKendaraanService);
    return DaftarKendaraanService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/kendaraan/detailKendaraan/confirm-dialog.component.html":
/***/ (function(module, exports) {

module.exports = "<h1 matDialogTitle>Confirm</h1>\r\n<div mat-dialog-content *ngIf=\"data.status\">\r\n\tNon Aktifkan kendaraan dengan rute {{ data.kendaraan.rute.kota.asal }} - {{ data.kendaraan.rute.kota.tujuan }}, \r\n\ttanggal {{ data.kendaraan.rute.tanggal }} ?\r\n</div>\r\n<div mat-dialog-content *ngIf=\"!data.status\">\r\n\tAktifkan kendaraan dengan rute {{ data.kendaraan.rute.kota.asal }} - {{ data.kendaraan.rute.kota.tujuan }}, \r\n\ttanggal {{ data.kendaraan.rute.tanggal }} ?\r\n</div>\r\n<div mat-dialog-actions class=\"pt-24\">\r\n    <button mat-raised-button class=\"mat-accent mr-16\" (click)=\"confirm()\">Confirm</button>\r\n    <button mat-button (click)=\"dialogRef.close(data.status ? true : false)\">Cancel</button>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/kendaraan/detailKendaraan/detailKendaraan.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"product\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <mat-progress-bar *ngIf=\"showLoadingBar\" class=\"loading-bar\" color=\"accent\" mode=\"indeterminate\"></mat-progress-bar>\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"start center\" style=\"width: 80%\">\r\n\r\n                <button class=\"mr-0 mr-sm-16\" mat-icon-button (click)=\"back()\">\r\n                    <mat-icon>arrow_back</mat-icon>\r\n                </button>\r\n\r\n                <div fxLayout=\"column\" fxLayoutAlign=\"start start\"\r\n                     *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">\r\n                    <div class=\"h2\" *ngIf=\"pageType ==='edit' && product.rute.kota.asal\">\r\n                        {{ product.rute.kota.asal }} - {{ product.rute.kota.tujuan }} - {{ product.nomor}}\r\n                    </div>\r\n                    <div class=\"h2\" *ngIf=\"pageType ==='new'\">\r\n                        Tambah Kendaraan\r\n                    </div>\r\n                    <div class=\"subtitle secondary-text\">\r\n                        <span>Detail Kendaraan</span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"onProcess\"\r\n                    *ngIf=\"pageType ==='new' && !productForm.invalid && !(role < 3)\" (click)=\"addProduct()\">\r\n                <span>Simpan</span>\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    *ngIf=\"pageType ==='edit' && !productForm.invalid && !(role < 3)\" (click)=\"saveProduct()\">\r\n                <span>Simpan</span>\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    *ngIf=\"pageType ==='edit' && !(role < 1)\" (click)=\"hapus()\">\r\n                <span>Hapus</span>\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    *ngIf=\"status && pageType ==='edit' && !(role < 1)\"\r\n                    (click)=\"changeStatus()\"\r\n                    style=\"padding-right: 30px !important;\">\r\n                Non Aktifkan\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    *ngIf=\"!status && pageType ==='edit' && !(role < 1)\"\r\n                    (click)=\"changeStatus()\">\r\n                Aktifkan\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    *ngIf=\"pageType ==='edit' && !(role < 1)\"\r\n                    (click)=\"print()\">\r\n                Print\r\n            </button>\r\n\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <!-- CONTENT -->\r\n            <div class=\"content\">\r\n\r\n                <form name=\"productForm\" [formGroup]=\"productForm\" class=\"product w-100-p\" fxLayout=\"column\" fxFlex>\r\n\r\n                    <mat-tab-group>\r\n\r\n                        <mat-tab label=\"Info Kendaraan\">\r\n                            <div class=\"tab-content p-24\" fusePerfectScrollbar>\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                  <mat-select matInput \r\n                                              formControlName=\"rute_id\" \r\n                                              placeholder=\"Rute\" \r\n                                              required>\r\n                                    <mat-option *ngFor=\"let kota of rute\" [value]=\"kota.id\">\r\n                                    \t{{kota.kota.asal}} - {{kota.kota.tujuan}} - {{ kota.tanggal.substring(8, 11) }} {{ bulan(kota.tanggal) }} {{ kota.tanggal.substring(0, 4) }} - {{kota.keterangan}}\r\n                                    </mat-option>\r\n                                  </mat-select>\r\n                                  <mat-icon matPrefix> navigation </mat-icon>\r\n                                </mat-form-field>\r\n\r\n                                <mat-form-field class=\"w-100-p\" *ngIf=\"pageType ==='edit'\">\r\n                                    <input matInput\r\n                                           name=\"nomor\"\r\n                                           formControlName=\"nomor\"\r\n                                           placeholder=\"Nomor\"\r\n                                           required>\r\n                                    <mat-icon matPrefix> format_list_numbered </mat-icon>\r\n                                </mat-form-field>\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                    <input matInput\r\n                                           type=\"number\" \r\n                                           name=\"baris\"\r\n                                           formControlName=\"baris\"\r\n                                           placeholder=\"Baris\"\r\n                                           required>\r\n                                    <mat-icon matPrefix> view_list </mat-icon>\r\n                                </mat-form-field>\r\n\r\n                                <mat-form-field class=\"w-100-p\">\r\n                                    <input matInput\r\n                                           type=\"number\" \r\n                                           name=\"kolom\"\r\n                                           formControlName=\"kolom\"\r\n                                           placeholder=\"Kolom\"\r\n                                           required>\r\n                                    <mat-icon matPrefix> view_column </mat-icon>\r\n                                </mat-form-field>\r\n\r\n                                <mat-form-field class=\"w-100-p\" \r\n                                                *ngIf=\"pageType === 'new'\">\r\n                                    <input matInput\r\n                                           type=\"number\" \r\n                                           name=\"jumlah\"\r\n                                           formControlName=\"jumlah\"\r\n                                           placeholder=\"Jumlah\"\r\n                                           required>\r\n                                    <mat-icon matPrefix> directions_bus </mat-icon>\r\n                                </mat-form-field>\r\n\r\n                                <!-- <mat-slide-toggle matInput\r\n                                                  formControlName=\"status\"> \r\n                                                Aktif \r\n                                </mat-slide-toggle> -->\r\n\r\n                            </div>\r\n                        </mat-tab>\r\n\r\n                        <mat-tab label=\"Kursi\" *ngIf=\"pageType !== 'new' && product.kursis[0]\">\r\n                            <div class=\"tab-content p-24\" fusePerfectScrollbar>\r\n\r\n                            \t<div class=\"keterangan\">\r\n                            \t\t<div class=\"box-keterangan available\"></div>\r\n                            \t\t<h5>Tersedia</h5>\r\n                            \t\t<div class=\"box-keterangan reserved\"></div>\r\n                            \t\t<h5>Sudah Dipesan</h5>\r\n                            \t\t<div class=\"box-keterangan disable\"></div>\r\n                            \t\t<h5>Tidak Aktif</h5>\r\n                                <div class=\"box-keterangan disable\"></div>\r\n                                <h5>Kuota : {{ kuota }} </h5>\r\n                            \t</div>\r\n\r\n                              \t<table class=\"tabel_kursi\" *ngIf=\"product.kolom == 5\">\r\n\t                                <tr>\r\n\t                                  <!-- supir -->\r\n\t                                  <td class=\"kursi\" style=\"border: 2px solid rgba(0, 0, 0, 0.12) !important;\">Pintu Depan</td>\r\n\t                                  <td class=\"non_kursi\"></td>\r\n\t                                  <td class=\"non_kursi\"></td>\r\n\t                                  <td class=\"non_kursi\"></td>\r\n\t                                  <td class=\"non_kursi\"></td>\r\n\t                                  <td class=\"kursi\" style=\"border: 2px solid rgba(0, 0, 0, 0.12) !important;\">Supir</td>\r\n\r\n\t                                </tr>\r\n\r\n                                    <tr *ngFor=\"let i of index\">\r\n\r\n                                        <td class=\"kursi\" \r\n                                            (click)=\"updateKursi(product.kursis[(i*5)+0]['id'], (i*5)+0)\"\r\n                                            *ngIf=\"product.kursis[(i*5)+0]['status'] <= 1\">\r\n                                            <div class=\"box {{product.kursis[(i*5)+0]['status'] == 0 ? \r\n                                            'disable' : 'available'}}\">\r\n                                              {{product.kursis[(i*5)+0]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n                                        <td class=\"kursi\" \r\n                                            *ngIf=\"product.kursis[(i*5)+0]['status'] > 1\"\r\n                                            matTooltip=\"Nomor Tiket : {{product.kursis[(i*5)+0]['nomor_tiket']}}\" matTooltipPosition=\"above\">\r\n                                            <div class=\"box reserved\">\r\n                                              {{product.kursis[(i*5)+0]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            (click)=\"updateKursi(product.kursis[(i*5)+1]['id'], (i*5)+1)\"\r\n                                            *ngIf=\"product.kursis[(i*5)+1]['status'] <= 1\">\r\n                                            <div class=\"box {{product.kursis[(i*5)+1]['status'] == 0 ? \r\n                                            'disable' : 'available'}}\">\r\n                                              {{product.kursis[(i*5)+1]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n                                        <td class=\"kursi\" \r\n                                            *ngIf=\"product.kursis[(i*5)+1]['status'] > 1\"\r\n                                            matTooltip=\"Nomor Tiket : {{product.kursis[(i*5)+1]['nomor_tiket']}}\" matTooltipPosition=\"above\">\r\n                                            <div class=\"box reserved\">\r\n                                              {{product.kursis[(i*5)+1]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"non_kursi\"></td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            (click)=\"updateKursi(product.kursis[(i*5)+2]['id'], (i*5)+2)\"\r\n                                            *ngIf=\"product.kursis[(i*5)+2]['status'] <= 1\">\r\n                                            <div class=\"box {{product.kursis[(i*5)+2]['status'] == 0 ? \r\n                                            'disable' : 'available'}}\">\r\n                                              {{product.kursis[(i*5)+2]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n                                        <td class=\"kursi\" \r\n                                            *ngIf=\"product.kursis[(i*5)+2]['status'] > 1\"\r\n                                            matTooltip=\"Nomor Tiket : {{product.kursis[(i*5)+2]['nomor_tiket']}}\" matTooltipPosition=\"above\">\r\n                                            <div class=\"box reserved\">\r\n                                              {{product.kursis[(i*5)+2]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            (click)=\"updateKursi(product.kursis[(i*5)+3]['id'], (i*5)+3)\"\r\n                                            *ngIf=\"product.kursis[(i*5)+3]['status'] <= 1\">\r\n                                            <div class=\"box {{product.kursis[(i*5)+3]['status'] == 0 ? \r\n                                            'disable' : 'available'}}\">\r\n                                              {{product.kursis[(i*5)+3]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n                                        <td class=\"kursi\" \r\n                                            *ngIf=\"product.kursis[(i*5)+3]['status'] > 1\"\r\n                                            matTooltip=\"Nomor Tiket : {{product.kursis[(i*5)+3]['nomor_tiket']}}\" matTooltipPosition=\"above\">\r\n                                            <div class=\"box reserved\">\r\n                                              {{product.kursis[(i*5)+3]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            (click)=\"updateKursi(product.kursis[(i*5)+4]['id'], (i*5)+4)\"\r\n                                            *ngIf=\"product.kursis[(i*5)+4]['status'] <= 1\">\r\n                                            <div class=\"box {{product.kursis[(i*5)+4]['status'] == 0 ? \r\n                                            'disable' : 'available'}}\">\r\n                                              {{product.kursis[(i*5)+4]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n                                        <td class=\"kursi\" \r\n                                            *ngIf=\"product.kursis[(i*5)+4]['status'] > 1\"\r\n                                            matTooltip=\"Nomor Tiket : {{product.kursis[(i*5)+4]['nomor_tiket']}}\" matTooltipPosition=\"above\">\r\n                                            <div class=\"box reserved\">\r\n                                              {{product.kursis[(i*5)+4]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n                                    </tr>\r\n\r\n\t                                <tr>\r\n\r\n                                        <td class=\"kursi\" style=\"border: 2px solid rgba(0, 0, 0, 0.12) !important;\">\r\n                                            Pintu Belakang\r\n                                        </td>\r\n\r\n                                        <td class=\"non_kursi\"></td>\r\n\r\n                                        <td class=\"non_kursi\"></td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            (click)=\"updateKursi(product.kursis[50]['id'], 50)\"\r\n                                            *ngIf=\"product.kursis[50]['status'] <= 1\">\r\n                                            <div class=\"box {{product.kursis[50]['status'] == 0 ? \r\n                                            'disable' : 'available'}}\">\r\n                                                {{product.kursis[50]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            *ngIf=\"product.kursis[50]['status'] > 1\"\r\n                                            matTooltip=\"Nomor Tiket : {{product.kursis[50]['nomor_tiket']}}\" matTooltipPosition=\"above\">\r\n                                            <div class=\"box reserved\">\r\n                                              {{product.kursis[50]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            (click)=\"updateKursi(product.kursis[51]['id'], 51)\"\r\n                                            *ngIf=\"product.kursis[51]['status'] <= 1\">\r\n                                            <div class=\"box {{product.kursis[51]['status'] == 0 ? \r\n                                                'disable' : 'available'}}\">\r\n                                                {{product.kursis[51]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            *ngIf=\"product.kursis[51]['status'] > 1\"\r\n                                            matTooltip=\"Nomor Tiket : {{product.kursis[51]['nomor_tiket']}}\" matTooltipPosition=\"above\">\r\n                                            <div class=\"box reserved\">\r\n                                                {{product.kursis[51]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            (click)=\"updateKursi(product.kursis[52]['id'], 52)\"\r\n                                            *ngIf=\"product.kursis[52]['status'] <= 1\">\r\n                                            <div class=\"box {{product.kursis[52]['status'] == 0 ? \r\n                                                'disable' : 'available'}}\">\r\n                                                {{product.kursis[52]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            *ngIf=\"product.kursis[52]['status'] > 1\"\r\n                                            matTooltip=\"Nomor Tiket : {{product.kursis[52]['nomor_tiket']}}\" matTooltipPosition=\"above\">\r\n                                            <div class=\"box reserved\">\r\n                                                {{product.kursis[52]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n\t                                </tr>\r\n\r\n                                    <tr>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            (click)=\"updateKursi(product.kursis[53]['id'], 53)\"\r\n                                            *ngIf=\"product.kursis[53]['status'] <= 1\">\r\n                                            <div class=\"box {{product.kursis[53]['status'] == 0 ? \r\n                                            'disable' : 'available'}}\">\r\n                                              {{product.kursis[53]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            *ngIf=\"product.kursis[53]['status'] > 1\"\r\n                                            matTooltip=\"Nomor Tiket : {{product.kursis[53]['nomor_tiket']}}\" matTooltipPosition=\"above\">\r\n                                            <div class=\"box reserved\">\r\n                                              {{product.kursis[53]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            (click)=\"updateKursi(product.kursis[54]['id'], 54)\"\r\n                                            *ngIf=\"product.kursis[54]['status'] <= 1\">\r\n                                            <div class=\"box {{product.kursis[54]['status'] == 0 ? \r\n                                            'disable' : 'available'}}\">\r\n                                              {{product.kursis[54]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            *ngIf=\"product.kursis[54]['status'] > 1\"\r\n                                            matTooltip=\"Nomor Tiket : {{product.kursis[54]['nomor_tiket']}}\" matTooltipPosition=\"above\">\r\n                                            <div class=\"box reserved\">\r\n                                              {{product.kursis[54]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi_tengah\" \r\n                                            (click)=\"updateKursi(product.kursis[55]['id'], 55)\"\r\n                                            *ngIf=\"product.kursis[55]['status'] <= 1\">\r\n                                            <div class=\"box {{product.kursis[55]['status'] == 0 ? \r\n                                            'disable' : 'available'}}\">\r\n                                              {{product.kursis[55]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi_tengah\" \r\n                                            *ngIf=\"product.kursis[55]['status'] > 1\"\r\n                                            matTooltip=\"Nomor Tiket : {{product.kursis[55]['nomor_tiket']}}\" matTooltipPosition=\"above\">\r\n                                            <div class=\"box reserved\">\r\n                                              {{product.kursis[55]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            (click)=\"updateKursi(product.kursis[56]['id'], 56)\"\r\n                                            *ngIf=\"product.kursis[56]['status'] <= 1\">\r\n                                            <div class=\"box {{product.kursis[56]['status'] == 0 ? \r\n                                            'disable' : 'available'}}\">\r\n                                              {{product.kursis[56]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            *ngIf=\"product.kursis[56]['status'] > 1\"\r\n                                            matTooltip=\"Nomor Tiket : {{product.kursis[56]['nomor_tiket']}}\" matTooltipPosition=\"above\">\r\n                                            <div class=\"box reserved\">\r\n                                              {{product.kursis[56]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            (click)=\"updateKursi(product.kursis[57]['id'], 57)\"\r\n                                            *ngIf=\"product.kursis[57]['status'] <= 1\">\r\n                                            <div class=\"box {{product.kursis[57]['status'] == 0 ? \r\n                                            'disable' : 'available'}}\">\r\n                                              {{product.kursis[57]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            *ngIf=\"product.kursis[57]['status'] > 1\"\r\n                                            matTooltip=\"Nomor Tiket : {{product.kursis[57]['nomor_tiket']}}\" matTooltipPosition=\"above\">\r\n                                            <div class=\"box reserved\">\r\n                                              {{product.kursis[57]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                        <td class=\"kursi\" \r\n                                            (click)=\"updateKursi(product.kursis[58]['id'], 58)\"\r\n                                            *ngIf=\"product.kursis[58]['status'] <= 1\">\r\n                                            <div class=\"box {{product.kursis[58]['status'] == 0 ? \r\n                                            'disable' : 'available'}}\">\r\n                                              {{product.kursis[58]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n                                        \r\n                                        <td class=\"kursi\" \r\n                                            *ngIf=\"product.kursis[58]['status'] > 1\"\r\n                                            matTooltip=\"Nomor Tiket : {{product.kursis[58]['nomor_tiket']}}\" matTooltipPosition=\"above\">\r\n                                            <div class=\"box reserved\">\r\n                                              {{product.kursis[58]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n\r\n                                    </tr>\r\n\r\n                              \t</table>\r\n\r\n                            </div>\r\n                        </mat-tab>\r\n\r\n                        <mat-tab label=\"Detail Kursi\" *ngIf=\"pageType !== 'new'\">\r\n                        \t<div class=\"tab-content p-24\" fusePerfectScrollbar>\r\n\r\n                                <div class=\"header-print\">\r\n                                    <h1 style=\"margin: 0px 0px\">{{ product.nomor }}</h1>\r\n                                    <h2 style=\"margin: 0px 0px\">{{ product.rute.kota.asal }} - {{ product.rute.kota.tujuan }}</h2>\r\n                                    <h3 style=\"margin: 0px 0px\">{{ product.rute.keterangan }} -{{ product.rute.tanggal }} </h3>\r\n                                </div>\r\n\r\n\t                            <table *ngIf=\"product.kolom == 5\">\r\n\r\n\t                            \t<tr>\r\n\t                                  <!-- supir -->\r\n\t                                  <td class=\"kursi\" style=\"border: 2px solid rgba(0, 0, 0, 0.12) !important\">Pintu Depan</td>\r\n\t                                  <td class=\"non_kursi\"></td>\r\n\t                                  <td class=\"non_kursi\"></td>\r\n\t                                  <td class=\"non_kursi\"></td>\r\n\t                                  <td class=\"non_kursi\"></td>\r\n\t                                  <td class=\"kursi\" style=\"border: 2px solid rgba(0, 0, 0, 0.12) !important\">Supir</td>\r\n\t                                </tr>\r\n\r\n\t\t\t\t\t\t\t\t\t<tr *ngFor=\"let i of index\">\r\n                                        <td class=\"detail_kursi\">\r\n                                            <div class=\"box-detail reserved\" \r\n                                                    (click)=\"detailTiket(product.kursis[(i*5)+0]['nomor_tiket'])\" \r\n                                                    *ngIf=\"product.kursis[(i*5)+0]['pemudik']\">\r\n                                                <div class=\"kapital\">\r\n                                                    {{product.kursis[(i*5)+0]['nama']}}\r\n                                                </div>\r\n                                                <div class=\"small\">\r\n                                                    {{product.kursis[(i*5)+0]['pemudik']}}<br>\r\n                                                    {{product.kursis[(i*5)+0]['nomor_tiket']}}\r\n                                                </div>\r\n                                            </div>\r\n                                            <div class=\"box disable\" *ngIf=\"!product.kursis[(i*5)+0]['pemudik']\">\r\n                                                {{product.kursis[(i*5)+0]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n                                        <td class=\"detail_kursi\">\r\n                                            <div class=\"box-detail reserved\" \r\n                                                    (click)=\"detailTiket(product.kursis[(i*5)+1]['nomor_tiket'])\" \r\n                                                    *ngIf=\"product.kursis[(i*5)+1]['pemudik']\">\r\n                                                <div class=\"kapital\">\r\n                                                    {{product.kursis[(i*5)+1]['nama']}}\r\n                                                </div>\r\n                                                <div class=\"small\">\r\n                                                    {{product.kursis[(i*5)+1]['pemudik']}}<br>\r\n                                                    {{product.kursis[(i*5)+1]['nomor_tiket']}}\r\n                                                </div>\r\n                                            </div>\r\n                                            <div class=\"box disable\" *ngIf=\"!product.kursis[(i*5)+1]['pemudik']\">\r\n                                                {{product.kursis[(i*5)+1]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n                                        <td>  </td>\r\n                                        <td class=\"detail_kursi\">\r\n                                            <div class=\"box-detail reserved\" \r\n                                                    (click)=\"detailTiket(product.kursis[(i*5)+2]['nomor_tiket'])\" \r\n                                                    *ngIf=\"product.kursis[(i*5)+2]['pemudik']\">\r\n                                                <div class=\"kapital\">\r\n                                                    {{product.kursis[(i*5)+2]['nama']}}\r\n                                                </div>\r\n                                                <div class=\"small\">\r\n                                                    {{product.kursis[(i*5)+2]['pemudik']}}<br>\r\n                                                    {{product.kursis[(i*5)+2]['nomor_tiket']}}\r\n                                                </div>\r\n                                            </div>\r\n                                            <div class=\"box disable\" *ngIf=\"!product.kursis[(i*5)+2]['pemudik']\">\r\n                                                {{product.kursis[(i*5)+2]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n                                        <td class=\"detail_kursi\">\r\n                                            <div class=\"box-detail reserved\" \r\n                                                (click)=\"detailTiket(product.kursis[(i*5)+3]['nomor_tiket'])\" \r\n                                                *ngIf=\"product.kursis[(i*5)+3]['pemudik']\">\r\n                                                <div class=\"kapital\">\r\n                                                    {{product.kursis[(i*5)+3]['nama']}}\r\n                                                </div>\r\n                                                <div class=\"small\">\r\n                                                    {{product.kursis[(i*5)+3]['pemudik']}}<br>\r\n                                                    {{product.kursis[(i*5)+3]['nomor_tiket']}}\r\n                                                </div>\r\n                                            </div>\r\n                                            <div class=\"box disable\" *ngIf=\"!product.kursis[(i*5)+3]['pemudik']\">\r\n                                                {{product.kursis[(i*5)+3]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n                                        <td class=\"detail_kursi\">\r\n                                            <div class=\"box-detail reserved\" \r\n                                                    (click)=\"detailTiket(product.kursis[(i*5)+4]['nomor_tiket'])\" \r\n                                                    *ngIf=\"product.kursis[(i*5)+4]['pemudik']\">\r\n                                                <div class=\"kapital\">\r\n                                                    {{product.kursis[(i*5)+4]['nama']}}\r\n                                                </div>\r\n                                                <div class=\"small\">\r\n                                                    {{product.kursis[(i*5)+4]['pemudik']}}<br>\r\n                                                    {{product.kursis[(i*5)+4]['nomor_tiket']}}\r\n                                                </div>\r\n                                            </div>\r\n                                            <div class=\"box disable\" *ngIf=\"!product.kursis[(i*5)+4]['pemudik']\">\r\n                                                {{product.kursis[(i*5)+4]['nama']}}\r\n                                            </div>\r\n                                        </td>\r\n                                    </tr>\r\n\r\n                                  <tr>\r\n                                    <td class=\"kursi\" style=\"border: 2px solid rgba(0, 0, 0, 0.12) !important\">Pintu Belakang</td>\r\n                                    <td class=\"non_kursi\"></td>\r\n                                    <td>  </td>\r\n                                    <td class=\"detail_kursi\">\r\n                                        <div class=\"box-detail reserved\" \r\n                                             (click)=\"detailTiket(product.kursis[50]['nomor_tiket'])\" \r\n                                             *ngIf=\"product.kursis[50]['pemudik']\">\r\n                                          <div class=\"kapital\">\r\n                                            {{product.kursis[50]['nama']}}\r\n                                          </div>\r\n                                          <div class=\"small\">\r\n                                            {{product.kursis[50]['pemudik']}}<br>\r\n                                            {{product.kursis[50]['nomor_tiket']}}\r\n                                          </div>\r\n                                      </div>\r\n                                      <div class=\"box disable\" *ngIf=\"!product.kursis[50]['pemudik']\">\r\n                                        {{product.kursis[50]['nama']}}\r\n                                      </div>\r\n                                    </td>\r\n                                    <td class=\"detail_kursi\">\r\n                                        <div class=\"box-detail reserved\" \r\n                                             (click)=\"detailTiket(product.kursis[51]['nomor_tiket'])\" \r\n                                             *ngIf=\"product.kursis[51]['pemudik']\">\r\n                                          <div class=\"kapital\">\r\n                                            {{product.kursis[51]['nama']}}\r\n                                          </div>\r\n                                          <div class=\"small\">\r\n                                            {{product.kursis[51]['pemudik']}}<br>\r\n                                            {{product.kursis[51]['nomor_tiket']}}\r\n                                          </div>\r\n                                      </div>\r\n                                      <div class=\"box disable\" *ngIf=\"!product.kursis[51]['pemudik']\">\r\n                                        {{product.kursis[51]['nama']}}\r\n                                      </div>\r\n                                    </td>\r\n                                    <td class=\"detail_kursi\">\r\n                                        <div class=\"box-detail reserved\" \r\n                                             (click)=\"detailTiket(product.kursis[52]['nomor_tiket'])\" \r\n                                             *ngIf=\"product.kursis[52]['pemudik']\">\r\n                                          <div class=\"kapital\">\r\n                                            {{product.kursis[52]['nama']}}\r\n                                          </div>\r\n                                          <div class=\"small\">\r\n                                            {{product.kursis[52]['pemudik']}}<br>\r\n                                            {{product.kursis[52]['nomor_tiket']}}\r\n                                          </div>\r\n                                      </div>\r\n                                      <div class=\"box disable\" *ngIf=\"!product.kursis[52]['pemudik']\">\r\n                                        {{product.kursis[52]['nama']}}\r\n                                      </div>\r\n                                    </td>\r\n                                  </tr>\r\n\r\n                                  <tr>\r\n                                    <td class=\"detail_kursi\">\r\n                                        <div class=\"box-detail reserved\" \r\n                                             (click)=\"detailTiket(product.kursis[53]['nomor_tiket'])\" \r\n                                             *ngIf=\"product.kursis[53]['pemudik']\">\r\n                                          <div class=\"kapital\">\r\n                                            {{product.kursis[53]['nama']}}\r\n                                          </div>\r\n                                          <div class=\"small\">\r\n                                            {{product.kursis[53]['pemudik']}}<br>\r\n                                            {{product.kursis[53]['nomor_tiket']}}\r\n                                          </div>\r\n                                      </div>\r\n                                      <div class=\"box disable\" *ngIf=\"!product.kursis[53]['pemudik']\">\r\n                                        {{product.kursis[53]['nama']}}\r\n                                      </div>\r\n                                    </td>\r\n                                    <td class=\"detail_kursi\">\r\n                                        <div class=\"box-detail reserved\" \r\n                                             (click)=\"detailTiket(product.kursis[54]['nomor_tiket'])\" \r\n                                             *ngIf=\"product.kursis[54]['pemudik']\">\r\n                                          <div class=\"kapital\">\r\n                                            {{product.kursis[54]['nama']}}\r\n                                          </div>\r\n                                          <div class=\"small\">\r\n                                            {{product.kursis[54]['pemudik']}}<br>\r\n                                            {{product.kursis[54]['nomor_tiket']}}\r\n                                          </div>\r\n                                      </div>\r\n                                      <div class=\"box disable\" *ngIf=\"!product.kursis[54]['pemudik']\">\r\n                                        {{product.kursis[54]['nama']}}\r\n                                      </div>\r\n                                    </td>\r\n                                    <td class=\"kursi_tengah\">\r\n                                        <div class=\"box-detail reserved\" \r\n                                             (click)=\"detailTiket(product.kursis[55]['nomor_tiket'])\" \r\n                                             *ngIf=\"product.kursis[55]['pemudik']\">\r\n                                          <div class=\"kapital\">\r\n                                            {{product.kursis[55]['nama']}}\r\n                                          </div>\r\n                                          <div class=\"small\">\r\n                                            {{product.kursis[55]['pemudik']}}<br>\r\n                                            {{product.kursis[55]['nomor_tiket']}}\r\n                                          </div>\r\n                                      </div>\r\n                                      <div class=\"box disable\" *ngIf=\"!product.kursis[55]['pemudik']\">\r\n                                        {{product.kursis[55]['nama']}}\r\n                                      </div>\r\n                                    </td>\r\n                                    <td class=\"detail_kursi\">\r\n                                        <div class=\"box-detail reserved\" \r\n                                             (click)=\"detailTiket(product.kursis[56]['nomor_tiket'])\" \r\n                                             *ngIf=\"product.kursis[56]['pemudik']\">\r\n                                          <div class=\"kapital\">\r\n                                            {{product.kursis[56]['nama']}}\r\n                                          </div>\r\n                                          <div class=\"small\">\r\n                                            {{product.kursis[56]['pemudik']}}<br>\r\n                                            {{product.kursis[56]['nomor_tiket']}}\r\n                                          </div>\r\n                                      </div>\r\n                                      <div class=\"box disable\" *ngIf=\"!product.kursis[56]['pemudik']\">\r\n                                        {{product.kursis[56]['nama']}}\r\n                                      </div>\r\n                                    </td>\r\n                                    <td class=\"detail_kursi\">\r\n                                        <div class=\"box-detail reserved\" \r\n                                             (click)=\"detailTiket(product.kursis[57]['nomor_tiket'])\" \r\n                                             *ngIf=\"product.kursis[57]['pemudik']\">\r\n                                          <div class=\"kapital\">\r\n                                            {{product.kursis[57]['nama']}}\r\n                                          </div>\r\n                                          <div class=\"small\">\r\n                                            {{product.kursis[57]['pemudik']}}<br>\r\n                                            {{product.kursis[57]['nomor_tiket']}}\r\n                                          </div>\r\n                                      </div>\r\n                                      <div class=\"box disable\" *ngIf=\"!product.kursis[57]['pemudik']\">\r\n                                        {{product.kursis[57]['nama']}}\r\n                                      </div>\r\n                                    </td>\r\n                                    <td class=\"detail_kursi\">\r\n                                        <div class=\"box-detail reserved\" \r\n                                             (click)=\"detailTiket(product.kursis[58]['nomor_tiket'])\" \r\n                                             *ngIf=\"product.kursis[58]['pemudik']\">\r\n                                          <div class=\"kapital\">\r\n                                            {{product.kursis[58]['nama']}}\r\n                                          </div>\r\n                                          <div class=\"small\">\r\n                                            {{product.kursis[58]['pemudik']}}<br>\r\n                                            {{product.kursis[58]['nomor_tiket']}}\r\n                                          </div>\r\n                                      </div>\r\n                                      <div class=\"box disable\" *ngIf=\"!product.kursis[58]['pemudik']\">\r\n                                        {{product.kursis[58]['nama']}}\r\n                                      </div>\r\n                                    </td>\r\n                                  </tr>\r\n\t                            </table>\r\n\r\n                        \t</div>\r\n\r\n                        </mat-tab>\r\n\r\n                    </mat-tab-group>\r\n                </form>\r\n\r\n            </div>\r\n            <!-- / CONTENT -->\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/kendaraan/detailKendaraan/detailKendaraan.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media print {\n  #product .mat-accent-bg {\n    background-color: white !important;\n    background-image: none; } }\n\n#product .loading-bar {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  width: 100%; }\n\n@media print {\n  #product .header {\n    display: none !important; } }\n\n#product .header .product-image {\n  overflow: hidden;\n  width: 56px;\n  height: 56px;\n  border: 3px solid rgba(0, 0, 0, 0.12); }\n\n#product .header .product-image img {\n    height: 100%;\n    width: auto;\n    max-width: none; }\n\n#product .header .save-product-button {\n  margin: 0px 20px;\n  background-color: #F96D01; }\n\n#product .header .subtitle {\n  margin: 6px 0 0 0; }\n\n#product .content .header-print {\n  display: none;\n  margin: auto !important;\n  width: 100%;\n  text-align: center; }\n\n@media print {\n    #product .content .header-print {\n      display: block; } }\n\n#product .content .mat-tab-group,\n#product .content .mat-tab-body-wrapper,\n#product .content .tab-content {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  max-width: 100%; }\n\n@media print {\n  #product .content .mat-tab-header {\n    display: none !important; } }\n\n#product .content .w-100-p {\n  margin-bottom: 8px; }\n\n#product .content .w-90-p {\n  margin-bottom: 8px; }\n\n#product .content .mat-form-field-prefix .mat-icon, #product .content .mat-form-field-suffix .mat-icon {\n  margin-right: 10px; }\n\n#product .content .mat-tab-body-content {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n\n#product .content .mat-tab-label {\n  height: 35px; }\n\n#product .content .mat-tab-header {\n  height: 40px;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n\n#product .content .mat-grid-tile .mat-figure {\n  -webkit-box-align: left !important;\n      -ms-flex-align: left !important;\n          align-items: left !important;\n  -webkit-box-pack: left !important;\n      -ms-flex-pack: left !important;\n          justify-content: left !important; }\n\n#product .content .save-product-button {\n  background-color: #F96D01; }\n\n#product .content .product-image {\n  overflow: hidden;\n  width: 128px;\n  height: 128px;\n  margin-right: 16px;\n  margin-bottom: 16px;\n  border: 3px solid rgba(0, 0, 0, 0.12); }\n\n#product .content .product-image img {\n    height: 100%;\n    width: auto;\n    max-width: none; }\n\n#product .content .tabel_kursi {\n  width: 100%; }\n\n#product .content .kursi {\n  width: 15%; }\n\n#product .content .detail_kursi {\n  width: 15%; }\n\n#product .content .kursi_tengah {\n  width: 15%; }\n\n#product .content table, #product .content th, #product .content td {\n  margin: auto;\n  padding: 10px;\n  text-align: center;\n  border: initial !important; }\n\n#product .content td {\n  margin-bottom: 6px;\n  margin-left: 3px;\n  margin-right: 3px; }\n\n#product .content .box {\n  padding: 13px;\n  border: 2px solid rgba(0, 0, 0, 0.12);\n  margin: -8px;\n  border-radius: 5px; }\n\n#product .content .box-detail {\n  text-align: left;\n  font-size: 13px;\n  padding: 2px;\n  border: 2px solid rgba(0, 0, 0, 0.12);\n  margin: -8px;\n  border-radius: 5px; }\n\n#product .content .kapital {\n  text-transform: uppercase; }\n\n#product .content .small {\n  font-size: 10px !important; }\n\n#product .content table {\n  width: 100%;\n  margin-top: 10px;\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  border-radius: 5px; }\n\n#product .content .non_kursi {\n  border: none !important;\n  width: 5% !important; }\n\n#product .content .available {\n  background-color: #2094c8; }\n\n#product .content .available:hover {\n  border-color: #cecece;\n  -webkit-box-shadow: 1px 1px #cecece;\n          box-shadow: 1px 1px #cecece; }\n\n#product .content .disable {\n  background-color: #cecece; }\n\n#product .content .reserved {\n  background-color: white; }\n\n#product .content .keterangan {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n\n@media print {\n    #product .content .keterangan {\n      display: none !important; } }\n\n#product .content .box-keterangan {\n  height: 25px;\n  width: 25px;\n  border: 1px solid rgba(0, 0, 0, 0.12); }\n\n#product .content h5 {\n  margin: 4px 10px; }\n\n@media print {\n  #product div.content-card.mat-white-bg {\n    box-shadow: 0px 4px 5px -2px transparent, 0px 7px 10px 1px transparent, 0px 2px 16px 1px transparent;\n    -webkit-box-shadow: 0px 4px 5px -2px transparent, 0px 7px 10px 1px transparent, 0px 2px 16px 1px transparent; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/kendaraan/detailKendaraan/detailKendaraan.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailKendaraanComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FuseConfirmDialog; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__detailKendaraan_service__ = __webpack_require__("../../../../../src/app/main/content/apps/kendaraan/detailKendaraan/detailKendaraan.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_observable_fromEvent__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/fromEvent.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__detailKendaraan_model__ = __webpack_require__("../../../../../src/app/main/content/apps/kendaraan/detailKendaraan/detailKendaraan.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_17_sweetalert2__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


















var EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
var EXCEL_EXTENSION = '.xlsx';
var DetailKendaraanComponent = /** @class */ (function () {
    function DetailKendaraanComponent(productService, formBuilder, snackBar, router, http, dialog, location, _sessionService) {
        this.productService = productService;
        this.formBuilder = formBuilder;
        this.snackBar = snackBar;
        this.router = router;
        this.http = http;
        this.dialog = dialog;
        this.location = location;
        this._sessionService = _sessionService;
        this.product = new __WEBPACK_IMPORTED_MODULE_9__detailKendaraan_model__["a" /* Product */]();
        this.token = this._sessionService.get().token;
        this.i = 0;
        this.index = [];
        this.role = this._sessionService.get().role;
        this.showLoadingBar = true;
        this.onProcess = false;
    }
    DetailKendaraanComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Subscribe to update product on changes
        this.onProductChanged =
            this.productService.onProductChanged
                .subscribe(function (product) {
                if (product) {
                    _this.product = new __WEBPACK_IMPORTED_MODULE_9__detailKendaraan_model__["a" /* Product */](product);
                    _this.pageType = 'edit';
                    for (var i = 0; i < _this.product.baris - 2; ++i) {
                        _this.index.push(i);
                    }
                    _this.kuota = _this.product.kuota;
                }
                else {
                    _this.pageType = 'new';
                    _this.product = new __WEBPACK_IMPORTED_MODULE_9__detailKendaraan_model__["a" /* Product */]();
                }
                _this.productForm = _this.createProductForm();
                _this.status = _this.product.status == 1 ? true : false;
                _this.showLoadingBar = false;
            });
        this.getRute();
    };
    DetailKendaraanComponent.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_14__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        options.headers = headers;
    };
    DetailKendaraanComponent.prototype.getRute = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_14__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        this.http.get(__WEBPACK_IMPORTED_MODULE_15__environments_environment__["a" /* environment */].setting.base_url + "/admin/rute", options)
            .map(function (res) { return res.json(); })
            .subscribe(function (response) {
            _this.rute = response['data'];
        });
    };
    DetailKendaraanComponent.prototype.updateKursi = function (id, index) {
        var _this = this;
        if (Number(this.role) > 3) {
            if (this.product.kursis[index]['status'] == 1) {
                var raw = {
                    "status": 0
                };
                var options = new __WEBPACK_IMPORTED_MODULE_14__angular_http__["g" /* RequestOptions */]();
                this.setHeader(options);
                this.http.put(__WEBPACK_IMPORTED_MODULE_15__environments_environment__["a" /* environment */].setting.base_url + '/admin/kursi/' + id, raw, options)
                    .map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    if (data['status'] == true) {
                        _this.snackBar.open('Update Kursi Sukses', 'OK', {
                            verticalPosition: 'top',
                            duration: 5000
                        });
                        _this.product.kursis[index]['status'] = 0;
                        _this.kuota--;
                    }
                }, function (err) {
                    console.log(err);
                });
            }
            else {
                var raw = {
                    "status": 1
                };
                var options = new __WEBPACK_IMPORTED_MODULE_14__angular_http__["g" /* RequestOptions */]();
                this.setHeader(options);
                this.http.put(__WEBPACK_IMPORTED_MODULE_15__environments_environment__["a" /* environment */].setting.base_url + '/admin/kursi/' + id, raw, options)
                    .map(function (res) { return res.json(); })
                    .subscribe(function (response) {
                    if (response['status'] == true) {
                        _this.snackBar.open('Update Kursi Sukses', 'OK', {
                            verticalPosition: 'top',
                            duration: 5000
                        });
                        _this.product.kursis[index]['status'] = 1;
                        _this.kuota++;
                    }
                }, function (err) {
                    console.log(err);
                });
            }
        }
    };
    DetailKendaraanComponent.prototype.createProductForm = function () {
        if (Number(this.role) < 1) {
            return this.formBuilder.group({
                rute_id: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: '', disabled: true }, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
                jenis: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: this.product.jenis, disabled: true }),
                nomor: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: this.product.nomor, disabled: true }),
                status: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: this.product.status, disabled: true }),
                baris: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: this.product.baris, disabled: true }, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
                kolom: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: this.product.kolom, disabled: true }, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
                jumlah: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: this.product.jumlah, disabled: true }, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
                kapasitas: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: this.product.kapasitas, disabled: true }),
                rute: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: this.product.rute, disabled: true }),
                kursis: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */]({ value: this.product.kursis, disabled: true })
            });
        }
        else {
            return this.formBuilder.group({
                rute_id: ['', [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]],
                jenis: [this.product.jenis],
                nomor: [{ value: this.product.nomor, disabled: true }],
                status: [this.product.status == 1 ? true : false],
                baris: [this.product.baris, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]],
                kolom: [this.product.kolom, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]],
                jumlah: [this.product.jumlah, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]],
                kapasitas: [this.product.kapasitas],
                rute: [this.product.rute],
                kursis: [this.product.kursis]
            });
        }
    };
    DetailKendaraanComponent.prototype.saveProduct = function () {
        var _this = this;
        var data = this.productForm.getRawValue();
        this.showLoadingBar = true;
        this.onProcess = true;
        this.productService.saveProduct(data)
            .then(function () {
            // Trigger the subscription with new data
            _this.productService.onProductChanged.next(data);
            // Show the success message
            _this.snackBar.open('Update Kendaraan Sukses', 'OK', {
                verticalPosition: 'top',
                duration: 5000
            });
            _this.showLoadingBar = false;
            _this.onProcess = false;
            // Change the location with new one
            _this.back();
        });
    };
    DetailKendaraanComponent.prototype.salinProduct = function () {
        this.pageType = "salin";
    };
    DetailKendaraanComponent.prototype.addProduct = function () {
        var _this = this;
        this.showLoadingBar = true;
        this.onProcess = true;
        var data = this.productForm.getRawValue();
        this.productService.addProduct(data)
            .then(function () {
            // Trigger the subscription with new data
            // this.productService.onProductChanged.next(data);
            _this.showLoadingBar = false;
            _this.onProcess = false;
            // Show the success message
            _this.snackBar.open('Tambah Kendaraan Sukses', 'OK', {
                verticalPosition: 'top',
                duration: 5000
            });
            // Change the location with new one
            _this.back();
        });
    };
    DetailKendaraanComponent.prototype.changeStatus = function () {
        var _this = this;
        var pesan = "Aktifkan Kendaraan dari Rute ";
        var url = "/admin/kendaraan/status/";
        var navigate = '/apps/kendaraan/daftarKendaraan';
        var confirmDialog = this.dialog.open(FuseConfirmDialog, {
            width: '250',
            data: { kendaraan: this.product, status: this.status, pesan: pesan, url: url, navigate: navigate }
        });
        confirmDialog.afterClosed().subscribe(function (result) {
            _this.status = result;
        });
    };
    DetailKendaraanComponent.prototype.ngOnDestroy = function () {
        this.onProductChanged.unsubscribe();
    };
    DetailKendaraanComponent.prototype.hapus = function () {
        var _this = this;
        // var pesan = "Hapus Kendaraan dari Rute ";
        // var url = "/admin/kendaraan/"
        // var navigate = '/apps/kendaraan/daftarKendaraan';
        // let deleteDialog = this.dialog.open(FuseConfirmDialogComponent, {
        //     width: '250',
        //     data: { id: this.product.id, rute: this.product.rute, pesan: pesan, url: url, navigate: navigate}
        // });
        __WEBPACK_IMPORTED_MODULE_17_sweetalert2___default()({
            title: 'Hapus Kendaraan dari Rute' + this.product.rute['kota']['asal'] + " - " + this.product.rute['kota']['tujuan'],
            text: "Kendaraan yang telah dihapus tidak bisa dikembalikan lagi",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus !',
            cancelButtonText: 'Batal!',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                _this.productService.hapus()
                    .then(function (response) {
                    if (response.status) {
                        __WEBPACK_IMPORTED_MODULE_17_sweetalert2___default()({
                            title: 'Hapus Kendaraan Berhasil!',
                            text: 'Kendaraan ' + _this.product.rute['kota']['asal'] + ' - ' + _this.product.rute['kota']['tujuan'] + ' Telah Dihapus',
                            type: 'success',
                            timer: 2000
                        });
                        _this.back();
                    }
                    else {
                        __WEBPACK_IMPORTED_MODULE_17_sweetalert2___default()('Hapus Kendaraan Gagal!', response.message, 'error');
                    }
                })
                    .catch(function (error) {
                    console.log(error);
                    __WEBPACK_IMPORTED_MODULE_17_sweetalert2___default()('Hapus Kendaraan Gagal!', 'Internal server error', 'error');
                });
            }
        });
    };
    DetailKendaraanComponent.prototype.bulan = function (date) {
        if (date.substring(5, 7) === '01') {
            return "Januari";
        }
        else if (date.substring(5, 7) === '02') {
            return "Februari";
        }
        else if (date.substring(5, 7) === '03') {
            return "Maret";
        }
        else if (date.substring(5, 7) === '04') {
            return "April";
        }
        else if (date.substring(5, 7) === '05') {
            return "Mei";
        }
        else if (date.substring(5, 7) === '06') {
            return "Juni";
        }
        else if (date.substring(5, 7) === '07') {
            return "Juli";
        }
        else if (date.substring(5, 7) === '08') {
            return "Agustus";
        }
        else if (date.substring(5, 7) === '09') {
            return "September";
        }
        else if (date.substring(5, 7) === '10') {
            return "Oktober";
        }
        else if (date.substring(5, 7) === '11') {
            return "November";
        }
        else if (date.substring(5, 7) === '12') {
            return "Desember";
        }
    };
    DetailKendaraanComponent.prototype.back = function () {
        this.router.navigate(['/apps/kendaraan/daftarKendaraan']);
    };
    DetailKendaraanComponent.prototype.detailTiket = function (tiket) {
        this.router.navigate(['/apps/tiket/detailTiket/' + tiket]);
    };
    DetailKendaraanComponent.prototype.print = function () {
        this.productService.printKendaraan()
            .then(function (response) {
            if (response.status) {
                __WEBPACK_IMPORTED_MODULE_17_sweetalert2___default()({
                    title: 'Berhasil Download Tiket.',
                    text: "Berhasil mendownload denah kendaraan",
                    type: 'success'
                });
            }
        })
            .catch(function (error) {
            __WEBPACK_IMPORTED_MODULE_17_sweetalert2___default()({
                title: 'Internal Server Error',
                text: error.message,
                type: 'error'
            });
        });
    };
    DetailKendaraanComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-product',
            template: __webpack_require__("../../../../../src/app/main/content/apps/kendaraan/detailKendaraan/detailKendaraan.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/kendaraan/detailKendaraan/detailKendaraan.component.scss")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
            animations: __WEBPACK_IMPORTED_MODULE_2__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__detailKendaraan_service__["a" /* DetailKendaraanService */],
            __WEBPACK_IMPORTED_MODULE_10__angular_forms__["c" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_11__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_12__angular_router__["f" /* Router */],
            __WEBPACK_IMPORTED_MODULE_14__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_11__angular_material__["m" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_13__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_16__session_service__["a" /* SessionService */]])
    ], DetailKendaraanComponent);
    return DetailKendaraanComponent;
}());

var FuseConfirmDialog = /** @class */ (function () {
    function FuseConfirmDialog(dialogRef, data, http, snackBar, router, location, _sessionService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.http = http;
        this.snackBar = snackBar;
        this.router = router;
        this.location = location;
        this._sessionService = _sessionService;
        this.token = this._sessionService.get().token;
    }
    FuseConfirmDialog.prototype.ngOnInit = function () {
    };
    FuseConfirmDialog.prototype.confirm = function () {
        var _this = this;
        var st;
        if (this.data.status == 1) {
            st = {
                "status": 0
            };
        }
        else {
            st = {
                "status": 1
            };
        }
        var options = new __WEBPACK_IMPORTED_MODULE_14__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        this.http.put(__WEBPACK_IMPORTED_MODULE_15__environments_environment__["a" /* environment */].setting.base_url + "/admin/kendaraan/status/" + this.data.kendaraan.id, st, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data['status']) {
                _this.snackBar.open('Update status sukses', 'OK', {
                    verticalPosition: 'top',
                    duration: 5000
                });
                if (_this.data.status == 1) {
                    _this.dialogRef.close(false);
                }
                else {
                    _this.dialogRef.close(true);
                }
            }
            else {
                _this.dialogRef.close();
                _this.snackBar.open(data["message"], 'OK', {
                    verticalPosition: 'top',
                    duration: 5000
                });
            }
        });
    };
    FuseConfirmDialog.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_14__angular_http__["b" /* Headers */]();
        headers.append('Authorization', 'Bearer ' + this.token);
        options.headers = headers;
    };
    FuseConfirmDialog = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-confirm-dialog',
            template: __webpack_require__("../../../../../src/app/main/content/apps/kendaraan/detailKendaraan/confirm-dialog.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/kendaraan/detailKendaraan/detailKendaraan.component.scss")]
        }),
        __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_11__angular_material__["e" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_11__angular_material__["o" /* MatDialogRef */], Object, __WEBPACK_IMPORTED_MODULE_14__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_11__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_12__angular_router__["f" /* Router */],
            __WEBPACK_IMPORTED_MODULE_13__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_16__session_service__["a" /* SessionService */]])
    ], FuseConfirmDialog);
    return FuseConfirmDialog;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/kendaraan/detailKendaraan/detailKendaraan.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Product; });
var Product = /** @class */ (function () {
    function Product(product) {
        if (product) {
            product = product;
            this.id = product.id;
            this.rute_id = product.rute_id;
            this.jenis = product.jenis;
            this.nomor = product.nomor;
            this.nama = product.nama;
            this.baris = product.baris;
            this.kolom = product.kolom;
            this.status = product.status;
            this.jumlah = 0;
            this.kapasitas = product.kapasitas;
            this.rute = product.rute;
            this.kursis = product.kursis;
            this.kuota = product.kuota;
        }
        else {
            product = {};
            this.id = 0;
            this.rute_id = 0;
            this.jenis = 1;
            this.nomor = 'nomor';
            this.nama = 'nama';
            this.baris = 12;
            this.kolom = 5;
            this.jumlah = 1;
            this.status = 0;
            this.kapasitas = [];
            this.rute = [];
            this.kursis = [];
        }
    }
    return Product;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/kendaraan/detailKendaraan/detailKendaraan.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailKendaraanService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DetailKendaraanService = /** @class */ (function () {
    function DetailKendaraanService(http, snackBar, _sessionService) {
        this.http = http;
        this.snackBar = snackBar;
        this._sessionService = _sessionService;
        this.onProductChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({});
        this.token = this._sessionService.get().token;
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DetailKendaraanService.prototype.resolve = function (route, state) {
        var _this = this;
        this.routeParams = route.params;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getProduct()
            ]).then(function () {
                resolve();
            }, reject);
        });
    };
    DetailKendaraanService.prototype.getProduct = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.routeParams.id === 'new') {
                _this.onProductChanged.next(false);
                resolve(false);
            }
            else {
                var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
                _this.setHeader(options);
                _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/kendaraan/' + _this.routeParams.id, options)
                    .map(function (res) { return res.json(); })
                    .subscribe(function (response) {
                    _this.product = response['data'];
                    _this.onProductChanged.next(_this.product);
                    resolve(response);
                }, function (err) {
                    var eror = JSON.parse(err._body);
                    _this.snackBar.open(eror.message, 'Error !', {
                        verticalPosition: 'top',
                        duration: 5000
                    });
                    reject(err);
                });
            }
        });
    };
    DetailKendaraanService.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        options.headers = headers;
    };
    DetailKendaraanService.prototype.saveProduct = function (product) {
        var _this = this;
        var object = {
            rute_id: product.rute_id,
            jenis: product.jenis,
            nomor: product.nomor,
            nama: product.nama,
            baris: product.baris,
            kolom: product.kolom
        };
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.put(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/kendaraan/' + _this.routeParams.id, object, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (err) {
                var eror = JSON.parse(err._body);
                _this.snackBar.open(eror.message, 'Error !', {
                    verticalPosition: 'top',
                    duration: 5000
                });
                reject(err);
            });
        });
    };
    DetailKendaraanService.prototype.addProduct = function (product) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/kendaraan', product, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (err) {
                var eror = JSON.parse(err._body);
                _this.snackBar.open(eror.message, 'Error !', {
                    verticalPosition: 'top',
                    duration: 5000
                });
                reject(err);
            });
        });
    };
    DetailKendaraanService.prototype.hapus = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.delete(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/kendaraan/' + _this.routeParams.id, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetailKendaraanService.prototype.printKendaraan = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        var header = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        header.append('Authorization', "Bearer " + this.token);
        header.append('Content-Type', 'application/json');
        header.append('Accept', 'application/pdf');
        options.headers = header;
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + "/admin/kendaraan/printDenah/" + _this.routeParams.id, {
                headers: header, responseType: __WEBPACK_IMPORTED_MODULE_3__angular_http__["i" /* ResponseContentType */].Blob
            })
                .map(function (res) {
                return {
                    filename: "kendaraan-" + _this.product.nomor + ".pdf",
                    data: res.blob(),
                    success: true
                };
            })
                .subscribe(function (res) {
                var url = window.URL.createObjectURL(res.data);
                var a = document.createElement('a');
                document.body.appendChild(a);
                a.setAttribute('style', 'display: none');
                a.href = url;
                a.download = res.filename;
                a.click();
                window.URL.revokeObjectURL(url);
                a.remove(); // remove the element
                resolve(res);
            }, function (err) {
                reject(err);
                console.log('download error:', JSON.stringify(err));
            }, function () {
            });
        });
    };
    DetailKendaraanService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_6__session_service__["a" /* SessionService */]])
    ], DetailKendaraanService);
    return DetailKendaraanService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/kendaraan/kendaraan.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KendaraanModule", function() { return KendaraanModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__ = __webpack_require__("../../../../@swimlane/ngx-charts/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__ = __webpack_require__("../../../../../src/app/core/modules/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__ = __webpack_require__("../../../../../src/app/core/components/widget/widget.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__daftarKendaraan_daftarKendaraan_component__ = __webpack_require__("../../../../../src/app/main/content/apps/kendaraan/daftarKendaraan/daftarKendaraan.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__daftarKendaraan_daftarKendaraan_service__ = __webpack_require__("../../../../../src/app/main/content/apps/kendaraan/daftarKendaraan/daftarKendaraan.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__detailKendaraan_detailKendaraan_component__ = __webpack_require__("../../../../../src/app/main/content/apps/kendaraan/detailKendaraan/detailKendaraan.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__detailKendaraan_detailKendaraan_service__ = __webpack_require__("../../../../../src/app/main/content/apps/kendaraan/detailKendaraan/detailKendaraan.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__core_components_confirm_dialog_confirm_dialog_component__ = __webpack_require__("../../../../../src/app/core/components/confirm-dialog/confirm-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__auth_guard__ = __webpack_require__("../../../../../src/app/main/content/apps/auth.guard.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var routes = [
    {
        path: 'daftarKendaraan',
        component: __WEBPACK_IMPORTED_MODULE_5__daftarKendaraan_daftarKendaraan_component__["a" /* DaftarKendaraanComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_11__auth_guard__["a" /* AuthGuard */]],
        resolve: {
            data: __WEBPACK_IMPORTED_MODULE_6__daftarKendaraan_daftarKendaraan_service__["a" /* DaftarKendaraanService */]
        }
    },
    {
        path: 'detailKendaraan/:id',
        component: __WEBPACK_IMPORTED_MODULE_7__detailKendaraan_detailKendaraan_component__["a" /* DetailKendaraanComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_11__auth_guard__["a" /* AuthGuard */]],
        resolve: {
            data: __WEBPACK_IMPORTED_MODULE_8__detailKendaraan_detailKendaraan_service__["a" /* DetailKendaraanService */]
        }
    }
];
var KendaraanModule = /** @class */ (function () {
    function KendaraanModule() {
    }
    KendaraanModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__["a" /* FuseWidgetModule */],
                __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__["NgxChartsModule"],
                __WEBPACK_IMPORTED_MODULE_10__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
                })
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__daftarKendaraan_daftarKendaraan_component__["a" /* DaftarKendaraanComponent */],
                __WEBPACK_IMPORTED_MODULE_9__core_components_confirm_dialog_confirm_dialog_component__["a" /* FuseConfirmDialogComponent */],
                __WEBPACK_IMPORTED_MODULE_7__detailKendaraan_detailKendaraan_component__["b" /* FuseConfirmDialog */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__daftarKendaraan_daftarKendaraan_component__["a" /* DaftarKendaraanComponent */],
                __WEBPACK_IMPORTED_MODULE_7__detailKendaraan_detailKendaraan_component__["a" /* DetailKendaraanComponent */],
                __WEBPACK_IMPORTED_MODULE_7__detailKendaraan_detailKendaraan_component__["b" /* FuseConfirmDialog */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__daftarKendaraan_daftarKendaraan_service__["a" /* DaftarKendaraanService */],
                __WEBPACK_IMPORTED_MODULE_8__detailKendaraan_detailKendaraan_service__["a" /* DetailKendaraanService */]
            ]
        })
    ], KendaraanModule);
    return KendaraanModule;
}());



/***/ })

});
//# sourceMappingURL=kendaraan.module.chunk.js.map