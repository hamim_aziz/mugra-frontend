import {Component, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {merge} from 'rxjs/observable/merge';
import {of as observableOf} from 'rxjs/observable/of';
import {catchError} from 'rxjs/operators/catchError';
import {map} from 'rxjs/operators/map';
import {startWith} from 'rxjs/operators/startWith';
import {switchMap} from 'rxjs/operators/switchMap';
import { fuseAnimations } from '../../../../../core/animations';
import { environment } from './../../../../../../environments/environment';
import { Router } from '@angular/router';
import { DaftarTiketService } from './daftarTiket.service';

@Component({
    selector   : 'fuse-e-commerce-products',
    templateUrl: './daftarTiket.component.html',
    styleUrls  : ['./daftarTiket.component.scss'],
    animations : fuseAnimations
})
export class DaftarTiketComponent implements AfterViewInit
{
    displayedColumns = ['id', 'kode_tiket', 'rute', 'tanggal', 'kendaraan', 'telp', 'dewasa', 'anak', 'status'];
  exampleDatabase: ExampleHttpDao | null;
  dataSource = new MatTableDataSource();

  resultsLength = 0;
  isLoadingResults = false;
  isRateLimitReached = false;

  search_query: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('filter') filter: ElementRef;

  constructor(private http: HttpClient, private router: Router, private _daftarTiketService: DaftarTiketService) {}

  ngAfterViewInit() {
    this.exampleDatabase = new ExampleHttpDao(this.http);

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.exampleDatabase!.getPemudik(
            this.sort.active, this.sort.direction, this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          this.resultsLength = data.data['total'];

          return data.data;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe(data => {
          this.dataSource.data = data['data'];
      });
    }

    clickTiket(tiket, status): void{
        if (status == 1) {
          const fileURL = environment.setting.base_url + '/tiket/' + tiket + '/download';
          window.open(fileURL, '_blank');
        }
    }

    clear(){
        this.paginator.pageIndex = 0;
        merge(this.sort.sortChange, this.paginator.page)
          .pipe(
            startWith({}),
            switchMap(() => {
              this.isLoadingResults = true;
              return this.exampleDatabase!.getPemudik(
                this.sort.active, this.sort.direction, this.paginator.pageIndex);
            }),
            map(data => {
              // Flip flag to show that loading has finished.
              this.isLoadingResults = false;
              this.isRateLimitReached = false;
              this.resultsLength = data.data['total'];

              return data.data;
            }),
            catchError(() => {
              this.isLoadingResults = false;
              // Catch if the GitHub API has reached its rate limit. Return empty data.
              this.isRateLimitReached = true;
              return observableOf([]);
            })
          ).subscribe(data => {
              this.dataSource.data = data['data'];
          });
    }

    search(){
        let search = this.search_query;
        merge(this.sort.sortChange, this.paginator.page)
          .pipe(
            startWith({}),
            switchMap(() => {
              this.isLoadingResults = true;
              return this.exampleDatabase!.getPemudikSearch(
                this.sort.active, this.sort.direction, search);
            }),
            map(data => {
              // Flip flag to show that loading has finished.
              this.isLoadingResults = false;
              this.isRateLimitReached = false;
              this.resultsLength = data.data['total'];

              return data.data;
            }),
            catchError(() => {
              this.isLoadingResults = false;
              // Catch if the GitHub API has reached its rate limit. Return empty data.
              this.isRateLimitReached = true;
              return observableOf([]);
            })
          ).subscribe(data => {
              this.dataSource.data = data['data'];
          });
    }

    bulan(date){
        if (date.substring(5, 7) === '01') {
            return "Januari";
        } else if (date.substring(5, 7) === '02') {
            return "Februari";
        } else if (date.substring(5, 7) === '03') {
            return "Maret";
        } else if (date.substring(5, 7) === '04') {
            return "April";
        } else if (date.substring(5, 7) === '05') {
            return "Mei";
        } else if (date.substring(5, 7) === '06') {
            return "Juni";
        } else if (date.substring(5, 7) === '07') {
            return "Juli";
        } else if (date.substring(5, 7) === '08') {
            return "Agustus";
        } else if (date.substring(5, 7) === '09') {
            return "September";
        } else if (date.substring(5, 7) === '10') {
            return "Oktober";
        } else if (date.substring(5, 7) === '11') {
            return "November";
        } else if (date.substring(5, 7) === '12') {
            return "Desember";
        }
    }
}

export interface pemudikApi {
  data: pemudikApiData[];
  total: number;
}

export interface pemudikApiData {
  data: any;
  total: number;
}

/** An example database that the data source uses to retrieve data for the table. */
export class ExampleHttpDao {
  constructor(private http: HttpClient) {}

  getPemudik(sort: string, order: string, page: number): Observable<pemudikApi> {
    const href = environment.setting.base_url + '/admin/tiket/1';
    const requestUrl =
        `${href}?page=${page + 1}`;

    return this.http.get<pemudikApi>(requestUrl, httpOptions);
  }

  getPemudikSearch(sort: string, order: string, search: string): Observable<pemudikApi> {
    const href = environment.setting.base_url + '/admin/tiket/search/'+search;

    return this.http.get<pemudikApi>(href, httpOptions);
  }

}

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': "Bearer " + JSON.parse(localStorage.getItem('currentUser')).token
  })
};
