import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { DetailContactService } from './detailContact.service';
import { fuseAnimations } from '../../../../../core/animations';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Contact } from './detailContact.model';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { FuseConfirmDialogComponent } from './../../../../../core/components/confirm-dialog/confirm-dialog.component';
import swal from 'sweetalert2';
import { SessionService } from './../../session.service';

@Component({
    selector     : 'fuse-e-commerce-contact',
    templateUrl  : './detailContact.component.html',
    styleUrls    : ['./detailContact.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class DetailContactComponent implements OnInit, OnDestroy
{
    contact = new Contact();
    onContactChanged: Subscription;
    pageType: string;
    contactForm: FormGroup;
    token = this._sessionService.get().token;
    role = this._sessionService.get().role;

    constructor(
        private contactService: DetailContactService,
        private formBuilder: FormBuilder,
        public snackBar: MatSnackBar,
        private location: Location,
        private http: Http,
        private route: ActivatedRoute,
        private dialog: MatDialog,
        private router: Router,
        private _sessionService: SessionService
    )
    {

    }

    ngOnInit()
    {
        // Subscribe to update contact on changes
        this.onContactChanged =
            this.contactService.onContactChanged
                .subscribe(contact => {

                    if ( contact )
                    {
                        this.contact = new Contact(contact);
                        this.pageType = 'edit';
                    }
                    else
                    {
                        this.pageType = 'new';
                        this.contact = new Contact();
                    }

                    this.contactForm = this.createContactForm();
                });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }

    createContactForm()
    {
        return this.formBuilder.group({
            name              : new FormControl(this.contact.name, [Validators.required]),
            phone             : new FormControl(this.contact.phone, [Validators.required]),
            link              : new FormControl(this.contact.link, [Validators.required]),
        });
    }

    saveContact()
    {
        const data = this.contactForm.getRawValue();
        this.contactService.saveContact(data)
            .then((response: any) => {

                // Change the location with new one
                if (response.status) {
                    this.location.back();
                    swal({
                        title: 'Update Contact Berhasil!',
                        text: 'Contact ' + data.name + ' Telah Update',
                        type: 'success',
                        timer: 2000
                    })
                } else {
                    swal({
                        title: 'Edit Contact Gagal!',
                        text: response.message,
                        type: 'error'
                    })
                }
            });
    }

    salinContact()
    {
        this.pageType = "salin";
        this.contactForm.controls.jenis.disable();
        this.contactForm.controls.tujuan.disable();
        this.contactForm.controls.asal.disable();
    }

    addContact()
    {
        const data = this.contactForm.getRawValue();
        this.contactService.addContact(data)
            .then((response: any) => {

                this.contactService.onContactChanged.next(data);

                if (response.status) {
                    this.location.back();
                    swal({
                        title: 'Tambah Contact Berhasil!',
                        text: 'Contact ' + data.name + ' Telah Ditambahkan',
                        type: 'success',
                        timer: 2000
                    })
                } else {
                    swal({
                        title: 'Tambah Contact Gagal!',
                        text: response.message,
                        type: 'error'
                    })
                }
            })
            .catch((error) => {
                swal({
                    title: 'Error !',
                    text: 'Internal server error',
                    type: 'error'
                })
            });
    }

    ngOnDestroy()
    {
        this.onContactChanged.unsubscribe();
    }

    hapus(): void{
        swal({
            title: 'Hapus Contact' + this.contact.name,
            text: "Contact yang telah dihapus tidak bisa dikembalikan lagi",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus !',
            cancelButtonText: 'Batal!',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                this.contactService.hapus(this.contactForm.getRawValue())
                    .then((response: any) => {
                        if (response.status) {
                            this.back();
                            swal({
                                title: 'Hapus Contact Berhasil!',
                                text: 'Contact' + this.contact.name + ' Telah Dihapus',
                                type: 'success',
                                timer: 2000
                            })
                        } else {
                            swal(
                                'Hapus Contact Gagal!',
                                response.message,
                                'error'
                            )
                        }
                    })
            }
        })
    }

    back(){
        this.location.back();
    }
}
