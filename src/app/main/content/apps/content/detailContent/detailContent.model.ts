import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatChipInputEvent } from '@angular/material';

export class Content
{
    id: number;
    label: string;
    content: string;

    constructor(content?)
    {
        if (content) {
            content = content;
            this.id = content.id;
            this.label = content.label;
            this.content = content.content;
        } else {
            content = {};
            this.id = 0;
            this.label = "";
            this.content = "";
        }
    }

    // addCategory(event: MatChipInputEvent): void
    // {
    //     const input = event.input;
    //     const value = event.value;

    //     // Add category
    //     if ( value )
    //     {
    //         this.categories.push(value);
    //     }

    //     // Reset the input value
    //     if ( input )
    //     {
    //         input.value = '';
    //     }
    // }

    // removeCategory(category)
    // {
    //     const index = this.categories.indexOf(category);

    //     if ( index >= 0 )
    //     {
    //         this.categories.splice(index, 1);
    //     }
    // }

    // addTag(event: MatChipInputEvent): void
    // {
    //     const input = event.input;
    //     const value = event.value;

    //     // Add tag
    //     if ( value )
    //     {
    //         this.tags.push(value);
    //     }

    //     // Reset the input value
    //     if ( input )
    //     {
    //         input.value = '';
    //     }
    // }

    // removeTag(tag)
    // {
    //     const index = this.tags.indexOf(tag);

    //     if ( index >= 0 )
    //     {
    //         this.tags.splice(index, 1);
    //     }
    // }
}
