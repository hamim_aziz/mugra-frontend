import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatChipInputEvent } from '@angular/material';

export class Product
{
    id: number;
    nik: string;
    no_kk: string;
    comment: string;
    nilai: number;
    status: number;

    constructor(product?)
    {
        if (product) {
            product = product;
            this.id = product.id;
            this.nik = product.nik;
            this.no_kk = product.no_kk;
            this.comment = product.comment;
            this.nilai = product.nilai;
            this.status = product.status;
        } else {
            product = {};
            this.id = 0;
            this.nik = '';
            this.no_kk = '';
            this.comment = '';
            this.nilai = 0;
            this.status = 0;
        }
    }

}
