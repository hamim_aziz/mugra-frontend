import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
// import { DetailTiketService } from './detailTiket.service';
import { fuseAnimations } from '../../../../../core/animations';
import { Router, ParamMap, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Product } from './detailTiket.model';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar } from '@angular/material';
import { Location } from '@angular/common';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from './../../../../../../environments/environment';

@Component({
    selector     : 'fuse-e-commerce-product',
    templateUrl  : './detailTiket.component.html',
    styleUrls    : ['./detailTiket.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class DetailTiketComponent implements OnInit, OnDestroy
{
    token = sessionStorage.getItem('token');
    pageType: string;
    isLinear = false;
    editable = true;
    tujuans: any = [];
    asal: any = [];
    nomor_tiket : any;
    dewasa = [];
    dewasa1 = [];
    dewasa2 = [];
    elementType : 'url' | 'canvas' | 'img' = 'url';
    tiket1: any;
    tiket2: any;
    tiket: any;
    

    constructor(
        private formBuilder: FormBuilder,
        public snackBar: MatSnackBar,
        private location: Location,
        private http: Http,
        private router: Router,
        private route: ActivatedRoute
    )
    {

    }

    ngOnInit()
    {
        if (this.route.snapshot.queryParamMap['params']['tiket'] != "0") {
            this.getTiket(this.route.snapshot.queryParamMap['params']['tiket']);
        }
        if (this.route.snapshot.queryParamMap['params']['mudik'] != "0") {
            this.getTiketMudik(this.route.snapshot.queryParamMap['params']['mudik']);
        }
        if (this.route.snapshot.queryParamMap['params']['balik'] != "0") {
            this.getTiketBalik(this.route.snapshot.queryParamMap['params']['balik']);
        }
    }

    getTiket(tiket){
        let options = new RequestOptions();
        this.setHeader(options);
        this.http.get(environment.setting.base_url + '/pemudik/booking/' + tiket, options)
            .map((res: Response) => res.json())
            .subscribe((response: any) => {
                this.tiket = response['data'];
                for (var i = 0; i < this.tiket.pemudik_dewasa; ++i) {
                    this.dewasa.push(i);
                }
            })
    }

    getTiketMudik(tiket){
        let options = new RequestOptions();
        this.setHeader(options);
        this.http.get(environment.setting.base_url + '/pemudik/booking/' + tiket, options)
            .map((res: Response) => res.json())
            .subscribe((response: any) => {
                this.tiket1 = response['data'];
                console.log(this.tiket1);
                for (var i = 0; i < this.tiket1.pemudik_dewasa; ++i) {
                    this.dewasa1.push(i);
                }
            })
    }

    getTiketBalik(tiket){
        let options = new RequestOptions();
        this.setHeader(options);
        this.http.get(environment.setting.base_url + '/pemudik/booking/' + tiket, options)
            .map((res: Response) => res.json())
            .subscribe((response: any) => {
                this.tiket2 = response['data'];
                console.log(this.tiket2);
                for (var i = 0; i < this.tiket2.pemudik_dewasa; ++i) {
                    this.dewasa2.push(i);
                }
            })
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.token);
        headers.append('Content-Type', 'application/json');
        options.headers = headers;
    }

    ngOnDestroy()
    {
        // this.onProductChanged.unsubscribe();
    }

    print(){
        window['print']();
    }

    bulan(date){
        if (date.substring(5, 7) === '01') {
            return "Januari";
        } else if (date.substring(5, 7) === '02') {
            return "Februari";
        } else if (date.substring(5, 7) === '03') {
            return "Maret";
        } else if (date.substring(5, 7) === '04') {
            return "April";
        } else if (date.substring(5, 7) === '05') {
            return "Mei";
        } else if (date.substring(5, 7) === '06') {
            return "Juni";
        } else if (date.substring(5, 7) === '07') {
            return "Juli";
        } else if (date.substring(5, 7) === '08') {
            return "Agustus";
        } else if (date.substring(5, 7) === '09') {
            return "September";
        } else if (date.substring(5, 7) === '10') {
            return "Oktober";
        } else if (date.substring(5, 7) === '11') {
            return "November";
        } else if (date.substring(5, 7) === '12') {
            return "Desember";
        }
    }

    back(){
        this.location.back();
    }

}
