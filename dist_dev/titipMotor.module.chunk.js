webpackJsonp(["titipMotor.module"],{

/***/ "../../../../../src/app/main/content/apps/titip_motor/daftarTitipMotor/daftarTitipMotor.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"products\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\"\r\n             fxLayout=\"column\" fxLayoutAlign=\"center center\"\r\n             fxLayout.gt-xs=\"row\" fxLayoutAlign.gt-xs=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div class=\"logo my-12 m-sm-0\"\r\n                 fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <mat-icon class=\"logo-icon mr-16\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'50ms',scale:'0.2'}}\">navigation</mat-icon>\r\n                <span class=\"logo-text h1\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">Daftar Titipan Motor</span>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <!-- SEARCH -->\r\n            <div class=\"search-input-wrapper mx-12 m-md-0\"\r\n                 fxFlex=\"1 0 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n                <label for=\"search\" class=\"mr-8\">\r\n                    <mat-icon class=\"secondary-text\">search</mat-icon>\r\n                </label>\r\n                <mat-form-field floatPlaceholder=\"never\" fxFlex=\"1 0 auto\">\r\n                    <input id=\"search\" matInput #filter placeholder=\"Search\">\r\n                </mat-form-field>\r\n            </div>\r\n            <!-- / SEARCH -->\r\n\r\n            <div class=\"my-12\">\r\n                <button mat-raised-button class=\"add-product-button m-sm-0\" [matMenuTriggerFor]=\"filter\" fxHide.xs>\r\n                    <span>Filter</span>\r\n                </button>\r\n\r\n                <button mat-raised-button class=\"add-product-button m-sm-0\" [matMenuTriggerFor]=\"option\" fxHide fxShow.xs>\r\n                    <span>Option</span>\r\n                </button>\r\n\r\n                <mat-menu #option=\"matMenu\">\r\n                    <button mat-menu-item [matMenuTriggerFor]=\"filter\">Filter</button>\r\n                    <button mat-menu-item [routerLink]=\"'/apps/titipMotor/detailTitipMotor/new'\" [disabled]=\"role < 3\">Pendaftaran</button>\r\n                </mat-menu>\r\n\r\n                <mat-menu #filter=\"matMenu\">\r\n                    <button mat-menu-item  (click)=\"filterButton('')\">Semua</button>\r\n                    <button mat-menu-item [matMenuTriggerFor]=\"status\">Status</button>\r\n                    <button mat-menu-item [matMenuTriggerFor]=\"tanggal\">Tanggal</button>\r\n                    <button mat-menu-item [matMenuTriggerFor]=\"jenis\">Jenis Mudik</button>\r\n                    <button mat-menu-item  (click)=\"filterButton('Mudik Motor')\">Mudik Motor</button>\r\n                </mat-menu>\r\n\r\n                <mat-menu #status=\"matMenu\">\r\n                    <button (click)=\"filterButton('Siap Mudik')\" mat-menu-item>Siap Mudik</button>\r\n                    <button (click)=\"filterButton('Belum Siap')\" mat-menu-item>Belum Siap</button>\r\n                </mat-menu>\r\n\r\n                <mat-menu #jenis=\"matMenu\">\r\n                    <button (click)=\"filterButton('Mudik')\" mat-menu-item>Mudik</button>\r\n                    <button (click)=\"filterButton('Balik')\" mat-menu-item>Balik</button>\r\n                </mat-menu>\r\n\r\n                <mat-menu #tanggal=\"matMenu\" fusePerfectScrollbar>\r\n                    <button *ngFor=\"let tgl of dateIndo\" (click)=\"filterButton(tgl[0])\" mat-menu-item> {{ tgl[1] }} </button>\r\n                </mat-menu>\r\n            </div>\r\n\r\n            <div class=\"my-12\" fxHide.xs>\r\n\r\n                <button mat-raised-button\r\n                        [routerLink]=\"'/apps/titipMotor/detailTitipMotor/new'\"\r\n                        class=\"add-product-button m-sm-0\"\r\n                        [disabled]=\"role < 3\">\r\n                    <span>Pendaftaran</span>\r\n                </button>\r\n            </div>\r\n\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <mat-table class=\"products-table\"\r\n                       #table [dataSource]=\"dataSource\"\r\n                       matSort\r\n                       [@animateStagger]=\"{value:'50'}\"\r\n                       fusePerfectScrollbar>\r\n\r\n                <!-- ID Column -->\r\n                <ng-container cdkColumnDef=\"id\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>No.</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product; let i = index\">\r\n                        <p class=\"text-truncate\">{{ (paginator.pageIndex * paginator.pageSize) + (i + 1) }}.</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Nama Column -->\r\n                <ng-container cdkColumnDef=\"nama\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Nama</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"text-truncate\">{{product.nama}}</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Id_rute Column -->\r\n                <ng-container cdkColumnDef=\"id_rute\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>ID Rute</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"text-truncate\">{{product.rute_id}}</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Alamat Column -->\r\n                <ng-container cdkColumnDef=\"alamat\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Alamat</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"category text-truncate\"> {{product.alamat}} </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- No HP Column -->\r\n                <ng-container cdkColumnDef=\"no_hp\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>No HP</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"price text-truncate\">\r\n                            {{ product.no_hp }}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- No_kendaraan Motor -->\r\n                <ng-container cdkColumnDef=\"no_kendaraan\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxHide fxShow.gt-sm>No Kendaraan</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxHide fxShow.gt-sm>\r\n                        <p class=\"price text-truncate\">\r\n                            {{ product.no_kendaraan }}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- no_id_card -->\r\n                <ng-container cdkColumnDef=\"no_id_card\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>ID Card</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"price text-truncate\">\r\n                            {{ product.no_id_card }}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <mat-header-row *cdkHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n\r\n                <mat-row *cdkRowDef=\"let product; columns: displayedColumns;\"\r\n                         class=\"product\"\r\n                         matRipple\r\n                         (click)=\"clickTitipMotor(product.id, product.asal, product.tujuan)\">\r\n                </mat-row>\r\n\r\n            </mat-table>\r\n\r\n            <mat-paginator #paginator\r\n                           [length]=\"dataSource.filteredData.length\"\r\n                           [pageSize]=\"25\"\r\n                           [pageSizeOptions]=\"[5, 10, 25, 100]\">\r\n            </mat-paginator>\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/titip_motor/daftarTitipMotor/daftarTitipMotor.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n:host .header .search-input-wrapper {\n  max-width: 480px; }\n:host .header .add-product-button {\n  background-color: #F96D01; }\n@media (max-width: 599px) {\n  :host .header {\n    height: 176px !important;\n    min-height: 176px !important;\n    max-height: 176px !important; } }\n@media (max-width: 599px) {\n  :host .top-bg {\n    height: 240px; } }\n:host .products-table {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  border-bottom: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-header-row {\n    min-height: 35px;\n    min-width: 500px;\n    background-color: rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-row {\n    min-width: 500px; }\n:host .products-table .product {\n    position: relative;\n    cursor: pointer;\n    height: 60px; }\n:host .products-table .mat-cell {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n:host .products-table .mat-column-id {\n    min-width: 0;\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 30px;\n            flex: 0 1 30px; }\n:host .products-table .mat-column-nama {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 15%;\n            flex: 0 1 15%; }\n:host .products-table .mat-column-alamat {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 30%;\n            flex: 0 1 30%; }\n:host .products-table .mat-column-buttons {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 80px;\n            flex: 0 1 80px; }\n:host .products-table .quantity-indicator {\n    display: inline-block;\n    vertical-align: middle;\n    width: 8px;\n    height: 8px;\n    border-radius: 4px;\n    margin-right: 8px; }\n:host .products-table .quantity-indicator + span {\n      display: inline-block;\n      vertical-align: middle; }\n:host .products-table .active-icon {\n    border-radius: 50%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/titip_motor/daftarTitipMotor/daftarTitipMotor.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarTitipMotorComponent; });
/* unused harmony export FilesDataSource */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__daftarTitipMotor_service__ = __webpack_require__("../../../../../src/app/main/content/apps/titip_motor/daftarTitipMotor/daftarTitipMotor.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_cdk_collections__ = __webpack_require__("../../../cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs_add_observable_fromEvent__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/fromEvent.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__core_fuseUtils__ = __webpack_require__("../../../../../src/app/core/fuseUtils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var DaftarTitipMotorComponent = /** @class */ (function () {
    function DaftarTitipMotorComponent(productsService, dialog, router, _sessionService) {
        this.productsService = productsService;
        this.dialog = dialog;
        this.router = router;
        this._sessionService = _sessionService;
        this.hapus = false;
        this.displayedColumns = ['id', 'nama', 'id_rute', 'alamat', 'no_hp', 'no_kendaraan', 'no_id_card'];
        this.role = this._sessionService.get().role;
        this.titipMotors = this.productsService.products;
        this.date = [];
        this.dateIndo = [];
    }
    DaftarTitipMotorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._tanggal();
        this.dataSource = new FilesDataSource(this.productsService, this.paginator, this.sort);
        __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].fromEvent(this.filter.nativeElement, 'keyup')
            .debounceTime(150)
            .distinctUntilChanged()
            .subscribe(function () {
            if (!_this.dataSource) {
                return;
            }
            _this.dataSource.filter = _this.filter.nativeElement.value;
        });
    };
    DaftarTitipMotorComponent.prototype._tanggal = function () {
        for (var i = 0; i < this.titipMotors.length; ++i) {
            if (this.date.indexOf(this.titipMotors[i]["tanggal"]) == -1) {
                this.dateIndo.push([this.titipMotors[i]["tanggal"], this.convertTanggal(this.titipMotors[i]["tanggal"])]);
                this.date.push(this.titipMotors[i]["tanggal"]);
            }
        }
    };
    DaftarTitipMotorComponent.prototype.convertTanggal = function (tgl) {
        var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
        var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
        var date = new Date(tgl);
        var day = date.getDay();
        var month = date.getMonth();
        return hari[day] + ", " + date.getDate() + " " + bulan[month] + " " + date.getFullYear();
    };
    DaftarTitipMotorComponent.prototype.filterButton = function (filter) {
        this.dataSource.filter = filter;
    };
    DaftarTitipMotorComponent.prototype.semua = function () {
        this.dataSource.filter = "";
    };
    DaftarTitipMotorComponent.prototype.toggleHapus = function () {
        this.hapus == true ? this.hapus = false : this.hapus = true;
    };
    DaftarTitipMotorComponent.prototype.clickTitipMotor = function (id) {
        var url = '/apps/titipMotor/detailTitipMotor/' + id;
        if (this.role >= 1) {
            this.router.navigate([url]);
        }
    };
    DaftarTitipMotorComponent.prototype.bulan = function (date) {
        if (date.substring(5, 7) === '01') {
            return "Januari";
        }
        else if (date.substring(5, 7) === '02') {
            return "Februari";
        }
        else if (date.substring(5, 7) === '03') {
            return "Maret";
        }
        else if (date.substring(5, 7) === '04') {
            return "April";
        }
        else if (date.substring(5, 7) === '05') {
            return "Mei";
        }
        else if (date.substring(5, 7) === '06') {
            return "Juni";
        }
        else if (date.substring(5, 7) === '07') {
            return "Juli";
        }
        else if (date.substring(5, 7) === '08') {
            return "Agustus";
        }
        else if (date.substring(5, 7) === '09') {
            return "September";
        }
        else if (date.substring(5, 7) === '10') {
            return "Oktober";
        }
        else if (date.substring(5, 7) === '11') {
            return "November";
        }
        else if (date.substring(5, 7) === '12') {
            return "Desember";
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__angular_material__["z" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__angular_material__["z" /* MatPaginator */])
    ], DaftarTitipMotorComponent.prototype, "paginator", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('filter'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], DaftarTitipMotorComponent.prototype, "filter", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__angular_material__["M" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__angular_material__["M" /* MatSort */])
    ], DaftarTitipMotorComponent.prototype, "sort", void 0);
    DaftarTitipMotorComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-products',
            template: __webpack_require__("../../../../../src/app/main/content/apps/titip_motor/daftarTitipMotor/daftarTitipMotor.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/titip_motor/daftarTitipMotor/daftarTitipMotor.component.scss")],
            animations: __WEBPACK_IMPORTED_MODULE_4__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__daftarTitipMotor_service__["a" /* DaftarTitipMotorService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_material__["m" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_7__angular_router__["f" /* Router */],
            __WEBPACK_IMPORTED_MODULE_15__session_service__["a" /* SessionService */]])
    ], DaftarTitipMotorComponent);
    return DaftarTitipMotorComponent;
}());

var FilesDataSource = /** @class */ (function (_super) {
    __extends(FilesDataSource, _super);
    function FilesDataSource(productsService, _paginator, _sort) {
        var _this = _super.call(this) || this;
        _this.productsService = productsService;
        _this._paginator = _paginator;
        _this._sort = _sort;
        _this._filterChange = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        _this._filteredDataChange = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        _this.filteredData = _this.productsService.products;
        return _this;
    }
    Object.defineProperty(FilesDataSource.prototype, "filteredData", {
        get: function () {
            return this._filteredDataChange.value;
        },
        set: function (value) {
            this._filteredDataChange.next(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FilesDataSource.prototype, "filter", {
        get: function () {
            return this._filterChange.value;
        },
        set: function (filter) {
            this._filterChange.next(filter);
        },
        enumerable: true,
        configurable: true
    });
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    FilesDataSource.prototype.connect = function () {
        var _this = this;
        var displayDataChanges = [
            this.productsService.onProductsChanged,
            this._paginator.page,
            this._filterChange,
            this._sort.sortChange
        ];
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].merge.apply(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"], displayDataChanges).map(function () {
            var data = _this.productsService.products.slice();
            data = _this.filterData(data);
            _this.filteredData = data.slice();
            data = _this.sortData(data);
            // Grab the page's slice of data.
            // if (!this.filter) {
            //     sessionStorage.setItem("pageIndexTitipMotor", String(this._paginator.pageIndex));
            // }
            var startIndex = _this._paginator.pageIndex * _this._paginator.pageSize;
            return data.splice(startIndex, _this._paginator.pageSize);
        });
    };
    FilesDataSource.prototype.filterData = function (data) {
        if (!this.filter) {
            return data;
        }
        return __WEBPACK_IMPORTED_MODULE_14__core_fuseUtils__["a" /* FuseUtils */].filterArrayByString(data, this.filter);
    };
    FilesDataSource.prototype.sortData = function (data) {
        var _this = this;
        if (!this._sort.active || this._sort.direction === '') {
            return data;
        }
        return data.sort(function (a, b) {
            var propertyA = '';
            var propertyB = '';
            switch (_this._sort.active) {
                case 'id':
                    _a = [a.id, b.id], propertyA = _a[0], propertyB = _a[1];
                    break;
                case 'asal':
                    _b = [a.asal, b.asal], propertyA = _b[0], propertyB = _b[1];
                    break;
                case 'tujuan':
                    _c = [a.tujuan, b.tujuan], propertyA = _c[0], propertyB = _c[1];
                    break;
                case 'tanggal':
                    _d = [a.tanggal, b.tanggal], propertyA = _d[0], propertyB = _d[1];
                    break;
                case 'jenis':
                    _e = [a.jenis, b.jenis], propertyA = _e[0], propertyB = _e[1];
                    break;
                case 'kuota_motor':
                    _f = [a.kouta_motor, b.kouta_motor], propertyA = _f[0], propertyB = _f[1];
                    break;
                case 'status':
                    _g = [a.status, b.status], propertyA = _g[0], propertyB = _g[1];
                    break;
            }
            var valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            var valueB = isNaN(+propertyB) ? propertyB : +propertyB;
            return (valueA < valueB ? -1 : 1) * (_this._sort.direction === 'asc' ? 1 : -1);
            var _a, _b, _c, _d, _e, _f, _g;
        });
    };
    FilesDataSource.prototype.disconnect = function () {
    };
    return FilesDataSource;
}(__WEBPACK_IMPORTED_MODULE_2__angular_cdk_collections__["a" /* DataSource */]));



/***/ }),

/***/ "../../../../../src/app/main/content/apps/titip_motor/daftarTitipMotor/daftarTitipMotor.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarTitipMotorService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DaftarTitipMotorService = /** @class */ (function () {
    function DaftarTitipMotorService(http, _sessionService, snackBar) {
        this.http = http;
        this._sessionService = _sessionService;
        this.snackBar = snackBar;
        this.onProductsChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({});
        this.token = this._sessionService.get().token;
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DaftarTitipMotorService.prototype.resolve = function (route, state) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getProducts()
            ]).then(function () {
                resolve();
            }, reject)
                .catch(function (error) {
                var eror = JSON.parse(error._body);
                _this.snackBar.open(eror.message, eror.errors.email ? eror.errors.email : eror.errors.password, {
                    verticalPosition: 'top',
                    duration: 5000
                });
            });
        });
    };
    DaftarTitipMotorService.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        options.headers = headers;
    };
    DaftarTitipMotorService.prototype.getProducts = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        var role = Number(this._sessionService.get().role);
        var titipMotor_id = this._sessionService.get().titipMotor_id;
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/titipMotor', options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                console.log(response);
                _this.products = response['data']['data'];
                _this.onProductsChanged.next(_this.products);
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DaftarTitipMotorService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__session_service__["a" /* SessionService */],
            __WEBPACK_IMPORTED_MODULE_6__angular_material__["K" /* MatSnackBar */]])
    ], DaftarTitipMotorService);
    return DaftarTitipMotorService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/titip_motor/detailTitipMotor/detailTitipMotor.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"product\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"start center\" style=\"width: 80%\">\r\n\r\n                <button class=\"mr-0 mr-sm-16\" mat-icon-button (click)=\"back()\">\r\n                    <mat-icon>arrow_back</mat-icon>\r\n                </button>\r\n\r\n                <div fxLayout=\"column\" fxLayoutAlign=\"start start\"\r\n                     *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">\r\n                    <div class=\"h2\" *ngIf=\"pageType ==='edit' || pageType === 'salin'\">\r\n                        {{product.nama}}\r\n                    </div>\r\n                    <div class=\"h2\" *ngIf=\"pageType ==='new'\">\r\n                        Daftar Titip Motor\r\n                    </div>\r\n                    <div class=\"subtitle secondary-text\">\r\n                        <span>Detail Titipan Motor</span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"productForm.invalid || role < 3\"\r\n                    *ngIf=\"pageType ==='new'\" (click)=\"addProduct()\" fxHide.xs>\r\n                <span>Simpan</span>\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"saveProduct()\" fxHide.xs>\r\n                <span>Simpan</span>\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"print(product.no_tiket)\" fxHide.xs>\r\n                <span>Print</span>\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"hapus()\" fxHide.xs>\r\n                <span>Hapus</span>\r\n            </button>\r\n\r\n\r\n            <button mat-icon-button fxHide fxShow.xs [matMenuTriggerFor]=\"menu\">\r\n              <mat-icon>more_vert</mat-icon>\r\n            </button>\r\n            \r\n            <mat-menu #menu=\"matMenu\">\r\n              <button mat-menu-item [disabled]=\"productForm.invalid || role < 3\"\r\n                    *ngIf=\"pageType ==='new'\" (click)=\"addProduct()\">\r\n                <mat-icon>save_alt</mat-icon>\r\n                <span>Simpan</span>\r\n              </button>\r\n              <button mat-menu-item [disabled]=\"role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"saveProduct()\">\r\n                <mat-icon>save_alt</mat-icon>\r\n                <span>Simpan</span>\r\n              </button>\r\n              <button mat-menu-item [disabled]=\"role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"hapus()\">\r\n                <mat-icon>delete_forever</mat-icon>\r\n                <span>Hapus</span>\r\n              </button>\r\n            </mat-menu>\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <!-- CONTENT -->\r\n            <div class=\"content\">\r\n\r\n                <form name=\"productForm\" [formGroup]=\"productForm\" class=\"product w-100-p\" fxLayout=\"column\" fxFlex>\r\n\r\n                    <div class=\"tab-content p-24\" fusePerfectScrollbar>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                          <mat-select matInput \r\n                                      formControlName=\"rute_id\" \r\n                                      placeholder=\"Rute\" \r\n                                      required>\r\n                            <mat-option *ngFor=\"let rute of ruteAll\" [value]=\"rute.id\"> \r\n                                {{ rute.kota.asal }} - {{ rute.kota.tujuan }} - {{ rute.keterangan }} \r\n                            </mat-option>\r\n                          </mat-select>\r\n                          <mat-icon matPrefix> place </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                            <input matInput\r\n                                   name=\"nama\"\r\n                                   formControlName=\"nama\"\r\n                                   placeholder=\"Nama\"\r\n                                   required>\r\n                            <mat-error *ngIf=\"productForm.controls.nama.invalid\">\r\n                              {{productForm.controls.nama.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                            </mat-error>\r\n                            <mat-icon matPrefix> people </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                            <input matInput\r\n                                   name=\"alamat\"\r\n                                   formControlName=\"alamat\"\r\n                                   placeholder=\"Alamat\"\r\n                                   rquired>\r\n                            <mat-error *ngIf=\"productForm.controls.alamat.invalid\">\r\n                              {{productForm.controls.alamat.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                            </mat-error>\r\n                            <mat-icon matPrefix> place </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                            <input matInput\r\n                                   name=\"no_hp\"\r\n                                   formControlName=\"no_hp\"\r\n                                   placeholder=\"Nomor Hp\"\r\n                                   required>\r\n                            <mat-error *ngIf=\"productForm.controls.no_hp.invalid\">\r\n                                 {{productForm.controls.no_hp.errors['required'] ? 'Tidak Boleh Kosong': productForm.controls.no_hp.errors['pattern'] ? 'Format Nomor Telepon Tidak Valid' : ''}}\r\n                            </mat-error>\r\n                            <mat-icon matPrefix> smartphone </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                            <input matInput\r\n                                   name=\"no_kendaraan\"\r\n                                   formControlName=\"no_kendaraan\"\r\n                                   placeholder=\"Plat Nomor Kendaraan\"\r\n                                   required>\r\n                            <mat-hint>Contoh Format Nomor Kendaraan XX-1234-XXX </mat-hint>\r\n                            <mat-error *ngIf=\"productForm.controls.no_kendaraan.invalid\">\r\n                                 {{productForm.controls.no_kendaraan.errors['required'] ? 'Tidak Boleh Kosong': productForm.controls.no_kendaraan.errors['pattern'] ? 'Format Plat Nomor Tidak Valid' : ''}}\r\n                            </mat-error>\r\n                            <mat-icon matPrefix> motorcycle </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                            <input matInput\r\n                                   name=\"no_ktp\"\r\n                                   formControlName=\"no_ktp\"\r\n                                   placeholder=\"Nomor KTP\">\r\n                            <mat-icon matPrefix> credit_card </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                            <input matInput\r\n                                   name=\"no_sim\"\r\n                                   formControlName=\"no_sim\"\r\n                                   placeholder=\"Nomor SIM\">\r\n                            <mat-icon matPrefix> credit_card </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                    </div>\r\n                </form>\r\n\r\n            </div>\r\n            <!-- / CONTENT -->\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/titip_motor/detailTitipMotor/detailTitipMotor.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#product .header .product-image {\n  overflow: hidden;\n  width: 56px;\n  height: 56px;\n  border: 3px solid rgba(0, 0, 0, 0.12); }\n  #product .header .product-image img {\n    height: 100%;\n    width: auto;\n    max-width: none; }\n  #product .header .save-product-button {\n  background-color: #F96D01;\n  margin: 0px 20px; }\n  #product .header .subtitle {\n  margin: 6px 0 0 0; }\n  #product .content .example-container {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  max-height: 500px;\n  min-width: 300px; }\n  #product .content .w-100-p {\n  margin-bottom: 8px; }\n  #product .content .w-90-p {\n  margin-bottom: 8px; }\n  #product .content .mat-form-field-prefix .mat-icon, #product .content .mat-form-field-suffix .mat-icon {\n  margin-right: 10px; }\n  #product .content .mat-tab-group,\n#product .content .mat-tab-body-wrapper,\n#product .content .tab-content {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  max-width: 100%; }\n  #product .content .mat-tab-body-content {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n  #product .content .mat-tab-header {\n  height: 40px;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n  #product .content .mat-tab-label {\n  height: 64px; }\n  #product .content .product-image {\n  overflow: hidden;\n  width: 128px;\n  height: 128px;\n  margin-right: 16px;\n  margin-bottom: 16px;\n  border: 3px solid rgba(0, 0, 0, 0.12); }\n  #product .content .product-image img {\n    height: 100%;\n    width: auto;\n    max-width: none; }\n  #product .content .mat-card .mat-divider.mat-divider-inset {\n  position: absolute !important; }\n  #product .content .mat-list .mat-list-item .mat-divider.mat-divider-inset {\n  left: 0 !important;\n  width: 100% !important; }\n  #product .content .mat-grid-tile .mat-figure {\n  -webkit-box-pack: left !important;\n      -ms-flex-pack: left !important;\n          justify-content: left !important; }\n  #product .content .mat-list .mat-list-item {\n  height: unset !important; }\n  #product .content .mat-list .mat-list-item {\n  font-size: 13px !important; }\n  #product .content .mat-card-title {\n  font-size: 15px !important; }\n  #product .content .mat-card-actions, #product .content .mat-card-content, #product .content .mat-card-subtitle, #product .content .mat-card-title {\n  padding-bottom: 10px !important; }\n  #product .content .mat-card {\n  padding: 10px !important; }\n  #product .content .mat-card-subtitle, #product .content .mat-card-content, #product .content .mat-card-header .mat-card-title {\n  font-size: 12px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/titip_motor/detailTitipMotor/detailTitipMotor.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailTitipMotorComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__detailTitipMotor_service__ = __webpack_require__("../../../../../src/app/main/content/apps/titip_motor/detailTitipMotor/detailTitipMotor.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_observable_fromEvent__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/fromEvent.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__detailTitipMotor_model__ = __webpack_require__("../../../../../src/app/main/content/apps/titip_motor/detailTitipMotor/detailTitipMotor.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_16_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



















var DetailTitipMotorComponent = /** @class */ (function () {
    function DetailTitipMotorComponent(productService, formBuilder, snackBar, location, http, route, dialog, router, _sessionService) {
        this.productService = productService;
        this.formBuilder = formBuilder;
        this.snackBar = snackBar;
        this.location = location;
        this.http = http;
        this.route = route;
        this.dialog = dialog;
        this.router = router;
        this._sessionService = _sessionService;
        this.product = new __WEBPACK_IMPORTED_MODULE_9__detailTitipMotor_model__["a" /* Product */]();
        this.token = this._sessionService.get().token;
        this.role = this._sessionService.get().role;
    }
    DetailTitipMotorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.ruteAll = this.productService.ruteAll;
        // Subscribe to update product on changes
        this.onProductChanged =
            this.productService.onProductChanged
                .subscribe(function (product) {
                if (product) {
                    _this.product = new __WEBPACK_IMPORTED_MODULE_9__detailTitipMotor_model__["a" /* Product */](product);
                    _this.pageType = 'edit';
                }
                else {
                    _this.pageType = 'new';
                    _this.product = new __WEBPACK_IMPORTED_MODULE_9__detailTitipMotor_model__["a" /* Product */]();
                }
                _this.productForm = _this.createProductForm();
            });
    };
    DetailTitipMotorComponent.prototype.createProductForm = function () {
        return this.formBuilder.group({
            rute_id: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.rute_id, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            nama: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.nama, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            alamat: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.alamat, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            no_hp: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.no_hp, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].pattern(/^[0-9]{9,}$/)]),
            no_kendaraan: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.no_kendaraan, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].pattern(/^[A-Z]{1,2}\-[0-9]{1,4}\-[A-Z]{1,3}$/)]),
            no_ktp: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.no_ktp),
            no_sim: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.product.no_sim)
        });
    };
    DetailTitipMotorComponent.prototype.saveProduct = function () {
        var _this = this;
        var data = this.productForm.getRawValue();
        this.productService.saveProduct(data)
            .then(function (response) {
            if (response.status) {
                _this.productService.onProductChanged.next(data);
                _this.back();
                __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                    title: 'Update TitipMotor Berhasil!',
                    text: 'TitipMotor ' + data.nama + ' Telah Update',
                    type: 'success',
                    timer: 2000
                });
            }
            else {
                __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                    title: 'Edit TitipMotor Gagal!',
                    text: response.message,
                    type: 'error'
                });
            }
        })
            .catch(function (error) {
            var eror = JSON.parse(error._body);
            __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                title: 'Error !',
                text: eror.message,
                type: 'error'
            });
        });
    };
    DetailTitipMotorComponent.prototype.addProduct = function () {
        var _this = this;
        var data = this.productForm.getRawValue();
        this.productService.addProduct(data)
            .then(function (response) {
            if (response.status) {
                _this.productService.onProductChanged.next(data);
                _this.back();
                __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                    title: 'Tambah TitipMotor Berhasil!',
                    text: 'TitipMotor ' + data.nama + ' Telah Ditambahkan',
                    type: 'success',
                    timer: 2000
                });
            }
            else {
                __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                    title: 'Tambah TitipMotor Gagal!',
                    text: response.message,
                    type: 'error'
                });
            }
        })
            .catch(function (error) {
            var eror = JSON.parse(error._body);
            __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                title: 'Error !',
                text: eror.message,
                type: 'error'
            });
        });
    };
    DetailTitipMotorComponent.prototype.ngOnDestroy = function () {
        this.onProductChanged.unsubscribe();
    };
    DetailTitipMotorComponent.prototype.hapus = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
            title: 'Hapus TitipMotor' + this.product.nama,
            text: 'TitipMotor yang telah dihapus tidak bisa dikembalikan lagi',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus !',
            cancelButtonText: 'Batal!',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                _this.productService.hapus(_this.productForm.getRawValue())
                    .then(function (response) {
                    if (response.status) {
                        _this.back();
                        __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                            title: 'Hapus TitipMotor Berhasil!',
                            text: 'TitipMotor' + _this.product.nama + ' Telah Dihapus',
                            type: 'success',
                            timer: 2000
                        });
                    }
                    else {
                        __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()('Hapus TitipMotor Gagal!', response.message, 'error');
                    }
                })
                    .catch(function (error) {
                    var eror = JSON.parse(error._body);
                    __WEBPACK_IMPORTED_MODULE_16_sweetalert2___default()({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    });
                });
            }
        });
    };
    DetailTitipMotorComponent.prototype.print = function (tiket) {
        var fileURL = __WEBPACK_IMPORTED_MODULE_13__environments_environment__["a" /* environment */].setting.base_url + '/titipMotor/' + tiket + '/download';
        window.open(fileURL, '_blank');
    };
    DetailTitipMotorComponent.prototype.back = function () {
        this.router.navigate(['/apps/titipMotor/daftarTitipMotor']);
    };
    DetailTitipMotorComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-product',
            template: __webpack_require__("../../../../../src/app/main/content/apps/titip_motor/detailTitipMotor/detailTitipMotor.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/titip_motor/detailTitipMotor/detailTitipMotor.component.scss")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
            animations: __WEBPACK_IMPORTED_MODULE_2__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__detailTitipMotor_service__["a" /* DetailTitipMotorService */],
            __WEBPACK_IMPORTED_MODULE_10__angular_forms__["c" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_11__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_12__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_15__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_14__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_11__angular_material__["m" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_14__angular_router__["f" /* Router */],
            __WEBPACK_IMPORTED_MODULE_17__session_service__["a" /* SessionService */]])
    ], DetailTitipMotorComponent);
    return DetailTitipMotorComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/titip_motor/detailTitipMotor/detailTitipMotor.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Product; });
var Product = /** @class */ (function () {
    function Product(product) {
        if (product) {
            product = product;
            this.rute_id = product.rute_id;
            this.nama = product.nama;
            this.alamat = product.alamat;
            this.no_hp = product.no_hp;
            this.no_kendaraan = product.no_kendaraan;
            this.no_ktp = product.no_ktp;
            this.no_tiket = product.no_tiket;
            this.no_sim = product.no_sim;
        }
        else {
            product = {};
            this.rute_id = 0;
            this.nama = '';
            this.alamat = '';
            this.no_hp = '';
            this.no_kendaraan = '';
            this.no_ktp = '';
            this.no_tiket = '';
            this.no_sim = '';
        }
    }
    return Product;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/titip_motor/detailTitipMotor/detailTitipMotor.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailTitipMotorService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DetailTitipMotorService = /** @class */ (function () {
    function DetailTitipMotorService(http, snackBar, _sessionService) {
        this.http = http;
        this.snackBar = snackBar;
        this._sessionService = _sessionService;
        this.onProductChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({});
        this.token = this._sessionService.get().token;
        this.ruteAll = [];
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DetailTitipMotorService.prototype.resolve = function (route, state) {
        var _this = this;
        this.routeParams = route.params;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getProduct(),
                _this.getRuteAllMudik(),
                _this.getRuteAllBalik()
            ]).then(function () {
                resolve();
            }, reject)
                .catch(function (error) {
                var eror = JSON.parse(error._body);
                _this.snackBar.open(eror.message, eror.errors.email ? eror.errors.email : eror.errors.password, {
                    verticalPosition: 'top',
                    duration: 5000
                });
            });
        });
    };
    DetailTitipMotorService.prototype.getRuteAllMudik = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/getRute/titipMotor', options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                _this.ruteAll = response['data'];
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTitipMotorService.prototype.getRuteAllBalik = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/getRuteBalik/titipMotor', options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                for (var i = 0; i < response.data.length; ++i) {
                    _this.ruteAll.push(response.data[i]);
                }
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTitipMotorService.prototype.getProduct = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            if (_this.routeParams.id === 'new') {
                _this.onProductChanged.next(false);
                resolve(false);
            }
            else {
                _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/titipMotor/' + _this.routeParams.id, options)
                    .map(function (res) { return res.json(); })
                    .subscribe(function (response) {
                    _this.product = response['data'];
                    _this.onProductChanged.next(_this.product);
                    resolve(response);
                }, function (reject) {
                    var eror = JSON.parse(reject._body);
                    _this.snackBar.open(eror.message, 'Error !', {
                        verticalPosition: 'top',
                        duration: 5000
                    });
                });
            }
        });
    };
    DetailTitipMotorService.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        options.headers = headers;
    };
    DetailTitipMotorService.prototype.saveProduct = function (product) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.put(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/titipMotor/' + _this.routeParams.id, product, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTitipMotorService.prototype.addProduct = function (product) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/titipMotor/', product, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTitipMotorService.prototype.hapus = function (product) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.delete(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/titipMotor/' + _this.routeParams.id, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTitipMotorService.prototype.printTiket = function (tiket) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        var header = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        header.append('Authorization', "Bearer " + this.token);
        header.append('Content-Type', 'application/json');
        header.append('Accept', 'application/pdf');
        options.headers = header;
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + "/admin/titipMotor/" + tiket + "/download", {
                headers: header, responseType: __WEBPACK_IMPORTED_MODULE_3__angular_http__["i" /* ResponseContentType */].Blob
            })
                .map(function (res) {
                return {
                    filename: "tiket-" + tiket + ".pdf",
                    data: res.blob(),
                    success: true
                };
            })
                .subscribe(function (res) {
                console.log();
                var url = window.URL.createObjectURL(res.data);
                var a = document.createElement('a');
                document.body.appendChild(a);
                a.setAttribute('style', 'display: none');
                a.href = url;
                a.download = res.filename;
                a.click();
                window.URL.revokeObjectURL(url);
                a.remove(); // remove the element
                resolve(res);
            }, function (error) {
                console.log('download error:', JSON.stringify(error));
            }, function () {
                console.log('Completed file download.');
            });
        });
    };
    DetailTitipMotorService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_6__session_service__["a" /* SessionService */]])
    ], DetailTitipMotorService);
    return DetailTitipMotorService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/titip_motor/titipMotor.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TitipMotorModule", function() { return TitipMotorModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__ = __webpack_require__("../../../../@swimlane/ngx-charts/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__ = __webpack_require__("../../../../../src/app/core/modules/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__ = __webpack_require__("../../../../../src/app/core/components/widget/widget.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__daftarTitipMotor_daftarTitipMotor_component__ = __webpack_require__("../../../../../src/app/main/content/apps/titip_motor/daftarTitipMotor/daftarTitipMotor.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__daftarTitipMotor_daftarTitipMotor_service__ = __webpack_require__("../../../../../src/app/main/content/apps/titip_motor/daftarTitipMotor/daftarTitipMotor.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__detailTitipMotor_detailTitipMotor_component__ = __webpack_require__("../../../../../src/app/main/content/apps/titip_motor/detailTitipMotor/detailTitipMotor.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__detailTitipMotor_detailTitipMotor_service__ = __webpack_require__("../../../../../src/app/main/content/apps/titip_motor/detailTitipMotor/detailTitipMotor.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__core_components_confirm_dialog_confirm_dialog_component__ = __webpack_require__("../../../../../src/app/core/components/confirm-dialog/confirm-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__ = __webpack_require__("../../../cdk/esm5/table.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__ = __webpack_require__("../../../../ng2-dnd/ng2-dnd.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__auth_guard__ = __webpack_require__("../../../../../src/app/main/content/apps/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_material_table__ = __webpack_require__("../../../material/esm5/table.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var routes = [
    {
        path: 'daftarTitipMotor',
        component: __WEBPACK_IMPORTED_MODULE_5__daftarTitipMotor_daftarTitipMotor_component__["a" /* DaftarTitipMotorComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_13__auth_guard__["a" /* AuthGuard */]],
        resolve: {
            data: __WEBPACK_IMPORTED_MODULE_6__daftarTitipMotor_daftarTitipMotor_service__["a" /* DaftarTitipMotorService */]
        }
    },
    {
        path: 'detailTitipMotor/:id',
        component: __WEBPACK_IMPORTED_MODULE_7__detailTitipMotor_detailTitipMotor_component__["a" /* DetailTitipMotorComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_13__auth_guard__["a" /* AuthGuard */]],
        resolve: {
            data: __WEBPACK_IMPORTED_MODULE_8__detailTitipMotor_detailTitipMotor_service__["a" /* DetailTitipMotorService */]
        }
    }
];
var TitipMotorModule = /** @class */ (function () {
    function TitipMotorModule() {
    }
    TitipMotorModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__["a" /* FuseWidgetModule */],
                __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__["NgxChartsModule"],
                __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__["m" /* CdkTableModule */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["a" /* DndModule */],
                __WEBPACK_IMPORTED_MODULE_14__angular_material_table__["b" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_10__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
                })
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__["m" /* CdkTableModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__daftarTitipMotor_daftarTitipMotor_component__["a" /* DaftarTitipMotorComponent */],
                __WEBPACK_IMPORTED_MODULE_9__core_components_confirm_dialog_confirm_dialog_component__["a" /* FuseConfirmDialogComponent */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__daftarTitipMotor_daftarTitipMotor_component__["a" /* DaftarTitipMotorComponent */],
                __WEBPACK_IMPORTED_MODULE_7__detailTitipMotor_detailTitipMotor_component__["a" /* DetailTitipMotorComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__daftarTitipMotor_daftarTitipMotor_service__["a" /* DaftarTitipMotorService */],
                __WEBPACK_IMPORTED_MODULE_8__detailTitipMotor_detailTitipMotor_service__["a" /* DetailTitipMotorService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["d" /* DragDropSortableService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["c" /* DragDropService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["b" /* DragDropConfig */]
            ]
        })
    ], TitipMotorModule);
    return TitipMotorModule;
}());



/***/ })

});
//# sourceMappingURL=titipMotor.module.chunk.js.map