import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { DaftarMotorComponent } from './daftarMotor/daftarMotor.component';
import { DaftarMotorService } from './daftarMotor/daftarMotor.service';
import { FuseConfirmDialogComponent } from './../../../../core/components/confirm-dialog/confirm-dialog.component';
import { AgmCoreModule } from '@agm/core';
import { AuthGuard } from './../auth.guard';

const routes: Routes = [
    {
        path     : 'daftarMotor',
        component: DaftarMotorComponent,
        canActivate : [AuthGuard]
    }

];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    entryComponents: [
        DaftarMotorComponent,
        FuseConfirmDialogComponent
    ],
    declarations: [
        DaftarMotorComponent
    ],
    providers   : [
        DaftarMotorService
    ]
})
export class MotorModule
{
}
