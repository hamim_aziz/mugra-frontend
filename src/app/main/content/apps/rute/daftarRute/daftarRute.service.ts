import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from './../../session.service';

@Injectable()
export class DaftarRuteService implements Resolve<any>
{
    products: any[];
    onProductsChanged: BehaviorSubject<any> = new BehaviorSubject({});
    token = this._sessionService.get().token;

    constructor(
        private http: Http,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProducts()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    setHeader(options: RequestOptions) {
        const headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }

    getProducts(): Promise<any>
    {
        const options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url + '/admin/rute', options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    this.products = response['data'];
                    for (let i = 0; i < this.products.length; ++i) {
                        if (this.products[i]['jenis'] === 1) {
                            this.products[i]['jenis'] = 'Mudik';
                        } else {
                            this.products[i]['jenis'] = 'Balik';
                        }
                        if (this.products[i]['is_mudik_motor'] === 1) {
                            this.products[i]['is_mudik_motor'] = 'Mudik Motor';
                        } else {
                            this.products[i]['is_mudik_motor'] = 'Mudik Biasa';
                        }
                        if (this.products[i]['status'] === 1) {
                            this.products[i]['status'] = 'Siap Mudik';
                        } else {
                            this.products[i]['status'] = 'Belum Siap';
                        }
                        if (this.products[i].kota) {
                            this.products[i].asal = this.products[i].kota.asal + ' - ' + this.products[i].kota.tujuan;
                        }
                    }
                    this.onProductsChanged.next(this.products);
                    resolve(response);
                }, reject);
        });
    }
}
