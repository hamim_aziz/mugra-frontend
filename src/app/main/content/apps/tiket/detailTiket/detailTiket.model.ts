import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatChipInputEvent } from '@angular/material';

export class Product
{
    id: string;
    nomor_tiket: string;
    alamat: string;
    telepon: string;
    status: number;
    kota_tujuan: string;
    pemudik_dewasa: number;
    pemudik_anak: number;
    kendaraan: any;
    foto_kk: any;
    pemudik: {
        id: string,
        kursi: string,
        nama: string,
        jenis: number,
        nik: string,
        sim: string,
        stnk: string,
        gender: number,
        tempat_lahir: string,
        tanggal_lahir: string,
        pekerjaan: number,
        usia: number,
        foto_pemudik: string,
        kursi_id: string
    }[];
    rute: {};

    constructor(product?)
    {
        if (product) {
            product = product;
            this.id = product.id;
            this.alamat = product.alamat;
            this.telepon = product.telepon;
            this.status = product.status;
            this.kota_tujuan = product.kota_tujuan;
            this.pemudik = product.pemudiks;
            this.kendaraan = product.kendaraan;
            this.pemudik_dewasa = product.pemudik_dewasa;
            this.pemudik_anak = product.pemudik_anak;
            this.rute = product.rute;
            this.nomor_tiket = product.nomor_tiket;
            this.foto_kk = product.foto_kk;
        } else {
            product = {};
            this.nomor_tiket = '';
            this.pemudik_anak = 0;
            this.pemudik_dewasa = 0;
            this.alamat = '';
            this.telepon = '';
            this.status = 0;
            this.kota_tujuan = '';
            this.pemudik = [];
            this.kendaraan = [];
            this.rute = [];
        }
    }
    
}
