webpackJsonp(["contact.module"],{

/***/ "../../../../../src/app/main/content/apps/contact/contact.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactModule", function() { return ContactModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__ = __webpack_require__("../../../../@swimlane/ngx-charts/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__ = __webpack_require__("../../../../../src/app/core/modules/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__ = __webpack_require__("../../../../../src/app/core/components/widget/widget.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__daftarContact_daftarContact_component__ = __webpack_require__("../../../../../src/app/main/content/apps/contact/daftarContact/daftarContact.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__daftarContact_daftarContact_service__ = __webpack_require__("../../../../../src/app/main/content/apps/contact/daftarContact/daftarContact.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__detailContact_detailContact_component__ = __webpack_require__("../../../../../src/app/main/content/apps/contact/detailContact/detailContact.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__detailContact_detailContact_service__ = __webpack_require__("../../../../../src/app/main/content/apps/contact/detailContact/detailContact.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__core_components_confirm_dialog_confirm_dialog_component__ = __webpack_require__("../../../../../src/app/core/components/confirm-dialog/confirm-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__ = __webpack_require__("../../../cdk/esm5/table.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__ = __webpack_require__("../../../../ng2-dnd/ng2-dnd.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__auth_guard__ = __webpack_require__("../../../../../src/app/main/content/apps/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_material_table__ = __webpack_require__("../../../material/esm5/table.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var routes = [
    {
        path: 'daftarContact',
        component: __WEBPACK_IMPORTED_MODULE_5__daftarContact_daftarContact_component__["a" /* DaftarContactComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_13__auth_guard__["a" /* AuthGuard */]],
        resolve: {
            data: __WEBPACK_IMPORTED_MODULE_6__daftarContact_daftarContact_service__["a" /* DaftarContactService */]
        }
    },
    {
        path: 'detailContact/:id',
        component: __WEBPACK_IMPORTED_MODULE_7__detailContact_detailContact_component__["a" /* DetailContactComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_13__auth_guard__["a" /* AuthGuard */]],
        resolve: {
            data: __WEBPACK_IMPORTED_MODULE_8__detailContact_detailContact_service__["a" /* DetailContactService */]
        }
    }
];
var ContactModule = /** @class */ (function () {
    function ContactModule() {
    }
    ContactModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__["a" /* FuseWidgetModule */],
                __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__["NgxChartsModule"],
                __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__["m" /* CdkTableModule */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["a" /* DndModule */],
                __WEBPACK_IMPORTED_MODULE_14__angular_material_table__["b" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_10__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
                })
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_11__angular_cdk_table__["m" /* CdkTableModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__daftarContact_daftarContact_component__["a" /* DaftarContactComponent */],
                __WEBPACK_IMPORTED_MODULE_9__core_components_confirm_dialog_confirm_dialog_component__["a" /* FuseConfirmDialogComponent */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__daftarContact_daftarContact_component__["a" /* DaftarContactComponent */],
                __WEBPACK_IMPORTED_MODULE_7__detailContact_detailContact_component__["a" /* DetailContactComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__daftarContact_daftarContact_service__["a" /* DaftarContactService */],
                __WEBPACK_IMPORTED_MODULE_8__detailContact_detailContact_service__["a" /* DetailContactService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["d" /* DragDropSortableService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["c" /* DragDropService */],
                __WEBPACK_IMPORTED_MODULE_12_ng2_dnd__["b" /* DragDropConfig */]
            ]
        })
    ], ContactModule);
    return ContactModule;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/contact/daftarContact/daftarContact.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"products\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\"\r\n             fxLayout=\"column\" fxLayoutAlign=\"center center\"\r\n             fxLayout.gt-xs=\"row\" fxLayoutAlign.gt-xs=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div class=\"logo my-12 m-sm-0\"\r\n                 fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <mat-icon class=\"logo-icon mr-16\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'50ms',scale:'0.2'}}\">navigation</mat-icon>\r\n                <span class=\"logo-text h1\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">Daftar Contact</span>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <!-- SEARCH -->\r\n            <div class=\"search-input-wrapper mx-12 m-md-0\"\r\n                 fxFlex=\"1 0 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n                <label for=\"search\" class=\"mr-8\">\r\n                    <mat-icon class=\"secondary-text\">search</mat-icon>\r\n                </label>\r\n                <mat-form-field floatPlaceholder=\"never\" fxFlex=\"1 0 auto\">\r\n                    <input id=\"search\" matInput #filter placeholder=\"Search\">\r\n                </mat-form-field>\r\n            </div>\r\n            <!-- / SEARCH -->\r\n\r\n            <div class=\"my-12\">\r\n\r\n                <button mat-raised-button\r\n                        [routerLink]=\"'/apps/contact/detailContact/new'\"\r\n                        class=\"add-product-button m-sm-0\"\r\n                        [disabled]=\"role < 3\">\r\n                    <span>Tambah Contact</span>\r\n                </button>\r\n            </div>\r\n\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <mat-table class=\"products-table\"\r\n                       #table [dataSource]=\"dataSource\"\r\n                       matSort\r\n                       [@animateStagger]=\"{value:'50'}\"\r\n                       fusePerfectScrollbar>\r\n\r\n                <!-- ID Column -->\r\n                <ng-container cdkColumnDef=\"id\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>No.</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let contact; let i = index\">\r\n                        <p class=\"text-truncate\">{{ (paginator.pageIndex * paginator.pageSize) + (i + 1) }}.</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Asal Column -->\r\n                <ng-container cdkColumnDef=\"name\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Nama</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let contact\">\r\n                        <p class=\"text-truncate\">{{contact.name}}</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Tujuan Column -->\r\n                <ng-container cdkColumnDef=\"phone\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Nomor Telepon</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let contact\">\r\n                        <p class=\"category text-truncate\"> {{contact.phone}} </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container cdkColumnDef=\"link\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Link WA</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let contact\">\r\n                        <p class=\"category text-truncate\"> {{contact.link}} </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <mat-header-row *cdkHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n\r\n                <mat-row *cdkRowDef=\"let contact; columns: displayedColumns;\"\r\n                         class=\"product\"\r\n                         matRipple\r\n                         (click)=\"clickContact(contact)\">\r\n                </mat-row>\r\n\r\n            </mat-table>\r\n\r\n            <mat-paginator #paginator\r\n                           [length]=\"dataSource.filteredData.length\"\r\n                           [pageSize]=\"25\"\r\n                           [pageSizeOptions]=\"[5, 10, 25, 100]\">\r\n            </mat-paginator>\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/contact/daftarContact/daftarContact.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n:host .header .search-input-wrapper {\n  max-width: 480px; }\n:host .header .add-product-button {\n  background-color: #F96D01; }\n@media (max-width: 599px) {\n  :host .header {\n    height: 176px !important;\n    min-height: 176px !important;\n    max-height: 176px !important; } }\n@media (max-width: 599px) {\n  :host .top-bg {\n    height: 240px; } }\n:host .products-table {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  border-bottom: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-header-row {\n    min-height: 35px;\n    min-width: 500px;\n    background-color: rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-row {\n    min-width: 500px; }\n:host .products-table .product {\n    position: relative;\n    cursor: pointer;\n    height: 60px; }\n:host .products-table .mat-cell {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n:host .products-table .mat-column-id {\n    min-width: 0;\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 30px;\n            flex: 0 1 30px; }\n:host .products-table .mat-column-jenis, :host .products-table .mat-column-kuota_motor, :host .products-table .mat-column-status {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center !important;\n        -ms-flex-pack: center !important;\n            justify-content: center !important; }\n:host .products-table .mat-column-keterangan {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 20%;\n            flex: 0 1 20%; }\n:host .products-table .mat-column-image {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 84px;\n            flex: 0 1 84px; }\n:host .products-table .mat-column-image .product-image {\n      width: 52px;\n      height: 52px;\n      border: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-column-buttons {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 80px;\n            flex: 0 1 80px; }\n:host .products-table .quantity-indicator {\n    display: inline-block;\n    vertical-align: middle;\n    width: 8px;\n    height: 8px;\n    border-radius: 4px;\n    margin-right: 8px; }\n:host .products-table .quantity-indicator + span {\n      display: inline-block;\n      vertical-align: middle; }\n:host .products-table .active-icon {\n    border-radius: 50%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/contact/daftarContact/daftarContact.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarContactComponent; });
/* unused harmony export FilesDataSource */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__daftarContact_service__ = __webpack_require__("../../../../../src/app/main/content/apps/contact/daftarContact/daftarContact.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_cdk_collections__ = __webpack_require__("../../../cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs_add_observable_fromEvent__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/fromEvent.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__core_fuseUtils__ = __webpack_require__("../../../../../src/app/core/fuseUtils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var DaftarContactComponent = /** @class */ (function () {
    function DaftarContactComponent(contactsService, dialog, router, _sessionService) {
        this.contactsService = contactsService;
        this.dialog = dialog;
        this.router = router;
        this._sessionService = _sessionService;
        this.hapus = false;
        this.displayedColumns = ['id', 'name', 'phone', 'link'];
        this.role = this._sessionService.get().role;
        this.contacts = this.contactsService.contacts;
        this.date = [];
        this.dateIndo = [];
    }
    DaftarContactComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._tanggal();
        if (sessionStorage.getItem("pageIndexContact")) {
            this.paginator.pageIndex = Number(sessionStorage.getItem("pageIndexContact"));
        }
        this.dataSource = new FilesDataSource(this.contactsService, this.paginator, this.sort);
        __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].fromEvent(this.filter.nativeElement, 'keyup')
            .debounceTime(150)
            .distinctUntilChanged()
            .subscribe(function () {
            if (!_this.dataSource) {
                return;
            }
            if (!_this.filter.nativeElement.value) {
                _this.paginator.pageIndex = Number(sessionStorage.getItem("pageIndexContact"));
            }
            else {
                _this.paginator.pageIndex = 0;
            }
            sessionStorage.setItem("filterContact", _this.filter.nativeElement.value);
            _this.dataSource.filter = _this.filter.nativeElement.value;
        });
        __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].fromEvent(this.filter.nativeElement, 'click')
            .debounceTime(150)
            .distinctUntilChanged()
            .subscribe(function () {
            if (!_this.dataSource) {
                return;
            }
            if (_this.filter.nativeElement.value || sessionStorage.getItem("filterContact")) {
                _this.paginator.pageIndex = 0;
            }
            else {
                _this.paginator.pageIndex = Number(sessionStorage.getItem("pageIndexContact"));
            }
            _this.filter.nativeElement.value = sessionStorage.getItem("filterContact");
            _this.dataSource.filter = _this.filter.nativeElement.value;
        });
    };
    DaftarContactComponent.prototype._tanggal = function () {
        for (var i = 0; i < this.contacts.length; ++i) {
            if (this.date.indexOf(this.contacts[i]["tanggal"]) == -1) {
                this.dateIndo.push([this.contacts[i]["tanggal"], this.convertTanggal(this.contacts[i]["tanggal"])]);
                this.date.push(this.contacts[i]["tanggal"]);
            }
        }
    };
    DaftarContactComponent.prototype.convertTanggal = function (tgl) {
        var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
        var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
        var date = new Date(tgl);
        var day = date.getDay();
        var month = date.getMonth();
        return hari[day] + ", " + date.getDate() + " " + bulan[month] + " " + date.getFullYear();
    };
    DaftarContactComponent.prototype.cobaSwal = function () {
        __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()('Deleted!', 'Your file has been deleted.', 'success');
            }
            else if (
            // Read more about handling dismissals
            result.dismiss === __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default.a.DismissReason.cancel) {
                __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()('Cancelled', 'Your imaginary file is safe :)', 'error');
            }
        });
    };
    DaftarContactComponent.prototype.filterButton = function (filter) {
        this.dataSource.filter = filter;
    };
    DaftarContactComponent.prototype.semua = function () {
        this.dataSource.filter = "";
    };
    DaftarContactComponent.prototype.toggleHapus = function () {
        this.hapus == true ? this.hapus = false : this.hapus = true;
    };
    DaftarContactComponent.prototype.clickContact = function (contact) {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()({
            title: 'Edit Contact',
            html: "<input id=\"swal-input1\" class=\"swal2-input\" value=\"" + contact.name + "\" required>" +
                ("<input id=\"swal-input2\" class=\"swal2-input\" value=\"" + contact.phone + "\" required>"),
            focusConfirm: false,
            showCancelButton: true,
            showCloseButton: true,
            cancelButtonText: 'Delete',
            confirmButtonText: 'Save',
            preConfirm: function () {
                return [
                    document.getElementById('swal-input1').value,
                    document.getElementById('swal-input2').value
                ];
            }
        }).then(function (result) {
            if (result.value) {
                var obj = {
                    name: document.getElementById('swal-input1').value,
                    phone: document.getElementById('swal-input2').value,
                    link: "https://wa.me/" + document.getElementById('swal-input2').value,
                    id: contact.id
                };
                _this.contactsService.updateContact(obj)
                    .then(function (response) {
                    if (response.status) {
                        console.log(_this.dataSource);
                        __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()('Updated!', response.message, 'success');
                    }
                })
                    .catch(function (error) {
                    __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()('Failed', 'Failed update data', 'error');
                });
            }
            if (result.dismiss == __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default.a.DismissReason.cancel) {
                __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()('Apakah anda yakin ?', 'Kontak yang telah dihapus tidak bisa dikembalikan', 'question').then(function (result_hapus) {
                    if (result_hapus.value) {
                        _this.contactsService.deleteContact(contact)
                            .then(function (response) {
                            if (response.status) {
                                console.log(_this.dataSource);
                                __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()('Deleted!', response.message, 'success');
                            }
                        })
                            .catch(function (error) {
                            __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()('Failed', 'Failed update data', 'error');
                        });
                    }
                });
            }
        });
    };
    DaftarContactComponent.prototype.bulan = function (date) {
        if (date.substring(5, 7) === '01') {
            return "Januari";
        }
        else if (date.substring(5, 7) === '02') {
            return "Februari";
        }
        else if (date.substring(5, 7) === '03') {
            return "Maret";
        }
        else if (date.substring(5, 7) === '04') {
            return "April";
        }
        else if (date.substring(5, 7) === '05') {
            return "Mei";
        }
        else if (date.substring(5, 7) === '06') {
            return "Juni";
        }
        else if (date.substring(5, 7) === '07') {
            return "Juli";
        }
        else if (date.substring(5, 7) === '08') {
            return "Agustus";
        }
        else if (date.substring(5, 7) === '09') {
            return "September";
        }
        else if (date.substring(5, 7) === '10') {
            return "Oktober";
        }
        else if (date.substring(5, 7) === '11') {
            return "November";
        }
        else if (date.substring(5, 7) === '12') {
            return "Desember";
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__angular_material__["z" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__angular_material__["z" /* MatPaginator */])
    ], DaftarContactComponent.prototype, "paginator", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('filter'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], DaftarContactComponent.prototype, "filter", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_5__angular_material__["M" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__angular_material__["M" /* MatSort */])
    ], DaftarContactComponent.prototype, "sort", void 0);
    DaftarContactComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-contacts',
            template: __webpack_require__("../../../../../src/app/main/content/apps/contact/daftarContact/daftarContact.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/contact/daftarContact/daftarContact.component.scss")],
            animations: __WEBPACK_IMPORTED_MODULE_4__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__daftarContact_service__["a" /* DaftarContactService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_material__["m" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_7__angular_router__["f" /* Router */],
            __WEBPACK_IMPORTED_MODULE_16__session_service__["a" /* SessionService */]])
    ], DaftarContactComponent);
    return DaftarContactComponent;
}());

var FilesDataSource = /** @class */ (function (_super) {
    __extends(FilesDataSource, _super);
    function FilesDataSource(contactsService, _paginator, _sort) {
        var _this = _super.call(this) || this;
        _this.contactsService = contactsService;
        _this._paginator = _paginator;
        _this._sort = _sort;
        _this._filterChange = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        _this._filteredDataChange = new __WEBPACK_IMPORTED_MODULE_6_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        _this.filteredData = _this.contactsService.contacts;
        return _this;
    }
    Object.defineProperty(FilesDataSource.prototype, "filteredData", {
        get: function () {
            return this._filteredDataChange.value;
        },
        set: function (value) {
            this._filteredDataChange.next(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FilesDataSource.prototype, "filter", {
        get: function () {
            return this._filterChange.value;
        },
        set: function (filter) {
            this._filterChange.next(filter);
        },
        enumerable: true,
        configurable: true
    });
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    FilesDataSource.prototype.connect = function () {
        var _this = this;
        var displayDataChanges = [
            this.contactsService.onContactsChanged,
            this._paginator.page,
            this._filterChange,
            this._sort.sortChange
        ];
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].merge.apply(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"], displayDataChanges).map(function () {
            var data = _this.contactsService.contacts.slice();
            data = _this.filterData(data);
            _this.filteredData = data.slice();
            data = _this.sortData(data);
            // Grab the page's slice of data.
            if (!_this.filter) {
                sessionStorage.setItem("pageIndexContact", String(_this._paginator.pageIndex));
            }
            var startIndex = _this._paginator.pageIndex * _this._paginator.pageSize;
            return data.splice(startIndex, _this._paginator.pageSize);
        });
    };
    FilesDataSource.prototype.filterData = function (data) {
        if (!this.filter) {
            return data;
        }
        return __WEBPACK_IMPORTED_MODULE_14__core_fuseUtils__["a" /* FuseUtils */].filterArrayByString(data, this.filter);
    };
    FilesDataSource.prototype.sortData = function (data) {
        var _this = this;
        if (!this._sort.active || this._sort.direction === '') {
            return data;
        }
        return data.sort(function (a, b) {
            var propertyA = '';
            var propertyB = '';
            switch (_this._sort.active) {
                case 'id':
                    _a = [a.id, b.id], propertyA = _a[0], propertyB = _a[1];
                    break;
                case 'asal':
                    _b = [a.asal, b.asal], propertyA = _b[0], propertyB = _b[1];
                    break;
                case 'tujuan':
                    _c = [a.tujuan, b.tujuan], propertyA = _c[0], propertyB = _c[1];
                    break;
                case 'tanggal':
                    _d = [a.tanggal, b.tanggal], propertyA = _d[0], propertyB = _d[1];
                    break;
                case 'jenis':
                    _e = [a.jenis, b.jenis], propertyA = _e[0], propertyB = _e[1];
                    break;
                case 'kuota_motor':
                    _f = [a.kouta_motor, b.kouta_motor], propertyA = _f[0], propertyB = _f[1];
                    break;
                case 'status':
                    _g = [a.status, b.status], propertyA = _g[0], propertyB = _g[1];
                    break;
            }
            var valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            var valueB = isNaN(+propertyB) ? propertyB : +propertyB;
            return (valueA < valueB ? -1 : 1) * (_this._sort.direction === 'asc' ? 1 : -1);
            var _a, _b, _c, _d, _e, _f, _g;
        });
    };
    FilesDataSource.prototype.disconnect = function () {
    };
    return FilesDataSource;
}(__WEBPACK_IMPORTED_MODULE_2__angular_cdk_collections__["a" /* DataSource */]));



/***/ }),

/***/ "../../../../../src/app/main/content/apps/contact/daftarContact/daftarContact.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarContactService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DaftarContactService = /** @class */ (function () {
    function DaftarContactService(http, _sessionService) {
        this.http = http;
        this._sessionService = _sessionService;
        this.onContactsChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({});
        this.token = this._sessionService.get().token;
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DaftarContactService.prototype.resolve = function (route, state) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getContacts()
            ]).then(function () {
                resolve();
            }, reject);
        });
    };
    DaftarContactService.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        options.headers = headers;
    };
    DaftarContactService.prototype.getContacts = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/cps', options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                console.log(response);
                _this.contacts = response['data'];
                _this.onContactsChanged.next(_this.contacts);
                resolve(response);
            }, reject);
        });
    };
    DaftarContactService.prototype.updateContact = function (contact) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.put(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/cps/' + contact.id, contact, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DaftarContactService.prototype.deleteContact = function (contact) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.put(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/cps/' + contact.id, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DaftarContactService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__session_service__["a" /* SessionService */]])
    ], DaftarContactService);
    return DaftarContactService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/contact/detailContact/detailContact.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"product\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"start center\" style=\"width: 80%\">\r\n\r\n                <button class=\"mr-0 mr-sm-16\" mat-icon-button (click)=\"back()\">\r\n                    <mat-icon>arrow_back</mat-icon>\r\n                </button>\r\n\r\n                <div fxLayout=\"column\" fxLayoutAlign=\"start start\"\r\n                     *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">\r\n                    <div class=\"h2\" *ngIf=\"pageType ==='edit' || pageType === 'salin'\">\r\n                        {{contact.label}}\r\n                    </div>\r\n                    <div class=\"h2\" *ngIf=\"pageType ==='new'\">\r\n                        Tambah Contact\r\n                    </div>\r\n                    <div class=\"subtitle secondary-text\">\r\n                        <span>Detail Contact</span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"contactForm.invalid || role < 3\"\r\n                    *ngIf=\"pageType ==='new'\" (click)=\"addContact()\" fxHide.xs>\r\n                <span>Simpan</span>\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"saveContact()\" fxHide.xs>\r\n                <span>Simpan</span>\r\n            </button>\r\n\r\n            <button mat-raised-button\r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    [disabled]=\"role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"hapus()\" fxHide.xs>\r\n                <span>Hapus</span>\r\n            </button>\r\n\r\n            <button mat-icon-button fxHide fxShow.xs [matMenuTriggerFor]=\"menu\">\r\n                <mat-icon>more_vert</mat-icon>\r\n            </button>\r\n            \r\n            <mat-menu #menu=\"matMenu\">\r\n              <button mat-menu-item [disabled]=\"contactForm.invalid || role < 3\"\r\n                    *ngIf=\"pageType ==='new'\" (click)=\"addContact()\">\r\n                <mat-icon>save_alt</mat-icon>\r\n                <span>Simpan</span>\r\n              </button>\r\n              <button mat-menu-item [disabled]=\"role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"saveContact()\">\r\n                <mat-icon>save_alt</mat-icon>\r\n                <span>Simpan</span>\r\n              </button>\r\n              <button mat-menu-item [disabled]=\"role < 3\"\r\n                    *ngIf=\"pageType ==='edit'\" (click)=\"hapus()\">\r\n                <mat-icon>delete_forever</mat-icon>\r\n                <span>Hapus</span>\r\n              </button>\r\n            </mat-menu>\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <!-- CONTENT -->\r\n            <div class=\"content\">\r\n\r\n                <form name=\"contactForm\" [formGroup]=\"contactForm\" class=\"product w-100-p\" fxLayout=\"column\" fxFlex>\r\n\r\n                    <div class=\"tab-content p-24\" fusePerfectScrollbar>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                            <input matInput\r\n                                   name=\"name\"\r\n                                   formControlName=\"name\"\r\n                                   placeholder=\"nama\"\r\n                                   required>\r\n                            <mat-error *ngIf=\"contactForm.controls.name.invalid\">\r\n                              {{contactForm.controls.name.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                            </mat-error>\r\n                            <mat-icon matPrefix> description </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                            <input matInput\r\n                                   name=\"phone\"\r\n                                   formControlName=\"phone\"\r\n                                   placeholder=\"Nomor Telepon\"\r\n                                   rquired>\r\n                            <mat-error *ngIf=\"contactForm.controls.phone.invalid\">\r\n                              {{contactForm.controls.phone.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                            </mat-error>\r\n                            <mat-icon matPrefix> contact_phone </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                        <mat-form-field class=\"w-100-p\">\r\n                            <input matInput\r\n                                   name=\"link\"\r\n                                   formControlName=\"link\"\r\n                                   placeholder=\"Link WA\"\r\n                                   rquired>\r\n                            <mat-error *ngIf=\"contactForm.controls.link.invalid\">\r\n                              {{contactForm.controls.link.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                            </mat-error>\r\n                            <mat-icon matPrefix> link </mat-icon>\r\n                        </mat-form-field>\r\n\r\n                    </div>\r\n\r\n                </form>\r\n\r\n            </div>\r\n            <!-- / CONTENT -->\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/contact/detailContact/detailContact.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#product .header .product-image {\n  overflow: hidden;\n  width: 56px;\n  height: 56px;\n  border: 3px solid rgba(0, 0, 0, 0.12); }\n  #product .header .product-image img {\n    height: 100%;\n    width: auto;\n    max-width: none; }\n  #product .header .save-product-button {\n  background-color: #F96D01;\n  margin: 0px 20px; }\n  #product .header .subtitle {\n  margin: 6px 0 0 0; }\n  #product .content .w-100-p {\n  margin-bottom: 8px; }\n  #product .content .w-90-p {\n  margin-bottom: 8px; }\n  #product .content .mat-form-field-prefix .mat-icon, #product .content .mat-form-field-suffix .mat-icon {\n  margin-right: 10px; }\n  #product .content .mat-tab-group,\n#product .content .mat-tab-body-wrapper,\n#product .content .tab-content {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  max-width: 100%; }\n  #product .content .mat-tab-body-content {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n  #product .content .mat-tab-header {\n  height: 40px;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n  #product .content .mat-tab-label {\n  height: 64px; }\n  #product .content .mat-card .mat-divider.mat-divider-inset {\n  position: absolute !important; }\n  #product .content .mat-list .mat-list-item .mat-divider.mat-divider-inset {\n  left: 0 !important;\n  width: 100% !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/contact/detailContact/detailContact.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailContactComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__detailContact_service__ = __webpack_require__("../../../../../src/app/main/content/apps/contact/detailContact/detailContact.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_observable_fromEvent__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/fromEvent.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__detailContact_model__ = __webpack_require__("../../../../../src/app/main/content/apps/contact/detailContact/detailContact.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


















var DetailContactComponent = /** @class */ (function () {
    function DetailContactComponent(contactService, formBuilder, snackBar, location, http, route, dialog, router, _sessionService) {
        this.contactService = contactService;
        this.formBuilder = formBuilder;
        this.snackBar = snackBar;
        this.location = location;
        this.http = http;
        this.route = route;
        this.dialog = dialog;
        this.router = router;
        this._sessionService = _sessionService;
        this.contact = new __WEBPACK_IMPORTED_MODULE_9__detailContact_model__["a" /* Contact */]();
        this.token = this._sessionService.get().token;
        this.role = this._sessionService.get().role;
    }
    DetailContactComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Subscribe to update contact on changes
        this.onContactChanged =
            this.contactService.onContactChanged
                .subscribe(function (contact) {
                if (contact) {
                    _this.contact = new __WEBPACK_IMPORTED_MODULE_9__detailContact_model__["a" /* Contact */](contact);
                    _this.pageType = 'edit';
                }
                else {
                    _this.pageType = 'new';
                    _this.contact = new __WEBPACK_IMPORTED_MODULE_9__detailContact_model__["a" /* Contact */]();
                }
                _this.contactForm = _this.createContactForm();
            });
    };
    DetailContactComponent.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_14__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        options.headers = headers;
    };
    DetailContactComponent.prototype.createContactForm = function () {
        return this.formBuilder.group({
            name: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.contact.name, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            phone: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.contact.phone, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
            link: new __WEBPACK_IMPORTED_MODULE_10__angular_forms__["d" /* FormControl */](this.contact.link, [__WEBPACK_IMPORTED_MODULE_10__angular_forms__["l" /* Validators */].required]),
        });
    };
    DetailContactComponent.prototype.saveContact = function () {
        var _this = this;
        var data = this.contactForm.getRawValue();
        this.contactService.saveContact(data)
            .then(function (response) {
            // Change the location with new one
            if (response.status) {
                _this.location.back();
                __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()({
                    title: 'Update Contact Berhasil!',
                    text: 'Contact ' + data.name + ' Telah Update',
                    type: 'success',
                    timer: 2000
                });
            }
            else {
                __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()({
                    title: 'Edit Contact Gagal!',
                    text: response.message,
                    type: 'error'
                });
            }
        });
    };
    DetailContactComponent.prototype.salinContact = function () {
        this.pageType = "salin";
        this.contactForm.controls.jenis.disable();
        this.contactForm.controls.tujuan.disable();
        this.contactForm.controls.asal.disable();
    };
    DetailContactComponent.prototype.addContact = function () {
        var _this = this;
        var data = this.contactForm.getRawValue();
        this.contactService.addContact(data)
            .then(function (response) {
            _this.contactService.onContactChanged.next(data);
            if (response.status) {
                _this.location.back();
                __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()({
                    title: 'Tambah Contact Berhasil!',
                    text: 'Contact ' + data.name + ' Telah Ditambahkan',
                    type: 'success',
                    timer: 2000
                });
            }
            else {
                __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()({
                    title: 'Tambah Contact Gagal!',
                    text: response.message,
                    type: 'error'
                });
            }
        })
            .catch(function (error) {
            __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()({
                title: 'Error !',
                text: 'Internal server error',
                type: 'error'
            });
        });
    };
    DetailContactComponent.prototype.ngOnDestroy = function () {
        this.onContactChanged.unsubscribe();
    };
    DetailContactComponent.prototype.hapus = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()({
            title: 'Hapus Contact' + this.contact.name,
            text: "Contact yang telah dihapus tidak bisa dikembalikan lagi",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus !',
            cancelButtonText: 'Batal!',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                _this.contactService.hapus(_this.contactForm.getRawValue())
                    .then(function (response) {
                    if (response.status) {
                        _this.back();
                        __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()({
                            title: 'Hapus Contact Berhasil!',
                            text: 'Contact' + _this.contact.name + ' Telah Dihapus',
                            type: 'success',
                            timer: 2000
                        });
                    }
                    else {
                        __WEBPACK_IMPORTED_MODULE_15_sweetalert2___default()('Hapus Contact Gagal!', response.message, 'error');
                    }
                });
            }
        });
    };
    DetailContactComponent.prototype.back = function () {
        this.location.back();
    };
    DetailContactComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-contact',
            template: __webpack_require__("../../../../../src/app/main/content/apps/contact/detailContact/detailContact.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/contact/detailContact/detailContact.component.scss")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
            animations: __WEBPACK_IMPORTED_MODULE_2__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__detailContact_service__["a" /* DetailContactService */],
            __WEBPACK_IMPORTED_MODULE_10__angular_forms__["c" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_11__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_12__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_14__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_13__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_11__angular_material__["m" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_13__angular_router__["f" /* Router */],
            __WEBPACK_IMPORTED_MODULE_16__session_service__["a" /* SessionService */]])
    ], DetailContactComponent);
    return DetailContactComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/contact/detailContact/detailContact.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Contact; });
var Contact = /** @class */ (function () {
    function Contact(contact) {
        if (contact) {
            contact = contact;
            this.id = contact.id;
            this.name = contact.name;
            this.phone = contact.phone;
            this.link = contact.link;
        }
        else {
            contact = {};
            this.id = 0;
            this.link = "";
            this.phone = "";
            this.link = "";
        }
    }
    return Contact;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/contact/detailContact/detailContact.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailContactService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var DetailContactService = /** @class */ (function () {
    function DetailContactService(http, snackBar, _sessionService) {
        this.http = http;
        this.snackBar = snackBar;
        this._sessionService = _sessionService;
        this.onContactChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({});
        this.token = this._sessionService.get().token;
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DetailContactService.prototype.resolve = function (route, state) {
        var _this = this;
        this.routeParams = route.params;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getContact()
                    .then()
                    .catch(function (error) {
                    console.log(error);
                })
            ]).then(function () {
                resolve();
            }, reject);
        });
    };
    DetailContactService.prototype.getContact = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            if (_this.routeParams.id === 'new') {
                _this.onContactChanged.next(false);
                resolve(false);
            }
            else {
                _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/cps/' + _this.routeParams.id, options)
                    .subscribe(function (response) {
                    _this.contact = response['data'];
                    _this.onContactChanged.next(_this.contact);
                    resolve(response);
                }, function (error) {
                    reject(error);
                });
            }
        });
    };
    DetailContactService.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        headers.append('Contact-Type', 'application/x-www-form-urlencoded');
        options.headers = headers;
    };
    DetailContactService.prototype.saveContact = function (contact) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.put(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/cps/' + _this.routeParams.id, contact, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (reject) {
                var eror = JSON.parse(reject._body);
                __WEBPACK_IMPORTED_MODULE_6_sweetalert2___default()({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
            });
        });
    };
    DetailContactService.prototype.addContact = function (contact) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/cps/', contact, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailContactService.prototype.hapus = function (contact) {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.delete(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/cps/' + _this.routeParams.id, options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (reject) {
                var eror = JSON.parse(reject._body);
                __WEBPACK_IMPORTED_MODULE_6_sweetalert2___default()({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
            });
        });
    };
    DetailContactService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_7__session_service__["a" /* SessionService */]])
    ], DetailContactService);
    return DetailContactService;
}());



/***/ })

});
//# sourceMappingURL=contact.module.chunk.js.map