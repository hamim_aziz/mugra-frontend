webpackJsonp(["tiket.module.0"],{

/***/ "../../../../../src/app/main/content/apps/tiket/daftarTiket/daftarTiket.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"products\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\"\r\n             fxLayout=\"column\" fxLayoutAlign=\"center center\"\r\n             fxLayout.gt-xs=\"row\" fxLayoutAlign.gt-xs=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div class=\"logo my-12 m-sm-0\"\r\n                 fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <mat-icon class=\"logo-icon mr-16\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'50ms',scale:'0.2'}}\">local_play</mat-icon>\r\n                <span class=\"logo-text h1\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">Daftar Tiket</span>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <!-- SEARCH -->\r\n            <div class=\"search-input-wrapper mx-12 m-md-0\"\r\n                 fxFlex=\"1 0 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <label for=\"search\" class=\"mr-8\">\r\n                    <mat-icon class=\"secondary-text\">search</mat-icon>\r\n                </label>\r\n                <mat-form-field floatPlaceholder=\"never\" fxFlex=\"1 0 auto\">\r\n                    <input [(ngModel)]=\"search_query\" id=\"search\" matInput #filter placeholder=\"Search\" (keyup.enter)=\"search()\">\r\n                </mat-form-field>\r\n            </div>\r\n            <!-- / SEARCH -->\r\n\r\n            <div class=\"my-12\" fxHide.xs>\r\n                <button mat-raised-button\r\n                        (click)=\"clear()\"\r\n                        class=\"add-product-button m-sm-0\">\r\n                    <span>Clear</span>\r\n                </button>\r\n            </div>\r\n\r\n            <div class=\"my-12\">\r\n                <button mat-raised-button fxHide.xs class=\"add-product-button m-sm-0\" [matMenuTriggerFor]=\"filter\">\r\n                    <span>Filter</span>\r\n                </button>\r\n\r\n                <button mat-raised-button class=\"add-product-button m-sm-0\" [matMenuTriggerFor]=\"option\" fxHide fxShow.xs>\r\n                    <span>Option</span>\r\n                </button>\r\n\r\n                <mat-menu #option=\"matMenu\">\r\n                    <button mat-menu-item (click)=\"clear()\">Clear</button>\r\n                    <button mat-menu-item [matMenuTriggerFor]=\"filter\">Filter</button>\r\n                </mat-menu>\r\n\r\n                <mat-menu #filter=\"matMenu\">\r\n                    <button (click)=\"filterButton('0')\" mat-menu-item>Belum Disetujui</button>\r\n                    <button (click)=\"filterButton('1')\" mat-menu-item>Disetujui</button>\r\n                    <button (click)=\"filterButton('2')\" mat-menu-item>Pending</button>\r\n                    <button (click)=\"filterButton('3')\" mat-menu-item>Hadir</button>\r\n                    <button (click)=\"filterButton('4')\" mat-menu-item>Cancel</button>\r\n                    <button (click)=\"filterButton('5')\" mat-menu-item>Ditolak</button>\r\n                </mat-menu>\r\n                \r\n            </div>\r\n            \r\n\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg example-container mat-elevation-z8\">\r\n\r\n            <!-- <div class=\"example-loading-shade\"\r\n                *ngIf=\"isRateLimitReached\">\r\n                <mat-spinner *ngIf=\"isLoadingResults\"></mat-spinner>\r\n            </div> -->\r\n\r\n            <mat-table class=\"products-table\"\r\n                       #table [dataSource]=\"dataSource\"\r\n                       matSort matSortActive=\"created\" matSortDisableClear matSortDirection=\"asc\"\r\n                       [@animateStagger]=\"{value:'50'}\"\r\n                       fusePerfectScrollbar>\r\n\r\n                <ng-container matColumnDef=\"id\">\r\n                  <mat-header-cell *matHeaderCellDef>ID</mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let row; let i = index\">{{ (paginator.pageIndex * paginator.pageSize) + (i + 1) }}</mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"kode_tiket\">\r\n                  <mat-header-cell *matHeaderCellDef>kode_tiket</mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let row\">{{ row.nomor_tiket }}</mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"rute\">\r\n                  <mat-header-cell *matHeaderCellDef>Rute</mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let row\">{{ row.asal }} - {{ row.tujuan }}</mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container cdkColumnDef=\"tanggal\">\r\n                    <mat-header-cell *cdkHeaderCellDef>Tanggal</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let row\">\r\n                        <span>{{ row.tanggal.substring(8, 11) }} {{ bulan(row.tanggal) }} {{ row.tanggal.substring(0, 4) }}</span>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"kendaraan\">\r\n                  <mat-header-cell *matHeaderCellDef>Kendaraan</mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let row\">{{ row.kendaraan.nomor }}</mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container cdkColumnDef=\"telp\">\r\n                    <mat-header-cell *cdkHeaderCellDef>Telepon</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let row\">\r\n                        <p class=\"price text-truncate\">\r\n                            {{row.telepon}}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container cdkColumnDef=\"dewasa\">\r\n                    <mat-header-cell *cdkHeaderCellDef>Dewasa</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let row\">\r\n                        <p class=\"price text-truncate\">\r\n                            {{row.pemudik_dewasa}}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container cdkColumnDef=\"anak\">\r\n                    <mat-header-cell *cdkHeaderCellDef>Anak</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let row\">\r\n                        <p class=\"price text-truncate\">\r\n                            {{row.pemudik_anak}}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container cdkColumnDef=\"status\">\r\n                    <mat-header-cell *cdkHeaderCellDef>Status</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let row\">\r\n                        <mat-icon *ngIf=\"row.status == 5\" class=\"active-icon danger s-16\">cancel</mat-icon>\r\n                        <mat-icon *ngIf=\"row.status == 4\" class=\"active-icon mat-green-600-bg s-16\">check</mat-icon>\r\n                        <mat-icon *ngIf=\"row.status == 3\" class=\"active-icon mat-green-600-bg s-16\">check</mat-icon>\r\n                        <mat-icon *ngIf=\"row.status == 2\" class=\"active-icon danger s-16\">warning</mat-icon>\r\n                        <mat-icon *ngIf=\"row.status == 1\" class=\"active-icon mat-green-600-bg s-16\">check</mat-icon>\r\n                        <mat-icon *ngIf=\"row.status == 0\" class=\"active-icon warning s-16\">warning</mat-icon>\r\n                        <p *ngIf=\"row.status == 5\" class=\"text-truncate\">Ditolak</p>\r\n                        <p *ngIf=\"row.status == 4\" class=\"text-truncate\">Cancel</p>\r\n                        <p *ngIf=\"row.status == 3\" class=\"text-truncate\">Hadir</p>\r\n                        <p *ngIf=\"row.status == 2\" class=\"text-truncate\">Pending</p>\r\n                        <p *ngIf=\"row.status == 1\" class=\"text-truncate\">Disetujui</p>\r\n                        <p *ngIf=\"row.status == 0\" class=\"text-truncate\">Belum Disetujui</p>\r\n                        <!-- {{product.status}} -->\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <mat-header-row *cdkHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n\r\n                <mat-row *cdkRowDef=\"let row; columns: displayedColumns;\"\r\n                         class=\"product\"\r\n                         matRipple\r\n                         (click)=\"clickTiket(row.nomor_tiket, row.status)\">\r\n                </mat-row>\r\n\r\n            </mat-table>\r\n\r\n            <mat-paginator [length]=\"resultsLength\" [pageSize]=\"25\">\r\n            </mat-paginator>\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/tiket/daftarTiket/daftarTiket.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n:host .header .search-input-wrapper {\n  max-width: 480px; }\n:host .header .add-product-button {\n  background-color: #F96D01; }\n@media (max-width: 599px) {\n  :host .header {\n    height: 176px !important;\n    min-height: 176px !important;\n    max-height: 176px !important; } }\n@media (max-width: 599px) {\n  :host .top-bg {\n    height: 240px; } }\n:host .example-container {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  position: relative; }\n:host .example-loading-shade {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 56px;\n  right: 0;\n  background: rgba(0, 0, 0, 0.15);\n  z-index: 1;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n:host .products-table {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  border-bottom: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-header-row {\n    min-height: 35px;\n    min-width: 800px;\n    background-color: rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-row {\n    min-width: 800px; }\n:host .products-table .product {\n    position: relative;\n    cursor: pointer;\n    height: 60px; }\n:host .products-table .mat-cell {\n    min-width: 0;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex; }\n:host .products-table .mat-column-rute {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 20%;\n            flex: 0 1 20%;\n    display: block !important; }\n:host .products-table .mat-column-alamat {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 15%;\n            flex: 0 1 15%; }\n:host .products-table .mat-column-id {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 5%;\n            flex: 0 1 5%; }\n:host .products-table .mat-column-dewasa, :host .products-table .mat-column-anak, :host .products-table .mat-column-kendaraan {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center !important;\n        -ms-flex-pack: center !important;\n            justify-content: center !important; }\n:host .products-table .mat-column-nomor_tiket {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 8%;\n            flex: 0 1 8%; }\n:host .products-table .mat-column-image {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 84px;\n            flex: 0 1 84px; }\n:host .products-table .mat-column-image .product-image {\n      width: 52px;\n      height: 52px;\n      border: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-column-buttons {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 80px;\n            flex: 0 1 80px; }\n:host .products-table .quantity-indicator {\n    display: inline-block;\n    vertical-align: middle;\n    width: 8px;\n    height: 8px;\n    border-radius: 4px;\n    margin-right: 8px; }\n:host .products-table .quantity-indicator + span {\n      display: inline-block;\n      vertical-align: middle; }\n:host .products-table .active-icon {\n    border-radius: 50%;\n    margin-top: 10px;\n    margin-right: 5px; }\n:host .products-table .warning {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 5%;\n            flex: 0 1 5%;\n    color: #f95d01 !important; }\n:host .products-table .danger {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 5%;\n            flex: 0 1 5%;\n    color: #f90101 !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/tiket/daftarTiket/daftarTiket.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarTiketComponent; });
/* unused harmony export ExampleHttpDao */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_of__ = __webpack_require__("../../../../rxjs/_esm5/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_operators_catchError__ = __webpack_require__("../../../../rxjs/_esm5/operators/catchError.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_operators_map__ = __webpack_require__("../../../../rxjs/_esm5/operators/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_operators_startWith__ = __webpack_require__("../../../../rxjs/_esm5/operators/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_operators_switchMap__ = __webpack_require__("../../../../rxjs/_esm5/operators/switchMap.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var DaftarTiketComponent = /** @class */ (function () {
    function DaftarTiketComponent(http, router, _sessionService, _cdr) {
        this.http = http;
        this.router = router;
        this._sessionService = _sessionService;
        this._cdr = _cdr;
        this.displayedColumns = ['id', 'kode_tiket', 'rute', 'tanggal', 'kendaraan', 'telp', 'dewasa', 'anak', 'status'];
        this.dataSource = new __WEBPACK_IMPORTED_MODULE_2__angular_material__["Q" /* MatTableDataSource */]();
        this.resultsLength = 0;
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
    }
    DaftarTiketComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        if (localStorage.getItem('tiket-page')) {
            // console.log('tiket-page: ', localStorage.getItem('tiket-page'));
            this.paginator.pageIndex = Number(localStorage.getItem('tiket-page'));
        }
        this.exampleDatabase = new ExampleHttpDao(this.http, this._sessionService);
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
        this.sub = Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_merge__["a" /* merge */])(this.sort.sortChange, this.paginator.page)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators_startWith__["a" /* startWith */])({}), Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators_switchMap__["a" /* switchMap */])(function () {
            // this.isLoadingResults = true;
            return _this.exampleDatabase.getPemudik(_this.sort.active, _this.sort.direction, _this.paginator.pageIndex);
        }), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators_map__["a" /* map */])(function (data) {
            // this.isLoadingResults = false;
            // this.isRateLimitReached = false;
            _this.resultsLength = data.data['total'];
            return data.data;
        }), Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators_catchError__["a" /* catchError */])(function () {
            // this.isLoadingResults = false;
            // this.isRateLimitReached = true;
            return Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_of__["a" /* of */])([]);
        })).subscribe(function (data) {
            _this.dataSource.data = data['data'];
        });
    };
    DaftarTiketComponent.prototype.clickTiket = function (nomor_tiket, status) {
        if (status != 2 && status != 4) {
            localStorage.setItem('tiket-page', String(this.paginator.pageIndex));
            localStorage.removeItem('pemudik-page');
            localStorage.removeItem('motor-page');
            var url = '/apps/tiket/detailTiket/' + nomor_tiket;
            this.router.navigate([url]);
        }
    };
    DaftarTiketComponent.prototype.clear = function () {
        var _this = this;
        this.sub.unsubscribe();
        this.paginator.pageIndex = 0;
        this.sub = Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_merge__["a" /* merge */])(this.sort.sortChange, this.paginator.page)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators_startWith__["a" /* startWith */])({}), Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators_switchMap__["a" /* switchMap */])(function () {
            _this.isLoadingResults = true;
            return _this.exampleDatabase.getPemudik(_this.sort.active, _this.sort.direction, _this.paginator.pageIndex);
        }), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators_map__["a" /* map */])(function (data) {
            _this.isLoadingResults = false;
            _this.isRateLimitReached = false;
            _this.resultsLength = data.data['total'];
            return data.data;
        }), Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators_catchError__["a" /* catchError */])(function () {
            _this.isLoadingResults = false;
            _this.isRateLimitReached = true;
            return Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_of__["a" /* of */])([]);
        })).subscribe(function (data) {
            _this.dataSource.data = data['data'];
        });
    };
    DaftarTiketComponent.prototype.search = function () {
        var _this = this;
        this.sub.unsubscribe();
        this.paginator.pageIndex = 0;
        var search = this.search_query;
        this.sub = Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_merge__["a" /* merge */])(this.sort.sortChange, this.paginator.page)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators_startWith__["a" /* startWith */])({}), Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators_switchMap__["a" /* switchMap */])(function () {
            _this.isLoadingResults = true;
            return _this.exampleDatabase.getPemudikSearch(_this.sort.active, _this.sort.direction, search, _this.paginator.pageIndex);
        }), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators_map__["a" /* map */])(function (data) {
            _this.isLoadingResults = false;
            _this.isRateLimitReached = false;
            _this.resultsLength = data.data['total'];
            return data.data;
        }), Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators_catchError__["a" /* catchError */])(function () {
            _this.isLoadingResults = false;
            _this.isRateLimitReached = true;
            return Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_of__["a" /* of */])([]);
        })).subscribe(function (data) {
            _this.dataSource.data = data['data'];
        });
    };
    DaftarTiketComponent.prototype.filterButton = function (filter) {
        var _this = this;
        this.sub.unsubscribe();
        this.paginator.pageIndex = 0;
        this.sub = Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_merge__["a" /* merge */])(this.sort.sortChange, this.paginator.page)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators_startWith__["a" /* startWith */])({}), Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators_switchMap__["a" /* switchMap */])(function () {
            _this.isLoadingResults = true;
            return _this.exampleDatabase.getPemudikFilter(_this.sort.active, _this.sort.direction, filter, _this.paginator.pageIndex);
        }), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators_map__["a" /* map */])(function (data) {
            _this.isLoadingResults = false;
            _this.isRateLimitReached = false;
            _this.resultsLength = data.data['total'];
            return data.data;
        }), Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators_catchError__["a" /* catchError */])(function () {
            _this.isLoadingResults = false;
            _this.isRateLimitReached = true;
            return Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_of__["a" /* of */])([]);
        })).subscribe(function (data) {
            _this.dataSource.data = data['data'];
        });
    };
    DaftarTiketComponent.prototype.bulan = function (date) {
        if (date.substring(5, 7) === '01') {
            return "Januari";
        }
        else if (date.substring(5, 7) === '02') {
            return "Februari";
        }
        else if (date.substring(5, 7) === '03') {
            return "Maret";
        }
        else if (date.substring(5, 7) === '04') {
            return "April";
        }
        else if (date.substring(5, 7) === '05') {
            return "Mei";
        }
        else if (date.substring(5, 7) === '06') {
            return "Juni";
        }
        else if (date.substring(5, 7) === '07') {
            return "Juli";
        }
        else if (date.substring(5, 7) === '08') {
            return "Agustus";
        }
        else if (date.substring(5, 7) === '09') {
            return "September";
        }
        else if (date.substring(5, 7) === '10') {
            return "Oktober";
        }
        else if (date.substring(5, 7) === '11') {
            return "November";
        }
        else if (date.substring(5, 7) === '12') {
            return "Desember";
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["z" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__angular_material__["z" /* MatPaginator */])
    ], DaftarTiketComponent.prototype, "paginator", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["M" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__angular_material__["M" /* MatSort */])
    ], DaftarTiketComponent.prototype, "sort", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('filter'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], DaftarTiketComponent.prototype, "filter", void 0);
    DaftarTiketComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-products',
            template: __webpack_require__("../../../../../src/app/main/content/apps/tiket/daftarTiket/daftarTiket.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/tiket/daftarTiket/daftarTiket.component.scss")],
            animations: __WEBPACK_IMPORTED_MODULE_9__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_11__angular_router__["f" /* Router */], __WEBPACK_IMPORTED_MODULE_12__session_service__["a" /* SessionService */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]])
    ], DaftarTiketComponent);
    return DaftarTiketComponent;
}());

/** An example database that the data source uses to retrieve data for the table. */
var ExampleHttpDao = /** @class */ (function () {
    function ExampleHttpDao(http, _sessionService) {
        this.http = http;
        this._sessionService = _sessionService;
    }
    ExampleHttpDao.prototype.getPemudik = function (sort, order, page) {
        var href = __WEBPACK_IMPORTED_MODULE_10__environments_environment__["a" /* environment */].setting.base_url + '/admin/tiket';
        var requestUrl = href + "?page=" + (page + 1);
        return this.http.get(requestUrl, httpOptions);
    };
    ExampleHttpDao.prototype.getPemudikFilter = function (sort, order, search, page) {
        var href = __WEBPACK_IMPORTED_MODULE_10__environments_environment__["a" /* environment */].setting.base_url + '/admin/tiket/' + search + ("?page=" + (page + 1));
        return this.http.get(href, httpOptions);
    };
    ExampleHttpDao.prototype.getPemudikSearch = function (sort, order, search, page) {
        var href = __WEBPACK_IMPORTED_MODULE_10__environments_environment__["a" /* environment */].setting.base_url + '/admin/tiket/search/' + search + ("?page=" + (page + 1));
        return this.http.get(href, httpOptions);
    };
    return ExampleHttpDao;
}());

var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpHeaders */]({
        'Content-Type': 'application/json',
        'Authorization': "Bearer " + JSON.parse(localStorage.getItem('currentUser')).token
    })
};


/***/ }),

/***/ "../../../../../src/app/main/content/apps/tiket/daftarTiket/daftarTiket.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarTiketService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DaftarTiketService = /** @class */ (function () {
    function DaftarTiketService(http, _sessionService) {
        this.http = http;
        this._sessionService = _sessionService;
        this.token = this._sessionService.get().token();
        this.onProductsChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({});
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DaftarTiketService.prototype.resolve = function (route, state) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getProducts()
            ]).then(function () {
                resolve();
            }, reject);
        });
    };
    DaftarTiketService.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        options.headers = headers;
    };
    DaftarTiketService.prototype.getProducts = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/tiket', options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                _this.products = response['data'];
                if (Number(_this._sessionService.get().role) < 3) {
                    for (var i = 0; i < _this.products.length; ++i) {
                        if (_this.products[i]["status"] != 0 && _this.products[i]["status"] != 1) {
                            _this.products.splice(i, 1);
                            i--;
                        }
                    }
                }
                _this.onProductsChanged.next(_this.products);
                resolve(response);
            }, reject);
        });
    };
    DaftarTiketService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__session_service__["a" /* SessionService */]])
    ], DaftarTiketService);
    return DaftarTiketService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/tiket/detailTiket/detailTiket.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"product\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"start center\" style=\"width: 80%; height: 50px !important; min-height: 50px !important;\">\r\n\r\n                <button class=\"mr-0 mr-sm-16\" \r\n                        mat-icon-button \r\n                        (click)=\"back()\">\r\n                    <mat-icon>arrow_back</mat-icon>\r\n                </button>\r\n\r\n                <div fxLayout=\"column\" fxLayoutAlign=\"start start\"\r\n                     *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">\r\n                    <div class=\"h2\" *ngIf=\"pageType ==='edit'\">\r\n                        {{product.nomor_tiket}} - \r\n                        <span *ngIf=\"product.status == 0\">Belum Disetujui</span>\r\n                        <span *ngIf=\"product.status == 1\">Sudah Disetujui</span>\r\n                        <span *ngIf=\"product.status == 2\">Pending (Proses Pengisian Data)</span>\r\n                        <span *ngIf=\"product.status == 3\">Hadir</span>\r\n                        <span *ngIf=\"product.status == 4\">Batal</span>\r\n                    </div>\r\n                    <div class=\"h2\" *ngIf=\"pageType ==='new'\">\r\n                        Tambah Pemudik\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <button mat-raised-button \r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    *ngIf=\"pageType === 'edit' && !tiketForm.pristine\" \r\n                    (click)=\"saveSwal.show()\">\r\n                <span>Simpan</span>\r\n            </button>\r\n            <!-- <button mat-raised-button \r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    *ngIf=\"pageType === 'edit'\" \r\n                    (click)=\"deleteTiketSwal.show()\">\r\n                <span>Hapus</span>\r\n            </button> -->\r\n            <button mat-raised-button \r\n                    class=\"accept-product-button mt-16 mt-sm-0\"\r\n                    *ngIf=\"pageType == 'edit' && status == 0\" \r\n                    (click)=\"approveSwal.show()\">\r\n                <span>Setujui</span>\r\n            </button>\r\n            <button mat-raised-button \r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    *ngIf=\"pageType == 'edit' && status != 4 && status != 5\" \r\n                    (click)=\"declineSwal.show()\">\r\n                <span>Tolak</span>\r\n            </button>\r\n            <button mat-raised-button \r\n                    class=\"save-product-button mt-16 mt-sm-0\"\r\n                    *ngIf=\"pageType === 'edit' && status == 1\" \r\n                    (click)=\"print(product.nomor_tiket)\">\r\n                <span>Print</span>\r\n            </button>\r\n\r\n            <swal\r\n                #approveSwal\r\n                title=\"Apakah anda yakin akan menyetujui tiket ini ?\"\r\n                type=\"question\"\r\n                [showCancelButton]=\"true\"\r\n                [focusCancel]=\"true\"\r\n                (confirm)=\"approve()\">\r\n            </swal>\r\n\r\n            <swal\r\n                #declineSwal\r\n                title=\"Apakah anda yakin akan menolak tiket ini ?\"\r\n                type=\"question\"\r\n                [showCancelButton]=\"true\"\r\n                [focusCancel]=\"true\"\r\n                (confirm)=\"decline()\">\r\n            </swal>\r\n\r\n            <swal\r\n                #saveSwal\r\n                title=\"Apakah anda yakin ?\"\r\n                type=\"question\"\r\n                [showCancelButton]=\"true\"\r\n                [focusCancel]=\"true\"\r\n                (confirm)=\"simpan()\">\r\n            </swal>\r\n\r\n            <swal\r\n                #deleteTiketSwal\r\n                title=\"Apakah anda yakin mengahapus tiket ini ?\"\r\n                type=\"question\"\r\n                [showCancelButton]=\"true\"\r\n                [focusCancel]=\"true\"\r\n                (confirm)=\"hapusTiket()\">\r\n            </swal>\r\n\r\n            <swal\r\n                #successSwal\r\n                title=\"Action Success\"\r\n                type=\"success\"\r\n                showConfirmButton='false'\r\n                timer='1500'>\r\n            </swal>\r\n            \r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <!-- CONTENT -->\r\n            <div class=\"content\">\r\n\r\n                <form name=\"tiketForm\" [formGroup]=\"tiketForm\" *ngIf=\"pageType=='edit'\" class=\"product w-100-p\" fxLayout=\"column\" fxFlex>\r\n                \r\n\r\n                    <mat-card class=\"example-card\" *ngIf=\"product\">\r\n                        <mat-card-header>\r\n                            <mat-card-title>Informasi Tiket</mat-card-title>\r\n                        </mat-card-header>\r\n                        <mat-divider class=\"divider\"> </mat-divider>\r\n                        <mat-card-content>\r\n                            <table class=\"table-info\">\r\n                                <tr class=\"tr\">\r\n                                    <td class=\"td\">Alamat</td>\r\n                                    <td class=\"td\">:</td>\r\n                                    <td class=\"td\">\r\n                                        <mat-form-field class=\"table-form-field\">\r\n                                            <input matInput\r\n                                                   name=\"alamat\"\r\n                                                   formControlName=\"alamat\"\r\n                                                   required>\r\n                                            <mat-icon matPrefix> place </mat-icon>\r\n                                        </mat-form-field>\r\n                                    </td>\r\n                                    <td class=\"td\"></td>\r\n                                </tr>\r\n                                <tr class=\"tr\">\r\n                                    <td class=\"td\">Telepon</td>\r\n                                    <td class=\"td\">:</td>\r\n                                    <td class=\"td\">\r\n                                        <mat-form-field class=\"table-form-field\">\r\n                                            <input matInput\r\n                                                   name=\"telepon\"\r\n                                                   formControlName=\"telepon\"\r\n                                                   required>\r\n                                            <mat-icon matPrefix> smartphone </mat-icon>\r\n                                        </mat-form-field>\r\n                                    </td>\r\n                                    <td class=\"td\"></td>\r\n                                </tr>\r\n                                <tr class=\"tr\">\r\n                                    <td class=\"td\">Kota Tujuan</td>\r\n                                    <td class=\"td\">:</td>\r\n                                    <td class=\"td\">\r\n                                        <mat-form-field class=\"table-form-field\">\r\n                                            <input matInput\r\n                                                   name=\"kota_tujuan\"\r\n                                                   formControlName=\"kota_tujuan\"\r\n                                                   required>\r\n                                            <mat-icon matPrefix> place </mat-icon>\r\n                                        </mat-form-field>\r\n                                    </td>\r\n                                    <td class=\"td\"></td>\r\n                                </tr>\r\n                                <tr class=\"tr\">\r\n                                    <td class=\"td\">Rute</td>\r\n                                    <td class=\"td\">:</td>\r\n                                    <td class=\"td\">\r\n                                        {{ product.rute.kota.asal }} - {{ product.rute.kota.tujuan }} - {{ product.rute.keterangan }}\r\n                                    </td>\r\n                                    <td class=\"td\"></td>\r\n                                </tr>\r\n                                <tr class=\"tr\">\r\n                                    <td class=\"td\">Tanggal Berangkat</td>\r\n                                    <td class=\"td\">:</td>\r\n                                    <td class=\"td\">\r\n                                        {{ product.rute.tanggal }} - {{ product.rute.jam }}\r\n                                    </td>\r\n                                    <td class=\"td\"></td>\r\n                                </tr>\r\n                                <tr class=\"tr\">\r\n                                    <td class=\"td\">Kendaraan</td>\r\n                                    <td class=\"td\">:</td>\r\n                                    <td class=\"td\">\r\n                                        {{ product.kendaraan.nomor }}\r\n                                    </td>\r\n                                    <td class=\"td\">\r\n                                        <button mat-raised-button color=\"accent\" (click)=\"kendaraanSwal(product.kendaraan.id)\">Detail</button>\r\n                                    </td>\r\n                                </tr>\r\n                            </table>\r\n                        </mat-card-content>\r\n                    </mat-card>\r\n\r\n                    <mat-card class=\"example-card\">\r\n                        <mat-card-header>\r\n                            <mat-card-title> \r\n                                <button mat-raised-button color=\"accent\" (click)=\"editPemudik()\">Edit</button>\r\n                            </mat-card-title>\r\n                        </mat-card-header>\r\n                        <mat-divider class=\"divider\"> </mat-divider>\r\n                        <mat-card-content>\r\n                            <table class=\"table-info\" *ngIf=\"pageType2 === 'view'\">\r\n                                <tr class=\"tr\">\r\n                                    <td class=\"th\">No.</td>\r\n                                    <th class=\"th\">Nama</th>\r\n                                    <th class=\"th\">NIK</th>\r\n                                    <th class=\"th\">Jenis Kelamin</th>\r\n                                    <th class=\"th\">Tanggal Lahir</th>\r\n                                    <th class=\"th\">Tempat Lahir</th>\r\n                                    <th class=\"th\">Pekerjaan</th>\r\n                                    <th class=\"th\">Kursi</th>\r\n                                    <!-- <th class=\"th\"></th> -->\r\n                                </tr>\r\n                                <tr *ngFor=\"let pemudik of product.pemudik; let i = index\" class=\"tr\">\r\n                                    <td class=\"td\"> {{ i+1 }} </td>\r\n                                    <td class=\"td\"> \r\n                                        <div style=\"display: flex\">\r\n                                            <mat-icon class=\"icon-pemudik\"> \r\n                                                {{ i < product.pemudik_dewasa ? 'accessibility_new' : 'child_care'}} \r\n                                            </mat-icon> \r\n                                            <div>{{ pemudik.nama }}</div>\r\n                                        </div>\r\n                                    </td>\r\n                                    <td class=\"td\"> {{ pemudik.nik }} </td>\r\n                                    <td class=\"td\"> {{ pemudik.gender == 1 ? 'Laki-Laki' : 'Perempuan' }} </td>\r\n                                    <td class=\"td\"> {{ pemudik.tanggal_lahir }} </td>\r\n                                    <td class=\"td\"> {{ pemudik.tempat_lahir }} </td>\r\n                                    <td class=\"td-css\" *ngIf=\"pemudik.pekerjaan == 1\"> PNS / TNI / Polri / Pensiunan </td>\r\n                                    <td class=\"td-css\" *ngIf=\"pemudik.pekerjaan == 2\"> Wiraswasta </td>\r\n                                    <td class=\"td-css\" *ngIf=\"pemudik.pekerjaan == 3\"> Karyawan Swasta / Buruh</td>\r\n                                    <td class=\"td-css\" *ngIf=\"pemudik.pekerjaan == 4\"> Pelajar / Mahasiswa </td>\r\n                                    <td class=\"td-css\" *ngIf=\"pemudik.pekerjaan == 5\"> Pembantu Rumah Tangga </td>\r\n                                    <td class=\"td-css\" *ngIf=\"pemudik.pekerjaan == 6\"> Dokter / Pengacara / Profesional </td>\r\n                                    <td class=\"td-css\" *ngIf=\"pemudik.pekerjaan == 7\"> Tidak Bekerja / Ibu Rumah Tangga / Lainnya </td>\r\n                                    <td class=\"td\"> {{ pemudik.kursi }} </td>\r\n                                    <!-- <td class=\"td\" (click)=\"deletePemudik(pemudik.id)\"> <mat-icon> delete </mat-icon> </td> -->\r\n                                </tr>\r\n                            </table>\r\n\r\n                            <div formArrayName=\"pemudiks\" *ngIf=\"pageType2 === 'edit'\">\r\n                                <mat-card class=\"example-card\" \r\n                                          *ngFor=\"let item of tiketForm.controls.pemudiks.controls; let i = index\" \r\n                                          [formGroupName]=\"i\">\r\n                                    <mat-card-header>\r\n                                        <mat-card-title *ngIf=\"i<product.pemudik_dewasa\">Pemudik Dewasa</mat-card-title>\r\n                                        <mat-card-title *ngIf=\"i>=product.pemudik_dewasa\">Pemudik Anak</mat-card-title>\r\n                                    </mat-card-header>\r\n                                    <mat-divider class=\"divider\"></mat-divider>\r\n                                    <mat-card-content>\r\n                                        <mat-grid-list cols=\"4\" rowHeight=\"80px\">\r\n                                            <mat-grid-tile [colspan]=\"1\">\r\n                                                <mat-form-field appearance=\"outline\">\r\n                                                    <mat-label>Nama</mat-label>\r\n                                                    <input matInput name=\"nama\" formControlName=\"nama\" placeholder=\"Nama\">\r\n                                                    <mat-icon matPrefix>person</mat-icon>\r\n                                                </mat-form-field>\r\n                                            </mat-grid-tile>\r\n                                            <mat-grid-tile [colspan]=\"1\">\r\n                                                <mat-form-field appearance=\"outline\">\r\n                                                    <mat-label>NIK</mat-label>\r\n                                                    <input matInput \r\n                                                           name=\"nik\" \r\n                                                           formControlName=\"nik\" \r\n                                                           placeholder=\"NIK\"\r\n                                                           type=\"number\">\r\n                                                    <mat-icon matPrefix>chrome_reader_mode</mat-icon>\r\n                                                </mat-form-field>\r\n                                            </mat-grid-tile>\r\n                                            <mat-grid-tile [colspan]=\"1\">\r\n                                                <mat-form-field appearance=\"outline\">\r\n                                                    <mat-label>Jenis Kelamin</mat-label>\r\n                                                    <mat-select matInput formControlName=\"gender\" \r\n                                                                placeholder=\"Jenis Kelamin\"\r\n                                                                required>\r\n                                                        <mat-option [value]=\"1\"> Laki-laki </mat-option>\r\n                                                        <mat-option [value]=\"2\"> Perempuan </mat-option>\r\n                                                    </mat-select>\r\n                                                    <mat-icon matPrefix> wc </mat-icon>\r\n                                                </mat-form-field>\r\n                                            </mat-grid-tile>\r\n                                            <mat-grid-tile [colspan]=\"1\">\r\n                                                <mat-form-field appearance=\"outline\">\r\n                                                    <mat-label>Tanggal Lahir</mat-label>\r\n                                                    <input matInput type=\"date\" name=\"tanggal_lahir\" \r\n                                                           formControlName=\"tanggal_lahir\" placeholder=\"Tanggal Lahir\">\r\n                                                    <mat-icon matPrefix>date_range</mat-icon>\r\n                                                </mat-form-field>\r\n                                            </mat-grid-tile>\r\n                                            <mat-grid-tile [colspan]=\"1\">\r\n                                                <mat-form-field appearance=\"outline\">\r\n                                                    <mat-label>Tempat Lahir</mat-label>\r\n                                                    <input matInput name=\"tempat_lahir\" formControlName=\"tempat_lahir\" \r\n                                                           placeholder=\"Tempat Lahir\" required>\r\n                                                    <mat-icon matPrefix>place</mat-icon>\r\n                                                </mat-form-field>\r\n                                            </mat-grid-tile>\r\n                                            <!-- <mat-grid-tile [colspan]=\"1\" *ngIf=\"i < product.pemudik_dewasa\">\r\n                                                <mat-form-field appearance=\"outline\">\r\n                                                    <mat-label>Pekerjaan</mat-label>\r\n                                                    <mat-select matInput formControlName=\"pekerjaan\" \r\n                                                                placeholder=\"Pekerjaan\"\r\n                                                                required>\r\n                                                        <mat-option value=\"1\"> PNS / TNI / Polri / Pensiunan </mat-option>\r\n                                                        <mat-option value=\"2\"> Wiraswasta </mat-option>\r\n                                                        <mat-option value=\"3\"> Karyawan Swasta / Buruh</mat-option>\r\n                                                        <mat-option value=\"4\"> Pelajar / Mahasiswa </mat-option>\r\n                                                        <mat-option value=\"5\"> Pembantu Rumah Tangga </mat-option>\r\n                                                        <mat-option value=\"6\"> Dokter / Pengacara / Profesional </mat-option>\r\n                                                        <mat-option value=\"7\"> Tidak Bekerja / Ibu Rumah Tangga / Lainnya </mat-option>\r\n                                                    </mat-select>\r\n                                                    <mat-icon matPrefix> work </mat-icon>\r\n                                                </mat-form-field>\r\n                                            </mat-grid-tile> -->\r\n                                            <mat-grid-tile [colspan]=\"1\" *ngIf=\"product.pemudik[i].jenis === 1\">\r\n                                                <mat-form-field appearance=\"outline\">\r\n                                                    <mat-label>Pindah Kursi</mat-label>\r\n                                                    <mat-select matInput formControlName=\"pindah_kursi\" \r\n                                                                placeholder=\"{{ tiketForm.controls.pemudiks.controls[i].controls.pindah_kursi.value }}\">\r\n                                                        <mat-option value=\"{{ tiketForm.controls.pemudiks.controls[i].controls.pindah_kursi.value }}\"> {{ product.pemudik[i].kursi }} (Kursi Sekarang) </mat-option>\r\n                                                        <mat-option \r\n                                                                *ngFor=\"let kursi of kursi_available\" \r\n                                                                [value]=\"kursi.id\"> \r\n                                                            {{ kursi.nama }}\r\n                                                        </mat-option>\r\n                                                    </mat-select>\r\n                                                    <mat-icon matPrefix> wc </mat-icon>\r\n                                                </mat-form-field>\r\n                                            </mat-grid-tile>\r\n                                            <mat-grid-tile [colspan]=\"1\" *ngIf=\"motor\">\r\n                                                <mat-form-field appearance=\"outline\">\r\n                                                    <mat-label>SIM</mat-label>\r\n                                                    <input matInput \r\n                                                           name=\"sim\" \r\n                                                           formControlName=\"sim\" \r\n                                                           placeholder=\"SIM\"\r\n                                                           type=\"number\">\r\n                                                    <mat-icon matPrefix>chrome_reader_mode</mat-icon>\r\n                                                </mat-form-field>\r\n                                            </mat-grid-tile>\r\n                                            <mat-grid-tile [colspan]=\"1\" *ngIf=\"motor\">\r\n                                                <mat-form-field appearance=\"outline\">\r\n                                                    <mat-label>SIM</mat-label>\r\n                                                    <input matInput \r\n                                                           name=\"stnk\" \r\n                                                           formControlName=\"stnk\" \r\n                                                           placeholder=\"STNK\"\r\n                                                           type=\"number\">\r\n                                                    <mat-icon matPrefix>chrome_reader_mode</mat-icon>\r\n                                                </mat-form-field>\r\n                                            </mat-grid-tile>\r\n                                        </mat-grid-list>\r\n                                    </mat-card-content>\r\n                                    <mat-card-actions>\r\n                                        <button mat-raised-button color=\"accent\" (click)=\"savePemudikSwal.show()\">\r\n                                            Simpan\r\n                                        </button>\r\n                                        <button mat-raised-button color=\"warn\" (click)=\"hapusPemudikSwal.show()\">\r\n                                            Hapus Pemudik\r\n                                        </button>\r\n                                    </mat-card-actions>\r\n                                    <swal\r\n                                        #savePemudikSwal\r\n                                        title=\"Apakah anda yakin ?\"\r\n                                        type=\"question\"\r\n                                        [showCancelButton]=\"true\"\r\n                                        [focusCancel]=\"true\"\r\n                                        (confirm)=\"simpanPemudik(i)\">\r\n                                    </swal>\r\n                                    <swal\r\n                                        #hapusPemudikSwal\r\n                                        title=\"Apakah anda yakin akan menghapus pemudik ini ?\"\r\n                                        type=\"warning\"\r\n                                        [showCancelButton]=\"true\"\r\n                                        [focusCancel]=\"true\"\r\n                                        (confirm)=\"hapusPemudik(i)\">\r\n                                    </swal>\r\n                                    <mat-divider class=\"divider\"></mat-divider>\r\n                                </mat-card>\r\n                            </div>\r\n                        </mat-card-content>\r\n                    </mat-card>\r\n\r\n                    <mat-card>\r\n                        <mat-card-header>\r\n                            <mat-card-title> Daftar Foto </mat-card-title>\r\n                        </mat-card-header>\r\n                        <mat-grid-list cols=\"3\">\r\n                            <mat-grid-tile [colspan]=\"1\">\r\n                                <mat-card>\r\n                                    <mat-card-header>\r\n                                        <mat-card-title> Foto KK </mat-card-title>\r\n                                    </mat-card-header>\r\n                                    <mat-divider></mat-divider>\r\n                                    <mat-card-content>\r\n                                        <img class=\"foto_kk\" height=\"200px\" width=\"300px\" \r\n                                             src=\"{{url_image + product.foto_kk}}\" \r\n                                             (click)=\"openDialog(url_image + product.foto_kk.kartu_keluarga)\">\r\n                                        <img class=\"foto_kk\" *ngIf=\"!product.foto_kk\" height=\"200px\" width=\"300px\" \r\n                                             src=\"assets/images/avatars/kk-kosong.jpg\" (click)=\"openDialog('assets/images/avatars/kk-kosong.jpg')\">\r\n                                    </mat-card-content>\r\n                                </mat-card>\r\n                            </mat-grid-tile>\r\n                            <mat-grid-tile [colspan]=\"1\" *ngFor=\"let pemudik of product.pemudik\">\r\n                                <mat-card>\r\n                                    <mat-card-header>\r\n                                        <mat-card-title> Foto {{ pemudik.nama }} </mat-card-title>\r\n                                    </mat-card-header>\r\n                                    <mat-divider></mat-divider>\r\n                                    <mat-card-content>\r\n                                        <img class=\"foto_pemudik\" *ngIf=\"pemudik.foto_pemudik\" height=\"200px\" \r\n                                             width=\"300px\" src=\"{{url_image + pemudik.foto_pemudik.nik}}\" \r\n                                             (click)=\"openDialog(url_image + pemudik.foto_pemudik.nik)\">\r\n                                        <img class=\"foto_pemudik\" *ngIf=\"!pemudik.foto_pemudik\" height=\"200px\" \r\n                                             width=\"300px\" src=\"assets/images/avatars/profile.jpg\" \r\n                                             (click)=\"openDialog('assets/images/avatars/profile.jpg')\">\r\n                                    </mat-card-content>\r\n                                </mat-card>\r\n                            </mat-grid-tile>\r\n                        </mat-grid-list>\r\n                    </mat-card>\r\n\r\n                </form>\r\n\r\n                <mat-horizontal-stepper *ngIf=\"pageType == 'new'\" [linear]=\"isLinear\" #stepper=\"matHorizontalStepper\" #stepper>\r\n                    \r\n                    <mat-step [stepControl]=\"informasiMudikForm1\">\r\n                        <form [formGroup]=\"informasiMudikForm1\">\r\n                            <ng-template matStepLabel>Informasi Mudik</ng-template>\r\n                            <mat-grid-list cols=\"2\" rowHeight=\"300px\">\r\n                                <mat-grid-tile [colspan]=\"1\">\r\n                                    <mat-card class=\"example-card w-100-p\">\r\n                                        <mat-card-header>\r\n                                            <mat-card-title>\r\n                                                <mat-checkbox (change)=\"mudik()\">\r\n                                                    <img src=\"assets/images/avatars/Slice_4.png\">\r\n                                                    <div class=\"title\">Mudik</div>\r\n                                                </mat-checkbox>\r\n                                            </mat-card-title>\r\n                                        </mat-card-header>\r\n                                        <mat-card-content>\r\n                                            <mat-form-field class=\"w-100-p\">\r\n                                                <mat-select formControlName=\"asal_mudik\" placeholder=\"Pilih Asal Mudik\" (change)=\"updateTujuanMudik()\">\r\n                                                    <mat-option *ngFor=\"let kota of asalMudik\" [value]=\"kota\"> {{ kota }} </mat-option>\r\n                                                </mat-select>\r\n                                                <mat-error *ngIf=\"informasiMudikForm1.controls.asal_mudik.invalid\">\r\n                                                     {{informasiMudikForm1.controls.asal_mudik.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                                </mat-error>\r\n                                                <mat-icon matPrefix> add_location </mat-icon>\r\n                                            </mat-form-field>\r\n                                            <mat-form-field class=\"w-100-p\">\r\n                                                <mat-select formControlName=\"tujuan_mudik\" placeholder=\"Pilih Tujuan\" (change)=\"updateRuteMudik()\">\r\n                                                    <mat-option *ngFor=\"let tujuan of tujuanMudik\" [value]=\"tujuan\"> {{ tujuan }} </mat-option>\r\n                                                </mat-select>\r\n                                                <mat-error *ngIf=\"informasiMudikForm1.controls.tujuan_mudik.invalid\">\r\n                                                     {{informasiMudikForm1.controls.tujuan_mudik.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                                </mat-error>\r\n                                                <mat-icon matPrefix> add_location </mat-icon>\r\n                                            </mat-form-field>\r\n                                            <mat-form-field class=\"w-100-p\">\r\n                                                <mat-select formControlName=\"rute_mudik\" placeholder=\"Pilih Rute dan Tanggal Mudik\" (change)=\"selectRuteMudik()\">\r\n                                                    <mat-option *ngFor=\"let rute of ruteMudik\" [disabled]=\"rute.sisa < 1 ? true : false\" [value]=\"rute.id\"> {{ rute.tanggal.substring(8, 11) }} {{ bulan(rute.tanggal) }} {{ rute.tanggal.substring(0, 4) }} - Kursi Tersedia : {{ rute.sisa }} - {{rute.keterangan}}</mat-option>\r\n                                                </mat-select>\r\n                                                <mat-error *ngIf=\"informasiMudikForm1.controls.rute_mudik.invalid\">\r\n                                                     {{informasiMudikForm1.controls.rute_mudik.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                                </mat-error>\r\n                                                <mat-icon matPrefix> date_range </mat-icon>\r\n                                            </mat-form-field>\r\n                                            <!-- <mat-form-field class=\"w-100-p\">\r\n                                                <mat-select formControlName=\"rute_mudik_motor\" placeholder=\"Pilih Rute dan Tanggal Mudik Motor\" (change)=\"selectRuteMudikMotor()\">\r\n                                                    <mat-option [value]=\"\"> Pilih Rute Mudik Motor </mat-option>\r\n                                                    <mat-option *ngFor=\"let rute of ruteMudikMotor\" [disabled]=\"rute.kouta_motor < 1 ? true : false\" [value]=\"rute.id\"> {{ rute.tanggal.substring(8, 11) }} {{ bulan(rute.tanggal) }} {{ rute.tanggal.substring(0, 4) }} - {{rute.keterangan}}</mat-option>\r\n                                                </mat-select>\r\n                                                <mat-error *ngIf=\"informasiMudikForm1.controls.rute_mudik_motor.invalid\">\r\n                                                     {{informasiMudikForm1.controls.rute_mudik_motor.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                                </mat-error>\r\n                                                <mat-icon matPrefix> date_range </mat-icon>\r\n                                            </mat-form-field> -->\r\n                                        </mat-card-content>\r\n                                    </mat-card>\r\n                                </mat-grid-tile>\r\n                                <mat-grid-tile [colspan]=\"1\">\r\n                                    <mat-card class=\"example-card w-100-p\">\r\n                                        <mat-card-header>\r\n                                            <mat-card-title>\r\n                                                <mat-checkbox (change)=\"balik()\">\r\n                                                    <img src=\"assets/images/avatars/Slice_5.png\">\r\n                                                    <div class=\"title\">Balik</div>\r\n                                                </mat-checkbox>\r\n                                            </mat-card-title>\r\n                                        </mat-card-header>\r\n                                        <mat-card-content>\r\n                                            <mat-form-field class=\"w-100-p\">\r\n                                                <mat-select formControlName=\"asal_balik\" \r\n                                                            placeholder=\"Pilih Asal Mudik\" \r\n                                                            (change)=\"updateTujuanBalik()\">\r\n                                                    <mat-option *ngFor=\"let kota of asalBalik\" [value]=\"kota\"> {{ kota }} </mat-option>\r\n                                                </mat-select>\r\n                                                <mat-error *ngIf=\"informasiMudikForm1.controls.asal_balik.invalid\">\r\n                                                     {{informasiMudikForm1.controls.asal_balik.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                                </mat-error>\r\n                                                <mat-icon matPrefix> add_location </mat-icon>\r\n                                            </mat-form-field>\r\n                                            <mat-form-field class=\"w-100-p\">\r\n                                                <mat-select formControlName=\"tujuan_balik\" \r\n                                                            placeholder=\"Pilih Tujuan\"\r\n                                                            (change)=\"updateRuteBalik()\">\r\n                                                    <mat-option *ngFor=\"let tujuan of tujuanBalik\" [value]=\"tujuan\"> {{ tujuan }} </mat-option>\r\n                                                </mat-select>\r\n                                                <mat-error *ngIf=\"informasiMudikForm1.controls.tujuan_balik.invalid\">\r\n                                                     {{informasiMudikForm1.controls.tujuan_balik.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                                </mat-error>\r\n                                                <mat-icon matPrefix> add_location </mat-icon>\r\n                                            </mat-form-field>\r\n                                            <mat-form-field class=\"w-100-p\">\r\n                                                <mat-select formControlName=\"rute_balik\" placeholder=\"Pilih Tanggal Mudik\" (change)=\"selectRuteBalik()\">\r\n                                                    <mat-option *ngFor=\"let rute of ruteBalik\" [disabled]=\"rute.sisa < 1 ? true : false\" [value]=\"rute.id\"> {{ rute.tanggal.substring(8, 11) }} {{ bulan(rute.tanggal) }} {{ rute.tanggal.substring(0, 4) }} - Kursi Tersedia : {{ rute.sisa }} - {{rute.keterangan}}</mat-option>\r\n                                                </mat-select>\r\n                                                <mat-error *ngIf=\"informasiMudikForm1.controls.rute_balik.invalid\">\r\n                                                     {{informasiMudikForm1.controls.rute_balik.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                                </mat-error>\r\n                                                <mat-icon matPrefix> date_range </mat-icon>\r\n                                            </mat-form-field>\r\n                                            <!-- <mat-form-field class=\"w-100-p\">\r\n                                                <mat-select formControlName=\"rute_balik_motor\" placeholder=\"Pilih Rute dan Tanggal Balik Motor\" (change)=\"selectRuteBalikMotor()\">\r\n                                                    <mat-option [value]=\"\"> Pilih Rute Balik Motor </mat-option>\r\n                                                    <mat-option *ngFor=\"let rute of ruteBalikMotor\" [disabled]=\"rute.kouta_motor < 1 ? true : false\" [value]=\"rute.id\"> {{ rute.tanggal.substring(8, 11) }} {{ bulan(rute.tanggal) }} {{ rute.tanggal.substring(0, 4) }} - {{rute.keterangan}}</mat-option>\r\n                                                </mat-select>\r\n                                                <mat-error *ngIf=\"informasiMudikForm1.controls.rute_balik_motor.invalid\">\r\n                                                     {{informasiMudikForm1.controls.rute_balik_motor.errors['required'] ? 'Tidak Boleh Kosong': ''}}\r\n                                                </mat-error>\r\n                                                <mat-icon matPrefix> date_range </mat-icon>\r\n                                            </mat-form-field> -->\r\n                                        </mat-card-content>\r\n                                    </mat-card>\r\n                                </mat-grid-tile>\r\n                            </mat-grid-list>\r\n\r\n                            <mat-grid-list cols=\"2\" rowHeight=\"525px\">\r\n                                <mat-grid-tile [colspan]=\"1\" class=\"grid-list-kendaraan\">\r\n                                    <table *ngIf=\"selectedRuteMudik[0]\" class=\"table-kendaraan\">\r\n                                        <tr>\r\n                                            <th>No Kendaraan</th>\r\n                                            <th>Kapasitas Sisa</th>\r\n                                            <th></th>\r\n                                        </tr>\r\n                                        <tr *ngFor=\"let kendaraan of selectedRuteMudik[0].kendaraans\">\r\n                                            <td> {{ kendaraan.nomor }} </td>\r\n                                            <td> {{ kendaraan.kapasitas }} </td>\r\n                                            <td> <button mat-raised-button (click)=\"kendaraanSwal(kendaraan.id)\"> Detail </button> </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                </mat-grid-tile>\r\n                                <mat-grid-tile [colspan]=\"1\" class=\"grid-list-kendaraan\">\r\n                                    <table *ngIf=\"selectedRuteBalik[0]\" class=\"table-kendaraan\">\r\n                                        <tr>\r\n                                            <th>No Kendaraan</th>\r\n                                            <th>Kapasitas Sisa</th>\r\n                                            <th></th>\r\n                                        </tr>\r\n                                        <tr *ngFor=\"let kendaraan of selectedRuteBalik[0].kendaraans\">\r\n                                            <td> {{ kendaraan.nomor }} </td>\r\n                                            <td> {{ kendaraan.kapasitas }} </td>\r\n                                            <td> <button mat-raised-button (click)=\"kendaraanSwal(kendaraan.id)\"> Detail </button> </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                </mat-grid-tile>\r\n                            </mat-grid-list>\r\n                        </form>\r\n                        <div class=\"stepper-but\">\r\n                            <button mat-raised-button \r\n                                    matStepperNext \r\n                                    class=\"add-product-button\" \r\n                                    [disabled]=\"!isMudik && !isBalik\">\r\n                                    Lanjutkan\r\n                            </button>\r\n                        </div>\r\n                    </mat-step>\r\n\r\n                    <mat-step [stepControl]=\"informasiMudikForm2\">\r\n                        <form [formGroup]=\"informasiMudikForm2\">\r\n                            <ng-template matStepLabel>Informasi Mudik</ng-template>\r\n                            <mat-form-field class=\"w-100-p\">\r\n                                <input matInput\r\n                                       name=\"alamat\"\r\n                                       formControlName=\"alamat\"\r\n                                       placeholder=\"Alamat\"\r\n                                       required>\r\n                                <mat-error *ngIf=\"informasiMudikForm2.controls.alamat.invalid\">\r\n                                     {{informasiMudikForm2.controls.alamat.errors['required'] ? 'Tidak Boleh Kosong': informasiMudikForm2.controls.alamat.errors['minlength'] ? 'Panjang Minimal 8' : ''}}\r\n                                </mat-error>\r\n                                <mat-icon matPrefix> place </mat-icon>\r\n                            </mat-form-field>\r\n                            <mat-form-field class=\"w-100-p\">\r\n                                <input matInput\r\n                                       name=\"telepon\"\r\n                                       formControlName=\"telepon\"\r\n                                       placeholder=\"Telepon\">\r\n                                <mat-error *ngIf=\"informasiMudikForm2.controls.telepon.invalid\">\r\n                                 {{informasiMudikForm2.controls.telepon.errors['required'] ? 'Tidak Boleh Kosong': informasiMudikForm2.controls.telepon.errors['pattern'] ? 'Format Nomor Telepon Tidak Valid' : ''}}\r\n                            </mat-error>\r\n                                <mat-icon matPrefix> phone </mat-icon>\r\n                            </mat-form-field>\r\n                            <mat-form-field class=\"w-100-p\" style=\"margin-top: 20px;\">\r\n                                <mat-select matInput\r\n                                           formControlName=\"pemudik_dewasa\"\r\n                                           placeholder=\"Pemudik Dewasa\"\r\n                                           (change)=\"validateAnakdanMotor()\" required>\r\n                                    <mat-option [value]=\"1\"> 1 </mat-option>\r\n                                    <mat-option [value]=\"2\"> 2 </mat-option>\r\n                                    <mat-option [value]=\"3\"> 3 </mat-option>\r\n                                    <mat-option [value]=\"4\"> 4 </mat-option>\r\n                                    <mat-option [value]=\"5\"> 5 </mat-option>\r\n                                    <mat-option [value]=\"6\"> 6 </mat-option>\r\n                                    <mat-option [value]=\"7\"> 7 </mat-option>\r\n                                    <mat-option [value]=\"8\"> 8 </mat-option>\r\n                                    <mat-option [value]=\"9\"> 9 </mat-option>\r\n                                    <mat-option [value]=\"10\"> 10 </mat-option>\r\n                                </mat-select>\r\n                                <mat-error *ngIf=\"informasiMudikForm2.controls.pemudik_dewasa.invalid\">\r\n                                     {{informasiMudikForm2.controls.pemudik_dewasa.errors['required'] ? 'Tidak Boleh Kosong': 'Kuota Kendaraan Tidak Cukup'}}\r\n                                </mat-error>\r\n                                <mat-icon matPrefix> person </mat-icon>\r\n                            </mat-form-field>\r\n                            <mat-form-field class=\"w-100-p\">\r\n                                <mat-select matInput\r\n                                           formControlName=\"pemudik_anak\"\r\n                                           placeholder=\"Pemudik Anak (Di bawah 4 Tahun)\"\r\n                                           required>\r\n                                    <mat-option [value]=\"0\"> 0 </mat-option>\r\n                                    <mat-option [value]=\"1\"> 1 </mat-option>\r\n                                    <mat-option *ngFor=\"let i of maxAnak\" [value]=\"i\"> {{ i }} </mat-option>\r\n                                </mat-select>\r\n                                <mat-error *ngIf=\"informasiMudikForm2.controls.pemudik_anak.invalid\">\r\n                                     {{informasiMudikForm2.controls.pemudik_anak.errors['required'] ? 'Tidak Boleh Kosong': 'Jumlah Pemudik Anak Tidak Boleh Melebihi Pemudik Dewasa'}}\r\n                                </mat-error>\r\n                                <mat-icon matPrefix> child_care </mat-icon>\r\n                            </mat-form-field>\r\n                            <!-- <mat-form-field class=\"w-100-p\">\r\n                                <mat-select matInput\r\n                                           formControlName=\"banyak_motor\"\r\n                                           placeholder=\"Jumlah Motor\"\r\n                                           required>\r\n                                    <mat-option [value]=\"0\"> 0 </mat-option>\r\n                                    <mat-option [value]=\"1\"> 1 </mat-option>\r\n                                    <mat-option *ngFor=\"let i of maxMotor\" [value]=\"i\"> {{ i }} </mat-option>\r\n                                </mat-select>\r\n                                <mat-error *ngIf=\"informasiMudikForm2.controls.banyak_motor.invalid\">\r\n                                     {{informasiMudikForm2.controls.banyak_motor.errors['required'] ? 'Tidak Boleh Kosong': 'Jumlah Pemudik Motor Tidak Boleh Melebihi Pemudik Dewasa atau Kuota Motor'}}\r\n                                </mat-error>\r\n                                <mat-icon matPrefix> motorcycle </mat-icon>\r\n                            </mat-form-field> -->\r\n                        </form>\r\n                        <div class=\"stepper-but\">\r\n                            <button mat-raised-button matStepperPrevious class=\"add-product-button\">Kembali</button>\r\n                            <button mat-raised-button class=\"add-product-button\" (click)=\"booking()\">Lanjutkan</button>\r\n                        </div>\r\n                    </mat-step>\r\n\r\n                    <mat-step [stepControl]=\"tiketForm\">\r\n                        <form [formGroup]=\"tiketForm\">\r\n                            <ng-template matStepLabel>Informasi Pemudik</ng-template>\r\n                            <div formArrayName=\"pemudik\">\r\n                                <div *ngFor=\"let pemudik of tiketForm.controls.pemudik.controls; let i=index\"  [formGroupName]=\"i\">\r\n                                    <h4 *ngIf=\"i < informasiMudikForm2.controls.pemudik_dewasa.value\">\r\n                                        <span>Dewasa {{i+1}}</span>\r\n                                        <span *ngIf=\"i < informasiMudikForm2.controls.banyak_motor.value\"> + Motor</span>\r\n                                    </h4>\r\n                                    <h4 *ngIf=\"i >= informasiMudikForm2.controls.pemudik_dewasa.value\">Anak-Anak {{i-informasiMudikForm2.controls.pemudik_dewasa.value+1}} (Pemudik di bawah umur 4 th)</h4>\r\n                                    <!-- <h4 *ngIf=\"i >= informasiMudikForm2.controls.pemudik_dewasa.value + informasiMudikForm2.controls.pemudik_dewasa.value\">Pemudik Motor</h4> -->\r\n                                    <mat-grid-list cols=\"8\" rowHeight=\"80px\">\r\n                                        <mat-grid-tile [colspan]=\"4\">\r\n                                            <mat-form-field class=\"w-90-p\">\r\n                                              <input matInput\r\n                                                     name=\"nama\"\r\n                                                     formControlName=\"nama\"\r\n                                                     placeholder=\"Nama\"\r\n                                                     required>\r\n                                              <mat-error *ngIf=\"tiketForm.controls.pemudik.controls[i].controls.nama.invalid\">\r\n                                                   {{tiketForm.controls.pemudik.controls[i].controls.nama.errors['required'] ? 'Tidak Boleh Kosong': tiketForm.controls.pemudik.controls[i].controls.nama.errors['minlength'] ? 'Panjang minimal 2 karakter': ''}}\r\n                                              </mat-error>\r\n                                              <mat-icon matPrefix> person </mat-icon>\r\n                                            </mat-form-field>\r\n                                        </mat-grid-tile>\r\n                                        <mat-grid-tile [colspan]=\"4\">\r\n                                            <mat-form-field class=\"w-90-p\">\r\n                                                <input matInput\r\n                                                       name=\"nik\"\r\n                                                       formControlName=\"nik\"\r\n                                                       placeholder=\"NIK\"\r\n                                                       type=\"number\"\r\n                                                       (change)=\"generateNIK(i)\">\r\n                                                <mat-error *ngIf=\"tiketForm.controls.pemudik.controls[i].controls.nik.invalid\">\r\n                                                     {{tiketForm.controls.pemudik.controls[i].controls.nik.errors['required'] ? 'Tidak Boleh Kosong': tiketForm.controls.pemudik.controls[i].controls.nik.errors['minlength'] ? 'Panjang minimal 16 karakter' : tiketForm.controls.pemudik.controls[i].controls.nik.errors['maxlength'] ? 'Panjang maksimal 16 karakter' : ''}}\r\n                                                </mat-error>\r\n                                                <mat-error *ngIf=\"tiketForm.controls.pemudik.controls[i].controls.nik.invalid\">\r\n                                                    {{ tiketForm.controls.pemudik.controls[i].controls.nik.errors['nikTaken'] ? 'Nik Telah Digunakan': '' }}\r\n                                               </mat-error>\r\n                                                <mat-icon matPrefix> chrome_reader_mode </mat-icon>\r\n                                            </mat-form-field>\r\n                                        </mat-grid-tile>\r\n                                        <mat-grid-tile [colspan]=\"4\">\r\n                                            <mat-form-field class=\"w-90-p\">\r\n                                                <mat-select matInput\r\n                                                       formControlName=\"gender\"\r\n                                                       placeholder=\"Jenis Kelamin\"\r\n                                                       required>\r\n                                                    <mat-option [value]=\"1\"> Laki-laki </mat-option>\r\n                                                    <mat-option [value]=\"2\"> Perempuan </mat-option>\r\n                                                </mat-select>\r\n                                                <mat-error *ngIf=\"tiketForm.controls.pemudik.controls[i].controls.gender.invalid\">\r\n                                                     Tidak boleh kosong\r\n                                                </mat-error>\r\n                                                <mat-icon matPrefix> wc </mat-icon>\r\n                                            </mat-form-field>\r\n                                        </mat-grid-tile>\r\n                                        <mat-grid-tile [colspan]=\"4\" *ngIf=\"i < informasiMudikForm2.controls.pemudik_dewasa.value\">\r\n                                            <mat-form-field class=\"w-90-p\">\r\n                                                <mat-select matInput\r\n                                                       formControlName=\"pekerjaan\"\r\n                                                       placeholder=\"Pekerjaan\"\r\n                                                       required>\r\n                                                    <mat-option value=\"1\"> PNS / TNI / Polri / Pensiunan </mat-option>\r\n                                                    <mat-option value=\"2\"> Wiraswasta </mat-option>\r\n                                                    <mat-option value=\"3\"> Karyawan Swasta / Buruh</mat-option>\r\n                                                    <mat-option value=\"4\"> Pelajar / Mahasiswa </mat-option>\r\n                                                    <mat-option value=\"5\"> Pembantu Rumah Tangga </mat-option>\r\n                                                    <mat-option value=\"6\"> Dokter / Pengacara / Profesional </mat-option>\r\n                                                    <mat-option value=\"7\"> Tidak Bekerja / Ibu Rumah Tangga / Lainnya </mat-option>\r\n                                                </mat-select>\r\n                                                <mat-error *ngIf=\"tiketForm.controls.pemudik.controls[i].controls.pekerjaan.invalid\">\r\n                                                     Tidak boleh kosong\r\n                                                </mat-error>\r\n                                                <mat-icon matPrefix> work </mat-icon>\r\n                                            </mat-form-field>\r\n                                        </mat-grid-tile>\r\n                                        <mat-grid-tile [colspan]=\"1\">\r\n                                            <mat-form-field class=\"w-90-p\">\r\n                                                <mat-select matInput\r\n                                                       formControlName=\"tanggal\"\r\n                                                       placeholder=\"Tanggal Lahir\"\r\n                                                       required>\r\n                                                    <mat-option *ngFor=\"let i of list_tanggal\" [value]=\"i.id\"> {{ i.id }} </mat-option>\r\n                                                </mat-select>\r\n                                            </mat-form-field>\r\n                                        </mat-grid-tile>\r\n                                        <mat-grid-tile [colspan]=\"2\">\r\n                                            <mat-form-field class=\"w-90-p\">\r\n                                                <mat-select matInput\r\n                                                       formControlName=\"bulan\"\r\n                                                       placeholder=\"Bulan\"\r\n                                                       required>\r\n                                                    <mat-option *ngFor=\"let bulan of list_bulan\" [value]=\"bulan.id\"> {{bulan.nama}} </mat-option>\r\n                                                </mat-select>\r\n                                            </mat-form-field>\r\n                                        </mat-grid-tile>\r\n                                        <mat-grid-tile [colspan]=\"1\">\r\n                                            <mat-form-field class=\"w-90-p\">\r\n                                                <mat-select matInput\r\n                                                       formControlName=\"tahun\"\r\n                                                       placeholder=\"Tahun\"\r\n                                                       required>\r\n                                                    <mat-option *ngFor=\"let i of list_tahun\" [value]=\"i\"> {{ i }} </mat-option>\r\n                                                </mat-select>\r\n                                            </mat-form-field>\r\n                                        </mat-grid-tile>\r\n                                        <mat-grid-tile [colspan]=\"4\">\r\n                                            <mat-form-field class=\"w-90-p\">\r\n                                                <input matInput\r\n                                                       name=\"tempat_lahir\"\r\n                                                       formControlName=\"tempat_lahir\"\r\n                                                       placeholder=\"Tempat Lahir\"\r\n                                                       required>\r\n                                                <mat-error *ngIf=\"tiketForm.controls.pemudik.controls[i].controls.tempat_lahir.invalid\">\r\n                                                     Tidak boleh kosong\r\n                                                </mat-error>\r\n                                                <mat-icon matPrefix> place </mat-icon>\r\n                                            </mat-form-field>\r\n                                        </mat-grid-tile>\r\n                                        <mat-grid-tile [colspan]=\"4\" *ngIf=\"i < informasiMudikForm2.controls.banyak_motor.value && (nomor_tiket_mudik_motor || nomor_tiket_balik_motor)\">\r\n                                            <mat-form-field class=\"w-90-p\">\r\n                                                <input matInput\r\n                                                       name=\"sim\"\r\n                                                       formControlName=\"sim\"\r\n                                                       placeholder=\"SIM\"\r\n                                                       type=\"number\">\r\n                                                <mat-error *ngIf=\"tiketForm.controls.pemudik.controls[i].controls.sim.invalid\">\r\n                                                     Tidak boleh kosong\r\n                                                </mat-error>\r\n                                                <mat-icon matPrefix> place </mat-icon>\r\n                                            </mat-form-field>\r\n                                        </mat-grid-tile>\r\n\r\n                                        <mat-grid-tile [colspan]=\"4\" *ngIf=\"i < informasiMudikForm2.controls.banyak_motor.value && (nomor_tiket_mudik_motor || nomor_tiket_balik_motor)\">\r\n                                            <mat-form-field class=\"w-90-p\">\r\n                                                <input matInput\r\n                                                       name=\"stnk\"\r\n                                                       formControlName=\"stnk\"\r\n                                                       placeholder=\"STNK\"\r\n                                                       type=\"number\">\r\n                                                <mat-error *ngIf=\"tiketForm.controls.pemudik.controls[i].controls.stnk.invalid\">\r\n                                                     Tidak boleh kosong\r\n                                                </mat-error>\r\n                                                <mat-icon matPrefix> place </mat-icon>\r\n                                            </mat-form-field>\r\n                                        </mat-grid-tile>\r\n                                    </mat-grid-list>\r\n                                    <hr>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"stepper-but\">\r\n                                <button type=\"button\" matButton mat-raised-button class=\"save-product-button mat-red-bg mt-16 mt-sm-0\" (click)=\"batalSwal.show()\">Batal</button>\r\n                                <button type=\"submit\" matButton mat-raised-button class=\"save-product-button mat-blue-bg mt-16 mt-sm-0\" (click)=\"summary()\">Lanjutkan</button>\r\n                            </div>\r\n                            <swal\r\n                                #batalSwal\r\n                                title=\"Apakah anda yakin akan membatalkan tiket ini ?\"\r\n                                text=\"Kursi yang telah dibooking akan hilang\"\r\n                                type=\"question\"\r\n                                [showCancelButton]=\"true\"\r\n                                [focusCancel]=\"true\"\r\n                                (confirm)=\"tiketBatal()\">\r\n                            </swal>\r\n                        </form>\r\n                    </mat-step>\r\n\r\n                    <mat-step>\r\n                        <ng-template matStepLabel>Summary</ng-template>\r\n                        <div *ngIf=\"tiketForm.valid\">\r\n                            <mat-card class=\"example-card\" *ngIf=\"nomor_tiket_mudik\">\r\n                                <mat-card-header>\r\n                                    <mat-card-title>INFORMASI BUKTI PENDAFTARAN MUDIK</mat-card-title>\r\n                                </mat-card-header>\r\n                                <mat-card-content>\r\n                                    <table class=\"table-css\">\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Kode Pendaftaran</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{nomor_tiket_mudik}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tanggal Pendaftaran</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{date}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Rute</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRute.asal_mudik}} - {{summaryRute.tujuan_mudik}} ({{summaryRuteMudik.keterangan}})</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tanggal Berangkat</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRuteMudik.tanggal}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tempat Berangkat</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRuteMudik.tempat_berangkat}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tempat Ambil Tiket</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRuteMudik.tempat_ambil_tiket}}</td>\r\n                                        </tr>\r\n                                    </table>\r\n                                </mat-card-content>\r\n                            </mat-card>\r\n\r\n                            <mat-card class=\"example-card\" *ngIf=\"nomor_tiket_balik\">\r\n                                <mat-card-header>\r\n                                    <mat-card-title>INFORMASI BUKTI PENDAFTARAN BALIK</mat-card-title>\r\n                                </mat-card-header>\r\n                                <mat-card-content>\r\n                                    <table class=\"table-css\">\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Kode Pendaftaran</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{nomor_tiket_balik}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tanggal Pendaftaran</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{date}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Rute</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRute.asal_balik}} - {{summaryRute.tujuan_balik}} ({{summaryRuteBalik.keterangan}})</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tanggal Berangkat</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRuteBalik.tanggal}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tempat Berangkat</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRuteBalik.tempat_berangkat}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tempat Ambil Tiket</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRuteBalik.tempat_ambil_tiket}}</td>\r\n                                        </tr>\r\n                                    </table>\r\n                                </mat-card-content>\r\n                            </mat-card>\r\n\r\n                            <mat-card class=\"example-card\" *ngIf=\"nomor_tiket_mudik_motor\">\r\n                                <mat-card-header>\r\n                                    <mat-card-title>INFORMASI BUKTI PENDAFTARAN MUDIK MOTOR</mat-card-title>\r\n                                </mat-card-header>\r\n                                <mat-card-content>\r\n                                    <table class=\"table-css\">\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Kode Pendaftaran</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{nomor_tiket_mudik_motor}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tanggal Pendaftaran</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{date}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Rute</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRute.asal_mudik}} - {{summaryRute.tujuan_mudik}} ({{summaryRuteMudik.keterangan}})</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tanggal Berangkat</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRuteMudik.tanggal}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tempat Berangkat</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRuteMudik.tempat_berangkat}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tempat Ambil Tiket</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRuteMudik.tempat_ambil_tiket}}</td>\r\n                                        </tr>\r\n                                    </table>\r\n                                </mat-card-content>\r\n                            </mat-card>\r\n\r\n                            <mat-card class=\"example-card\" *ngIf=\"nomor_tiket_balik_motor\">\r\n                                <mat-card-header>\r\n                                    <mat-card-title>INFORMASI BUKTI PENDAFTARAN BALIK MOTOR</mat-card-title>\r\n                                </mat-card-header>\r\n                                <mat-card-content>\r\n                                    <table class=\"table-css\">\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Kode Pendaftaran</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{nomor_tiket_balik_motor}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tanggal Pendaftaran</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{date}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Rute</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRute.asal_balik}} - {{summaryRute.tujuan_balik}} ({{summaryRuteBalik.keterangan}})</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tanggal Berangkat</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRuteBalik.tanggal}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tempat Berangkat</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRuteBalik.tempat_berangkat}}</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"td-css\">Tempat Ambil Tiket</td>\r\n                                            <td class=\"td-css\">:</td>\r\n                                            <td class=\"td-css\">{{summaryRuteBalik.tempat_ambil_tiket}}</td>\r\n                                        </tr>\r\n                                    </table>\r\n                                </mat-card-content>\r\n                            </mat-card>\r\n\r\n                            <mat-card class=\"example-card\" *ngIf=\"nomor_tiket_mudik || nomor_tiket_balik\">\r\n                                <mat-card-header>\r\n                                    <mat-card-title>DATA PEMUDIK</mat-card-title>\r\n                                </mat-card-header>\r\n                                <mat-card-content>\r\n                                    <table class=\"table-css\">\r\n                                        <tr>\r\n                                            <th class=\"th-css\">Nama</th>\r\n                                            <th class=\"th-css\">NIK</th>\r\n                                            <th class=\"th-css\">Tanggal Lahir</th>\r\n                                            <th class=\"th-css\">Pekerjaan</th>\r\n                                        </tr>\r\n                                        <tr *ngFor=\"let pemudik of summaryMudik.pemudik\">\r\n                                            <td class=\"td-css\">{{pemudik.nama}}</td>\r\n                                            <td class=\"td-css\">{{pemudik.nik}}</td>\r\n                                            <td class=\"td-css\">{{pemudik.tanggal_lahir}}</td>\r\n                                            <td class=\"td-css\" *ngIf=\"pemudik.pekerjaan == 1\"> PNS / TNI / Polri / Pensiunan </td>\r\n                                            <td class=\"td-css\" *ngIf=\"pemudik.pekerjaan == 2\"> Wiraswasta </td>\r\n                                            <td class=\"td-css\" *ngIf=\"pemudik.pekerjaan == 3\"> Karyawan Swasta / Buruh</td>\r\n                                            <td class=\"td-css\" *ngIf=\"pemudik.pekerjaan == 4\"> Pelajar / Mahasiswa </td>\r\n                                            <td class=\"td-css\" *ngIf=\"pemudik.pekerjaan == 5\"> Pembantu Rumah Tangga </td>\r\n                                            <td class=\"td-css\" *ngIf=\"pemudik.pekerjaan == 6\"> Dokter / Pengacara / Profesional </td>\r\n                                            <td class=\"td-css\" *ngIf=\"pemudik.pekerjaan == 7\"> Tidak Bekerja / Ibu Rumah Tangga / Lainnya </td>\r\n                                        </tr>\r\n                                    </table>\r\n                                </mat-card-content>\r\n                            </mat-card>\r\n\r\n                        </div>\r\n                        <div  class=\"stepper-but\">\r\n                            <button type=\"button\" matButton mat-raised-button class=\"save-product-button mat-red-bg mt-16 mt-sm-0\" (click)=\"batalSwal.show()\">Batal</button>\r\n                            <button type=\"button\" matButton matStepperPrevious mat-raised-button class=\"save-product-button mat-blue-bg mt-16 mt-sm-0\">Kembali</button>\r\n                            <button type=\"submit\" matButton matStepperNext mat-raised-button class=\"save-product-button mat-blue-bg mt-16 mt-sm-0\" (click)=\"daftarMudik()\">Lanjutkan</button>\r\n                            <swal\r\n                                #approveSwal\r\n                                title=\"Apakah anda yakin akan membatalkan tiket ini ?\"\r\n                                text=\"Kursi yang telah dibooking akan hilang\"\r\n                                type=\"question\"\r\n                                [showCancelButton]=\"true\"\r\n                                [focusCancel]=\"true\"\r\n                                (confirm)=\"tiketBatal()\">\r\n                            </swal>\r\n                        </div>\r\n                    </mat-step>\r\n\r\n                </mat-horizontal-stepper>\r\n\r\n            </div>\r\n            <!-- / CONTENT -->\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/tiket/detailTiket/detailTiket.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#product .header .product-image {\n  overflow: hidden;\n  width: 56px;\n  height: 56px;\n  border: 3px solid rgba(0, 0, 0, 0.12); }\n  #product .header .product-image img {\n    height: 100%;\n    width: auto;\n    max-width: none; }\n  #product .header .save-product-button {\n  margin: 0px 20px;\n  background-color: #F96D01; }\n  #product .header .accept-product-button {\n  margin: 0px 20px;\n  background-color: limegreen; }\n  #product .header .decline-product-button {\n  margin: 0px 20px;\n  background-color: red; }\n  #product .header .print-product-button {\n  margin: 0px 20px;\n  background-color: dodgerblue; }\n  #product .header .subtitle {\n  margin: 6px 0 0 0; }\n  #product .content .mat-expansion-panel {\n  background: #2196f3; }\n  #product .content .mat-horizontal-stepper-header {\n  pointer-events: none !important; }\n  #product .content .mat-stepper-horizontal {\n  width: 100%; }\n  #product .content .mat-checkbox-inner-container {\n  height: 30px;\n  width: 30px; }\n  #product .content span.mat-checkbox-label {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n  #product .content .title {\n  font-size: 20px;\n  margin-top: 7px;\n  margin-left: 5px; }\n  #product .content .mat-tab-group,\n#product .content .mat-tab-body-wrapper,\n#product .content .tab-content {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  max-width: 100%; }\n  #product .content .mat-tab-header {\n  height: 40px;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n  #product .content .mat-horizontal-stepper-header {\n  height: 50px !important; }\n  #product .content .foto_kk {\n  max-height: 300px;\n  max-width: 400px; }\n  #product .content .foto_pemudik {\n  max-height: 300px;\n  max-width: 400px; }\n  #product .content .mat-tab-body-content {\n  display: block !important; }\n  #product .content .mat-tab-label {\n  height: 64px; }\n  #product .content .w-100-p {\n  margin-bottom: 2px; }\n  #product .content .w-90-p {\n  margin-bottom: 2px; }\n  #product .content .mat-form-field-prefix .mat-icon, #product .content .mat-form-field-suffix .mat-icon {\n  margin-right: 10px; }\n  #product .content .mat-form-field {\n  width: 90%; }\n  #product .content .product-image {\n  overflow: hidden;\n  width: 128px;\n  height: 128px;\n  margin-right: 16px;\n  margin-bottom: 16px;\n  border: 3px solid rgba(0, 0, 0, 0.12); }\n  #product .content .product-image img {\n    height: 100%;\n    width: auto;\n    max-width: none; }\n  #product .content .table-css {\n  border-collapse: collapse !important;\n  width: 100% !important; }\n  #product .content .th-css, #product .content .td-css {\n  padding: 8px !important;\n  text-align: left !important;\n  border-bottom: 1px solid #ddd !important; }\n  #product .content .table-info {\n  width: 100%; }\n  #product .content .table-kendaraan {\n  position: absolute;\n  left: 5px;\n  width: 100%;\n  top: 0px; }\n  #product .content .table-kendaraan {\n  font-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;\n  border-collapse: collapse; }\n  #product .content .table-kendaraan td, #product .content .table-kendaraan th {\n  border: 1px solid #ddd;\n  padding: 5px 8px; }\n  #product .content .grid-list-kendaraan {\n  overflow-x: hidden !important;\n  overflow-y: scroll !important; }\n  #product .content .table-kendaraan tr:nth-child(even) {\n  background-color: #f2f2f2; }\n  #product .content .table-kendaraan th {\n  padding-top: 12px;\n  padding-bottom: 12px;\n  text-align: left;\n  background-color: #4066b5;\n  color: white; }\n  #product .content .td, #product .content .th {\n  padding: 8px;\n  text-align: left;\n  border-bottom: 1px solid #ddd !important; }\n  #product .content .example-card {\n  width: 100%; }\n  #product .content .example-card-pemudik {\n  max-width: 400px;\n  margin-bottom: 2px; }\n  #product .content .table-form-field {\n  width: 100%;\n  margin-bottom: -25px; }\n  #product .content .icon-pemudik {\n  margin-right: 5px; }\n  #product .content .divider {\n  width: 95% !important;\n  left: 2.5% !important; }\n  #product .content .add-product-button {\n  background-color: #039be5; }\n  #product .content .radio-button {\n  margin: 8px; }\n  #product .content .stepper-but {\n  position: absolute;\n  left: 50px !important;\n  bottom: 20px !important; }\n  /deep/ dialog-overview-example-dialog {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n  .swal2-popup {\n  width: unset;\n  min-width: 300px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/tiket/detailTiket/detailTiket.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailTiketComponent; });
/* unused harmony export ValidateEmailNotTaken */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__detailTiket_service__ = __webpack_require__("../../../../../src/app/main/content/apps/tiket/detailTiket/detailTiket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_fromEvent__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/fromEvent.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__detailTiket_model__ = __webpack_require__("../../../../../src/app/main/content/apps/tiket/detailTiket/detailTiket.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__toverux_ngx_sweetalert2__ = __webpack_require__("../../../../@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_18_sweetalert2__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



















var DetailTiketComponent = /** @class */ (function () {
    function DetailTiketComponent(productService, formBuilder, snackBar, location, http, router, route, _location, dialog, _sessionService) {
        this.productService = productService;
        this.formBuilder = formBuilder;
        this.snackBar = snackBar;
        this.location = location;
        this.http = http;
        this.router = router;
        this.route = route;
        this._location = _location;
        this.dialog = dialog;
        this._sessionService = _sessionService;
        this.product = new __WEBPACK_IMPORTED_MODULE_10__detailTiket_model__["a" /* Product */];
        this.pageType2 = 'view';
        this.ruteMudik = [];
        this.ruteBalik = [];
        this.ruteMudikMotor = [];
        this.ruteBalikMotor = [];
        this.maxAnak = [];
        this.maxMotor = [];
        this.isLinear = true;
        this.isMudik = false;
        this.isBalik = false;
        this.selectedRuteMudik = [];
        this.selectedRuteBalik = [];
        this.statusDaftarMudik = false;
        this.statusDaftarBalik = false;
        this.statusDaftarMudikMotor = false;
        this.statusDaftarBalikMotor = false;
        this.url_image = __WEBPACK_IMPORTED_MODULE_15__environments_environment__["a" /* environment */].setting.base_url + '/';
        this.kursi_available = [];
        this.list_tanggal = [{ id: '01' }, { id: '02' }, { id: '03' }, { id: '04' }, { id: '05' },
            { id: '06' }, { id: '07' }, { id: '08' }, { id: '09' }, { id: '10' },
            { id: '11' }, { id: '12' }, { id: '13' }, { id: '14' }, { id: '15' },
            { id: '16' }, { id: '17' }, { id: '18' }, { id: '19' }, { id: '20' },
            { id: '21' }, { id: '22' }, { id: '23' }, { id: '24' }, { id: '25' },
            { id: '26' }, { id: '27' }, { id: '28' }, { id: '29' }, { id: '30' }, { id: '31' }];
        this.list_bulan = [{ id: '01', nama: 'Januari' }, { id: '02', nama: 'Februari' },
            { id: '03', nama: 'Maret' }, { id: '04', nama: 'April' },
            { id: '05', nama: 'Mei' }, { id: '06', nama: 'Juni' },
            { id: '07', nama: 'Juli' }, { id: '08', nama: 'Agustus' },
            { id: '09', nama: 'September' }, { id: '10', nama: 'Oktober' },
            { id: '11', nama: 'November' }, { id: '12', nama: 'Desember' }];
        this.list_tahun = [];
    }
    DetailTiketComponent.prototype.ngOnInit = function () {
        var _this = this;
        for (var i = 2018; i > 1930; --i) {
            this.list_tahun.push(i);
        }
        var timeStamp = new Date();
        this.date = timeStamp.getDate() + ' - ' + (timeStamp.getMonth() + 1) + ' - ' + timeStamp.getFullYear();
        this.kursi_available = [];
        // Subscribe to update product on changes
        this.onProductChanged = this.productService.onProductChanged
            .subscribe(function (product) {
            if (product) {
                _this.product = new __WEBPACK_IMPORTED_MODULE_10__detailTiket_model__["a" /* Product */](product);
                _this.status = _this.product.status;
                _this.pageType = 'edit';
                _this.tiketForm = _this.createTiketFormEdit();
                for (var i = 1; i < _this.product.pemudik_anak + _this.product.pemudik_dewasa; i++) {
                    _this.addPemudikEdit(i);
                }
                _this.kursi_available = _this.productService.list_kursi;
            }
            else {
                _this.pageType = 'new';
                _this.product = new __WEBPACK_IMPORTED_MODULE_10__detailTiket_model__["a" /* Product */]();
                _this.tiketForm = _this.createTiketFormNew();
                _this.informasiMudikForm1 = _this.createInformasiMudikForm1();
                _this.informasiMudikForm2 = _this.createInformasiMudikForm2();
            }
        });
        this.asalBalik = this.productService.asalBalik;
        this.asalMudik = this.productService.asalMudik;
        this.listKotaBalik = this.productService.listKotaBalik;
        this.listKotaMudik = this.productService.listKotaMudik;
    };
    DetailTiketComponent.prototype.ngOnDestroy = function () {
        this.onProductChanged.unsubscribe();
    };
    DetailTiketComponent.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_14__angular_http__["b" /* Headers */]();
        headers.append('Authorization', 'Bearer ' + this._sessionService.get().token);
        headers.append('Content-Type', 'application/json');
        options.headers = headers;
    };
    DetailTiketComponent.prototype.createTiketFormEdit = function () {
        return this.formBuilder.group({
            alamat: [this.product.alamat],
            telepon: [this.product.telepon],
            kota_tujuan: [this.product.kota_tujuan],
            pemudiks: this.formBuilder.array([
                this.formBuilder.group({
                    'nama': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[0].nama, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].minLength(2)]),
                    'nik': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[0].nik, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].minLength(16), __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].maxLength(16), __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
                    'sim': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[0].sim),
                    'usia': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[0].usia, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
                    'jenis': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[0].jenis, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
                    'stnk': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[0].stnk),
                    'gender': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[0].gender, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
                    'tanggal_lahir': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[0].tanggal_lahir),
                    'tempat_lahir': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[0].tempat_lahir, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
                    'pekerjaan': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[0].pekerjaan, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
                    'pindah_kursi': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[0].kursi_id)
                })
            ])
        });
    };
    DetailTiketComponent.prototype.createTiketFormNew = function () {
        return this.formBuilder.group({
            nomor_tiket: new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
            pemudik_dewasa: new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
            pemudik_anak: new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
            banyak_motor: new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](0, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
            pemudik: this.formBuilder.array([
                this.formBuilder.group({
                    'nama': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].minLength(2)]),
                    'nik': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].minLength(16), __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].maxLength(16), __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
                    'sim': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](''),
                    'usia': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](''),
                    'jenis': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](1, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
                    'stnk': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](''),
                    'gender': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
                    'tanggal_lahir': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](''),
                    'tempat_lahir': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
                    'pekerjaan': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
                    'tanggal': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](''),
                    'bulan': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](''),
                    'tahun': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('')
                })
            ])
        });
    };
    Object.defineProperty(DetailTiketComponent.prototype, "pemudikForm", {
        get: function () {
            return this.tiketForm.get('pemudik');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(DetailTiketComponent.prototype, "pemudikFormEdit", {
        get: function () {
            return this.tiketForm.get('pemudiks');
        },
        enumerable: true,
        configurable: true
    });
    DetailTiketComponent.prototype.addPemudikEdit = function (id) {
        this.pemudikFormEdit.push(this.formBuilder.group({
            'nama': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[id].nama, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].minLength(2)]),
            'nik': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[id].nik, this.product.pemudik[id].jenis === 1 ? [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].minLength(16), __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].maxLength(16), __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required] : []),
            'sim': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[id].sim),
            'usia': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[id].usia, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
            'jenis': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[id].jenis, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
            'stnk': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[id].stnk),
            'gender': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[id].gender, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
            'tanggal_lahir': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[id].tanggal_lahir),
            'tempat_lahir': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[id].tempat_lahir, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
            'pekerjaan': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[id].pekerjaan, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
            'pindah_kursi': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](this.product.pemudik[id].kursi_id)
        }));
    };
    DetailTiketComponent.prototype.addPemudikNew = function (jenis) {
        this.pemudikForm.push(this.formBuilder.group({
            'nama': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].minLength(2)]),
            'nik': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', jenis === 1 ? [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].minLength(16), __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].maxLength(16), __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required] : []),
            'sim': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](''),
            'usia': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](''),
            'jenis': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](jenis, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
            'stnk': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](''),
            'gender': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
            'tanggal_lahir': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](''),
            'tempat_lahir': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]),
            'pekerjaan': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', jenis === 1 ? [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required] : []),
            'tanggal': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](''),
            'bulan': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](''),
            'tahun': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('')
        }));
    };
    DetailTiketComponent.prototype.createInformasiMudikForm1 = function () {
        return this.formBuilder.group({
            'asal_mudik': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]({ value: '', disabled: true }),
            'tujuan_mudik': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]({ value: '', disabled: true }),
            'rute_mudik': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]({ value: '', disabled: true }),
            'rute_mudik_motor': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]({ value: '', disabled: true }),
            'asal_balik': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]({ value: '', disabled: true }),
            'tujuan_balik': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]({ value: '', disabled: true }),
            'rute_balik': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]({ value: '', disabled: true }),
            'rute_balik_motor': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]({ value: '', disabled: true })
        });
    };
    DetailTiketComponent.prototype.createInformasiMudikForm2 = function () {
        return this.formBuilder.group({
            'telepon': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].pattern(/^[0-9]{9,}$/)]),
            'alamat': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].minLength(8)]),
            'pemudik_dewasa': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](1, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].max(10), __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].min(1)]),
            'pemudik_anak': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](0, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].max(10), __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].min(0)]),
            'banyak_motor': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */]({ value: 0, disabled: true }, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].max(10), __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].min(0)])
        });
    };
    DetailTiketComponent.prototype.updateTujuanMudik = function () {
        var kotaAsal = this.informasiMudikForm1.controls.asal_mudik.value;
        this.tujuanMudik = [];
        for (var i = 0; i < this.listKotaMudik.length; ++i) {
            if (this.listKotaMudik[i].asal === kotaAsal) {
                for (var j = 0; j < this.listKotaMudik[i].tujuan.length; ++j) {
                    if (!this.tujuanMudik.includes(this.listKotaMudik[i].tujuan[j])) {
                        this.tujuanMudik.push(this.listKotaMudik[i].tujuan[j]);
                    }
                }
            }
        }
        this.informasiMudikForm1.controls.tujuan_mudik.enable();
    };
    DetailTiketComponent.prototype.updateTujuanBalik = function () {
        var kotaAsal = this.informasiMudikForm1.controls.asal_balik.value;
        this.tujuanBalik = [];
        for (var i = 0; i < this.listKotaBalik.length; ++i) {
            if (this.listKotaBalik[i].asal === kotaAsal) {
                for (var j = 0; j < this.listKotaBalik[i].tujuan.length; ++j) {
                    if (!this.tujuanBalik.includes(this.listKotaBalik[i].tujuan[j])) {
                        this.tujuanBalik.push(this.listKotaBalik[i].tujuan[j]);
                    }
                }
            }
        }
        this.informasiMudikForm1.controls.tujuan_balik.enable();
    };
    DetailTiketComponent.prototype.updateRuteMudik = function () {
        var _this = this;
        var asal = this.informasiMudikForm1.controls.asal_mudik.value;
        var tujuan = this.informasiMudikForm1.controls.tujuan_mudik.value;
        this.informasiMudikForm1.controls.rute_mudik.setValue('');
        this.ruteMudik = [];
        this.selectedRuteMudik = [];
        this.productService.getRuteByAsalTujuan(asal, tujuan)
            .map(function (res) { return res.json(); })
            .subscribe(function (response) {
            for (var i = 0; i < response.data.length; ++i) {
                if (response.data[i].kendaraans.length > 0) {
                    _this.ruteMudik.push(response.data[i]);
                }
                if (response.data[i].is_mudik_motor === 1) {
                    _this.ruteMudikMotor.push(response.data[i]);
                }
            }
            if (_this.ruteMudik.length > 0) {
                _this.informasiMudikForm1.controls.rute_mudik.enable();
            }
        });
    };
    DetailTiketComponent.prototype.updateRuteBalik = function () {
        var _this = this;
        var asal = this.informasiMudikForm1.controls.asal_balik.value;
        var tujuan = this.informasiMudikForm1.controls.tujuan_balik.value;
        this.informasiMudikForm1.controls.rute_balik.setValue('');
        this.ruteBalik = [];
        this.selectedRuteBalik = [];
        this.productService.getRuteByAsalTujuan(asal, tujuan)
            .map(function (res) { return res.json(); })
            .subscribe(function (response) {
            for (var i = 0; i < response.data.length; ++i) {
                if (response.data[i].kendaraans.length > 0) {
                    _this.ruteBalik.push(response.data[i]);
                }
                if (response.data[i].is_mudik_motor === 1) {
                    _this.ruteBalikMotor.push(response.data[i]);
                }
            }
            if (_this.ruteBalik.length > 0) {
                _this.informasiMudikForm1.controls.rute_balik.enable();
            }
        });
    };
    DetailTiketComponent.prototype.selectRuteMudik = function () {
        var _this = this;
        if (this.informasiMudikForm1.controls.rute_mudik.valid) {
            this.informasiMudikForm1.controls.rute_mudik_motor.enable();
            this.selectedRuteMudik = this.ruteMudik.filter(function (rute) {
                return rute.id === _this.informasiMudikForm1.controls.rute_mudik.value;
            });
        }
    };
    DetailTiketComponent.prototype.selectRuteBalik = function () {
        var _this = this;
        if (this.informasiMudikForm1.controls.rute_balik.valid) {
            this.informasiMudikForm1.controls.rute_balik_motor.enable();
            this.selectedRuteBalik = this.ruteBalik.filter(function (rute) {
                return rute.id === _this.informasiMudikForm1.controls.rute_balik.value;
            });
        }
    };
    DetailTiketComponent.prototype.validateAnakdanMotor = function () {
        if (this.informasiMudikForm2.controls.pemudik_anak.value > this.informasiMudikForm2.controls.pemudik_dewasa.value) {
            this.informasiMudikForm2.controls.pemudik_anak.setValue(0);
        }
        if (this.informasiMudikForm2.controls.banyak_motor.value > this.informasiMudikForm2.controls.pemudik_dewasa.value) {
            this.informasiMudikForm2.controls.banyak_motor.setValue(0);
        }
        this.maxAnak = [];
        this.maxMotor = [];
        for (var i = 1; i < this.informasiMudikForm2.controls.pemudik_dewasa.value; ++i) {
            this.maxAnak.push(i + 1);
        }
        if (this.informasiMudikForm1.controls.rute_mudik_motor.value) {
            if (this.ruteMudik[0].kouta_motor > this.informasiMudikForm2.controls.pemudik_dewasa.value) {
                for (var i = 1; i < this.informasiMudikForm2.controls.pemudik_dewasa.value; ++i) {
                    this.maxMotor.push(i + 1);
                }
            }
            else {
                for (var i = 1; i < this.ruteMudik[0].kouta_motor; ++i) {
                    this.maxMotor.push(i + 1);
                }
            }
            this.informasiMudikForm2.controls.banyak_motor.enable();
        }
    };
    DetailTiketComponent.prototype.booking = function () {
        var _this = this;
        var statusBookingAll = true;
        var errorMessage;
        var form1 = this.informasiMudikForm1.getRawValue();
        var form2 = this.informasiMudikForm2.getRawValue();
        var promiseList = [];
        var bookingForm = this.formBuilder.group({
            'rute_id': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](form1.rute_mudik),
            'jenis': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](1),
            'alamat': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](form2.alamat),
            'telepon': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](form2.telepon, [__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].pattern(/^[0-9]{9,}$/)]),
            'kota_tujuan': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](form1.tujuan_mudik),
            'pemudik_dewasa': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](form2.pemudik_dewasa),
            'pemudik_anak': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](form2.pemudik_anak),
            'banyak_motor': new __WEBPACK_IMPORTED_MODULE_11__angular_forms__["d" /* FormControl */](form2.banyak_motor),
        });
        if (form1.rute_mudik) {
            promiseList.push(this.productService.booking(bookingForm.getRawValue()));
        }
        else {
            promiseList.push(Promise.resolve({
                status: true
            }));
        }
        if (form1.rute_balik) {
            bookingForm.controls.rute_id.setValue(form1.rute_balik);
            bookingForm.controls.kota_tujuan.setValue(form1.tujuan_balik);
            bookingForm.controls.jenis.setValue(2);
            promiseList.push(this.productService.booking(bookingForm.getRawValue()));
        }
        else {
            promiseList.push(Promise.resolve({
                status: true
            }));
        }
        if (form2.banyak_motor > 0 && form1.rute_mudik_motor) {
            bookingForm.controls.rute_id.setValue(form1.rute_mudik);
            bookingForm.controls.banyak_motor.setValue(form2.banyak_motor);
            promiseList.push(this.productService.bookingMotor(bookingForm.getRawValue()));
        }
        else {
            promiseList.push(Promise.resolve({
                status: true
            }));
        }
        if (form2.banyak_motor > 0 && form1.rute_balik_motor) {
            bookingForm.controls.rute_id.setValue(form1.rute_balik);
            bookingForm.controls.banyak_motor.setValue(form2.banyak_motor);
            promiseList.push(this.productService.bookingMotor(bookingForm.getRawValue()));
        }
        else {
            promiseList.push(Promise.resolve({
                status: true
            }));
        }
        Promise.all(promiseList)
            .then(function (responses) {
            if (responses[0].status) {
                if (responses[0].data) {
                    _this.nomor_tiket_mudik = responses[0].data.nomor_tiket;
                }
            }
            else {
                statusBookingAll = false;
                errorMessage = responses[0].message;
            }
            if (responses[1].status) {
                if (responses[1].data) {
                    _this.nomor_tiket_balik = responses[1].data.nomor_tiket;
                }
            }
            else {
                statusBookingAll = false;
                errorMessage = responses[1].message;
            }
            if (responses[2].status) {
                if (responses[2].data) {
                    _this.nomor_tiket_mudik_motor = responses[2].data.nomor_tiket;
                }
            }
            else {
                statusBookingAll = false;
                errorMessage = responses[2].message;
            }
            if (responses[3].status) {
                if (responses[3].data) {
                    _this.nomor_tiket_balik_motor = responses[3].data.nomor_tiket;
                }
            }
            else {
                statusBookingAll = false;
                errorMessage = responses[3].message;
            }
            if (!statusBookingAll) {
                _this.tiketBatal();
                __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                    title: 'Error',
                    text: errorMessage,
                    type: 'error'
                });
            }
            else {
                for (var i = 0; i < (form2.pemudik_dewasa - 1); ++i) {
                    _this.addPemudikNew(1);
                }
                for (var i = 0; i < form2.pemudik_anak; ++i) {
                    _this.addPemudikNew(2);
                }
                _this.summaryRute = _this.informasiMudikForm1.getRawValue();
                _this.summaryInfo = _this.informasiMudikForm2.getRawValue();
                if (_this.summaryRute.rute_mudik) {
                    for (var i = 0; i < _this.ruteMudik.length; ++i) {
                        if (_this.summaryRute.rute_mudik === _this.ruteMudik[i].id) {
                            _this.summaryRuteMudik = _this.ruteMudik[i];
                        }
                    }
                }
                if (_this.summaryRute.rute_balik) {
                    for (var i = 0; i < _this.ruteBalik.length; ++i) {
                        if (_this.summaryRute.rute_balik === _this.ruteBalik[i].id) {
                            _this.summaryRuteBalik = _this.ruteBalik[i];
                        }
                    }
                }
                if (_this.informasiMudikForm2.value['banyak_motor']) {
                    _this.tiketForm.controls.banyak_motor.setValue(_this.informasiMudikForm2.value['banyak_motor']);
                }
                else {
                    _this.tiketForm.controls.banyak_motor.setValue(0);
                }
                _this.stepper.next();
            }
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DetailTiketComponent.prototype.selectRuteMudikMotor = function () {
        if (this.informasiMudikForm1.controls.rute_mudik_motor.value || this.informasiMudikForm1.controls.rute_balik_motor.value) {
            this.informasiMudikForm2.controls.banyak_motor.enable();
        }
        else {
            this.informasiMudikForm2.controls.banyak_motor.disable();
        }
    };
    DetailTiketComponent.prototype.selectRuteBalikMotor = function () {
        if (this.informasiMudikForm1.controls.rute_mudik_motor.value || this.informasiMudikForm1.controls.rute_balik_motor.value) {
            this.informasiMudikForm2.controls.banyak_motor.enable();
        }
        else {
            this.informasiMudikForm2.controls.banyak_motor.disable();
        }
    };
    DetailTiketComponent.prototype.mudik = function () {
        if (this.isMudik) {
            this.isMudik = false;
            this.informasiMudikForm1.controls.asal_mudik.disable();
            this.informasiMudikForm1.controls.tujuan_mudik.disable();
            this.informasiMudikForm1.controls.rute_mudik.disable();
            this.informasiMudikForm1.controls.asal_mudik.setValue('');
            this.informasiMudikForm1.controls.tujuan_mudik.setValue('');
            this.informasiMudikForm1.controls.rute_mudik.setValue('');
            this.informasiMudikForm1.controls.asal_mudik.setValidators([]);
            this.informasiMudikForm1.controls.tujuan_mudik.setValidators([]);
            this.informasiMudikForm1.controls.rute_mudik.setValidators([]);
        }
        else {
            this.isMudik = true;
            this.informasiMudikForm1.controls.asal_mudik.setValidators([__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]);
            this.informasiMudikForm1.controls.tujuan_mudik.setValidators([__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]);
            this.informasiMudikForm1.controls.rute_mudik.setValidators([__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]);
            this.informasiMudikForm1.controls.asal_mudik.enable();
        }
    };
    DetailTiketComponent.prototype.balik = function () {
        if (this.isBalik) {
            this.isBalik = false;
            this.informasiMudikForm1.controls.asal_balik.disable();
            this.informasiMudikForm1.controls.tujuan_balik.disable();
            this.informasiMudikForm1.controls.rute_balik.disable();
            this.informasiMudikForm1.controls.asal_balik.setValue('');
            this.informasiMudikForm1.controls.tujuan_balik.setValue('');
            this.informasiMudikForm1.controls.rute_balik.setValue('');
            this.informasiMudikForm1.controls.asal_balik.setValidators([]);
            this.informasiMudikForm1.controls.tujuan_balik.setValidators([]);
            this.informasiMudikForm1.controls.rute_balik.setValidators([]);
        }
        else {
            this.isBalik = true;
            this.informasiMudikForm1.controls.asal_balik.setValidators([__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]);
            this.informasiMudikForm1.controls.tujuan_balik.setValidators([__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]);
            this.informasiMudikForm1.controls.rute_balik.setValidators([__WEBPACK_IMPORTED_MODULE_11__angular_forms__["l" /* Validators */].required]);
            this.informasiMudikForm1.controls.asal_balik.enable();
        }
    };
    DetailTiketComponent.prototype.summary = function () {
        if (this.nomor_tiket_mudik) {
            this.tiketForm.controls.nomor_tiket.setValue(this.nomor_tiket_mudik);
        }
        if (this.nomor_tiket_balik) {
            this.tiketForm.controls.nomor_tiket.setValue(this.nomor_tiket_balik);
        }
        this.tiketForm.controls.pemudik_dewasa.setValue(this.informasiMudikForm2.value['pemudik_dewasa']);
        this.tiketForm.controls.pemudik_anak.setValue(this.informasiMudikForm2.value['pemudik_anak']);
        if (this.isMudik || this.isBalik) {
            for (var i = 0; i < this.informasiMudikForm2.value['pemudik_dewasa'] + this.informasiMudikForm2.value['pemudik_anak']; ++i) {
                var tanggal = this.tiketForm.controls['pemudik']['controls'][i]['controls']['tanggal']['value'];
                var bulan = this.tiketForm.controls['pemudik']['controls'][i]['controls']['bulan']['value'];
                var tahun = this.tiketForm.controls['pemudik']['controls'][i]['controls']['tahun']['value'];
                this.tiketForm.controls['pemudik']['controls'][i]['controls']['tanggal_lahir']['value'] = tahun + '/' + bulan + '/' + tanggal;
            }
        }
        this.summaryMudik = this.tiketForm.getRawValue();
        this.stepper.next();
    };
    DetailTiketComponent.prototype.bulan = function (date) {
        if (date.substring(5, 7) === '01') {
            return 'Januari';
        }
        else if (date.substring(5, 7) === '02') {
            return 'Februari';
        }
        else if (date.substring(5, 7) === '03') {
            return 'Maret';
        }
        else if (date.substring(5, 7) === '04') {
            return 'April';
        }
        else if (date.substring(5, 7) === '05') {
            return 'Mei';
        }
        else if (date.substring(5, 7) === '06') {
            return 'Juni';
        }
        else if (date.substring(5, 7) === '07') {
            return 'Juli';
        }
        else if (date.substring(5, 7) === '08') {
            return 'Agustus';
        }
        else if (date.substring(5, 7) === '09') {
            return 'September';
        }
        else if (date.substring(5, 7) === '10') {
            return 'Oktober';
        }
        else if (date.substring(5, 7) === '11') {
            return 'November';
        }
        else if (date.substring(5, 7) === '12') {
            return 'Desember';
        }
    };
    DetailTiketComponent.prototype.generateNIK = function (id) {
        var ctrl = this.tiketForm.controls['pemudik'].at(id);
        if (this.tiketForm.controls.pemudik['controls'][id]['controls']['nik']['valid']) {
            var tanggal = Number(String(this.tiketForm.controls.pemudik['controls'][id]['controls']['nik']['value']).substring(6, 8));
            var bulan = Number(String(this.tiketForm.controls.pemudik['controls'][id]['controls']['nik']['value']).substring(8, 10));
            var tahun = Number(String(this.tiketForm.controls.pemudik['controls'][id]['controls']['nik']['value']).substring(10, 12));
            if (tanggal > 40) {
                tanggal = tanggal - 40;
                ctrl['controls'].gender.setValue(2);
                if (tanggal < 10) {
                    ctrl['controls'].tanggal.setValue('0' + String(tanggal));
                }
                else {
                    ctrl['controls'].tanggal.setValue(String(tanggal));
                }
            }
            else {
                ctrl['controls'].gender.setValue(1);
                if (tanggal < 10) {
                    ctrl['controls'].tanggal.setValue('0' + String(tanggal));
                }
                else {
                    ctrl['controls'].tanggal.setValue(String(tanggal));
                }
            }
            if (bulan < 10) {
                ctrl['controls'].bulan.setValue('0' + String(bulan));
            }
            else {
                ctrl['controls'].bulan.setValue(String(bulan));
            }
            if (tahun > 18) {
                ctrl['controls'].tahun.setValue(1900 + tahun);
            }
            else {
                ctrl['controls'].tahun.setValue(2000 + tahun);
            }
            // tslint:disable-next-line: max-line-length
            this.tiketForm.controls.pemudik['controls'][id]['controls']['nik'].setAsyncValidators(ValidateEmailNotTaken.createValidator(this.productService, this.nomor_tiket_mudik, this.nomor_tiket_balik));
            this.tiketForm.controls.pemudik['controls'][id]['controls']['nik'].updateValueAndValidity();
        }
    };
    DetailTiketComponent.prototype.checkNIK = function (i) {
        if (this.tiketForm.controls.pemudik['controls'][i]['controls'].nik.valid) {
            if (this.nomor_tiket_mudik) {
                this.productService.checkNIK(this.nomor_tiket_mudik, this.tiketForm.controls.pemudik['controls'][i]['controls'].nik.value)
                    .then(function (result) {
                    return result.status;
                })
                    .catch(function (error) {
                    console.log(error);
                });
            }
            else if (this.nomor_tiket_balik) {
                this.productService.checkNIK(this.nomor_tiket_balik, this.tiketForm.controls.pemudik['controls'][i]['controls'].nik.value)
                    .then(function (result) {
                    return result.status;
                })
                    .catch(function (error) {
                    console.log(error);
                });
            }
        }
    };
    DetailTiketComponent.prototype.daftarMudik = function () {
        var _this = this;
        var promiseList = [];
        var failMessage;
        if (this.nomor_tiket_mudik && !this.statusDaftarMudik) {
            this.tiketForm.controls.nomor_tiket.setValue(this.nomor_tiket_mudik);
            promiseList.push(this.productService.addPemudik(this.tiketForm.getRawValue()));
        }
        else {
            promiseList.push(Promise.resolve({
                status: true
            }));
        }
        if (this.nomor_tiket_balik && !this.statusDaftarBalik) {
            this.tiketForm.controls.nomor_tiket.setValue(this.nomor_tiket_balik);
            promiseList.push(this.productService.addPemudik(this.tiketForm.getRawValue()));
        }
        else {
            promiseList.push(Promise.resolve({
                status: true
            }));
        }
        if (this.nomor_tiket_mudik_motor && !this.statusDaftarMudikMotor) {
            this.tiketForm.controls.nomor_tiket.setValue(this.nomor_tiket_mudik_motor);
            var tiket = this.tiketForm.getRawValue();
            var obj = {
                nomor_tiket: tiket.nomor_tiket,
                banyak_motor: tiket.banyak_motor,
                pemudik: []
            };
            for (var i = 0; i < tiket.banyak_motor; ++i) {
                obj.pemudik.push(tiket.pemudik[i]);
            }
            promiseList.push(this.productService.addPemudikMotor(obj));
        }
        else {
            promiseList.push(Promise.resolve({
                status: true
            }));
        }
        if (this.nomor_tiket_balik_motor && !this.statusDaftarBalikMotor) {
            this.tiketForm.controls.nomor_tiket.setValue(this.nomor_tiket_balik_motor);
            var tiket = this.tiketForm.getRawValue();
            var obj = {
                nomor_tiket: tiket.nomor_tiket,
                banyak_motor: tiket.banyak_motor,
                pemudik: []
            };
            for (var i = 0; i < tiket.banyak_motor; ++i) {
                obj.pemudik.push(tiket.pemudik[i]);
            }
            promiseList.push(this.productService.addPemudikMotor(obj));
        }
        else {
            promiseList.push(Promise.resolve({
                status: true
            }));
        }
        Promise.all(promiseList)
            .then(function (responses) {
            if (responses[0].status) {
                _this.statusDaftarMudik = true;
            }
            else {
                _this.statusDaftarMudik = false;
                if (responses[0].message) {
                    failMessage = responses[0].message;
                }
            }
            if (responses[1].status) {
                _this.statusDaftarBalik = true;
            }
            else {
                _this.statusDaftarBalik = false;
                if (responses[1].message) {
                    failMessage = responses[1].message;
                }
            }
            if (responses[2].status) {
                _this.statusDaftarMudikMotor = true;
            }
            else {
                _this.statusDaftarMudikMotor = false;
                if (responses[2].message) {
                    failMessage = responses[2].message;
                }
            }
            if (responses[3].status) {
                _this.statusDaftarBalikMotor = true;
            }
            else {
                _this.statusDaftarBalikMotor = false;
                if (responses[3].message) {
                    failMessage = responses[3].message;
                }
            }
            if (_this.statusDaftarMudik && _this.statusDaftarBalik && _this.statusDaftarMudikMotor && _this.statusDaftarBalikMotor) {
                if (_this.nomor_tiket_mudik && _this.statusDaftarMudik) {
                    _this.print(_this.nomor_tiket_mudik);
                }
                if (_this.nomor_tiket_balik && _this.statusDaftarBalik) {
                    _this.print(_this.nomor_tiket_balik);
                }
                if (_this.nomor_tiket_mudik_motor && _this.statusDaftarMudikMotor) {
                    _this.print(_this.nomor_tiket_mudik_motor);
                }
                if (_this.nomor_tiket_balik_motor && _this.statusDaftarBalikMotor) {
                    _this.print(_this.nomor_tiket_balik_motor);
                }
                _this.router.navigate(['apps/tiket/daftarTiket']);
            }
            else {
                __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                    title: 'Gagal memasukkan data',
                    text: failMessage,
                    type: 'error'
                });
            }
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    DetailTiketComponent.prototype.approve = function () {
        var _this = this;
        this.productService.approve(this.status)
            .then(function (response) {
            if (response.status === true) {
                _this.snackBar.open(response.message, 'Success', {
                    verticalPosition: 'top',
                    duration: 5000
                });
                _this.status = 1;
            }
            else {
                if (response.message === 'decline berhasil tapi sms tidak terkirim') {
                    _this.snackBar.open(response.message, 'Success', {
                        verticalPosition: 'top',
                        duration: 5000
                    });
                    _this.status = 1;
                }
                else {
                    _this.snackBar.open(response.message, 'Error', {
                        verticalPosition: 'top',
                        duration: 5000
                    });
                }
            }
        })
            .catch(function (error) {
            var eror = JSON.parse(error._body);
            __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                title: 'Error !',
                text: eror.message,
                type: 'error'
            });
        });
    };
    DetailTiketComponent.prototype.decline = function () {
        var _this = this;
        this.productService.decline(this.status)
            .then(function (response) {
            if (response.status === true) {
                _this.snackBar.open(response.message, 'Success', {
                    verticalPosition: 'top',
                    duration: 5000
                });
                _this.status = 5;
            }
            else {
                if (response.message === 'decline berhasil tapi sms tidak terkirim') {
                    _this.snackBar.open(response.message, 'Success', {
                        verticalPosition: 'top',
                        duration: 5000
                    });
                    _this.status = 5;
                }
                else {
                    _this.snackBar.open(response.message, 'Error', {
                        verticalPosition: 'top',
                        duration: 5000
                    });
                }
            }
        })
            .catch(function (error) {
            var eror = JSON.parse(error._body);
            __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                title: 'Error !',
                text: eror.message,
                type: 'error'
            });
        });
    };
    DetailTiketComponent.prototype.simpan = function () {
        var _this = this;
        this.productService.save(this.tiketForm.getRawValue(), this.product.id)
            .then(function (response) {
            _this.snackBar.open(response.message, response.status, {
                verticalPosition: 'top',
                duration: 5000
            });
        })
            .catch(function (error) {
            var eror = JSON.parse(error._body);
            __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                title: 'Error !',
                text: eror.message,
                type: 'error'
            });
        });
    };
    DetailTiketComponent.prototype.simpanPemudik = function (id) {
        var promise_list = [this.productService.savePemudik(this.tiketForm.getRawValue().pemudiks[id], this.product.pemudik[id].id)];
        var data = this.tiketForm.getRawValue().pemudiks[id];
        if (data.pindah_kursi) {
            var obj = {
                'pemudik_id': this.product.pemudik[id].id,
                'kendaraan_baru_id': this.product.kendaraan.id,
                'kursi_baru_id': data.pindah_kursi
            };
            promise_list.push(this.productService.pindahKursi(obj));
        }
        Promise.all(promise_list)
            .then(function (result) {
            if (result[1].status) {
                __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                    title: 'Sukses !',
                    text: result[1].message,
                    type: 'success'
                });
            }
            else {
                __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                    title: 'Error !',
                    text: result[1].message,
                    type: 'error'
                });
            }
        }).catch(function (error) {
            console.log(error);
        });
        // this.productService.savePemudik(this.tiketForm.getRawValue().pemudiks[id], this.product.pemudik[id].id)
        //     .then((response: any) => {
        //         this.snackBar.open(response.message, response.status, {
        //             verticalPosition: 'top',
        //             duration        : 5000
        //         })
        //     })
        //     .catch((error) => {
        //         let eror = JSON.parse(error._body);
        //         swal({
        //             title: 'Error !',
        //             text: eror.message,
        //             type: 'error'
        //         })
        //     })
    };
    DetailTiketComponent.prototype.hapusPemudik = function (id) {
        var _this = this;
        this.productService.hapusPemudik(this.product.pemudik[id].id)
            .then(function (response) {
            _this.snackBar.open(response.message, response.status, {
                verticalPosition: 'top',
                duration: 5000
            });
        })
            .catch(function (error) {
            var eror = JSON.parse(error._body);
            __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                title: 'Error !',
                text: eror.message,
                type: 'error'
            });
        });
    };
    DetailTiketComponent.prototype.hapusTiket = function () {
        var _this = this;
        if (this.pageType === 'edit') {
            this.productService.hapusTiket()
                .then(function (response) {
                _this.snackBar.open(response.message, response.status, {
                    verticalPosition: 'top',
                    duration: 5000
                });
                _this.back();
            })
                .catch(function (error) {
                var eror = JSON.parse(error._body);
                __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
            });
        }
    };
    DetailTiketComponent.prototype.tiketBatal = function () {
        var _this = this;
        this.tiketForm = this.createTiketFormNew();
        this.stepper.selectedIndex = 0;
        if (this.nomor_tiket_mudik) {
            this.productService.batalTiket(this.nomor_tiket_mudik)
                .then(function (response) {
                _this.snackBar.open(response.message, response.status, {
                    verticalPosition: 'top',
                    duration: 1000
                });
            })
                .catch(function (error) {
                var eror = JSON.parse(error._body);
                __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
            });
        }
        if (this.nomor_tiket_balik) {
            this.productService.batalTiket(this.nomor_tiket_balik)
                .then(function (response) {
                _this.snackBar.open(response.message, response.status, {
                    verticalPosition: 'top',
                    duration: 1000
                });
            })
                .catch(function (error) {
                var eror = JSON.parse(error._body);
                __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
            });
        }
        if (this.nomor_tiket_mudik_motor) {
            this.productService.batalTiket(this.nomor_tiket_mudik_motor)
                .then(function (response) {
                _this.snackBar.open(response.message, response.status, {
                    verticalPosition: 'top',
                    duration: 1000
                });
            })
                .catch(function (error) {
                var eror = JSON.parse(error._body);
                __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
            });
        }
        if (this.nomor_tiket_balik_motor) {
            this.productService.batalTiket(this.nomor_tiket_balik_motor)
                .then(function (response) {
                _this.snackBar.open(response.message, response.status, {
                    verticalPosition: 'top',
                    duration: 1000
                });
            })
                .catch(function (error) {
                var eror = JSON.parse(error._body);
                __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
            });
        }
    };
    DetailTiketComponent.prototype.print = function (tiket) {
        var fileURL = __WEBPACK_IMPORTED_MODULE_15__environments_environment__["a" /* environment */].setting.base_url + '/tiket/' + tiket + '/download';
        window.open(fileURL, '_blank');
    };
    DetailTiketComponent.prototype.back = function () {
        this.router.navigate(['apps/tiket/daftarTiket']);
        // this._location.back();
    };
    DetailTiketComponent.prototype.kendaraanSwal = function (id) {
        this.productService.getKendaraan(id)
            .then(function (response) {
            var kursis = response.data.kursis;
            __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                title: 'Detail Kendaraan',
                type: 'info',
                html: '<table style="width: 100%">' +
                    '<tr>' +
                    '<td style="width: 16%">Pintu Depan</td><td style="width: 16%"></td><td style="width: 16%"></td><td style="width: 16%"></td><td style="width: 16%"></td><td style="width: 16%">Supir</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="width: 16%;' + (kursis[0].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '1' + '</td>' +
                    '<td style="width: 16%;' + (kursis[1].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '2' + '</td>' +
                    '<td style="width: 16%"></td> ' +
                    '<td style="width: 16%;' + (kursis[2].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '3' + '</td>' +
                    '<td style="width: 16%;' + (kursis[3].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '4' + '</td>' +
                    '<td style="width: 16%;' + (kursis[4].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '5' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="width: 16%;' + (kursis[5].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '6' + '</td>' +
                    '<td style="width: 16%;' + (kursis[6].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '7' + '</td>' +
                    '<td style="width: 16%"></td> ' +
                    '<td style="width: 16%;' + (kursis[7].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '8' + '</td>' +
                    '<td style="width: 16%;' + (kursis[8].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '9' + '</td>' +
                    '<td style="width: 16%;' + (kursis[9].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '10' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="width: 16%;' + (kursis[10].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '11' + '</td>' +
                    '<td style="width: 16%;' + (kursis[11].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '12' + '</td>' +
                    '<td style="width: 16%"></td> ' +
                    '<td style="width: 16%;' + (kursis[12].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '13' + '</td>' +
                    '<td style="width: 16%;' + (kursis[13].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '14' + '</td>' +
                    '<td style="width: 16%;' + (kursis[14].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '15' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="width: 16%;' + (kursis[15].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '16' + '</td>' +
                    '<td style="width: 16%;' + (kursis[16].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '17' + '</td>' +
                    '<td style="width: 16%"></td> ' +
                    '<td style="width: 16%;' + (kursis[17].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '18' + '</td>' +
                    '<td style="width: 16%;' + (kursis[18].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '19' + '</td>' +
                    '<td style="width: 16%;' + (kursis[19].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '20' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="width: 16%;' + (kursis[20].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '21' + '</td>' +
                    '<td style="width: 16%;' + (kursis[21].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '22' + '</td>' +
                    '<td style="width: 16%"></td> ' +
                    '<td style="width: 16%;' + (kursis[22].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '23' + '</td>' +
                    '<td style="width: 16%;' + (kursis[23].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '24' + '</td>' +
                    '<td style="width: 16%;' + (kursis[24].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '25' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="width: 16%;' + (kursis[25].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '26' + '</td>' +
                    '<td style="width: 16%;' + (kursis[26].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '27' + '</td>' +
                    '<td style="width: 16%"></td> ' +
                    '<td style="width: 16%;' + (kursis[27].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '28' + '</td>' +
                    '<td style="width: 16%;' + (kursis[28].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '29' + '</td>' +
                    '<td style="width: 16%;' + (kursis[29].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '30' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="width: 16%;' + (kursis[30].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '31' + '</td>' +
                    '<td style="width: 16%;' + (kursis[31].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '32' + '</td>' +
                    '<td style="width: 16%"></td> ' +
                    '<td style="width: 16%;' + (kursis[32].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '33' + '</td>' +
                    '<td style="width: 16%;' + (kursis[33].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '34' + '</td>' +
                    '<td style="width: 16%;' + (kursis[34].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '35' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="width: 16%;' + (kursis[35].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '36' + '</td>' +
                    '<td style="width: 16%;' + (kursis[36].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '37' + '</td>' +
                    '<td style="width: 16%"></td> ' +
                    '<td style="width: 16%;' + (kursis[37].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '38' + '</td>' +
                    '<td style="width: 16%;' + (kursis[38].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '39' + '</td>' +
                    '<td style="width: 16%;' + (kursis[39].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '40' + '</td>' +
                    '<tr>' +
                    '<td style="width: 16%;' + (kursis[40].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '41' + '</td>' +
                    '<td style="width: 16%;' + (kursis[41].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '42' + '</td>' +
                    '<td style="width: 16%"></td> ' +
                    '<td style="width: 16%;' + (kursis[42].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '43' + '</td>' +
                    '<td style="width: 16%;' + (kursis[43].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '44' + '</td>' +
                    '<td style="width: 16%;' + (kursis[44].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '45' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="width: 16%;' + (kursis[45].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '46' + '</td>' +
                    '<td style="width: 16%;' + (kursis[46].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '47' + '</td>' +
                    '<td style="width: 16%"></td> ' +
                    '<td style="width: 16%;' + (kursis[47].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '48' + '</td>' +
                    '<td style="width: 16%;' + (kursis[48].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '49' + '</td>' +
                    '<td style="width: 16%;' + (kursis[49].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '50' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="width: 16%">Pintu Belakang</td> ' +
                    '<td style="width: 16%"></td> ' +
                    '<td style="width: 16%"></td> ' +
                    '<td style="width: 16%;' + (kursis[50].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '51' + '</td>' +
                    '<td style="width: 16%;' + (kursis[51].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '52' + '</td>' +
                    '<td style="width: 16%;' + (kursis[52].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '53' + '</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="width: 16%;' + (kursis[53].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '54' + '</td>' +
                    '<td style="width: 16%;' + (kursis[54].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '55' + '</td>' +
                    '<td style="width: 16%;' + (kursis[55].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '56' + '</td>' +
                    '<td style="width: 16%;' + (kursis[56].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '57' + '</td>' +
                    '<td style="width: 16%;' + (kursis[57].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '58' + '</td>' +
                    '<td style="width: 16%;' + (kursis[58].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '59' + '</td>' +
                    '</tr>' +
                    '</table>',
                width: 600
            });
        })
            .catch(function (error) {
            var eror = JSON.parse(error._body);
            __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
                title: 'Error !',
                text: eror.message,
                type: 'error'
            });
        });
    };
    DetailTiketComponent.prototype.editPemudik = function () {
        this.pageType2 === 'view' ? this.pageType2 = 'edit' : this.pageType2 = 'view';
    };
    DetailTiketComponent.prototype.openDialog = function (url) {
        __WEBPACK_IMPORTED_MODULE_18_sweetalert2___default()({
            imageUrl: url,
            imageWidth: 800,
            imageHeight: 400,
            imageAlt: 'Custom image',
            animation: false
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('successSwal'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_17__toverux_ngx_sweetalert2__["a" /* SwalComponent */])
    ], DetailTiketComponent.prototype, "successSwal", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('stepper'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_12__angular_material__["O" /* MatStepper */])
    ], DetailTiketComponent.prototype, "stepper", void 0);
    DetailTiketComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-product',
            template: __webpack_require__("../../../../../src/app/main/content/apps/tiket/detailTiket/detailTiket.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/tiket/detailTiket/detailTiket.component.scss")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
            animations: __WEBPACK_IMPORTED_MODULE_2__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__detailTiket_service__["a" /* DetailTiketService */],
            __WEBPACK_IMPORTED_MODULE_11__angular_forms__["c" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_12__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_13__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_14__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["f" /* Router */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_13__angular_common__["Location"],
            __WEBPACK_IMPORTED_MODULE_12__angular_material__["m" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_16__session_service__["a" /* SessionService */]])
    ], DetailTiketComponent);
    return DetailTiketComponent;
}());

var ValidateEmailNotTaken = /** @class */ (function () {
    function ValidateEmailNotTaken() {
    }
    ValidateEmailNotTaken.createValidator = function (productService, nomor_tiket_mudik, nomor_tiket_balik) {
        return function (control) {
            // return productService.checkEmailNotTaken(nomor_tiket_mudik, control.value).map(res => {
            //     console.log(res);
            //     console.log(nomor_tiket_mudik);
            //     return res.status ? null : { nikTaken: true };
            // });
            var promise_list = [];
            if (nomor_tiket_mudik) {
                promise_list.push(productService.checkNIK(nomor_tiket_mudik, control.value));
            }
            else {
                promise_list.push(Promise.resolve({
                    status: true
                }));
            }
            if (nomor_tiket_balik) {
                promise_list.push(productService.checkNIK(nomor_tiket_balik, control.value));
            }
            else {
                promise_list.push(Promise.resolve({
                    status: true
                }));
            }
            return Promise.all(promise_list)
                .then(function (result) {
                var status = true;
                for (var index = 0; index < result.length; index++) {
                    if (!result[index].status) {
                        status = false;
                        break;
                    }
                }
                return status ? null : { nikTaken: true };
            });
        };
    };
    return ValidateEmailNotTaken;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/tiket/detailTiket/detailTiket.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Product; });
var Product = /** @class */ (function () {
    function Product(product) {
        if (product) {
            product = product;
            this.id = product.id;
            this.alamat = product.alamat;
            this.telepon = product.telepon;
            this.status = product.status;
            this.kota_tujuan = product.kota_tujuan;
            this.pemudik = product.pemudiks;
            this.kendaraan = product.kendaraan;
            this.pemudik_dewasa = product.pemudik_dewasa;
            this.pemudik_anak = product.pemudik_anak;
            this.rute = product.rute;
            this.nomor_tiket = product.nomor_tiket;
            this.foto_kk = product.foto_kk;
        }
        else {
            product = {};
            this.nomor_tiket = '';
            this.pemudik_anak = 0;
            this.pemudik_dewasa = 0;
            this.alamat = '';
            this.telepon = '';
            this.status = 0;
            this.kota_tujuan = '';
            this.pemudik = [];
            this.kendaraan = [];
            this.rute = [];
        }
    }
    return Product;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/tiket/detailTiket/detailTiket.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailTiketService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DetailTiketService = /** @class */ (function () {
    function DetailTiketService(http, snackBar, _sessionService) {
        this.http = http;
        this.snackBar = snackBar;
        this._sessionService = _sessionService;
        this.token = this._sessionService.get().token;
        this.onProductChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({});
        this.asalMudik = [];
        this.asalBalik = [];
        this.list_kursi = [];
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DetailTiketService.prototype.resolve = function (route, state) {
        var _this = this;
        this.routeParams = route.params;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getProduct(),
                _this.getAsalBalik(),
                _this.getAsalMudik(),
            ]).then(function () {
                resolve();
            }, reject).catch(function (error) {
                console.log(error);
            });
        });
    };
    DetailTiketService.prototype.getProduct = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.routeParams.id === 'daftarMudik') {
                _this.onProductChanged.next(false);
                resolve(false);
            }
            else {
                _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/pemudik/booking/detail/' + _this.routeParams.id, _this.setHeader())
                    .map(function (res) { return res.json(); })
                    .subscribe(function (response) {
                    _this.getKursiKosong(response.data.kendaraan.id)
                        .then(function (result) {
                        _this.list_kursi = [];
                        for (var i = 0; i < result.data.length; ++i) {
                            var kursi = result.data[i];
                            _this.list_kursi.push(kursi);
                        }
                        _this.product = response['data'];
                        _this.onProductChanged.next(_this.product);
                        resolve(response);
                    });
                }, function (error) {
                    reject(error);
                });
            }
        });
    };
    DetailTiketService.prototype.getAsalMudik = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/kota/mudik', _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                for (var i = 0; i < response.data.length; ++i) {
                    if (!_this.asalMudik.includes(response.data[i].asal)) {
                        _this.asalMudik.push(response.data[i].asal);
                    }
                }
                _this.listKotaMudik = response.data;
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTiketService.prototype.getAsalBalik = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/kota/balik', _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                for (var i = 0; i < response.data.length; ++i) {
                    if (!_this.asalBalik.includes(response.data[i].asal)) {
                        _this.asalBalik.push(response.data[i].asal);
                    }
                }
                _this.listKotaBalik = response.data;
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTiketService.prototype.getRuteByAsalTujuan = function (asal, tujuan) {
        var url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/pemudik/rute/'
            + asal + '/'
            + tujuan;
        return this.http.get(url, this.setHeader());
    };
    DetailTiketService.prototype.booking = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/biasa/booking', data, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTiketService.prototype.bookingMotor = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/motor/booking', data, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTiketService.prototype.addPemudik = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/biasa/create', data, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetailTiketService.prototype.addPemudikMotor = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/motor/create', data, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTiketService.prototype.approve = function (status) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.put(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/approve/' + _this.routeParams.id, { 'status': status }, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTiketService.prototype.decline = function (status) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.put(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/decline/' + _this.routeParams.id, { 'status': status }, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTiketService.prototype.save = function (data, id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.put(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/tiket/' + id, data, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTiketService.prototype.savePemudik = function (data, id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.put(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/' + id, data, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTiketService.prototype.pindahKursi = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/pindah/kursi', data, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTiketService.prototype.hapusPemudik = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.delete(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/delete/' + id, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTiketService.prototype.hapusTiket = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.delete(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/biasa/cancel/' + _this.routeParams.id, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    DetailTiketService.prototype.batalTiket = function (tiket) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.delete(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/tiket/' + tiket, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (err) {
                reject(err);
            });
        });
    };
    // printTiket(tiket): Promise<any> {
    // const options = new RequestOptions();
    // const header = new Headers();
    // header.append('Authorization', `Bearer ${this.token}`);
    // header.append('Content-Type', 'application/json');
    // header.append('Accept', 'application/pdf');
    // options.headers = header;
    // return new Promise((resolve, reject) => {
    //     this.http.get(environment.setting.base_url + '/admin/tiket/' + tiket + '/download', {
    //             headers: header, responseType: ResponseContentType.Blob
    //         })
    //         .map((res) => {
    //             return {
    //                 filename: `tiket-${tiket}.pdf`,
    //                 data: res.blob(),
    //                 success: true
    //             };
    //         })
    //         .subscribe((res: any) => {
    //             const url = window.URL.createObjectURL(res.data);
    //             const a = document.createElement('a');
    //             document.body.appendChild(a);
    //             a.setAttribute('style', 'display: none');
    //             a.href = url;
    //             a.download = res.filename;
    //             a.click();
    //             window.URL.revokeObjectURL(url);
    //             a.remove(); // remove the element
    //             resolve(res);
    //         }, (err) => {
    //             console.log('download error:', JSON.stringify(err));
    //             reject(err);
    //         }, () => {
    //         });
    // });
    // }
    DetailTiketService.prototype.getKursiKosong = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/kendaraan/getListKursiKosong/' + id, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                console.log(response);
                resolve(response);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetailTiketService.prototype.getKendaraan = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/kendaraan/' + id, _this.setHeader())
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetailTiketService.prototype.checkNIK = function (tiket, nik) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/pemudik/checkNik/' + tiket + '/' + nik)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                resolve(response);
            }, function (err) {
                reject(err);
            });
        });
    };
    DetailTiketService.prototype.checkEmailNotTaken = function (tiket, nik) {
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/pemudik/checkNik/' + tiket + '/' + nik)
            .map(function (res) { return res.json(); });
    };
    DetailTiketService.prototype.setHeader = function () {
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        options.headers = headers;
        return options;
    };
    DetailTiketService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_4__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_6__session_service__["a" /* SessionService */]])
    ], DetailTiketService);
    return DetailTiketService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/tiket/tiket.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TiketModule", function() { return TiketModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__ = __webpack_require__("../../../../@swimlane/ngx-charts/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__ = __webpack_require__("../../../../../src/app/core/modules/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__ = __webpack_require__("../../../../../src/app/core/components/widget/widget.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__daftarTiket_daftarTiket_component__ = __webpack_require__("../../../../../src/app/main/content/apps/tiket/daftarTiket/daftarTiket.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__daftarTiket_daftarTiket_service__ = __webpack_require__("../../../../../src/app/main/content/apps/tiket/daftarTiket/daftarTiket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__detailTiket_detailTiket_component__ = __webpack_require__("../../../../../src/app/main/content/apps/tiket/detailTiket/detailTiket.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__detailTiket_detailTiket_service__ = __webpack_require__("../../../../../src/app/main/content/apps/tiket/detailTiket/detailTiket.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__auth_guard__ = __webpack_require__("../../../../../src/app/main/content/apps/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__toverux_ngx_sweetalert2__ = __webpack_require__("../../../../@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var routes = [
    {
        path: 'daftarTiket',
        component: __WEBPACK_IMPORTED_MODULE_5__daftarTiket_daftarTiket_component__["a" /* DaftarTiketComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_10__auth_guard__["a" /* AuthGuard */]]
    },
    {
        path: 'detailTiket/:id',
        component: __WEBPACK_IMPORTED_MODULE_7__detailTiket_detailTiket_component__["a" /* DetailTiketComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_10__auth_guard__["a" /* AuthGuard */]],
        resolve: {
            data: __WEBPACK_IMPORTED_MODULE_8__detailTiket_detailTiket_service__["a" /* DetailTiketService */]
        }
    }
];
var TiketModule = /** @class */ (function () {
    function TiketModule() {
    }
    TiketModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__["a" /* FuseWidgetModule */],
                __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__["NgxChartsModule"],
                __WEBPACK_IMPORTED_MODULE_9__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
                }),
                __WEBPACK_IMPORTED_MODULE_11__toverux_ngx_sweetalert2__["b" /* SweetAlert2Module */].forRoot()
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__daftarTiket_daftarTiket_component__["a" /* DaftarTiketComponent */],
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__daftarTiket_daftarTiket_component__["a" /* DaftarTiketComponent */],
                __WEBPACK_IMPORTED_MODULE_7__detailTiket_detailTiket_component__["a" /* DetailTiketComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__daftarTiket_daftarTiket_service__["a" /* DaftarTiketService */],
                __WEBPACK_IMPORTED_MODULE_8__detailTiket_detailTiket_service__["a" /* DetailTiketService */]
            ]
        })
    ], TiketModule);
    return TiketModule;
}());



/***/ }),

/***/ "../../../../@toverux/ngx-sweetalert2/esm5/toverux-ngx-sweetalert2.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export SwalPartialTargets */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SwalComponent; });
/* unused harmony export SwalDirective */
/* unused harmony export SwalPartialDirective */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SweetAlert2Module; });
/* unused harmony export ɵa */
/* unused harmony export ɵb */
/* unused harmony export ɵc */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert2__ = __webpack_require__("../../../../sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common__ = __webpack_require__("../../../common/esm5/common.js");




var SwalPartialTargets = /** @class */ (function () {
    function SwalPartialTargets() {
        this.title = function () { return __WEBPACK_IMPORTED_MODULE_0_sweetalert2___default.a.getTitle(); };
        this.content = function () { return __WEBPACK_IMPORTED_MODULE_0_sweetalert2___default.a.getContent(); };
        this.buttonsWrapper = function () { return __WEBPACK_IMPORTED_MODULE_0_sweetalert2___default.a.getButtonsWrapper(); };
        this.actions = function () { return __WEBPACK_IMPORTED_MODULE_0_sweetalert2___default.a.getActions(); };
        this.confirmButton = function () { return __WEBPACK_IMPORTED_MODULE_0_sweetalert2___default.a.getConfirmButton(); };
        this.cancelButton = function () { return __WEBPACK_IMPORTED_MODULE_0_sweetalert2___default.a.getCancelButton(); };
        this.footer = function () { return __WEBPACK_IMPORTED_MODULE_0_sweetalert2___default.a.getFooter(); };
    }
    return SwalPartialTargets;
}());
var SwalDefaults = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["InjectionToken"]('SwalDefaults');
function swalDefaultsProvider(options) {
    if (options === void 0) { options = {}; }
    return {
        provide: SwalDefaults,
        useValue: options
    };
}
var SwalComponent = /** @class */ (function () {
    function SwalComponent(defaultSwalOptions) {
        this.defaultSwalOptions = defaultSwalOptions;
        this.beforeOpen = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        this.open = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        this.close = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        this.confirm = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        this.cancel = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        this.nativeSwal = __WEBPACK_IMPORTED_MODULE_0_sweetalert2___default.a;
        this.isCurrentlyShown = false;
        this.touchedProps = new Set();
        this.markTouched = this.touchedProps.add.bind(this.touchedProps);
        this.show = this.show.bind(this);
    }
    Object.defineProperty(SwalComponent.prototype, "options", {
        get: function () {
            var _this = this;
            var options = {};
            this.touchedProps.forEach(function (prop) {
                options[prop] = ((_this))[prop];
            });
            return options;
        },
        set: function (options) {
            Object.assign(this, options);
            Object.keys(options).forEach(this.markTouched);
        },
        enumerable: true,
        configurable: true
    });
    SwalComponent.prototype.ngOnChanges = function (changes) {
        Object.keys(changes)
            .filter(function (prop) { return prop !== 'options'; })
            .forEach(this.markTouched);
    };
    SwalComponent.prototype.ngOnDestroy = function () {
        if (this.isCurrentlyShown) {
            __WEBPACK_IMPORTED_MODULE_0_sweetalert2___default.a.close();
        }
    };
    SwalComponent.prototype.show = function () {
        var _this = this;
        var options = Object.assign({}, this.defaultSwalOptions, this.options, { onBeforeOpen: function (modalElement) {
                _this.beforeOpen.emit({ modalElement: modalElement });
            }, onOpen: function (modalElement) {
                _this.isCurrentlyShown = true;
                _this.open.emit({ modalElement: modalElement });
            }, onClose: function (modalElement) {
                _this.isCurrentlyShown = false;
                _this.close.emit({ modalElement: modalElement });
            } });
        var promise = __WEBPACK_IMPORTED_MODULE_0_sweetalert2___default()(options);
        var useRejections = ((options)).useRejections;
        promise.then(function (result) {
            if (useRejections) {
                _this.confirm.emit(result);
            }
            else if ('value' in result) {
                _this.confirm.emit(result.value);
            }
            else {
                _this.cancel.emit(result.dismiss);
            }
        }, function (err) {
            if (useRejections) {
                _this.cancel.emit(err);
            }
        });
        return promise;
    };
    return SwalComponent;
}());
SwalComponent.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"], args: [{
                selector: 'swal',
                template: '',
                changeDetection: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ChangeDetectionStrategy"].OnPush
            },] },
];
SwalComponent.ctorParameters = function () { return [
    { type: undefined, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Inject"], args: [SwalDefaults,] },] },
]; };
SwalComponent.propDecorators = {
    "title": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "titleText": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "text": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "html": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "footer": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "type": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "backdrop": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "toast": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "target": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "input": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "width": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "padding": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "background": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "position": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "grow": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "customClass": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "timer": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "animation": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "allowOutsideClick": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "allowEscapeKey": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "allowEnterKey": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "showConfirmButton": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "showCancelButton": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "confirmButtonText": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "cancelButtonText": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "confirmButtonColor": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "cancelButtonColor": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "confirmButtonClass": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "cancelButtonClass": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "confirmButtonAriaLabel": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "cancelButtonAriaLabel": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "buttonsStyling": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "reverseButtons": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "focusConfirm": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "focusCancel": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "showCloseButton": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "closeButtonAriaLabel": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "showLoaderOnConfirm": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "preConfirm": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "imageUrl": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "imageWidth": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "imageHeight": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "imageAlt": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "imageClass": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "inputPlaceholder": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "inputValue": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "inputOptions": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "inputAutoTrim": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "inputAttributes": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "inputValidator": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "inputClass": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "progressSteps": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "currentProgressStep": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "progressStepsDistance": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "beforeOpen": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"] },],
    "open": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"] },],
    "close": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"] },],
    "confirm": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"] },],
    "cancel": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"] },],
    "options": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
};
var SwalDirective = /** @class */ (function () {
    function SwalDirective(viewContainerRef, resolver) {
        this.viewContainerRef = viewContainerRef;
        this.resolver = resolver;
        this.confirm = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
        this.cancel = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
    }
    Object.defineProperty(SwalDirective.prototype, "swal", {
        set: function (options) {
            if (options instanceof SwalComponent) {
                this.swalInstance = options;
            }
            else if (Array.isArray(options)) {
                this.swalOptions = __WEBPACK_IMPORTED_MODULE_0_sweetalert2___default.a.argsToParams(options);
            }
            else {
                this.swalOptions = options;
            }
        },
        enumerable: true,
        configurable: true
    });
    SwalDirective.prototype.ngOnInit = function () {
        if (!this.swalInstance) {
            var factory = this.resolver.resolveComponentFactory(SwalComponent);
            this.swalRef = this.viewContainerRef.createComponent(factory);
            this.swalInstance = this.swalRef.instance;
        }
    };
    SwalDirective.prototype.ngOnDestroy = function () {
        if (this.swalRef) {
            this.swalRef.destroy();
        }
    };
    SwalDirective.prototype.onHostClicked = function (event) {
        var _this = this;
        event.preventDefault();
        event.stopImmediatePropagation();
        event.stopPropagation();
        if (this.swalOptions) {
            this.swalInstance.options = this.swalOptions;
        }
        var confirmSub = this.swalInstance.confirm.asObservable().subscribe(function (v) { return _this.confirm.emit(v); });
        var cancelSub = this.swalInstance.cancel.asObservable().subscribe(function (v) { return _this.cancel.emit(v); });
        this.swalInstance.show().then(unsubscribe);
        function unsubscribe() {
            confirmSub.unsubscribe();
            cancelSub.unsubscribe();
        }
    };
    return SwalDirective;
}());
SwalDirective.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Directive"], args: [{
                selector: '[swal]'
            },] },
];
SwalDirective.ctorParameters = function () { return [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"], },
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ComponentFactoryResolver"], },
]; };
SwalDirective.propDecorators = {
    "swal": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
    "confirm": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"] },],
    "cancel": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"] },],
    "onHostClicked": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["HostListener"], args: ['click', ['$event'],] },],
};
var SwalPartialComponent = /** @class */ (function () {
    function SwalPartialComponent() {
    }
    return SwalPartialComponent;
}());
SwalPartialComponent.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"], args: [{
                template: '<ng-container *ngTemplateOutlet="template"></ng-container>',
                changeDetection: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ChangeDetectionStrategy"].OnPush
            },] },
];
SwalPartialComponent.ctorParameters = function () { return []; };
SwalPartialComponent.propDecorators = {
    "template": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
};
var SwalPartialDirective = /** @class */ (function () {
    function SwalPartialDirective(resolver, injector, app, templateRef, swalTargets, swalComponent) {
        this.resolver = resolver;
        this.injector = injector;
        this.app = app;
        this.templateRef = templateRef;
        this.swalTargets = swalTargets;
        this.swalComponent = swalComponent;
    }
    SwalPartialDirective.prototype.ngOnInit = function () {
        var _this = this;
        this.beforeOpenSubscription = this.swalComponent.beforeOpen.asObservable().subscribe(function () {
            var targetEl = _this.swalPartial ? _this.swalPartial() : _this.swalTargets.content();
            var factory = _this.resolver.resolveComponentFactory(SwalPartialComponent);
            _this.partialRef = factory.create(_this.injector, [], targetEl);
            _this.partialRef.instance.template = _this.templateRef;
            _this.app.attachView(_this.partialRef.hostView);
        });
        this.closeSubscription = this.swalComponent.close.asObservable().subscribe(function () {
            _this.app.detachView(_this.partialRef.hostView);
            _this.partialRef.destroy();
        });
    };
    SwalPartialDirective.prototype.ngOnDestroy = function () {
        this.beforeOpenSubscription.unsubscribe();
        this.closeSubscription.unsubscribe();
    };
    return SwalPartialDirective;
}());
SwalPartialDirective.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Directive"], args: [{
                selector: '[swalPartial]'
            },] },
];
SwalPartialDirective.ctorParameters = function () { return [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ComponentFactoryResolver"], },
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Injector"], },
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ApplicationRef"], },
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["TemplateRef"], },
    { type: SwalPartialTargets, },
    { type: SwalComponent, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Host"] },] },
]; };
SwalPartialDirective.propDecorators = {
    "swalPartial": [{ type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"] },],
};
var SweetAlert2Module = /** @class */ (function () {
    function SweetAlert2Module() {
    }
    SweetAlert2Module.forRoot = function (defaultSwalOptions) {
        return {
            ngModule: SweetAlert2Module,
            providers: [swalDefaultsProvider(defaultSwalOptions)]
        };
    };
    return SweetAlert2Module;
}());
SweetAlert2Module.decorators = [
    { type: __WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"], args: [{
                declarations: [
                    SwalComponent, SwalPartialDirective, SwalPartialComponent,
                    SwalDirective
                ],
                providers: [
                    SwalPartialTargets
                ],
                imports: [
                    __WEBPACK_IMPORTED_MODULE_2__angular_common__["CommonModule"]
                ],
                exports: [
                    SwalComponent, SwalPartialDirective,
                    SwalDirective
                ],
                entryComponents: [
                    SwalComponent, SwalPartialComponent
                ]
            },] },
];
SweetAlert2Module.ctorParameters = function () { return []; };


//# sourceMappingURL=toverux-ngx-sweetalert2.js.map


/***/ })

});
//# sourceMappingURL=tiket.module.0.chunk.js.map