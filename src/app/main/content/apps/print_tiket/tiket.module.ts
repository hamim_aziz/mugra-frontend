import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { DaftarTiketComponent } from './daftarTiket/daftarTiket.component';
import { DaftarTiketService } from './daftarTiket/daftarTiket.service';
import { DetailTiketComponent } from './detailTiket/detailTiket.component';
import { DetailTiketService } from './detailTiket/detailTiket.service';
import { FuseConfirmDialogComponent } from './../../../../core/components/confirm-dialog/confirm-dialog.component';
import { AgmCoreModule } from '@agm/core';
import { AuthGuard } from './../auth.guard';
import { NgxQRCodeModule } from 'ngx-qrcode2';

const routes: Routes = [
    {
        path     : 'daftarTiket',
        component: DaftarTiketComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DaftarTiketService
        }
    },
    {
        path     : 'detailTiket',
        component: DetailTiketComponent,
        canActivate : [AuthGuard],
        // resolve  : {
        //     data: DetailTiketService
        // }
    }

];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        NgxQRCodeModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    entryComponents: [
        DaftarTiketComponent,
        FuseConfirmDialogComponent
    ],
    declarations: [
        DaftarTiketComponent,
        DetailTiketComponent
    ],
    providers   : [
        DaftarTiketService,
        DetailTiketService
    ]
})
export class TiketModule
{
}
