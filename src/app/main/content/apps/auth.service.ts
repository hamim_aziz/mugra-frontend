import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { environment } from './../../../../environments/environment';

@Injectable()
export class AuthService {

  	constructor(private http: Http) { }

  	setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        options.headers = headers;
    }

    authLogin(login): Promise<any> {
    	let options = new RequestOptions();
        this.setHeader(options);
        return this.http.post(environment.setting.oauth_url, login, options).toPromise();
    }

    authGuard() {
        return true;
    }

}
