import { NgModule } from '@angular/core';
import { SharedModule } from '../../../core/modules/shared.module';
import { RouterModule } from '@angular/router';
import { FuseAngularMaterialModule } from '../components/angular-material/angular-material.module';
import { FuseLoginComponent } from './login/login.component';

import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { SessionService } from './session.service';

const routes = [
  {
    path: 'login',
    component: FuseLoginComponent
  },
  {

    path: 'dashboards/project',
    loadChildren: './dashboards/project/project.module#FuseProjectDashboardModule',
    canActive: [AuthGuard]
  },
  {
    path: 'user',
    loadChildren: './user/user.module#UserModule',
    canActive: [AuthGuard]
  },
  {
    path: 'rute',
    loadChildren: './rute/rute.module#RuteModule',
    canActive: [AuthGuard]
  },
  {
    path: 'titipMotor',
    loadChildren: './titip_motor/titipMotor.module#TitipMotorModule',
    canActive: [AuthGuard]
  },
  {
    path: 'content',
    loadChildren: './content/content.module#ContentModule',
    canActive: [AuthGuard]
  },
  {
    path: 'contact',
    loadChildren: './contact/contact.module#ContactModule',
    canActive: [AuthGuard]
  },
  {
    path: 'kendaraan',
    loadChildren: './kendaraan/kendaraan.module#KendaraanModule',
    canActive: [AuthGuard]
  },
  {
    path: 'tiket',
    loadChildren: './tiket/tiket.module#TiketModule',
    canActive: [AuthGuard]
  },
  {
    path: 'pemudik',
    loadChildren: './pemudik/pemudik.module#PemudikModule',
    canActive: [AuthGuard]
  },
  {
    path: 'print_tiket',
    loadChildren: './print_tiket/tiket.module#TiketModule',
    canActive: [AuthGuard]
  },
  {
    path: 'monitor_rute',
    loadChildren: './monitor_rute/monitor.module#MonitorModule',
    canActive: [AuthGuard]
  },
  {
    path: 'feedback',
    loadChildren: './feedback/feedback.module#FeedbackModule',
    canActive: [AuthGuard]
  },
  {
    path: 'laporan',
    loadChildren: './laporan/laporan.module#LaporanModule',
    canActive: [AuthGuard]
  },
  {
    path: 'sms',
    loadChildren: './sms/sms.module#SmsModule',
    canActive: [AuthGuard]
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    FuseAngularMaterialModule
  ],
  providers: [
    AuthGuard,
    AuthService,
    SessionService
  ],
  declarations: [
    FuseLoginComponent
  ]
})
export class FuseAppsModule {
}
