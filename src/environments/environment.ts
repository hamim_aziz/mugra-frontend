// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// https://apidev.mudikgratis.dishub.jatimprov.go.id/api
// http://localhost:8000/api

export const environment = {
    production: false,
    hmr       : false,
    setting   : {
        base_url  : 'https://ujicobaapi.bijii.co/api',
        oauth_url: 'https://ujicobaapi.bijii.co/api/login'
    }
};
