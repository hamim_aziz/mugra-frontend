import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '../../../../core/services/config.service';
import { fuseAnimations } from '../../../../core/animations';
import { AuthService } from './../auth.service';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { MatSnackBar } from '@angular/material';
import { environment } from './../../../../../environments/environment';
import { SessionService } from './../session.service';

@Component({
    selector   : 'fuse-login',
    templateUrl: './login.component.html',
    styleUrls  : ['./login.component.scss'],
    animations : fuseAnimations
})
export class FuseLoginComponent implements OnInit
{
    loginForm: FormGroup;
    loginFormErrors: any;

    constructor(
        private fuseConfig: FuseConfigService,
        private formBuilder: FormBuilder,
        private auth: AuthService,
        private router: Router,
        private snackBar: MatSnackBar,
        private http: Http,
        private _sessionService: SessionService
    )
    {
        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar   : 'none',
                footer    : 'none'
            }
        });

        this.loginFormErrors = {
            email: {},
            password: {}
        };
    }

    ngOnInit()
    {
        if (this._sessionService.get()) {
            this.http.post(environment.setting.base_url + '/logout', {}, this.setHeader())
                .subscribe(() => {
                    this._sessionService.clear();
                });
        }
        this.loginForm = this.formBuilder.group({
            email   : ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
        
        history.pushState(null, null, location.href);
        window.onpopstate = function () {
            history.go(1);
        };
    }

    setHeader() {
        const options = new RequestOptions();
        const headers = new Headers();
        headers.append('Authorization', `Bearer ${this._sessionService.get().token}`);
        options.headers = headers;
        return options;
    }

    onLoginFormValuesChanged()
    {
        for ( const field in this.loginFormErrors )
        {
            if ( !this.loginFormErrors.hasOwnProperty(field) )
            {
                continue;
            }

            // Clear previous errors
            this.loginFormErrors[field] = {};

            // Get the control
            const control = this.loginForm.get(field);

            if ( control && control.dirty && !control.valid )
            {
                this.loginFormErrors[field] = control.errors;
            }
        }
    }

    onSubmit(){
        const loginData = this.loginForm.getRawValue();
        this.auth.authLogin(loginData)
            .then((response: any) => {
                const data = JSON.parse(response._body);
                const rute_id = [];
                if (data.user.rute_id) {
                    const id = data.user.rute_id.slice(1, (data.user.rute_id.length - 1)).split(',');
                    for (let i = 0; i < id.length; ++i) {
                        rute_id.push(Number(id[i]));
                    }
                }
                const dt = {
                    username: data.user.username,
                    id      : data.user.id,
                    token   : data.token.access_token,
                    role    : data.user.group,
                    email   : data.user.email,
                    rute_id : rute_id
                };
                this._sessionService.set(dt);
                this.router.navigate(['/apps/dashboards/project']);
            })
            .catch((err) => {
                this.snackBar.open(err.json().message, 'Error!', {
                  duration: 3000
                });
            });
    }

}
