import { Component, OnDestroy, OnInit, ViewEncapsulation, Inject, ViewChild } from '@angular/core';
import { DetailTiketService } from './detailTiket.service';
import { fuseAnimations } from '../../../../../core/animations';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Product } from './detailTiket.model';
import { FormBuilder, FormGroup, FormArray, Validators, FormControl, AbstractControl, ValidationErrors, AsyncValidatorFn } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA , MatStepper} from '@angular/material';
import { Location } from '@angular/common';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from './../../../../../../environments/environment';
import { SessionService } from './../../session.service';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import swal from 'sweetalert2';
import { Observable } from 'rxjs/Observable';

@Component({
    selector     : 'fuse-e-commerce-product',
    templateUrl  : './detailTiket.component.html',
    styleUrls    : ['./detailTiket.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class DetailTiketComponent implements OnInit, OnDestroy
{
    @ViewChild('successSwal') private successSwal: SwalComponent;
    @ViewChild('stepper') stepper: MatStepper;

    product = new Product;
    onProductChanged: Subscription;
    pageType: String;
    pageType2 = 'view';
    tiketForm: FormGroup;
    status: number;

    nomor_tiket_mudik: String;
    nomor_tiket_balik: String;
    nomor_tiket_mudik_motor: String;
    nomor_tiket_balik_motor: String;
    pemudik_dewasa: number;
    pemudik_anak: number;

    listKotaMudik: any;
    listKotaBalik: any;
    asalMudik: any;
    asalBalik: any;
    tujuanMudik: any;
    tujuanBalik: any;
    ruteMudik = [];
    ruteBalik = [];
    ruteMudikMotor = [];
    ruteBalikMotor = [];
    maxAnak = [];
    maxMotor = [];

    informasiMudikForm1: FormGroup;
    informasiMudikForm2: FormGroup;
    informasiPemudikForm: FormGroup;

    isLinear = true;
    isMudik = false;
    isBalik = false;

    summaryRute: any;
    summaryMudik: any;
    summaryMotor: any;
    summaryInfo: any;

    summaryRuteMudik: any;
    summaryRuteBalik: any;
    date: any;

    statusBookingAll: boolean;

    selectedRuteMudik = [];
    selectedRuteBalik = [];

    statusDaftarMudik = false;
    statusDaftarBalik = false;
    statusDaftarMudikMotor = false;
    statusDaftarBalikMotor = false;

    url_image = environment.setting.base_url + '/';

    kursi_available = [];

    list_tanggal = [{id: '01'}, {id: '02'}, {id: '03'}, {id: '04'}, {id: '05'},
                    {id: '06'}, {id: '07'}, {id: '08'}, {id: '09'}, {id: '10'},
                    {id: '11'}, {id: '12'}, {id: '13'}, {id: '14'}, {id: '15'},
                    {id: '16'}, {id: '17'}, {id: '18'}, {id: '19'}, {id: '20'},
                    {id: '21'}, {id: '22'}, {id: '23'}, {id: '24'}, {id: '25'},
                    {id: '26'}, {id: '27'}, {id: '28'}, {id: '29'}, {id: '30'}, {id: '31'}];
    list_bulan = [{id: '01', nama: 'Januari'}, {id: '02', nama: 'Februari'},
                  {id: '03', nama: 'Maret'}, {id: '04', nama: 'April'},
                  {id: '05', nama: 'Mei'}, {id: '06', nama: 'Juni'},
                  {id: '07', nama: 'Juli'}, {id: '08', nama: 'Agustus'},
                  {id: '09', nama: 'September'}, {id: '10', nama: 'Oktober'},
                  {id: '11', nama: 'November'}, {id: '12', nama: 'Desember'}];
    list_tahun = [];

    constructor(
        private productService: DetailTiketService,
        private formBuilder: FormBuilder,
        public snackBar: MatSnackBar,
        private location: Location,
        private http: Http,
        private router: Router,
        private route: ActivatedRoute,
        private _location: Location,
        private dialog: MatDialog,
        private _sessionService: SessionService
    )
    {

    }

    ngOnInit(): void
    {
        for (let i = 2018; i > 1930; --i) {
            this.list_tahun.push(i);
        }

        const timeStamp = new Date();
        this.date = timeStamp.getDate() + ' - ' + (timeStamp.getMonth() + 1) + ' - ' + timeStamp.getFullYear();
        this.kursi_available = [];
        // Subscribe to update product on changes
        this.onProductChanged = this.productService.onProductChanged
            .subscribe(product => {
                if (product) {
                    this.product = new Product(product);
                    this.status = this.product.status;
                    this.pageType = 'edit';
                    this.tiketForm = this.createTiketFormEdit();
                    for (let i = 1; i < this.product.pemudik_anak + this.product.pemudik_dewasa; i++) {
                        this.addPemudikEdit(i);
                    }
                    this.kursi_available = this.productService.list_kursi;
                } else {
                    this.pageType = 'new';
                    this.product = new Product();
                    this.tiketForm = this.createTiketFormNew();
                    this.informasiMudikForm1 = this.createInformasiMudikForm1();
                    this.informasiMudikForm2 = this.createInformasiMudikForm2();
                }
            });
        this.asalBalik = this.productService.asalBalik;
        this.asalMudik = this.productService.asalMudik;
        this.listKotaBalik = this.productService.listKotaBalik;
        this.listKotaMudik = this.productService.listKotaMudik;
    }

    ngOnDestroy(): void
    {
        this.onProductChanged.unsubscribe();
    }

    setHeader(options: RequestOptions) {
        const headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this._sessionService.get().token);
        headers.append('Content-Type', 'application/json');
        options.headers = headers;
    }

    createTiketFormEdit(): FormGroup {
        return this.formBuilder.group({
            alamat        : [this.product.alamat],
            telepon       : [this.product.telepon],
            kota_tujuan   : [this.product.kota_tujuan],
            pemudiks       : this.formBuilder.array([
                this.formBuilder.group({
                    'nama'            : new FormControl(this.product.pemudik[0].nama, [Validators.required, Validators.minLength(2)]),
                    'nik'             : new FormControl(this.product.pemudik[0].nik, [Validators.minLength(16), Validators.maxLength(16), Validators.required]),
                    'sim'             : new FormControl(this.product.pemudik[0].sim),
                    'usia'            : new FormControl(this.product.pemudik[0].usia, [Validators.required]),
                    'jenis'           : new FormControl(this.product.pemudik[0].jenis, [Validators.required]),
                    'stnk'            : new FormControl(this.product.pemudik[0].stnk),
                    'gender'          : new FormControl(this.product.pemudik[0].gender, [Validators.required]),
                    'tanggal_lahir'   : new FormControl(this.product.pemudik[0].tanggal_lahir),
                    'tempat_lahir'    : new FormControl(this.product.pemudik[0].tempat_lahir, [Validators.required]),
                    'pekerjaan'       : new FormControl(this.product.pemudik[0].pekerjaan, [Validators.required]),
                    'pindah_kursi'    : new FormControl(this.product.pemudik[0].kursi_id)
                })
            ])
        });
    }

    createTiketFormNew(): FormGroup{
        return this.formBuilder.group({
            nomor_tiket    : new FormControl('', [Validators.required]),
            pemudik_dewasa : new FormControl('', [Validators.required]),
            pemudik_anak   : new FormControl('', [Validators.required]),
            banyak_motor   : new FormControl(0, [Validators.required]),
            pemudik       : this.formBuilder.array([
                this.formBuilder.group({
                    'nama'            : new FormControl('', [Validators.required, Validators.minLength(2)]),
                    'nik'             : new FormControl('', [Validators.minLength(16), Validators.maxLength(16), Validators.required]),
                    'sim'             : new FormControl(''),
                    'usia'            : new FormControl(''),
                    'jenis'           : new FormControl(1, [Validators.required]),
                    'stnk'            : new FormControl(''),
                    'gender'          : new FormControl('', [Validators.required]),
                    'tanggal_lahir'   : new FormControl(''),
                    'tempat_lahir'    : new FormControl('', [Validators.required]),
                    'pekerjaan'       : new FormControl('', [Validators.required]),
                    'tanggal'         : new FormControl(''),
                    'bulan'           : new FormControl(''),
                    'tahun'           : new FormControl('')
                })
            ])
        });
    }

    get pemudikForm(){
        return this.tiketForm.get('pemudik') as FormArray;
    }

    get pemudikFormEdit(){
        return this.tiketForm.get('pemudiks') as FormArray;
    }

    addPemudikEdit(id){
        this.pemudikFormEdit.push(
            this.formBuilder.group({
                'nama'            : new FormControl(this.product.pemudik[id].nama, [Validators.required, Validators.minLength(2)]),
                'nik'             : new FormControl(this.product.pemudik[id].nik, this.product.pemudik[id].jenis === 1 ? [Validators.minLength(16), Validators.maxLength(16), Validators.required] : []),
                'sim'             : new FormControl(this.product.pemudik[id].sim),
                'usia'            : new FormControl(this.product.pemudik[id].usia, [Validators.required]),
                'jenis'           : new FormControl(this.product.pemudik[id].jenis, [Validators.required]),
                'stnk'            : new FormControl(this.product.pemudik[id].stnk),
                'gender'          : new FormControl(this.product.pemudik[id].gender, [Validators.required]),
                'tanggal_lahir'   : new FormControl(this.product.pemudik[id].tanggal_lahir),
                'tempat_lahir'    : new FormControl(this.product.pemudik[id].tempat_lahir, [Validators.required]),
                'pekerjaan'       : new FormControl(this.product.pemudik[id].pekerjaan, [Validators.required]),
                'pindah_kursi'    : new FormControl(this.product.pemudik[id].kursi_id)
            })
        );
    }

    addPemudikNew(jenis){
        this.pemudikForm.push(
            this.formBuilder.group({
                'nama'            : new FormControl('', [Validators.required, Validators.minLength(2)]),
                'nik'             : new FormControl('', jenis === 1 ? [Validators.minLength(16), Validators.maxLength(16), Validators.required] : []),
                'sim'             : new FormControl(''),
                'usia'            : new FormControl(''),
                'jenis'           : new FormControl(jenis, [Validators.required]),
                'stnk'            : new FormControl(''),
                'gender'          : new FormControl('', [Validators.required]),
                'tanggal_lahir'   : new FormControl(''),
                'tempat_lahir'    : new FormControl('', [Validators.required]),
                'pekerjaan'       : new FormControl('', jenis === 1 ? [Validators.required] : []),
                'tanggal'         : new FormControl(''),
                'bulan'           : new FormControl(''),
                'tahun'           : new FormControl('')
            })
        );
    }

    createInformasiMudikForm1(): FormGroup{
        return this.formBuilder.group({
            'asal_mudik'          : new FormControl({value: '', disabled: true}),
            'tujuan_mudik'        : new FormControl({value: '', disabled: true}),
            'rute_mudik'          : new FormControl({value: '', disabled: true}),
            'rute_mudik_motor'    : new FormControl({value: '', disabled: true}),
            'asal_balik'          : new FormControl({value: '', disabled: true}),
            'tujuan_balik'        : new FormControl({value: '', disabled: true}),
            'rute_balik'          : new FormControl({value: '', disabled: true}),
            'rute_balik_motor'    : new FormControl({value: '', disabled: true})
        });
    }

    createInformasiMudikForm2(): FormGroup{
        return this.formBuilder.group({
            'telepon'         : new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{9,}$/)]),
            'alamat'          : new FormControl('', [Validators.required, Validators.minLength(8)]),
            'pemudik_dewasa'  : new FormControl(1, [Validators.required, Validators.max(10), Validators.min(1)]),
            'pemudik_anak'    : new FormControl(0, [Validators.required, Validators.max(10), Validators.min(0)]),
            'banyak_motor'    : new FormControl({value: 0, disabled: true}, [Validators.required, Validators.max(10), Validators.min(0)])
        });
    }

    updateTujuanMudik(){
        const kotaAsal = this.informasiMudikForm1.controls.asal_mudik.value;
        this.tujuanMudik = [];
        for (let i = 0; i < this.listKotaMudik.length; ++i) {
            if (this.listKotaMudik[i].asal === kotaAsal) {
                for (let j = 0; j < this.listKotaMudik[i].tujuan.length; ++j) {
                    if (!this.tujuanMudik.includes(this.listKotaMudik[i].tujuan[j])) {
                        this.tujuanMudik.push(this.listKotaMudik[i].tujuan[j]);
                    }
                }
            }
        }
        this.informasiMudikForm1.controls.tujuan_mudik.enable();
    }

    updateTujuanBalik(){
        const kotaAsal = this.informasiMudikForm1.controls.asal_balik.value;
        this.tujuanBalik = [];
        for (let i = 0; i < this.listKotaBalik.length; ++i) {
            if (this.listKotaBalik[i].asal === kotaAsal) {
                for (let j = 0; j < this.listKotaBalik[i].tujuan.length; ++j) {
                    if (!this.tujuanBalik.includes(this.listKotaBalik[i].tujuan[j])) {
                        this.tujuanBalik.push(this.listKotaBalik[i].tujuan[j]);
                    }
                }
            }
        }
        this.informasiMudikForm1.controls.tujuan_balik.enable();
    }

    updateRuteMudik(){
        const asal = this.informasiMudikForm1.controls.asal_mudik.value;
        const tujuan = this.informasiMudikForm1.controls.tujuan_mudik.value;
        this.informasiMudikForm1.controls.rute_mudik.setValue('');
        this.ruteMudik = [];
        this.selectedRuteMudik = [];
        this.productService.getRuteByAsalTujuan(asal, tujuan)
            .map((res: Response) => res.json())
            .subscribe((response: any) => {
                for (let i = 0; i < response.data.length; ++i) {
                    if (response.data[i].kendaraans.length > 0) {
                        this.ruteMudik.push(response.data[i]);
                    }
                    if (response.data[i].is_mudik_motor === 1) {
                        this.ruteMudikMotor.push(response.data[i]);
                    }
                }
                if (this.ruteMudik.length > 0) {
                    this.informasiMudikForm1.controls.rute_mudik.enable();
                }
            });
    }

    updateRuteBalik(){
        const asal = this.informasiMudikForm1.controls.asal_balik.value;
        const tujuan = this.informasiMudikForm1.controls.tujuan_balik.value;
        this.informasiMudikForm1.controls.rute_balik.setValue('');
        this.ruteBalik = [];
        this.selectedRuteBalik = [];
        this.productService.getRuteByAsalTujuan(asal, tujuan)
            .map((res: Response) => res.json())
            .subscribe((response: any) => {
                for (let i = 0; i < response.data.length; ++i) {
                    if (response.data[i].kendaraans.length > 0) {
                        this.ruteBalik.push(response.data[i]);
                    }
                    if (response.data[i].is_mudik_motor === 1) {
                        this.ruteBalikMotor.push(response.data[i]);
                    }
                }
                if (this.ruteBalik.length > 0) {
                    this.informasiMudikForm1.controls.rute_balik.enable();
                }
            });
    }

    selectRuteMudik(){
        if (this.informasiMudikForm1.controls.rute_mudik.valid) {
            this.informasiMudikForm1.controls.rute_mudik_motor.enable();
            this.selectedRuteMudik = this.ruteMudik.filter(rute => {
                return rute.id === this.informasiMudikForm1.controls.rute_mudik.value;
            });
        }
    }

    selectRuteBalik(){
        if (this.informasiMudikForm1.controls.rute_balik.valid) {
            this.informasiMudikForm1.controls.rute_balik_motor.enable();
            this.selectedRuteBalik = this.ruteBalik.filter(rute => {
                return rute.id === this.informasiMudikForm1.controls.rute_balik.value;
            });
        }
    }

    validateAnakdanMotor(){
        if (this.informasiMudikForm2.controls.pemudik_anak.value > this.informasiMudikForm2.controls.pemudik_dewasa.value) {
            this.informasiMudikForm2.controls.pemudik_anak.setValue(0);
        }
        if (this.informasiMudikForm2.controls.banyak_motor.value > this.informasiMudikForm2.controls.pemudik_dewasa.value) {
            this.informasiMudikForm2.controls.banyak_motor.setValue(0);
        }
        this.maxAnak = [];
        this.maxMotor = [];
        for (let i = 1; i < this.informasiMudikForm2.controls.pemudik_dewasa.value; ++i) {
            this.maxAnak.push(i + 1);
        }

        if (this.informasiMudikForm1.controls.rute_mudik_motor.value) {
            if (this.ruteMudik[0].kouta_motor > this.informasiMudikForm2.controls.pemudik_dewasa.value) {
                for (let i = 1; i < this.informasiMudikForm2.controls.pemudik_dewasa.value; ++i) {
                    this.maxMotor.push(i + 1);
                }
            } else {
                for (let i = 1; i < this.ruteMudik[0].kouta_motor; ++i) {
                    this.maxMotor.push(i + 1);
                }
            }
            this.informasiMudikForm2.controls.banyak_motor.enable();
        }
    }

    booking(){
        let statusBookingAll = true;
        let errorMessage: string;
        const form1 = this.informasiMudikForm1.getRawValue();
        const form2 = this.informasiMudikForm2.getRawValue();
        const promiseList = [];
        const bookingForm = this.formBuilder.group({
            'rute_id'        : new FormControl(form1.rute_mudik),
            'jenis'          : new FormControl(1),
            'alamat'         : new FormControl(form2.alamat),
            'telepon'        : new FormControl(form2.telepon, [Validators.required, Validators.pattern(/^[0-9]{9,}$/)]),
            'kota_tujuan'    : new FormControl(form1.tujuan_mudik),
            'pemudik_dewasa' : new FormControl(form2.pemudik_dewasa),
            'pemudik_anak'   : new FormControl(form2.pemudik_anak),
            'banyak_motor'   : new FormControl(form2.banyak_motor),
        });
        if (form1.rute_mudik) {
            promiseList.push(this.productService.booking(bookingForm.getRawValue()));
        } else {
            promiseList.push(Promise.resolve({
                status : true
            }));
        }
        if (form1.rute_balik) {
            bookingForm.controls.rute_id.setValue(form1.rute_balik);
            bookingForm.controls.kota_tujuan.setValue(form1.tujuan_balik);
            bookingForm.controls.jenis.setValue(2);
            promiseList.push(this.productService.booking(bookingForm.getRawValue()));
        } else {
            promiseList.push(Promise.resolve({
                status : true
            }));
        }
        if (form2.banyak_motor > 0 && form1.rute_mudik_motor) {
            bookingForm.controls.rute_id.setValue(form1.rute_mudik);
            bookingForm.controls.banyak_motor.setValue(form2.banyak_motor);
            promiseList.push(this.productService.bookingMotor(bookingForm.getRawValue()));
        } else {
            promiseList.push(Promise.resolve({
                status : true
            }));
        }
        if (form2.banyak_motor > 0 && form1.rute_balik_motor) {
            bookingForm.controls.rute_id.setValue(form1.rute_balik);
            bookingForm.controls.banyak_motor.setValue(form2.banyak_motor);
            promiseList.push(this.productService.bookingMotor(bookingForm.getRawValue()));
        } else {
            promiseList.push(Promise.resolve({
                status : true
            }));
        }

        Promise.all(promiseList)
               .then((responses: any) => {
                    if (responses[0].status) {
                        if (responses[0].data) {
                            this.nomor_tiket_mudik = responses[0].data.nomor_tiket;
                        }
                    } else {
                        statusBookingAll = false;
                        errorMessage = responses[0].message;
                    }
                    if (responses[1].status) {
                        if (responses[1].data) {
                            this.nomor_tiket_balik = responses[1].data.nomor_tiket;
                        }
                    } else {
                        statusBookingAll = false;
                        errorMessage = responses[1].message;
                    }
                    if (responses[2].status) {
                        if (responses[2].data) {
                            this.nomor_tiket_mudik_motor = responses[2].data.nomor_tiket;
                        }
                    } else {
                        statusBookingAll = false;
                        errorMessage = responses[2].message;
                    }
                    if (responses[3].status) {
                        if (responses[3].data) {
                            this.nomor_tiket_balik_motor = responses[3].data.nomor_tiket;
                        }
                    } else {
                        statusBookingAll = false;
                        errorMessage = responses[3].message;
                    }
                    if (!statusBookingAll) {
                        this.tiketBatal();
                        swal({
                            title: 'Error',
                            text: errorMessage,
                            type: 'error'
                        });
                    } else {
                        for (let i = 0; i < (form2.pemudik_dewasa - 1); ++i) {
                            this.addPemudikNew(1);
                        }
                        for (let i = 0; i < form2.pemudik_anak; ++i) {
                            this.addPemudikNew(2);
                        }
                        this.summaryRute = this.informasiMudikForm1.getRawValue();
                        this.summaryInfo = this.informasiMudikForm2.getRawValue();

                        if (this.summaryRute.rute_mudik) {
                            for (let i = 0; i < this.ruteMudik.length; ++i) {
                                if (this.summaryRute.rute_mudik === this.ruteMudik[i].id) {
                                    this.summaryRuteMudik = this.ruteMudik[i];
                                }
                            }
                        }
                        if (this.summaryRute.rute_balik) {
                            for (let i = 0; i < this.ruteBalik.length; ++i) {
                                if (this.summaryRute.rute_balik === this.ruteBalik[i].id) {
                                    this.summaryRuteBalik = this.ruteBalik[i];
                                }
                            }
                        }
                        if (this.informasiMudikForm2.value['banyak_motor']) {
                            this.tiketForm.controls.banyak_motor.setValue(this.informasiMudikForm2.value['banyak_motor']);
                        } else {
                            this.tiketForm.controls.banyak_motor.setValue(0);
                        }

                        this.stepper.next();
                    }
               })
               .catch((error) => {
                   console.log(error);
               });
    }

    selectRuteMudikMotor(){
        if (this.informasiMudikForm1.controls.rute_mudik_motor.value || this.informasiMudikForm1.controls.rute_balik_motor.value) {
            this.informasiMudikForm2.controls.banyak_motor.enable();
        } else {
            this.informasiMudikForm2.controls.banyak_motor.disable();
        }
    }

    selectRuteBalikMotor(){
        if (this.informasiMudikForm1.controls.rute_mudik_motor.value || this.informasiMudikForm1.controls.rute_balik_motor.value) {
            this.informasiMudikForm2.controls.banyak_motor.enable();
        } else {
            this.informasiMudikForm2.controls.banyak_motor.disable();
        }
    }

    mudik(){
        if (this.isMudik) {
            this.isMudik = false;
            this.informasiMudikForm1.controls.asal_mudik.disable();
            this.informasiMudikForm1.controls.tujuan_mudik.disable();
            this.informasiMudikForm1.controls.rute_mudik.disable();
            this.informasiMudikForm1.controls.asal_mudik.setValue('');
            this.informasiMudikForm1.controls.tujuan_mudik.setValue('');
            this.informasiMudikForm1.controls.rute_mudik.setValue('');
            this.informasiMudikForm1.controls.asal_mudik.setValidators([]);
            this.informasiMudikForm1.controls.tujuan_mudik.setValidators([]);
            this.informasiMudikForm1.controls.rute_mudik.setValidators([]);
        } else {
            this.isMudik = true;
            this.informasiMudikForm1.controls.asal_mudik.setValidators([Validators.required]);
            this.informasiMudikForm1.controls.tujuan_mudik.setValidators([Validators.required]);
            this.informasiMudikForm1.controls.rute_mudik.setValidators([Validators.required]);
            this.informasiMudikForm1.controls.asal_mudik.enable();
        }
    }

    balik(){
        if (this.isBalik) {
            this.isBalik = false;
            this.informasiMudikForm1.controls.asal_balik.disable();
            this.informasiMudikForm1.controls.tujuan_balik.disable();
            this.informasiMudikForm1.controls.rute_balik.disable();
            this.informasiMudikForm1.controls.asal_balik.setValue('');
            this.informasiMudikForm1.controls.tujuan_balik.setValue('');
            this.informasiMudikForm1.controls.rute_balik.setValue('');
            this.informasiMudikForm1.controls.asal_balik.setValidators([]);
            this.informasiMudikForm1.controls.tujuan_balik.setValidators([]);
            this.informasiMudikForm1.controls.rute_balik.setValidators([]);
        } else {
            this.isBalik = true;
            this.informasiMudikForm1.controls.asal_balik.setValidators([Validators.required]);
            this.informasiMudikForm1.controls.tujuan_balik.setValidators([Validators.required]);
            this.informasiMudikForm1.controls.rute_balik.setValidators([Validators.required]);
            this.informasiMudikForm1.controls.asal_balik.enable();
        }
    }

    summary(){
        if (this.nomor_tiket_mudik) {
            this.tiketForm.controls.nomor_tiket.setValue(this.nomor_tiket_mudik);
        }
        if (this.nomor_tiket_balik) {
            this.tiketForm.controls.nomor_tiket.setValue(this.nomor_tiket_balik);
        }
        this.tiketForm.controls.pemudik_dewasa.setValue(this.informasiMudikForm2.value['pemudik_dewasa']);
        this.tiketForm.controls.pemudik_anak.setValue(this.informasiMudikForm2.value['pemudik_anak']);
        if (this.isMudik || this.isBalik) {
            for (let i = 0; i < this.informasiMudikForm2.value['pemudik_dewasa'] + this.informasiMudikForm2.value['pemudik_anak']; ++i) {
                const tanggal = this.tiketForm.controls['pemudik']['controls'][i]['controls']['tanggal']['value'];
                const bulan = this.tiketForm.controls['pemudik']['controls'][i]['controls']['bulan']['value'];
                const tahun = this.tiketForm.controls['pemudik']['controls'][i]['controls']['tahun']['value'];
                this.tiketForm.controls['pemudik']['controls'][i]['controls']['tanggal_lahir']['value'] = tahun + '/' + bulan + '/' + tanggal;
            }
        }

        this.summaryMudik = this.tiketForm.getRawValue();
        this.stepper.next();
    }

    bulan(date){
        if (date.substring(5, 7) === '01') {
            return 'Januari';
        } else if (date.substring(5, 7) === '02') {
            return 'Februari';
        } else if (date.substring(5, 7) === '03') {
            return 'Maret';
        } else if (date.substring(5, 7) === '04') {
            return 'April';
        } else if (date.substring(5, 7) === '05') {
            return 'Mei';
        } else if (date.substring(5, 7) === '06') {
            return 'Juni';
        } else if (date.substring(5, 7) === '07') {
            return 'Juli';
        } else if (date.substring(5, 7) === '08') {
            return 'Agustus';
        } else if (date.substring(5, 7) === '09') {
            return 'September';
        } else if (date.substring(5, 7) === '10') {
            return 'Oktober';
        } else if (date.substring(5, 7) === '11') {
            return 'November';
        } else if (date.substring(5, 7) === '12') {
            return 'Desember';
        }
    }

    generateNIK(id){
        const ctrl = (<FormArray>this.tiketForm.controls['pemudik']).at(id);
        console.log(this.tiketForm.controls.pemudik['controls'][id]['controls']['nik']['valid']);
        console.log('2');
        let tanggal = Number(String(this.tiketForm.controls.pemudik['controls'][id]['controls']['nik']['value']).substring(6, 8));
        const bulan = Number(String(this.tiketForm.controls.pemudik['controls'][id]['controls']['nik']['value']).substring(8, 10));
        const tahun = Number(String(this.tiketForm.controls.pemudik['controls'][id]['controls']['nik']['value']).substring(10, 12));
        if (tanggal > 40) {
            tanggal = tanggal - 40;
            ctrl['controls'].gender.setValue(2);
            if (tanggal < 10) {
                ctrl['controls'].tanggal.setValue('0' + String(tanggal));
            } else {
                ctrl['controls'].tanggal.setValue(String(tanggal));
            }
        } else {
            ctrl['controls'].gender.setValue(1);
            if (tanggal < 10) {
                ctrl['controls'].tanggal.setValue('0' + String(tanggal));
            } else {
                ctrl['controls'].tanggal.setValue(String(tanggal));
            }
        }
        if (bulan < 10) {
            ctrl['controls'].bulan.setValue('0' + String(bulan));
        } else {
            ctrl['controls'].bulan.setValue(String(bulan));
        }
        if (tahun > 18) {
            ctrl['controls'].tahun.setValue(1900 + tahun);
        } else {
            ctrl['controls'].tahun.setValue(2000 + tahun);
        }
        // tslint:disable-next-line: max-line-length
        this.tiketForm.controls.pemudik['controls'][id]['controls']['nik'].setAsyncValidators(ValidateEmailNotTaken.createValidator(this.productService, this.nomor_tiket_mudik, this.nomor_tiket_balik));
        this.tiketForm.controls.pemudik['controls'][id]['controls']['nik'].setValidators([Validators.minLength(16), Validators.maxLength(16), Validators.required]);
        this.tiketForm.controls.pemudik['controls'][id]['controls']['nik'].updateValueAndValidity();
    }

    checkNIK(i){
        if (this.tiketForm.controls.pemudik['controls'][i]['controls'].nik.valid) {
            if (this.nomor_tiket_mudik) {
                this.productService.checkNIK(this.nomor_tiket_mudik, this.tiketForm.controls.pemudik['controls'][i]['controls'].nik.value)
                    .then((result) => {
                        return result.status;
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            } else if (this.nomor_tiket_balik) {
                this.productService.checkNIK(this.nomor_tiket_balik, this.tiketForm.controls.pemudik['controls'][i]['controls'].nik.value)
                    .then((result) => {
                        return result.status;
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            }
        }
    }

    daftarMudik(){
        const promiseList = [];
        let failMessage: string;
        if (this.nomor_tiket_mudik && !this.statusDaftarMudik) {
            this.tiketForm.controls.nomor_tiket.setValue(this.nomor_tiket_mudik);
            promiseList.push(this.productService.addPemudik(this.tiketForm.getRawValue()));
        } else {
            promiseList.push(Promise.resolve({
                status : true
            }));
        }
        if (this.nomor_tiket_balik && !this.statusDaftarBalik) {
            this.tiketForm.controls.nomor_tiket.setValue(this.nomor_tiket_balik);
            promiseList.push(this.productService.addPemudik(this.tiketForm.getRawValue()));
        } else {
            promiseList.push(Promise.resolve({
                status : true
            }));
        }
        if (this.nomor_tiket_mudik_motor && !this.statusDaftarMudikMotor) {
            this.tiketForm.controls.nomor_tiket.setValue(this.nomor_tiket_mudik_motor);
            const tiket = this.tiketForm.getRawValue();
            const obj = {
                nomor_tiket : tiket.nomor_tiket,
                banyak_motor: tiket.banyak_motor,
                pemudik: []
            };
            for (let i = 0; i < tiket.banyak_motor; ++i) {
                obj.pemudik.push(tiket.pemudik[i]);
            }
            promiseList.push(this.productService.addPemudikMotor(obj));
        } else {
            promiseList.push(Promise.resolve({
                status : true
            }));
        }
        if (this.nomor_tiket_balik_motor && !this.statusDaftarBalikMotor) {
            this.tiketForm.controls.nomor_tiket.setValue(this.nomor_tiket_balik_motor);
            const tiket = this.tiketForm.getRawValue();
            const obj = {
                nomor_tiket : tiket.nomor_tiket,
                banyak_motor: tiket.banyak_motor,
                pemudik: []
            };
            for (let i = 0; i < tiket.banyak_motor; ++i) {
                obj.pemudik.push(tiket.pemudik[i]);
            }
            promiseList.push(this.productService.addPemudikMotor(obj));
        } else {
            promiseList.push(Promise.resolve({
                status : true
            }));
        }
        Promise.all(promiseList)
               .then((responses: any) => {
                   if (responses[0].status) {
                       this.statusDaftarMudik = true;
                   } else {
                       this.statusDaftarMudik = false;
                       if (responses[0].message) {
                           failMessage = responses[0].message;
                       }
                   }
                   if (responses[1].status) {
                       this.statusDaftarBalik = true;
                   } else {
                       this.statusDaftarBalik = false;
                       if (responses[1].message) {
                           failMessage = responses[1].message;
                       }
                   }
                   if (responses[2].status) {
                       this.statusDaftarMudikMotor = true;
                   } else {
                       this.statusDaftarMudikMotor = false;
                       if (responses[2].message) {
                           failMessage = responses[2].message;
                       }
                   }
                   if (responses[3].status) {
                       this.statusDaftarBalikMotor = true;
                   } else {
                       this.statusDaftarBalikMotor = false;
                       if (responses[3].message) {
                           failMessage = responses[3].message;
                       }
                   }
                   if (this.statusDaftarMudik && this.statusDaftarBalik && this.statusDaftarMudikMotor && this.statusDaftarBalikMotor) {
                        if (this.nomor_tiket_mudik && this.statusDaftarMudik) {
                            this.print(this.nomor_tiket_mudik);
                        }
                        if (this.nomor_tiket_balik && this.statusDaftarBalik) {
                            this.print(this.nomor_tiket_balik);
                        }
                        if (this.nomor_tiket_mudik_motor && this.statusDaftarMudikMotor) {
                            this.print(this.nomor_tiket_mudik_motor);
                        }
                        if (this.nomor_tiket_balik_motor && this.statusDaftarBalikMotor) {
                            this.print(this.nomor_tiket_balik_motor);
                        }
                        this.router.navigate(['apps/tiket/daftarTiket']);
                    } else {
                        swal({
                            title: 'Gagal memasukkan data',
                            text: failMessage,
                            type: 'error'
                        });
                    }
               })
               .catch((error) => {
                   console.log(error);
               });
    }

    approve(){
        this.productService.approve(this.status)
            .then((response: any) => {
                if (response.status === true) {
                    this.snackBar.open(response.message, 'Success', {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                    this.status = 1;
                } else {
                    if (response.message === 'decline berhasil tapi sms tidak terkirim'){
                        this.snackBar.open(response.message, 'Success', {
                            verticalPosition: 'top',
                            duration        : 5000
                        });
                        this.status = 1;
                    } else {
                        this.snackBar.open(response.message, 'Error', {
                            verticalPosition: 'top',
                            duration        : 5000
                        });
                    }
                }
            })
            .catch((error) => {
                const eror = JSON.parse(error._body);
                swal({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
            });
    }

    decline(){
        this.productService.decline(this.status)
            .then((response: any) => {
                if (response.status === true) {
                    this.snackBar.open(response.message, 'Success', {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                    this.status = 5;
                } else {
                    if (response.message === 'decline berhasil tapi sms tidak terkirim'){
                        this.snackBar.open(response.message, 'Success', {
                            verticalPosition: 'top',
                            duration        : 5000
                        });
                        this.status = 5;
                    } else {
                        this.snackBar.open(response.message, 'Error', {
                            verticalPosition: 'top',
                            duration        : 5000
                        });
                    }
                }
            })
            .catch((error) => {
                const eror = JSON.parse(error._body);
                swal({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
            });
    }

    simpan(){
        this.productService.save(this.tiketForm.getRawValue(), this.product.id)
            .then((response: any) => {
                this.snackBar.open(response.message, response.status, {
                    verticalPosition: 'top',
                    duration        : 5000
                });
            })
            .catch((error) => {
                const eror = JSON.parse(error._body);
                swal({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
            });
    }

    simpanPemudik(id){
        const promise_list = [this.productService.savePemudik(this.tiketForm.getRawValue().pemudiks[id], this.product.pemudik[id].id)];
        const data = this.tiketForm.getRawValue().pemudiks[id];
        if (data.pindah_kursi) {
            const obj = {
                'pemudik_id'         : this.product.pemudik[id].id,
                'kendaraan_baru_id'  : this.product.kendaraan.id,
                'kursi_baru_id'      : data.pindah_kursi
            };
            promise_list.push(this.productService.pindahKursi(obj));
        }
        Promise.all(promise_list)
            .then((result) => {
                if (result[1].status) {
                    swal({
                        title: 'Sukses !',
                        text: result[1].message,
                        type: 'success'
                    });
                } else {
                    swal({
                        title: 'Error !',
                        text: result[1].message,
                        type: 'error'
                    });
                }
            }).catch((error) => {
                console.log(error);
            });
        // this.productService.savePemudik(this.tiketForm.getRawValue().pemudiks[id], this.product.pemudik[id].id)
        //     .then((response: any) => {
        //         this.snackBar.open(response.message, response.status, {
        //             verticalPosition: 'top',
        //             duration        : 5000
        //         })
        //     })
        //     .catch((error) => {
        //         let eror = JSON.parse(error._body);
        //         swal({
        //             title: 'Error !',
        //             text: eror.message,
        //             type: 'error'
        //         })
        //     })
    }

    hapusPemudik(id){
        this.productService.hapusPemudik(this.product.pemudik[id].id)
            .then((response: any) => {
                this.snackBar.open(response.message, response.status, {
                    verticalPosition: 'top',
                    duration        : 5000
                });
            })
            .catch((error) => {
                const eror = JSON.parse(error._body);
                swal({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
            });
    }

    hapusTiket(){
        if (this.pageType === 'edit') {
            this.productService.hapusTiket()
                .then((response: any) => {
                    this.snackBar.open(response.message, response.status, {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                    this.back();
                })
                .catch((error) => {
                    const eror = JSON.parse(error._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    });
                });
        }
    }

    tiketBatal(){
        this.tiketForm = this.createTiketFormNew();
        this.stepper.selectedIndex = 0;
        if (this.nomor_tiket_mudik) {
            this.productService.batalTiket(this.nomor_tiket_mudik)
                .then((response: any) => {
                    this.snackBar.open(response.message, response.status, {
                        verticalPosition: 'top',
                        duration        : 1000
                    });
                })
                .catch((error) => {
                    const eror = JSON.parse(error._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    });
                });
        }
        if (this.nomor_tiket_balik) {
            this.productService.batalTiket(this.nomor_tiket_balik)
                .then((response: any) => {
                    this.snackBar.open(response.message, response.status, {
                        verticalPosition: 'top',
                        duration        : 1000
                    });
                })
                .catch((error) => {
                    const eror = JSON.parse(error._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    });
                });
        }
        if (this.nomor_tiket_mudik_motor) {
            this.productService.batalTiket(this.nomor_tiket_mudik_motor)
                .then((response: any) => {
                    this.snackBar.open(response.message, response.status, {
                        verticalPosition: 'top',
                        duration        : 1000
                    });
                })
                .catch((error) => {
                    const eror = JSON.parse(error._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    });
                });
        }
        if (this.nomor_tiket_balik_motor) {
            this.productService.batalTiket(this.nomor_tiket_balik_motor)
                .then((response: any) => {
                    this.snackBar.open(response.message, response.status, {
                        verticalPosition: 'top',
                        duration        : 1000
                    });
                })
                .catch((error) => {
                    const eror = JSON.parse(error._body);
                    swal({
                        title: 'Error !',
                        text: eror.message,
                        type: 'error'
                    });
                });
        }
    }

    print(tiket) {
        const fileURL = environment.setting.base_url + '/tiket/' + tiket + '/download';
        window.open(fileURL, '_blank');
    }

    back(){
      this.router.navigate(['apps/tiket/daftarTiket']);
        // this._location.back();
    }

    kendaraanSwal(id){
        this.productService.getKendaraan(id)
            .then((response) => {
                const kursis = response.data.kursis;
                swal({
                    title: 'Detail Kendaraan',
                    type: 'info',
                    html:
                        '<table style="width: 100%">' +
                            '<tr>' +
// tslint:disable-next-line: max-line-length
                                '<td style="width: 16%">Pintu Depan</td><td style="width: 16%"></td><td style="width: 16%"></td><td style="width: 16%"></td><td style="width: 16%"></td><td style="width: 16%">Supir</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td style="width: 16%;' + (kursis[0].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '1' + '</td>' +
                                '<td style="width: 16%;' + (kursis[1].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '2' + '</td>' +
                                '<td style="width: 16%"></td> ' +
                                '<td style="width: 16%;' + (kursis[2].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '3' + '</td>' +
                                '<td style="width: 16%;' + (kursis[3].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '4' + '</td>' +
                                '<td style="width: 16%;' + (kursis[4].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '5' + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td style="width: 16%;' + (kursis[5].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '6' + '</td>' +
                                '<td style="width: 16%;' + (kursis[6].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '7' + '</td>' +
                                '<td style="width: 16%"></td> ' +
                                '<td style="width: 16%;' + (kursis[7].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '8' + '</td>' +
                                '<td style="width: 16%;' + (kursis[8].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '9' + '</td>' +
                                '<td style="width: 16%;' + (kursis[9].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '10' + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td style="width: 16%;' + (kursis[10].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '11' + '</td>' +
                                '<td style="width: 16%;' + (kursis[11].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '12' + '</td>' +
                                '<td style="width: 16%"></td> ' +
                                '<td style="width: 16%;' + (kursis[12].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '13' + '</td>' +
                                '<td style="width: 16%;' + (kursis[13].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '14' + '</td>' +
                                '<td style="width: 16%;' + (kursis[14].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '15' + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td style="width: 16%;' + (kursis[15].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '16' + '</td>' +
                                '<td style="width: 16%;' + (kursis[16].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '17' + '</td>' +
                                '<td style="width: 16%"></td> ' +
                                '<td style="width: 16%;' + (kursis[17].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '18' + '</td>' +
                                '<td style="width: 16%;' + (kursis[18].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '19' + '</td>' +
                                '<td style="width: 16%;' + (kursis[19].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '20' + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td style="width: 16%;' + (kursis[20].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '21' + '</td>' +
                                '<td style="width: 16%;' + (kursis[21].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '22' + '</td>' +
                                '<td style="width: 16%"></td> ' +
                                '<td style="width: 16%;' + (kursis[22].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '23' + '</td>' +
                                '<td style="width: 16%;' + (kursis[23].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '24' + '</td>' +
                                '<td style="width: 16%;' + (kursis[24].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '25' + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td style="width: 16%;' + (kursis[25].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '26' + '</td>' +
                                '<td style="width: 16%;' + (kursis[26].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '27' + '</td>' +
                                '<td style="width: 16%"></td> ' +
                                '<td style="width: 16%;' + (kursis[27].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '28' + '</td>' +
                                '<td style="width: 16%;' + (kursis[28].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '29' + '</td>' +
                                '<td style="width: 16%;' + (kursis[29].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '30' + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td style="width: 16%;' + (kursis[30].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '31' + '</td>' +
                                '<td style="width: 16%;' + (kursis[31].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '32' + '</td>' +
                                '<td style="width: 16%"></td> ' +
                                '<td style="width: 16%;' + (kursis[32].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '33' + '</td>' +
                                '<td style="width: 16%;' + (kursis[33].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '34' + '</td>' +
                                '<td style="width: 16%;' + (kursis[34].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '35' + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td style="width: 16%;' + (kursis[35].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '36' + '</td>' +
                                '<td style="width: 16%;' + (kursis[36].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '37' + '</td>' +
                                '<td style="width: 16%"></td> ' +
                                '<td style="width: 16%;' + (kursis[37].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '38' + '</td>' +
                                '<td style="width: 16%;' + (kursis[38].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '39' + '</td>' +
                                '<td style="width: 16%;' + (kursis[39].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '40' + '</td>' +
                            '<tr>' +
                                '<td style="width: 16%;' + (kursis[40].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '41' + '</td>' +
                                '<td style="width: 16%;' + (kursis[41].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '42' + '</td>' +
                                '<td style="width: 16%"></td> ' +
                                '<td style="width: 16%;' + (kursis[42].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '43' + '</td>' +
                                '<td style="width: 16%;' + (kursis[43].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '44' + '</td>' +
                                '<td style="width: 16%;' + (kursis[44].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '45' + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td style="width: 16%;' + (kursis[45].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '46' + '</td>' +
                                '<td style="width: 16%;' + (kursis[46].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '47' + '</td>' +
                                '<td style="width: 16%"></td> ' +
                                '<td style="width: 16%;' + (kursis[47].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '48' + '</td>' +
                                '<td style="width: 16%;' + (kursis[48].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '49' + '</td>' +
                                '<td style="width: 16%;' + (kursis[49].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '50' + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td style="width: 16%">Pintu Belakang</td> ' +
                                '<td style="width: 16%"></td> ' +
                                '<td style="width: 16%"></td> ' +
                                '<td style="width: 16%;' + (kursis[50].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '51' + '</td>' +
                                '<td style="width: 16%;' + (kursis[51].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '52' + '</td>' +
                                '<td style="width: 16%;' + (kursis[52].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '53' + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td style="width: 16%;' + (kursis[53].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '54' + '</td>' +
                                '<td style="width: 16%;' + (kursis[54].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '55' + '</td>' +
                                '<td style="width: 16%;' + (kursis[55].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '56' + '</td>' +
                                '<td style="width: 16%;' + (kursis[56].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '57' + '</td>' +
                                '<td style="width: 16%;' + (kursis[57].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '58' + '</td>' +
                                '<td style="width: 16%;' + (kursis[58].status === 1 ? 'background-color: #2094c8' : 'background-color: #cecece') + '">' + '59' + '</td>' +
                            '</tr>' +
                        '</table>',
                    width: 600
                });
            })
            .catch((error) => {
                const eror = JSON.parse(error._body);
                swal({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
            });
    }

    editPemudik(){
        this.pageType2 === 'view' ? this.pageType2 = 'edit' : this.pageType2 = 'view';
    }

    openDialog(url){
        swal({
            imageUrl: url,
            imageWidth: 800,
            imageHeight: 400,
            imageAlt: 'Custom image',
            animation: false
        });
    }

}

export class ValidateEmailNotTaken {
    static createValidator(productService: DetailTiketService, nomor_tiket_mudik, nomor_tiket_balik) {
        return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
            // return productService.checkEmailNotTaken(nomor_tiket_mudik, control.value).map(res => {
            //     console.log(res);
            //     console.log(nomor_tiket_mudik);
            //     return res.status ? null : { nikTaken: true };
            // });

            const promise_list = [];
            if (nomor_tiket_mudik) {
                promise_list.push(productService.checkNIK(nomor_tiket_mudik, control.value));
            } else {
                promise_list.push(Promise.resolve({
                    status : true
                }));
            }
            if (nomor_tiket_balik) {
                promise_list.push(productService.checkNIK(nomor_tiket_balik, control.value));
            } else {
                promise_list.push(Promise.resolve({
                    status : true
                }));
            }
            return Promise.all(promise_list)
                .then((result: any) => {
                    let status = true;
                    for (let index = 0; index < result.length; index++) {
                        if (!result[index].status) {
                            status = false;
                            break;
                        }
                    }
                    return status ? null : { nikTaken : true};
                });
        };
    }
}
