import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from './../session.service';

@Injectable()
export class DaftarRuteService implements Resolve<any>
{
    products: any[];
    reportTitipMotor: any[];
    onProductsChanged: BehaviorSubject<any> = new BehaviorSubject({});
    token = this._sessionService.get().token;

    constructor(
        private http: Http,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProducts(),
                this.getReportMotor()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    setHeader(options: RequestOptions) {
        const headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }

    getProducts(): Promise<any>
    {
        const options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url + '/admin/report/', options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    this.products = response['data'];
                    this.onProductsChanged.next(this.products);
                    resolve(response);
                }, (err) => {
                    reject(err);
                });
        });
    }

    getReportMotor(): Promise<any>
    {
        const options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url + '/admin/titipMotor/reportPerRute', options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    console.log(response);
                    this.reportTitipMotor = response['data'];
                    resolve(response);
                }, (err) => {
                    reject(err);
                });
        });
    }
}
