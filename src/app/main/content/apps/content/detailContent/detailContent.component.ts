import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { DetailContentService } from './detailContent.service';
import { fuseAnimations } from '../../../../../core/animations';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Content } from './detailContent.model';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { FuseConfirmDialogComponent } from './../../../../../core/components/confirm-dialog/confirm-dialog.component';
import swal from 'sweetalert2';
import { SessionService } from './../../session.service';

@Component({
    selector     : 'fuse-e-commerce-content',
    templateUrl  : './detailContent.component.html',
    styleUrls    : ['./detailContent.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class DetailContentComponent implements OnInit, OnDestroy
{
    content = new Content();
    onContentChanged: Subscription;
    pageType: string;
    contentForm: FormGroup;
    token = this._sessionService.get().token;
    role = this._sessionService.get().role;
    quillConfig = {
        container: [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            ['code-block'],
            [{ 'header': 1 }, { 'header': 2 }],               // custom button values
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
            [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
            [{ 'direction': 'rtl' }],                         // text direction
            [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
            [{ 'font': [] }],
            [{ 'align': [] }]
        ],
    };

    constructor(
        private contentService: DetailContentService,
        private formBuilder: FormBuilder,
        public snackBar: MatSnackBar,
        private location: Location,
        private http: Http,
        private route: ActivatedRoute,
        private dialog: MatDialog,
        private router: Router,
        private _sessionService: SessionService
    )
    {

    }

    ngOnInit()
    {
        // Subscribe to update content on changes
        this.onContentChanged =
            this.contentService.onContentChanged
                .subscribe(content => {

                    if ( content )
                    {
                        this.content = new Content(content);
                        this.pageType = 'edit';
                    }
                    else
                    {
                        this.pageType = 'new';
                        this.content = new Content();
                    }

                    this.contentForm = this.createContentForm();
                });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }

    createContentForm()
    {
        return this.formBuilder.group({
            label              : new FormControl(this.content.label, [Validators.required]),
            content            : new FormControl(this.content.content, [Validators.required])
        });
    }

    saveContent()
    {
        const data = this.contentForm.getRawValue();
        this.contentService.saveContent(data)
            .then((response: any) => {

                // Change the location with new one
                if (response.status) {
                    this.back();
                    swal({
                        title: 'Update Content Berhasil!',
                        text: 'Content ' + data.asal + ' - ' + data.tujuan + ' Telah Update',
                        type: 'success',
                        timer: 2000
                    })
                } else {
                    swal({
                        title: 'Edit Content Gagal!',
                        text: response.message,
                        type: 'error'
                    })
                }
            });
    }

    salinContent()
    {
        this.pageType = "salin";
        this.contentForm.controls.jenis.disable();
        this.contentForm.controls.tujuan.disable();
        this.contentForm.controls.asal.disable();
    }

    addContent()
    {
        const data = this.contentForm.getRawValue();
        this.contentService.addContent(data)
            .then((response: any) => {

                this.contentService.onContentChanged.next(data);

                if (response.status) {
                    this.back();
                    swal({
                        title: 'Tambah Content Berhasil!',
                        text: 'Content ' + data.asal + ' - ' + data.tujuan + ' Telah Ditambahkan',
                        type: 'success',
                        timer: 2000
                    })
                } else {
                    swal({
                        title: 'Tambah Content Gagal!',
                        text: response.message,
                        type: 'error'
                    })
                }
            });
    }

    ngOnDestroy()
    {
        this.onContentChanged.unsubscribe();
    }

    hapus(): void{
        swal({
            title: 'Hapus Content' + this.content.label,
            text: "Content yang telah dihapus tidak bisa dikembalikan lagi",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus !',
            cancelButtonText: 'Batal!',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                this.contentService.hapus(this.contentForm.getRawValue())
                    .then((response: any) => {
                        if (response.status) {
                            this.back();
                            swal({
                                title: 'Hapus Content Berhasil!',
                                text: 'Content' + this.content.label + ' Telah Dihapus',
                                type: 'success',
                                timer: 2000
                            })
                        } else {
                            swal(
                                'Hapus Content Gagal!',
                                response.message,
                                'error'
                            )
                        }
                    })
            }
        })
    }

    back(){
        this.router.navigate(['/apps/content/daftarContent']);
    }
}
