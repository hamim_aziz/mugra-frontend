import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatChipInputEvent } from '@angular/material';

export class Product
{
    id: number;
    nomor_tiket: string;
    alamat: string;
    telepon: string;
    status: number;
    pemudik_dewasa: number;
    pemudik_anak: number;
    kota_tujuan: string;
    rute: {
        tempat_berangkat: string,
        tanggal: string,
        kota: {
            asal: string,
            tujuan: string,
        }
    };
    kendaraan: {
        nomor: string,
        status: number,
    };
    pemudiks: {
        id: number,
        nama: string,
        jenis: number;
        nik: string,
        sim: string,
        stnk: string,
        gender: number,
        tanggal_lahir: string,
        tempat_lahir: string,
        kursi: string,
        usia: number,
        pekerjaan: string
    }[];

    constructor(product?)
    {
        if (product) {
            product = product;
            this.id = product.id;
            this.nomor_tiket = product.nomor_tiket;
            this.alamat = product.alamat;
            this.telepon = product.telepon;
            this.status = product.status;
            this.pemudik_dewasa = product.pemudik_dewasa;
            this.pemudik_anak = product.pemudik_anak;
            this.kota_tujuan = product.kota_tujuan;
            this.pemudiks = product.pemudiks;
            this.rute = product.rute;
            this.kendaraan = product.kendaraan;
        } else {
            product = {};
            this.id = 0;
            this.nomor_tiket = '';
            this.alamat = '';
            this.telepon = '';
            this.status = 0;
            this.pemudik_dewasa = 0;
            this.pemudik_anak = 0;
            this.kota_tujuan = '';
            this.pemudiks = [];
        }
    }
    
}
