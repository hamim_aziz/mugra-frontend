webpackJsonp(["apps.module"],{

/***/ "../../../../../src/app/core/components/copier/copier.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CopierService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
/**
 * This class is based on the code in the following projects:
 *
 * - https://github.com/zenorocha/select
 * - https://github.com/zenorocha/clipboard.js/
 *
 * Both released under MIT license - © Zeno Rocha
 */

var CopierService = /** @class */ (function () {
    function CopierService() {
    }
    /** Copy the text value to the clipboard. */
    CopierService.prototype.copyText = function (text) {
        this.createTextareaAndSelect(text);
        var copySuccessful = document.execCommand('copy');
        this.removeFake();
        return copySuccessful;
    };
    /**
     * Creates a hidden textarea element, sets its value from `text` property,
     * and makes a selection on it.
     */
    CopierService.prototype.createTextareaAndSelect = function (text) {
        // Create a fake element to hold the contents to copy
        this.textarea = document.createElement('textarea');
        // Prevent zooming on iOS
        this.textarea.style.fontSize = '12pt';
        // Hide the element
        this.textarea.classList.add('cdk-visually-hidden');
        // Move element to the same position vertically
        var yPosition = window.pageYOffset || document.documentElement.scrollTop;
        this.textarea.style.top = yPosition + 'px';
        this.textarea.setAttribute('readonly', '');
        this.textarea.value = text;
        document.body.appendChild(this.textarea);
        this.textarea.select();
        this.textarea.setSelectionRange(0, this.textarea.value.length);
    };
    /** Remove the text area from the DOM. */
    CopierService.prototype.removeFake = function () {
        if (this.textarea) {
            document.body.removeChild(this.textarea);
            this.textarea = null;
        }
    };
    CopierService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], CopierService);
    return CopierService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/apps.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuseAppsModule", function() { return FuseAppsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__core_modules_shared_module__ = __webpack_require__("../../../../../src/app/core/modules/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_angular_material_angular_material_module__ = __webpack_require__("../../../../../src/app/main/content/components/angular-material/angular-material.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login_component__ = __webpack_require__("../../../../../src/app/main/content/apps/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_guard__ = __webpack_require__("../../../../../src/app/main/content/apps/auth.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__auth_service__ = __webpack_require__("../../../../../src/app/main/content/apps/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    {
        path: 'login',
        component: __WEBPACK_IMPORTED_MODULE_4__login_login_component__["a" /* FuseLoginComponent */]
    },
    {
        path: 'dashboards/project',
        loadChildren: './dashboards/project/project.module#FuseProjectDashboardModule',
        canActive: [__WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */]]
    },
    {
        path: 'user',
        loadChildren: './user/user.module#UserModule',
        canActive: [__WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */]]
    },
    {
        path: 'rute',
        loadChildren: './rute/rute.module#RuteModule',
        canActive: [__WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */]]
    },
    {
        path: 'titipMotor',
        loadChildren: './titip_motor/titipMotor.module#TitipMotorModule',
        canActive: [__WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */]]
    },
    {
        path: 'content',
        loadChildren: './content/content.module#ContentModule',
        canActive: [__WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */]]
    },
    {
        path: 'contact',
        loadChildren: './contact/contact.module#ContactModule',
        canActive: [__WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */]]
    },
    {
        path: 'kendaraan',
        loadChildren: './kendaraan/kendaraan.module#KendaraanModule',
        canActive: [__WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */]]
    },
    {
        path: 'tiket',
        loadChildren: './tiket/tiket.module#TiketModule',
        canActive: [__WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */]]
    },
    {
        path: 'pemudik',
        loadChildren: './pemudik/pemudik.module#PemudikModule',
        canActive: [__WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */]]
    },
    {
        path: 'print_tiket',
        loadChildren: './print_tiket/tiket.module#TiketModule',
        canActive: [__WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */]]
    },
    {
        path: 'monitor_rute',
        loadChildren: './monitor_rute/monitor.module#MonitorModule',
        canActive: [__WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */]]
    },
    {
        path: 'feedback',
        loadChildren: './feedback/feedback.module#FeedbackModule',
        canActive: [__WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */]]
    },
    {
        path: 'laporan',
        loadChildren: './laporan/laporan.module#LaporanModule',
        canActive: [__WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */]]
    },
    {
        path: 'sms',
        loadChildren: './sms/sms.module#SmsModule',
        canActive: [__WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */]]
    }
];
var FuseAppsModule = /** @class */ (function () {
    function FuseAppsModule() {
    }
    FuseAppsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__core_modules_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["g" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_3__components_angular_material_angular_material_module__["a" /* FuseAngularMaterialModule */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__auth_guard__["a" /* AuthGuard */],
                __WEBPACK_IMPORTED_MODULE_6__auth_service__["a" /* AuthService */],
                __WEBPACK_IMPORTED_MODULE_7__session_service__["a" /* SessionService */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__login_login_component__["a" /* FuseLoginComponent */]
            ]
        })
    ], FuseAppsModule);
    return FuseAppsModule;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"login\" fxLayout=\"column\" fusePerfectScrollbar>\r\n\r\n    <div id=\"login-form-wrapper\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n\r\n        <div id=\"login-form\" *fuseIfOnDom [@animate]=\"{value:'*',params:{duration:'300ms',y:'100px'}}\">\r\n\r\n            <div class=\"logo\">\r\n                <img src=\"assets/images/logos/logo-login.gif\">\r\n            </div>\r\n\r\n            <div class=\"title\">LOGIN TO YOUR ACCOUNT</div>\r\n\r\n            <form name=\"loginForm\" [formGroup]=\"loginForm\" novalidate>\r\n\r\n                <mat-form-field>\r\n                    <input matInput placeholder=\"Email\" formControlName=\"email\">\r\n                    <mat-error *ngIf=\"loginFormErrors.email.required\">\r\n                        Email is required\r\n                    </mat-error>\r\n                </mat-form-field>\r\n\r\n                <mat-form-field>\r\n                    <input matInput type=\"password\" placeholder=\"Password\" formControlName=\"password\">\r\n                    <mat-error *ngIf=\"loginFormErrors.password.required\">\r\n                        Password is required\r\n                    </mat-error>\r\n                </mat-form-field>\r\n\r\n                <button mat-raised-button color=\"accent\" class=\"submit-button\" aria-label=\"LOG IN\"\r\n                        [disabled]=\"loginForm.invalid\" (click)=\"onSubmit()\">\r\n                    LOGIN\r\n                </button>\r\n\r\n            </form>\r\n\r\n        </div>\r\n\r\n    </div>\r\n\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/login/login.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n:host #login {\n  width: 100%;\n  overflow: auto;\n  background-color: #333366;\n  background-size: cover; }\n:host #login #login-form-wrapper {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 0 auto;\n            flex: 1 0 auto;\n    padding: 32px; }\n@media screen and (max-width: 599px) {\n      :host #login #login-form-wrapper {\n        padding: 16px; } }\n:host #login #login-form-wrapper #login-form {\n      width: 384px;\n      max-width: 384px;\n      padding: 32px;\n      background: #FFFFFF;\n      text-align: center;\n      -webkit-box-shadow: 0px 8px 10px -5px rgba(0, 0, 0, 0.2), 0px 16px 24px 2px rgba(0, 0, 0, 0.14), 0px 6px 30px 5px rgba(0, 0, 0, 0.12);\n              box-shadow: 0px 8px 10px -5px rgba(0, 0, 0, 0.2), 0px 16px 24px 2px rgba(0, 0, 0, 0.14), 0px 6px 30px 5px rgba(0, 0, 0, 0.12); }\n@media screen and (max-width: 599px) {\n        :host #login #login-form-wrapper #login-form {\n          padding: 24px;\n          width: 100%; } }\n:host #login #login-form-wrapper #login-form .logo {\n        width: 128px;\n        margin: 32px auto; }\n:host #login #login-form-wrapper #login-form .title {\n        font-size: 20px;\n        margin: 16px 0 32px 0; }\n:host #login #login-form-wrapper #login-form form {\n        width: 100%;\n        text-align: left; }\n:host #login #login-form-wrapper #login-form form mat-form-field {\n          width: 100%; }\n:host #login #login-form-wrapper #login-form form mat-checkbox {\n          margin: 0; }\n:host #login #login-form-wrapper #login-form form .remember-forgot-password {\n          font-size: 13px;\n          margin-top: 8px; }\n:host #login #login-form-wrapper #login-form form .remember-forgot-password .remember-me {\n            margin-bottom: 16px; }\n:host #login #login-form-wrapper #login-form form .remember-forgot-password .forgot-password {\n            font-size: 13px;\n            font-weight: 500;\n            margin-bottom: 16px; }\n:host #login #login-form-wrapper #login-form form .submit-button {\n          width: 220px;\n          margin: 16px auto;\n          display: block; }\n@media screen and (max-width: 599px) {\n            :host #login #login-form-wrapper #login-form form .submit-button {\n              width: 90%; } }\n:host #login #login-form-wrapper #login-form .register {\n        margin: 32px auto 24px auto;\n        font-weight: 500; }\n:host #login #login-form-wrapper #login-form .register .text {\n          margin-right: 8px; }\n:host #login #login-form-wrapper #login-form .separator {\n        font-size: 15px;\n        font-weight: 600;\n        margin: 24px auto;\n        position: relative;\n        overflow: hidden;\n        width: 100px;\n        color: rgba(0, 0, 0, 0.54); }\n:host #login #login-form-wrapper #login-form .separator .text {\n          display: -webkit-inline-box;\n          display: -ms-inline-flexbox;\n          display: inline-flex;\n          position: relative;\n          padding: 0 8px;\n          z-index: 9999; }\n:host #login #login-form-wrapper #login-form .separator .text:before, :host #login #login-form-wrapper #login-form .separator .text:after {\n            content: '';\n            display: block;\n            width: 30px;\n            position: absolute;\n            top: 10px;\n            border-top: 1px solid rgba(0, 0, 0, 0.12); }\n:host #login #login-form-wrapper #login-form .separator .text:before {\n            right: 100%; }\n:host #login #login-form-wrapper #login-form .separator .text:after {\n            left: 100%; }\n:host #login #login-form-wrapper #login-form button.google, :host #login #login-form-wrapper #login-form button.facebook {\n        width: 192px;\n        text-transform: none;\n        color: #FFFFFF;\n        font-size: 13px; }\n@media screen and (max-width: 599px) {\n        :host #login #login-form-wrapper #login-form button {\n          width: 80%; } }\n:host #login #login-form-wrapper #login-form button.google {\n        background-color: #D73D32;\n        margin-bottom: 8px; }\n:host #login #login-form-wrapper #login-form button.facebook {\n        background-color: #3f5c9a; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FuseLoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_service__ = __webpack_require__("../../../../../src/app/main/content/apps/auth.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var FuseLoginComponent = /** @class */ (function () {
    function FuseLoginComponent(fuseConfig, formBuilder, auth, router, snackBar, http, _sessionService) {
        this.fuseConfig = fuseConfig;
        this.formBuilder = formBuilder;
        this.auth = auth;
        this.router = router;
        this.snackBar = snackBar;
        this.http = http;
        this._sessionService = _sessionService;
        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });
        this.loginFormErrors = {
            email: {},
            password: {}
        };
    }
    FuseLoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this._sessionService.get()) {
            this.http.post(__WEBPACK_IMPORTED_MODULE_9__environments_environment__["a" /* environment */].setting.base_url + '/logout', {}, this.setHeader())
                .subscribe(function () {
                _this._sessionService.clear();
            });
        }
        this.loginForm = this.formBuilder.group({
            email: ['', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].email]],
            password: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required]
        });
        history.pushState(null, null, location.href);
        window.onpopstate = function () {
            history.go(1);
        };
    };
    FuseLoginComponent.prototype.setHeader = function () {
        var options = new __WEBPACK_IMPORTED_MODULE_6__angular_http__["g" /* RequestOptions */]();
        var headers = new __WEBPACK_IMPORTED_MODULE_6__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this._sessionService.get().token);
        options.headers = headers;
        return options;
    };
    FuseLoginComponent.prototype.onLoginFormValuesChanged = function () {
        for (var field in this.loginFormErrors) {
            if (!this.loginFormErrors.hasOwnProperty(field)) {
                continue;
            }
            // Clear previous errors
            this.loginFormErrors[field] = {};
            // Get the control
            var control = this.loginForm.get(field);
            if (control && control.dirty && !control.valid) {
                this.loginFormErrors[field] = control.errors;
            }
        }
    };
    FuseLoginComponent.prototype.onSubmit = function () {
        var _this = this;
        var loginData = this.loginForm.getRawValue();
        this.auth.authLogin(loginData)
            .then(function (response) {
            var data = JSON.parse(response._body);
            var rute_id = [];
            if (data.user.rute_id) {
                var id = data.user.rute_id.slice(1, (data.user.rute_id.length - 1)).split(',');
                for (var i = 0; i < id.length; ++i) {
                    rute_id.push(Number(id[i]));
                }
            }
            var dt = {
                username: data.user.username,
                id: data.user.id,
                token: data.token.access_token,
                role: data.user.group,
                email: data.user.email,
                rute_id: rute_id
            };
            _this._sessionService.set(dt);
            _this.router.navigate(['/apps/dashboards/project']);
        })
            .catch(function (err) {
            _this.snackBar.open(err.json().message, 'Error!', {
                duration: 3000
            });
        });
    };
    FuseLoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-login',
            template: __webpack_require__("../../../../../src/app/main/content/apps/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/login/login.component.scss")],
            animations: __WEBPACK_IMPORTED_MODULE_3__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__core_services_config_service__["a" /* FuseConfigService */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_4__auth_service__["a" /* AuthService */],
            __WEBPACK_IMPORTED_MODULE_5__angular_router__["f" /* Router */],
            __WEBPACK_IMPORTED_MODULE_8__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_10__session_service__["a" /* SessionService */]])
    ], FuseLoginComponent);
    return FuseLoginComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/components/angular-material/angular-material.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"page-layout simple fullwidth angular-material-element\" fusePerfectScrollbar>\r\n\r\n    <!-- HEADER -->\r\n    <div class=\"header mat-accent-bg p-24 h-160\" fxLayout=\"column\" fxLayoutAlign=\"center center\" fxLayout.gt-xs=\"row\" fxLayoutAlign.gt-xs=\"space-between center\">\r\n\r\n        <div fxLayout=\"column\" fxLayoutAlign=\"center center\" fxLayout.gt-xs=\"column\" fxLayoutAlign.gt-xs=\"center start\">\r\n            <div class=\"black-fg\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <mat-icon class=\"secondary-text s-16\">home</mat-icon>\r\n                <mat-icon class=\"secondary-text s-16\">chevron_right</mat-icon>\r\n                <span class=\"secondary-text\">Components</span>\r\n                <mat-icon class=\"secondary-text s-16\">chevron_right</mat-icon>\r\n                <span class=\"secondary-text\">Angular Material Elements</span>\r\n            </div>\r\n            <div class=\"h2 mt-16\">{{title}}</div>\r\n        </div>\r\n\r\n        <a mat-raised-button class=\"reference-button mat-white-bg mt-16 mt-sm-0\" href=\"https://material.angular.io/\" target=\"_blank\">\r\n            <mat-icon>link</mat-icon>\r\n            <span>Reference</span>\r\n        </a>\r\n    </div>\r\n    <!-- / HEADER -->\r\n\r\n    <!-- CONTENT -->\r\n    <div class=\"content p-24\">\r\n        <div *ngFor=\"let example of examples\">\r\n            <fuse-example-viewer [example]=\"example\"></fuse-example-viewer>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/components/angular-material/angular-material.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host .angular-material-element > .content {\n  max-width: 960px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/components/angular-material/angular-material.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FuseAngularMaterialComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__example_components__ = __webpack_require__("../../../../../src/app/main/content/components/angular-material/example-components.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FuseAngularMaterialComponent = /** @class */ (function () {
    function FuseAngularMaterialComponent(route) {
        this.route = route;
    }
    FuseAngularMaterialComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            var _title = _this.id.replace('-', ' ');
            _this.title = _title.charAt(0).toUpperCase() + _title.substring(1);
            _this.examples = __WEBPACK_IMPORTED_MODULE_2__example_components__["a" /* COMPONENT_MAP */][_this.id];
        });
    };
    FuseAngularMaterialComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-angular-material',
            template: __webpack_require__("../../../../../src/app/main/content/components/angular-material/angular-material.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/components/angular-material/angular-material.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]])
    ], FuseAngularMaterialComponent);
    return FuseAngularMaterialComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/components/angular-material/angular-material.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FuseAngularMaterialModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__core_modules_shared_module__ = __webpack_require__("../../../../../src/app/core/modules/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_components_widget_widget_module__ = __webpack_require__("../../../../../src/app/core/components/widget/widget.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__example_viewer_example_viewer__ = __webpack_require__("../../../../../src/app/main/content/components/angular-material/example-viewer/example-viewer.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__example_components__ = __webpack_require__("../../../../../src/app/main/content/components/angular-material/example-components.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material_component__ = __webpack_require__("../../../../../src/app/main/content/components/angular-material/angular-material.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: 'angular-material',
        children: [
            {
                path: ':id',
                component: __WEBPACK_IMPORTED_MODULE_6__angular_material_component__["a" /* FuseAngularMaterialComponent */]
            }
        ]
    }
];
var FuseAngularMaterialModule = /** @class */ (function () {
    function FuseAngularMaterialModule() {
    }
    FuseAngularMaterialModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__core_modules_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_3__core_components_widget_widget_module__["a" /* FuseWidgetModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__core_modules_shared_module__["a" /* SharedModule */]
            ],
            entryComponents: __WEBPACK_IMPORTED_MODULE_5__example_components__["c" /* EXAMPLE_LIST */],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__example_components__["c" /* EXAMPLE_LIST */].slice(),
                __WEBPACK_IMPORTED_MODULE_6__angular_material_component__["a" /* FuseAngularMaterialComponent */],
                __WEBPACK_IMPORTED_MODULE_4__example_viewer_example_viewer__["a" /* FuseExampleViewerComponent */]
            ]
        })
    ], FuseAngularMaterialModule);
    return FuseAngularMaterialModule;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/components/angular-material/example-components.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return COMPONENT_MAP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return EXAMPLE_COMPONENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return EXAMPLE_LIST; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_angular_material_examples_autocomplete_display_autocomplete_display_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/autocomplete-display/autocomplete-display-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__assets_angular_material_examples_autocomplete_filter_autocomplete_filter_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/autocomplete-filter/autocomplete-filter-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__assets_angular_material_examples_autocomplete_overview_autocomplete_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/autocomplete-overview/autocomplete-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__assets_angular_material_examples_autocomplete_simple_autocomplete_simple_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/autocomplete-simple/autocomplete-simple-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__assets_angular_material_examples_button_overview_button_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/button-overview/button-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assets_angular_material_examples_button_toggle_exclusive_button_toggle_exclusive_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/button-toggle-exclusive/button-toggle-exclusive-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_angular_material_examples_button_toggle_overview_button_toggle_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/button-toggle-overview/button-toggle-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_angular_material_examples_button_types_button_types_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/button-types/button-types-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__assets_angular_material_examples_card_fancy_card_fancy_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/card-fancy/card-fancy-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__assets_angular_material_examples_card_overview_card_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/card-overview/card-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__assets_angular_material_examples_cdk_table_basic_cdk_table_basic_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/cdk-table-basic/cdk-table-basic-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__assets_angular_material_examples_checkbox_configurable_checkbox_configurable_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/checkbox-configurable/checkbox-configurable-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__assets_angular_material_examples_checkbox_overview_checkbox_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/checkbox-overview/checkbox-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__assets_angular_material_examples_chips_input_chips_input_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/chips-input/chips-input-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__assets_angular_material_examples_chips_overview_chips_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/chips-overview/chips-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__assets_angular_material_examples_chips_stacked_chips_stacked_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/chips-stacked/chips-stacked-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__assets_angular_material_examples_datepicker_api_datepicker_api_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-api/datepicker-api-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__assets_angular_material_examples_datepicker_disabled_datepicker_disabled_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-disabled/datepicker-disabled-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__assets_angular_material_examples_datepicker_events_datepicker_events_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-events/datepicker-events-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__assets_angular_material_examples_datepicker_filter_datepicker_filter_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-filter/datepicker-filter-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__assets_angular_material_examples_datepicker_formats_datepicker_formats_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-formats/datepicker-formats-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__assets_angular_material_examples_datepicker_locale_datepicker_locale_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-locale/datepicker-locale-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__assets_angular_material_examples_datepicker_min_max_datepicker_min_max_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-min-max/datepicker-min-max-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__assets_angular_material_examples_datepicker_moment_datepicker_moment_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-moment/datepicker-moment-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__assets_angular_material_examples_datepicker_overview_datepicker_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-overview/datepicker-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__assets_angular_material_examples_datepicker_start_view_datepicker_start_view_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-start-view/datepicker-start-view-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__assets_angular_material_examples_datepicker_touch_datepicker_touch_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-touch/datepicker-touch-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__assets_angular_material_examples_datepicker_value_datepicker_value_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-value/datepicker-value-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__assets_angular_material_examples_dialog_content_dialog_content_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/dialog-content/dialog-content-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__assets_angular_material_examples_dialog_data_dialog_data_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/dialog-data/dialog-data-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__assets_angular_material_examples_dialog_elements_dialog_elements_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/dialog-elements/dialog-elements-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__assets_angular_material_examples_dialog_overview_dialog_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/dialog-overview/dialog-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__assets_angular_material_examples_elevation_overview_elevation_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/elevation-overview/elevation-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__assets_angular_material_examples_expansion_overview_expansion_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/expansion-overview/expansion-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__assets_angular_material_examples_expansion_steps_expansion_steps_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/expansion-steps/expansion-steps-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__assets_angular_material_examples_form_field_custom_control_form_field_custom_control_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/form-field-custom-control/form-field-custom-control-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__assets_angular_material_examples_form_field_error_form_field_error_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/form-field-error/form-field-error-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__assets_angular_material_examples_form_field_hint_form_field_hint_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/form-field-hint/form-field-hint-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__assets_angular_material_examples_form_field_overview_form_field_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/form-field-overview/form-field-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__assets_angular_material_examples_form_field_placeholder_form_field_placeholder_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/form-field-placeholder/form-field-placeholder-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__assets_angular_material_examples_form_field_prefix_suffix_form_field_prefix_suffix_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/form-field-prefix-suffix/form-field-prefix-suffix-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__assets_angular_material_examples_form_field_theming_form_field_theming_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/form-field-theming/form-field-theming-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__assets_angular_material_examples_grid_list_dynamic_grid_list_dynamic_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/grid-list-dynamic/grid-list-dynamic-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__assets_angular_material_examples_grid_list_overview_grid_list_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/grid-list-overview/grid-list-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__assets_angular_material_examples_icon_overview_icon_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/icon-overview/icon-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__assets_angular_material_examples_icon_svg_icon_svg_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/icon-svg/icon-svg-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__assets_angular_material_examples_input_autosize_textarea_input_autosize_textarea_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/input-autosize-textarea/input-autosize-textarea-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__assets_angular_material_examples_input_clearable_input_clearable_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/input-clearable/input-clearable-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__assets_angular_material_examples_input_error_state_matcher_input_error_state_matcher_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/input-error-state-matcher/input-error-state-matcher-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__assets_angular_material_examples_input_errors_input_errors_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/input-errors/input-errors-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__assets_angular_material_examples_input_form_input_form_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/input-form/input-form-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__assets_angular_material_examples_input_hint_input_hint_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/input-hint/input-hint-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__assets_angular_material_examples_input_overview_input_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/input-overview/input-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__assets_angular_material_examples_input_prefix_suffix_input_prefix_suffix_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/input-prefix-suffix/input-prefix-suffix-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__assets_angular_material_examples_list_overview_list_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/list-overview/list-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__assets_angular_material_examples_list_sections_list_sections_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/list-sections/list-sections-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__assets_angular_material_examples_list_selection_list_selection_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/list-selection/list-selection-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__assets_angular_material_examples_menu_icons_menu_icons_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/menu-icons/menu-icons-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__assets_angular_material_examples_menu_overview_menu_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/menu-overview/menu-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__assets_angular_material_examples_nested_menu_nested_menu_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/nested-menu/nested-menu-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__assets_angular_material_examples_paginator_configurable_paginator_configurable_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/paginator-configurable/paginator-configurable-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__assets_angular_material_examples_paginator_overview_paginator_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/paginator-overview/paginator-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__assets_angular_material_examples_progress_bar_buffer_progress_bar_buffer_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/progress-bar-buffer/progress-bar-buffer-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__assets_angular_material_examples_progress_bar_configurable_progress_bar_configurable_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/progress-bar-configurable/progress-bar-configurable-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__assets_angular_material_examples_progress_bar_determinate_progress_bar_determinate_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/progress-bar-determinate/progress-bar-determinate-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__assets_angular_material_examples_progress_bar_indeterminate_progress_bar_indeterminate_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/progress-bar-indeterminate/progress-bar-indeterminate-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__assets_angular_material_examples_progress_bar_query_progress_bar_query_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/progress-bar-query/progress-bar-query-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__assets_angular_material_examples_progress_spinner_configurable_progress_spinner_configurable_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/progress-spinner-configurable/progress-spinner-configurable-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68__assets_angular_material_examples_progress_spinner_overview_progress_spinner_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/progress-spinner-overview/progress-spinner-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_69__assets_angular_material_examples_radio_ng_model_radio_ng_model_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/radio-ng-model/radio-ng-model-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_70__assets_angular_material_examples_radio_overview_radio_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/radio-overview/radio-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_71__assets_angular_material_examples_select_custom_trigger_select_custom_trigger_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/select-custom-trigger/select-custom-trigger-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_72__assets_angular_material_examples_select_disabled_select_disabled_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/select-disabled/select-disabled-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_73__assets_angular_material_examples_select_error_state_matcher_select_error_state_matcher_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/select-error-state-matcher/select-error-state-matcher-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_74__assets_angular_material_examples_select_form_select_form_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/select-form/select-form-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_75__assets_angular_material_examples_select_hint_error_select_hint_error_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/select-hint-error/select-hint-error-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_76__assets_angular_material_examples_select_multiple_select_multiple_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/select-multiple/select-multiple-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_77__assets_angular_material_examples_select_no_ripple_select_no_ripple_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/select-no-ripple/select-no-ripple-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_78__assets_angular_material_examples_select_optgroup_select_optgroup_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/select-optgroup/select-optgroup-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_79__assets_angular_material_examples_select_overview_select_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/select-overview/select-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_80__assets_angular_material_examples_select_panel_class_select_panel_class_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/select-panel-class/select-panel-class-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_81__assets_angular_material_examples_select_reset_select_reset_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/select-reset/select-reset-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_82__assets_angular_material_examples_select_value_binding_select_value_binding_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/select-value-binding/select-value-binding-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_83__assets_angular_material_examples_sidenav_fab_sidenav_fab_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/sidenav-fab/sidenav-fab-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_84__assets_angular_material_examples_sidenav_overview_sidenav_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/sidenav-overview/sidenav-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_85__assets_angular_material_examples_slide_toggle_configurable_slide_toggle_configurable_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/slide-toggle-configurable/slide-toggle-configurable-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_86__assets_angular_material_examples_slide_toggle_forms_slide_toggle_forms_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/slide-toggle-forms/slide-toggle-forms-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_87__assets_angular_material_examples_slide_toggle_overview_slide_toggle_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/slide-toggle-overview/slide-toggle-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_88__assets_angular_material_examples_slider_configurable_slider_configurable_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/slider-configurable/slider-configurable-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_89__assets_angular_material_examples_slider_overview_slider_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/slider-overview/slider-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_90__assets_angular_material_examples_snack_bar_component_snack_bar_component_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/snack-bar-component/snack-bar-component-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_91__assets_angular_material_examples_snack_bar_overview_snack_bar_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/snack-bar-overview/snack-bar-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_92__assets_angular_material_examples_sort_overview_sort_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/sort-overview/sort-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_93__assets_angular_material_examples_stepper_overview_stepper_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/stepper-overview/stepper-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_94__assets_angular_material_examples_table_basic_table_basic_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/table-basic/table-basic-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_95__assets_angular_material_examples_table_filtering_table_filtering_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/table-filtering/table-filtering-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_96__assets_angular_material_examples_table_http_table_http_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/table-http/table-http-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_97__assets_angular_material_examples_table_overview_table_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/table-overview/table-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_98__assets_angular_material_examples_table_pagination_table_pagination_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/table-pagination/table-pagination-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_99__assets_angular_material_examples_table_sorting_table_sorting_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/table-sorting/table-sorting-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_100__assets_angular_material_examples_tabs_overview_tabs_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/tabs-overview/tabs-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_101__assets_angular_material_examples_tabs_template_label_tabs_template_label_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/tabs-template-label/tabs-template-label-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_102__assets_angular_material_examples_toolbar_multirow_toolbar_multirow_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/toolbar-multirow/toolbar-multirow-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_103__assets_angular_material_examples_toolbar_overview_toolbar_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/toolbar-overview/toolbar-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_104__assets_angular_material_examples_tooltip_overview_tooltip_overview_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/tooltip-overview/tooltip-overview-example.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_105__assets_angular_material_examples_tooltip_position_tooltip_position_example__ = __webpack_require__("../../../../../src/assets/angular-material-examples/tooltip-position/tooltip-position-example.ts");










































































































var COMPONENT_MAP = {
    'autocomplete': [
        'autocomplete-simple',
        'autocomplete-filter',
        'autocomplete-display',
        'autocomplete-overview'
    ],
    'checkbox': [
        'checkbox-overview',
        'checkbox-configurable'
    ],
    'datepicker': [
        'datepicker-overview',
        'datepicker-start-view',
        'datepicker-value',
        'datepicker-min-max',
        'datepicker-filter',
        'datepicker-events',
        'datepicker-disabled',
        'datepicker-touch',
        'datepicker-api',
        'datepicker-locale',
        'datepicker-moment',
        'datepicker-formats'
    ],
    'form-field': [
        'form-field-overview',
        'form-field-placeholder',
        'form-field-hint',
        'form-field-error',
        'form-field-prefix-suffix',
        'form-field-theming',
        'form-field-custom-control'
    ],
    'input': [
        'input-overview',
        'input-errors',
        'input-error-state-matcher',
        'input-autosize-textarea',
        'input-prefix-suffix',
        'input-hint',
        'input-clearable',
        'input-form'
    ],
    'radio-button': [
        'radio-overview',
        'radio-ng-model'
    ],
    'select': [
        'select-overview',
        'select-value-binding',
        'select-form',
        'select-hint-error',
        'select-disabled',
        'select-reset',
        'select-optgroup',
        'select-multiple',
        'select-custom-trigger',
        'select-no-ripple',
        'select-panel-class',
        'select-error-state-matcher'
    ],
    'slider': [
        'slider-overview',
        'slider-configurable'
    ],
    'slide-toggle': [
        'slide-toggle-overview',
        'slide-toggle-configurable',
        'slide-toggle-forms'
    ],
    'menu': [
        'menu-overview',
        'nested-menu',
        'menu-icons'
    ],
    'sidenav': [
        'sidenav-overview',
        'sidenav-fab'
    ],
    'toolbar': [
        'toolbar-overview',
        'toolbar-multirow'
    ],
    'list': [
        'list-overview',
        'list-sections',
        'list-selection'
    ],
    'grid-list': [
        'grid-list-overview',
        'grid-list-dynamic'
    ],
    'card': [
        'card-overview',
        'card-fancy'
    ],
    'stepper': [
        'stepper-overview'
    ],
    'tabs': [
        'tabs-overview',
        'tabs-template-label'
    ],
    'elevation': [
        'elevation-overview'
    ],
    'expansion-panel': [
        'expansion-overview',
        'expansion-steps'
    ],
    'button': [
        'button-overview',
        'button-types'
    ],
    'button-toggle': [
        'button-toggle-overview',
        'button-toggle-exclusive'
    ],
    'chips': [
        'chips-overview',
        'chips-input',
        'chips-stacked'
    ],
    'icon': [
        'icon-overview',
        'icon-svg'
    ],
    'progress-spinner': [
        'progress-spinner-overview',
        'progress-spinner-configurable'
    ],
    'progress-bar': [
        'progress-bar-determinate',
        'progress-bar-indeterminate',
        'progress-bar-query',
        'progress-bar-buffer',
        'progress-bar-configurable'
    ],
    'dialog': [
        'dialog-overview',
        'dialog-content',
        'dialog-data',
        'dialog-elements'
    ],
    'tooltip': [
        'tooltip-overview',
        'tooltip-position'
    ],
    'snackbar': [
        'snack-bar-overview'
    ],
    'data-table': [
        'table-overview',
        'table-basic',
        'table-filtering',
        'table-http'
    ],
    'sort-header': [
        'sort-overview',
        'table-sorting'
    ],
    'paginator': [
        'table-pagination'
    ]
};
var EXAMPLE_COMPONENTS = {
    'autocomplete-display': {
        title: 'Display value autocomplete',
        component: __WEBPACK_IMPORTED_MODULE_0__assets_angular_material_examples_autocomplete_display_autocomplete_display_example__["a" /* AutocompleteDisplayExample */],
        additionalFiles: null,
        selectorName: null
    },
    'autocomplete-filter': {
        title: 'Filter autocomplete',
        component: __WEBPACK_IMPORTED_MODULE_1__assets_angular_material_examples_autocomplete_filter_autocomplete_filter_example__["a" /* AutocompleteFilterExample */],
        additionalFiles: null,
        selectorName: null
    },
    'autocomplete-overview': {
        title: 'Autocomplete overview',
        component: __WEBPACK_IMPORTED_MODULE_2__assets_angular_material_examples_autocomplete_overview_autocomplete_overview_example__["a" /* AutocompleteOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'autocomplete-simple': {
        title: 'Simple autocomplete',
        component: __WEBPACK_IMPORTED_MODULE_3__assets_angular_material_examples_autocomplete_simple_autocomplete_simple_example__["a" /* AutocompleteSimpleExample */],
        additionalFiles: null,
        selectorName: null
    },
    'button-overview': {
        title: 'Basic buttons',
        component: __WEBPACK_IMPORTED_MODULE_4__assets_angular_material_examples_button_overview_button_overview_example__["a" /* ButtonOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'button-toggle-exclusive': {
        title: 'Exclusive selection',
        component: __WEBPACK_IMPORTED_MODULE_5__assets_angular_material_examples_button_toggle_exclusive_button_toggle_exclusive_example__["a" /* ButtonToggleExclusiveExample */],
        additionalFiles: null,
        selectorName: null
    },
    'button-toggle-overview': {
        title: 'Basic button-toggles',
        component: __WEBPACK_IMPORTED_MODULE_6__assets_angular_material_examples_button_toggle_overview_button_toggle_overview_example__["a" /* ButtonToggleOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'button-types': {
        title: 'Button varieties',
        component: __WEBPACK_IMPORTED_MODULE_7__assets_angular_material_examples_button_types_button_types_example__["a" /* ButtonTypesExample */],
        additionalFiles: null,
        selectorName: null
    },
    'card-fancy': {
        title: 'Card with multiple sections',
        component: __WEBPACK_IMPORTED_MODULE_8__assets_angular_material_examples_card_fancy_card_fancy_example__["a" /* CardFancyExample */],
        additionalFiles: null,
        selectorName: null
    },
    'card-overview': {
        title: 'Basic cards',
        component: __WEBPACK_IMPORTED_MODULE_9__assets_angular_material_examples_card_overview_card_overview_example__["a" /* CardOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'cdk-table-basic': {
        title: 'Basic CDK data-table',
        component: __WEBPACK_IMPORTED_MODULE_10__assets_angular_material_examples_cdk_table_basic_cdk_table_basic_example__["a" /* CdkTableBasicExample */],
        additionalFiles: null,
        selectorName: null
    },
    'checkbox-configurable': {
        title: 'Configurable checkbox',
        component: __WEBPACK_IMPORTED_MODULE_11__assets_angular_material_examples_checkbox_configurable_checkbox_configurable_example__["a" /* CheckboxConfigurableExample */],
        additionalFiles: null,
        selectorName: null
    },
    'checkbox-overview': {
        title: 'Basic checkboxes',
        component: __WEBPACK_IMPORTED_MODULE_12__assets_angular_material_examples_checkbox_overview_checkbox_overview_example__["a" /* CheckboxOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'chips-input': {
        title: 'Chips with input',
        component: __WEBPACK_IMPORTED_MODULE_13__assets_angular_material_examples_chips_input_chips_input_example__["a" /* ChipsInputExample */],
        additionalFiles: null,
        selectorName: null
    },
    'chips-overview': {
        title: 'Basic chips',
        component: __WEBPACK_IMPORTED_MODULE_14__assets_angular_material_examples_chips_overview_chips_overview_example__["a" /* ChipsOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'chips-stacked': {
        title: 'Stacked chips',
        component: __WEBPACK_IMPORTED_MODULE_15__assets_angular_material_examples_chips_stacked_chips_stacked_example__["a" /* ChipsStackedExample */],
        additionalFiles: null,
        selectorName: null
    },
    'datepicker-api': {
        title: 'Datepicker open method ',
        component: __WEBPACK_IMPORTED_MODULE_16__assets_angular_material_examples_datepicker_api_datepicker_api_example__["a" /* DatepickerApiExample */],
        additionalFiles: null,
        selectorName: null
    },
    'datepicker-disabled': {
        title: 'Disabled datepicker ',
        component: __WEBPACK_IMPORTED_MODULE_17__assets_angular_material_examples_datepicker_disabled_datepicker_disabled_example__["a" /* DatepickerDisabledExample */],
        additionalFiles: null,
        selectorName: null
    },
    'datepicker-events': {
        title: 'Datepicker input and change events ',
        component: __WEBPACK_IMPORTED_MODULE_18__assets_angular_material_examples_datepicker_events_datepicker_events_example__["a" /* DatepickerEventsExample */],
        additionalFiles: null,
        selectorName: null
    },
    'datepicker-filter': {
        title: 'Datepicker with filter validation ',
        component: __WEBPACK_IMPORTED_MODULE_19__assets_angular_material_examples_datepicker_filter_datepicker_filter_example__["a" /* DatepickerFilterExample */],
        additionalFiles: null,
        selectorName: null
    },
    'datepicker-formats': {
        title: 'Datepicker with custom formats ',
        component: __WEBPACK_IMPORTED_MODULE_20__assets_angular_material_examples_datepicker_formats_datepicker_formats_example__["a" /* DatepickerFormatsExample */],
        additionalFiles: null,
        selectorName: null
    },
    'datepicker-locale': {
        title: 'Datepicker with different locale ',
        component: __WEBPACK_IMPORTED_MODULE_21__assets_angular_material_examples_datepicker_locale_datepicker_locale_example__["a" /* DatepickerLocaleExample */],
        additionalFiles: null,
        selectorName: null
    },
    'datepicker-min-max': {
        title: 'Datepicker with min & max validation ',
        component: __WEBPACK_IMPORTED_MODULE_22__assets_angular_material_examples_datepicker_min_max_datepicker_min_max_example__["a" /* DatepickerMinMaxExample */],
        additionalFiles: null,
        selectorName: null
    },
    'datepicker-moment': {
        title: 'Datepicker that uses Moment.js dates ',
        component: __WEBPACK_IMPORTED_MODULE_23__assets_angular_material_examples_datepicker_moment_datepicker_moment_example__["a" /* DatepickerMomentExample */],
        additionalFiles: null,
        selectorName: null
    },
    'datepicker-overview': {
        title: 'Basic datepicker ',
        component: __WEBPACK_IMPORTED_MODULE_24__assets_angular_material_examples_datepicker_overview_datepicker_overview_example__["a" /* DatepickerOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'datepicker-start-view': {
        title: 'Datepicker start date ',
        component: __WEBPACK_IMPORTED_MODULE_25__assets_angular_material_examples_datepicker_start_view_datepicker_start_view_example__["a" /* DatepickerStartViewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'datepicker-touch': {
        title: 'Datepicker touch UI ',
        component: __WEBPACK_IMPORTED_MODULE_26__assets_angular_material_examples_datepicker_touch_datepicker_touch_example__["a" /* DatepickerTouchExample */],
        additionalFiles: null,
        selectorName: null
    },
    'datepicker-value': {
        title: 'Datepicker selected value ',
        component: __WEBPACK_IMPORTED_MODULE_27__assets_angular_material_examples_datepicker_value_datepicker_value_example__["a" /* DatepickerValueExample */],
        additionalFiles: null,
        selectorName: null
    },
    'dialog-content': {
        title: 'Dialog with header, scrollable content and actions',
        component: __WEBPACK_IMPORTED_MODULE_28__assets_angular_material_examples_dialog_content_dialog_content_example__["a" /* DialogContentExample */],
        additionalFiles: ['dialog-content-example-dialog.html'],
        selectorName: 'DialogContentExample, DialogContentExampleDialog'
    },
    'dialog-data': {
        title: 'Injecting data when opening a dialog',
        component: __WEBPACK_IMPORTED_MODULE_29__assets_angular_material_examples_dialog_data_dialog_data_example__["a" /* DialogDataExample */],
        additionalFiles: ['dialog-data-example-dialog.html'],
        selectorName: 'DialogDataExample, DialogDataExampleDialog'
    },
    'dialog-elements': {
        title: 'Dialog elements',
        component: __WEBPACK_IMPORTED_MODULE_30__assets_angular_material_examples_dialog_elements_dialog_elements_example__["a" /* DialogElementsExample */],
        additionalFiles: ['dialog-elements-example-dialog.html'],
        selectorName: 'DialogElementsExample, DialogElementsExampleDialog'
    },
    'dialog-overview': {
        title: 'Dialog Overview',
        component: __WEBPACK_IMPORTED_MODULE_31__assets_angular_material_examples_dialog_overview_dialog_overview_example__["a" /* DialogOverviewExample */],
        additionalFiles: ['dialog-overview-example-dialog.html'],
        selectorName: 'DialogOverviewExample, DialogOverviewExampleDialog'
    },
    'elevation-overview': {
        title: 'Elevation CSS classes',
        component: __WEBPACK_IMPORTED_MODULE_32__assets_angular_material_examples_elevation_overview_elevation_overview_example__["a" /* ElevationOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'expansion-overview': {
        title: 'Basic expansion panel',
        component: __WEBPACK_IMPORTED_MODULE_33__assets_angular_material_examples_expansion_overview_expansion_overview_example__["a" /* ExpansionOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'expansion-steps': {
        title: 'Expansion panel as accordion',
        component: __WEBPACK_IMPORTED_MODULE_34__assets_angular_material_examples_expansion_steps_expansion_steps_example__["a" /* ExpansionStepsExample */],
        additionalFiles: null,
        selectorName: null
    },
    'form-field-custom-control': {
        title: 'Form field with custom telephone number input control. ',
        component: __WEBPACK_IMPORTED_MODULE_35__assets_angular_material_examples_form_field_custom_control_form_field_custom_control_example__["a" /* FormFieldCustomControlExample */],
        additionalFiles: ['form-field-custom-control-example.html'],
        selectorName: 'FormFieldCustomControlExample, MyTelInput'
    },
    'form-field-error': {
        title: 'Form field with error messages ',
        component: __WEBPACK_IMPORTED_MODULE_36__assets_angular_material_examples_form_field_error_form_field_error_example__["a" /* FormFieldErrorExample */],
        additionalFiles: null,
        selectorName: null
    },
    'form-field-hint': {
        title: 'Form field with hints ',
        component: __WEBPACK_IMPORTED_MODULE_37__assets_angular_material_examples_form_field_hint_form_field_hint_example__["a" /* FormFieldHintExample */],
        additionalFiles: null,
        selectorName: null
    },
    'form-field-overview': {
        title: 'Simple form field ',
        component: __WEBPACK_IMPORTED_MODULE_38__assets_angular_material_examples_form_field_overview_form_field_overview_example__["a" /* FormFieldOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'form-field-placeholder': {
        title: 'Form field with placeholder ',
        component: __WEBPACK_IMPORTED_MODULE_39__assets_angular_material_examples_form_field_placeholder_form_field_placeholder_example__["a" /* FormFieldPlaceholderExample */],
        additionalFiles: null,
        selectorName: null
    },
    'form-field-prefix-suffix': {
        title: 'Form field with prefix & suffix ',
        component: __WEBPACK_IMPORTED_MODULE_40__assets_angular_material_examples_form_field_prefix_suffix_form_field_prefix_suffix_example__["a" /* FormFieldPrefixSuffixExample */],
        additionalFiles: null,
        selectorName: null
    },
    'form-field-theming': {
        title: 'Form field theming ',
        component: __WEBPACK_IMPORTED_MODULE_41__assets_angular_material_examples_form_field_theming_form_field_theming_example__["a" /* FormFieldThemingExample */],
        additionalFiles: null,
        selectorName: null
    },
    'grid-list-dynamic': {
        title: 'Dynamic grid-list',
        component: __WEBPACK_IMPORTED_MODULE_42__assets_angular_material_examples_grid_list_dynamic_grid_list_dynamic_example__["a" /* GridListDynamicExample */],
        additionalFiles: null,
        selectorName: null
    },
    'grid-list-overview': {
        title: 'Basic grid-list',
        component: __WEBPACK_IMPORTED_MODULE_43__assets_angular_material_examples_grid_list_overview_grid_list_overview_example__["a" /* GridListOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'icon-overview': {
        title: 'Basic icons',
        component: __WEBPACK_IMPORTED_MODULE_44__assets_angular_material_examples_icon_overview_icon_overview_example__["a" /* IconOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'icon-svg': {
        title: 'SVG icons',
        component: __WEBPACK_IMPORTED_MODULE_45__assets_angular_material_examples_icon_svg_icon_svg_example__["a" /* IconSvgExample */],
        additionalFiles: null,
        selectorName: null
    },
    'input-autosize-textarea': {
        title: 'Auto-resizing textarea ',
        component: __WEBPACK_IMPORTED_MODULE_46__assets_angular_material_examples_input_autosize_textarea_input_autosize_textarea_example__["a" /* InputAutosizeTextareaExample */],
        additionalFiles: null,
        selectorName: null
    },
    'input-clearable': {
        title: 'Input with a clear button',
        component: __WEBPACK_IMPORTED_MODULE_47__assets_angular_material_examples_input_clearable_input_clearable_example__["a" /* InputClearableExample */],
        additionalFiles: null,
        selectorName: null
    },
    'input-error-state-matcher': {
        title: 'Input with a custom ErrorStateMatcher ',
        component: __WEBPACK_IMPORTED_MODULE_48__assets_angular_material_examples_input_error_state_matcher_input_error_state_matcher_example__["a" /* InputErrorStateMatcherExample */],
        additionalFiles: null,
        selectorName: null
    },
    'input-errors': {
        title: 'Input with error messages',
        component: __WEBPACK_IMPORTED_MODULE_49__assets_angular_material_examples_input_errors_input_errors_example__["a" /* InputErrorsExample */],
        additionalFiles: null,
        selectorName: null
    },
    'input-form': {
        title: 'Inputs in a form',
        component: __WEBPACK_IMPORTED_MODULE_50__assets_angular_material_examples_input_form_input_form_example__["a" /* InputFormExample */],
        additionalFiles: null,
        selectorName: null
    },
    'input-hint': {
        title: 'Input with hints',
        component: __WEBPACK_IMPORTED_MODULE_51__assets_angular_material_examples_input_hint_input_hint_example__["a" /* InputHintExample */],
        additionalFiles: null,
        selectorName: null
    },
    'input-overview': {
        title: 'Basic Inputs',
        component: __WEBPACK_IMPORTED_MODULE_52__assets_angular_material_examples_input_overview_input_overview_example__["a" /* InputOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'input-prefix-suffix': {
        title: 'Inputs with prefixes and suffixes',
        component: __WEBPACK_IMPORTED_MODULE_53__assets_angular_material_examples_input_prefix_suffix_input_prefix_suffix_example__["a" /* InputPrefixSuffixExample */],
        additionalFiles: null,
        selectorName: null
    },
    'list-overview': {
        title: 'Basic list',
        component: __WEBPACK_IMPORTED_MODULE_54__assets_angular_material_examples_list_overview_list_overview_example__["a" /* ListOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'list-sections': {
        title: 'List with sections',
        component: __WEBPACK_IMPORTED_MODULE_55__assets_angular_material_examples_list_sections_list_sections_example__["a" /* ListSectionsExample */],
        additionalFiles: null,
        selectorName: null
    },
    'list-selection': {
        title: 'List with selection',
        component: __WEBPACK_IMPORTED_MODULE_56__assets_angular_material_examples_list_selection_list_selection_example__["a" /* ListSelectionExample */],
        additionalFiles: null,
        selectorName: null
    },
    'menu-icons': {
        title: 'Menu with icons',
        component: __WEBPACK_IMPORTED_MODULE_57__assets_angular_material_examples_menu_icons_menu_icons_example__["a" /* MenuIconsExample */],
        additionalFiles: null,
        selectorName: null
    },
    'menu-overview': {
        title: 'Basic menu',
        component: __WEBPACK_IMPORTED_MODULE_58__assets_angular_material_examples_menu_overview_menu_overview_example__["a" /* MenuOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'nested-menu': {
        title: 'Nested menu',
        component: __WEBPACK_IMPORTED_MODULE_59__assets_angular_material_examples_nested_menu_nested_menu_example__["a" /* NestedMenuExample */],
        additionalFiles: null,
        selectorName: null
    },
    'paginator-configurable': {
        title: 'Configurable paginator',
        component: __WEBPACK_IMPORTED_MODULE_60__assets_angular_material_examples_paginator_configurable_paginator_configurable_example__["a" /* PaginatorConfigurableExample */],
        additionalFiles: null,
        selectorName: null
    },
    'paginator-overview': {
        title: 'Paginator',
        component: __WEBPACK_IMPORTED_MODULE_61__assets_angular_material_examples_paginator_overview_paginator_overview_example__["a" /* PaginatorOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'progress-bar-buffer': {
        title: 'Buffer progress-bar',
        component: __WEBPACK_IMPORTED_MODULE_62__assets_angular_material_examples_progress_bar_buffer_progress_bar_buffer_example__["a" /* ProgressBarBufferExample */],
        additionalFiles: null,
        selectorName: null
    },
    'progress-bar-configurable': {
        title: 'Configurable progress-bar',
        component: __WEBPACK_IMPORTED_MODULE_63__assets_angular_material_examples_progress_bar_configurable_progress_bar_configurable_example__["a" /* ProgressBarConfigurableExample */],
        additionalFiles: null,
        selectorName: null
    },
    'progress-bar-determinate': {
        title: 'Determinate progress-bar',
        component: __WEBPACK_IMPORTED_MODULE_64__assets_angular_material_examples_progress_bar_determinate_progress_bar_determinate_example__["a" /* ProgressBarDeterminateExample */],
        additionalFiles: null,
        selectorName: null
    },
    'progress-bar-indeterminate': {
        title: 'Indeterminate progress-bar',
        component: __WEBPACK_IMPORTED_MODULE_65__assets_angular_material_examples_progress_bar_indeterminate_progress_bar_indeterminate_example__["a" /* ProgressBarIndeterminateExample */],
        additionalFiles: null,
        selectorName: null
    },
    'progress-bar-query': {
        title: 'Query progress-bar',
        component: __WEBPACK_IMPORTED_MODULE_66__assets_angular_material_examples_progress_bar_query_progress_bar_query_example__["a" /* ProgressBarQueryExample */],
        additionalFiles: null,
        selectorName: null
    },
    'progress-spinner-configurable': {
        title: 'Configurable progress spinner',
        component: __WEBPACK_IMPORTED_MODULE_67__assets_angular_material_examples_progress_spinner_configurable_progress_spinner_configurable_example__["a" /* ProgressSpinnerConfigurableExample */],
        additionalFiles: null,
        selectorName: null
    },
    'progress-spinner-overview': {
        title: 'Basic progress-spinner',
        component: __WEBPACK_IMPORTED_MODULE_68__assets_angular_material_examples_progress_spinner_overview_progress_spinner_overview_example__["a" /* ProgressSpinnerOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'radio-ng-model': {
        title: 'Radios with ngModel',
        component: __WEBPACK_IMPORTED_MODULE_69__assets_angular_material_examples_radio_ng_model_radio_ng_model_example__["a" /* RadioNgModelExample */],
        additionalFiles: null,
        selectorName: null
    },
    'radio-overview': {
        title: 'Basic radios',
        component: __WEBPACK_IMPORTED_MODULE_70__assets_angular_material_examples_radio_overview_radio_overview_example__["a" /* RadioOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'select-custom-trigger': {
        title: 'Select with custom trigger text ',
        component: __WEBPACK_IMPORTED_MODULE_71__assets_angular_material_examples_select_custom_trigger_select_custom_trigger_example__["a" /* SelectCustomTriggerExample */],
        additionalFiles: null,
        selectorName: null
    },
    'select-disabled': {
        title: 'Disabled select ',
        component: __WEBPACK_IMPORTED_MODULE_72__assets_angular_material_examples_select_disabled_select_disabled_example__["a" /* SelectDisabledExample */],
        additionalFiles: null,
        selectorName: null
    },
    'select-error-state-matcher': {
        title: 'Select with a custom ErrorStateMatcher ',
        component: __WEBPACK_IMPORTED_MODULE_73__assets_angular_material_examples_select_error_state_matcher_select_error_state_matcher_example__["a" /* SelectErrorStateMatcherExample */],
        additionalFiles: null,
        selectorName: null
    },
    'select-form': {
        title: 'Select in a form',
        component: __WEBPACK_IMPORTED_MODULE_74__assets_angular_material_examples_select_form_select_form_example__["a" /* SelectFormExample */],
        additionalFiles: null,
        selectorName: null
    },
    'select-hint-error': {
        title: 'Select with form field features ',
        component: __WEBPACK_IMPORTED_MODULE_75__assets_angular_material_examples_select_hint_error_select_hint_error_example__["a" /* SelectHintErrorExample */],
        additionalFiles: null,
        selectorName: null
    },
    'select-multiple': {
        title: 'Select with multiple selection ',
        component: __WEBPACK_IMPORTED_MODULE_76__assets_angular_material_examples_select_multiple_select_multiple_example__["a" /* SelectMultipleExample */],
        additionalFiles: null,
        selectorName: null
    },
    'select-no-ripple': {
        title: 'Select with no option ripple ',
        component: __WEBPACK_IMPORTED_MODULE_77__assets_angular_material_examples_select_no_ripple_select_no_ripple_example__["a" /* SelectNoRippleExample */],
        additionalFiles: null,
        selectorName: null
    },
    'select-optgroup': {
        title: 'Select with option groups ',
        component: __WEBPACK_IMPORTED_MODULE_78__assets_angular_material_examples_select_optgroup_select_optgroup_example__["a" /* SelectOptgroupExample */],
        additionalFiles: null,
        selectorName: null
    },
    'select-overview': {
        title: 'Basic select',
        component: __WEBPACK_IMPORTED_MODULE_79__assets_angular_material_examples_select_overview_select_overview_example__["a" /* SelectOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'select-panel-class': {
        title: 'Select with custom panel styling',
        component: __WEBPACK_IMPORTED_MODULE_80__assets_angular_material_examples_select_panel_class_select_panel_class_example__["a" /* SelectPanelClassExample */],
        additionalFiles: null,
        selectorName: null
    },
    'select-reset': {
        title: 'Select with reset option ',
        component: __WEBPACK_IMPORTED_MODULE_81__assets_angular_material_examples_select_reset_select_reset_example__["a" /* SelectResetExample */],
        additionalFiles: null,
        selectorName: null
    },
    'select-value-binding': {
        title: 'Select with 2-way value binding ',
        component: __WEBPACK_IMPORTED_MODULE_82__assets_angular_material_examples_select_value_binding_select_value_binding_example__["a" /* SelectValueBindingExample */],
        additionalFiles: null,
        selectorName: null
    },
    'sidenav-fab': {
        title: 'Sidenav with a FAB',
        component: __WEBPACK_IMPORTED_MODULE_83__assets_angular_material_examples_sidenav_fab_sidenav_fab_example__["a" /* SidenavFabExample */],
        additionalFiles: null,
        selectorName: null
    },
    'sidenav-overview': {
        title: 'Basic sidenav',
        component: __WEBPACK_IMPORTED_MODULE_84__assets_angular_material_examples_sidenav_overview_sidenav_overview_example__["a" /* SidenavOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'slide-toggle-configurable': {
        title: 'Configurable slide-toggle',
        component: __WEBPACK_IMPORTED_MODULE_85__assets_angular_material_examples_slide_toggle_configurable_slide_toggle_configurable_example__["a" /* SlideToggleConfigurableExample */],
        additionalFiles: null,
        selectorName: null
    },
    'slide-toggle-forms': {
        title: 'Slide-toggle with forms',
        component: __WEBPACK_IMPORTED_MODULE_86__assets_angular_material_examples_slide_toggle_forms_slide_toggle_forms_example__["a" /* SlideToggleFormsExample */],
        additionalFiles: null,
        selectorName: null
    },
    'slide-toggle-overview': {
        title: 'Basic slide-toggles',
        component: __WEBPACK_IMPORTED_MODULE_87__assets_angular_material_examples_slide_toggle_overview_slide_toggle_overview_example__["a" /* SlideToggleOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'slider-configurable': {
        title: 'Configurable slider',
        component: __WEBPACK_IMPORTED_MODULE_88__assets_angular_material_examples_slider_configurable_slider_configurable_example__["a" /* SliderConfigurableExample */],
        additionalFiles: null,
        selectorName: null
    },
    'slider-overview': {
        title: 'Basic slider',
        component: __WEBPACK_IMPORTED_MODULE_89__assets_angular_material_examples_slider_overview_slider_overview_example__["a" /* SliderOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'snack-bar-component': {
        title: 'Snack-bar with a custom component',
        component: __WEBPACK_IMPORTED_MODULE_90__assets_angular_material_examples_snack_bar_component_snack_bar_component_example__["b" /* SnackBarComponentExample */],
        additionalFiles: ['snack-bar-component-example-snack.html'],
        selectorName: 'SnackBarComponentExample, PizzaPartyComponent'
    },
    'snack-bar-overview': {
        title: 'Basic snack-bar',
        component: __WEBPACK_IMPORTED_MODULE_91__assets_angular_material_examples_snack_bar_overview_snack_bar_overview_example__["a" /* SnackBarOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'sort-overview': {
        title: 'Sorting overview',
        component: __WEBPACK_IMPORTED_MODULE_92__assets_angular_material_examples_sort_overview_sort_overview_example__["a" /* SortOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'stepper-overview': {
        title: 'Stepper overview',
        component: __WEBPACK_IMPORTED_MODULE_93__assets_angular_material_examples_stepper_overview_stepper_overview_example__["a" /* StepperOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'table-basic': {
        title: 'Basic table',
        component: __WEBPACK_IMPORTED_MODULE_94__assets_angular_material_examples_table_basic_table_basic_example__["a" /* TableBasicExample */],
        additionalFiles: null,
        selectorName: null
    },
    'table-filtering': {
        title: 'Table with filtering',
        component: __WEBPACK_IMPORTED_MODULE_95__assets_angular_material_examples_table_filtering_table_filtering_example__["a" /* TableFilteringExample */],
        additionalFiles: null,
        selectorName: null
    },
    'table-http': {
        title: 'Table retrieving data through HTTP',
        component: __WEBPACK_IMPORTED_MODULE_96__assets_angular_material_examples_table_http_table_http_example__["a" /* TableHttpExample */],
        additionalFiles: null,
        selectorName: null
    },
    'table-overview': {
        title: 'Data table with sorting, pagination, and filtering.',
        component: __WEBPACK_IMPORTED_MODULE_97__assets_angular_material_examples_table_overview_table_overview_example__["a" /* TableOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'table-pagination': {
        title: 'Table with pagination',
        component: __WEBPACK_IMPORTED_MODULE_98__assets_angular_material_examples_table_pagination_table_pagination_example__["a" /* TablePaginationExample */],
        additionalFiles: null,
        selectorName: null
    },
    'table-sorting': {
        title: 'Table with sorting',
        component: __WEBPACK_IMPORTED_MODULE_99__assets_angular_material_examples_table_sorting_table_sorting_example__["a" /* TableSortingExample */],
        additionalFiles: null,
        selectorName: null
    },
    'tabs-overview': {
        title: 'Basic tabs',
        component: __WEBPACK_IMPORTED_MODULE_100__assets_angular_material_examples_tabs_overview_tabs_overview_example__["a" /* TabsOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'tabs-template-label': {
        title: 'Complex Example',
        component: __WEBPACK_IMPORTED_MODULE_101__assets_angular_material_examples_tabs_template_label_tabs_template_label_example__["a" /* TabsTemplateLabelExample */],
        additionalFiles: null,
        selectorName: null
    },
    'toolbar-multirow': {
        title: 'Multi-row toolbar',
        component: __WEBPACK_IMPORTED_MODULE_102__assets_angular_material_examples_toolbar_multirow_toolbar_multirow_example__["a" /* ToolbarMultirowExample */],
        additionalFiles: null,
        selectorName: null
    },
    'toolbar-overview': {
        title: 'Basic toolbar',
        component: __WEBPACK_IMPORTED_MODULE_103__assets_angular_material_examples_toolbar_overview_toolbar_overview_example__["a" /* ToolbarOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'tooltip-overview': {
        title: 'Basic tooltip',
        component: __WEBPACK_IMPORTED_MODULE_104__assets_angular_material_examples_tooltip_overview_tooltip_overview_example__["a" /* TooltipOverviewExample */],
        additionalFiles: null,
        selectorName: null
    },
    'tooltip-position': {
        title: 'Tooltip with custom position',
        component: __WEBPACK_IMPORTED_MODULE_105__assets_angular_material_examples_tooltip_position_tooltip_position_example__["a" /* TooltipPositionExample */],
        additionalFiles: null,
        selectorName: null
    }
};
var EXAMPLE_LIST = [
    __WEBPACK_IMPORTED_MODULE_0__assets_angular_material_examples_autocomplete_display_autocomplete_display_example__["a" /* AutocompleteDisplayExample */],
    __WEBPACK_IMPORTED_MODULE_1__assets_angular_material_examples_autocomplete_filter_autocomplete_filter_example__["a" /* AutocompleteFilterExample */],
    __WEBPACK_IMPORTED_MODULE_2__assets_angular_material_examples_autocomplete_overview_autocomplete_overview_example__["a" /* AutocompleteOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_3__assets_angular_material_examples_autocomplete_simple_autocomplete_simple_example__["a" /* AutocompleteSimpleExample */],
    __WEBPACK_IMPORTED_MODULE_4__assets_angular_material_examples_button_overview_button_overview_example__["a" /* ButtonOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_5__assets_angular_material_examples_button_toggle_exclusive_button_toggle_exclusive_example__["a" /* ButtonToggleExclusiveExample */],
    __WEBPACK_IMPORTED_MODULE_6__assets_angular_material_examples_button_toggle_overview_button_toggle_overview_example__["a" /* ButtonToggleOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_7__assets_angular_material_examples_button_types_button_types_example__["a" /* ButtonTypesExample */],
    __WEBPACK_IMPORTED_MODULE_8__assets_angular_material_examples_card_fancy_card_fancy_example__["a" /* CardFancyExample */],
    __WEBPACK_IMPORTED_MODULE_9__assets_angular_material_examples_card_overview_card_overview_example__["a" /* CardOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_10__assets_angular_material_examples_cdk_table_basic_cdk_table_basic_example__["a" /* CdkTableBasicExample */],
    __WEBPACK_IMPORTED_MODULE_11__assets_angular_material_examples_checkbox_configurable_checkbox_configurable_example__["a" /* CheckboxConfigurableExample */],
    __WEBPACK_IMPORTED_MODULE_12__assets_angular_material_examples_checkbox_overview_checkbox_overview_example__["a" /* CheckboxOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_13__assets_angular_material_examples_chips_input_chips_input_example__["a" /* ChipsInputExample */],
    __WEBPACK_IMPORTED_MODULE_14__assets_angular_material_examples_chips_overview_chips_overview_example__["a" /* ChipsOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_15__assets_angular_material_examples_chips_stacked_chips_stacked_example__["a" /* ChipsStackedExample */],
    __WEBPACK_IMPORTED_MODULE_16__assets_angular_material_examples_datepicker_api_datepicker_api_example__["a" /* DatepickerApiExample */],
    __WEBPACK_IMPORTED_MODULE_17__assets_angular_material_examples_datepicker_disabled_datepicker_disabled_example__["a" /* DatepickerDisabledExample */],
    __WEBPACK_IMPORTED_MODULE_18__assets_angular_material_examples_datepicker_events_datepicker_events_example__["a" /* DatepickerEventsExample */],
    __WEBPACK_IMPORTED_MODULE_19__assets_angular_material_examples_datepicker_filter_datepicker_filter_example__["a" /* DatepickerFilterExample */],
    __WEBPACK_IMPORTED_MODULE_20__assets_angular_material_examples_datepicker_formats_datepicker_formats_example__["a" /* DatepickerFormatsExample */],
    __WEBPACK_IMPORTED_MODULE_21__assets_angular_material_examples_datepicker_locale_datepicker_locale_example__["a" /* DatepickerLocaleExample */],
    __WEBPACK_IMPORTED_MODULE_22__assets_angular_material_examples_datepicker_min_max_datepicker_min_max_example__["a" /* DatepickerMinMaxExample */],
    __WEBPACK_IMPORTED_MODULE_23__assets_angular_material_examples_datepicker_moment_datepicker_moment_example__["a" /* DatepickerMomentExample */],
    __WEBPACK_IMPORTED_MODULE_24__assets_angular_material_examples_datepicker_overview_datepicker_overview_example__["a" /* DatepickerOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_25__assets_angular_material_examples_datepicker_start_view_datepicker_start_view_example__["a" /* DatepickerStartViewExample */],
    __WEBPACK_IMPORTED_MODULE_26__assets_angular_material_examples_datepicker_touch_datepicker_touch_example__["a" /* DatepickerTouchExample */],
    __WEBPACK_IMPORTED_MODULE_27__assets_angular_material_examples_datepicker_value_datepicker_value_example__["a" /* DatepickerValueExample */],
    __WEBPACK_IMPORTED_MODULE_28__assets_angular_material_examples_dialog_content_dialog_content_example__["b" /* DialogContentExampleDialog */], __WEBPACK_IMPORTED_MODULE_28__assets_angular_material_examples_dialog_content_dialog_content_example__["a" /* DialogContentExample */],
    __WEBPACK_IMPORTED_MODULE_29__assets_angular_material_examples_dialog_data_dialog_data_example__["b" /* DialogDataExampleDialog */], __WEBPACK_IMPORTED_MODULE_29__assets_angular_material_examples_dialog_data_dialog_data_example__["a" /* DialogDataExample */],
    __WEBPACK_IMPORTED_MODULE_30__assets_angular_material_examples_dialog_elements_dialog_elements_example__["b" /* DialogElementsExampleDialog */], __WEBPACK_IMPORTED_MODULE_30__assets_angular_material_examples_dialog_elements_dialog_elements_example__["a" /* DialogElementsExample */],
    __WEBPACK_IMPORTED_MODULE_31__assets_angular_material_examples_dialog_overview_dialog_overview_example__["b" /* DialogOverviewExampleDialog */], __WEBPACK_IMPORTED_MODULE_31__assets_angular_material_examples_dialog_overview_dialog_overview_example__["a" /* DialogOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_32__assets_angular_material_examples_elevation_overview_elevation_overview_example__["a" /* ElevationOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_33__assets_angular_material_examples_expansion_overview_expansion_overview_example__["a" /* ExpansionOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_34__assets_angular_material_examples_expansion_steps_expansion_steps_example__["a" /* ExpansionStepsExample */],
    __WEBPACK_IMPORTED_MODULE_35__assets_angular_material_examples_form_field_custom_control_form_field_custom_control_example__["b" /* MyTelInput */], __WEBPACK_IMPORTED_MODULE_35__assets_angular_material_examples_form_field_custom_control_form_field_custom_control_example__["a" /* FormFieldCustomControlExample */],
    __WEBPACK_IMPORTED_MODULE_36__assets_angular_material_examples_form_field_error_form_field_error_example__["a" /* FormFieldErrorExample */],
    __WEBPACK_IMPORTED_MODULE_37__assets_angular_material_examples_form_field_hint_form_field_hint_example__["a" /* FormFieldHintExample */],
    __WEBPACK_IMPORTED_MODULE_38__assets_angular_material_examples_form_field_overview_form_field_overview_example__["a" /* FormFieldOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_39__assets_angular_material_examples_form_field_placeholder_form_field_placeholder_example__["a" /* FormFieldPlaceholderExample */],
    __WEBPACK_IMPORTED_MODULE_40__assets_angular_material_examples_form_field_prefix_suffix_form_field_prefix_suffix_example__["a" /* FormFieldPrefixSuffixExample */],
    __WEBPACK_IMPORTED_MODULE_41__assets_angular_material_examples_form_field_theming_form_field_theming_example__["a" /* FormFieldThemingExample */],
    __WEBPACK_IMPORTED_MODULE_42__assets_angular_material_examples_grid_list_dynamic_grid_list_dynamic_example__["a" /* GridListDynamicExample */],
    __WEBPACK_IMPORTED_MODULE_43__assets_angular_material_examples_grid_list_overview_grid_list_overview_example__["a" /* GridListOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_44__assets_angular_material_examples_icon_overview_icon_overview_example__["a" /* IconOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_45__assets_angular_material_examples_icon_svg_icon_svg_example__["a" /* IconSvgExample */],
    __WEBPACK_IMPORTED_MODULE_46__assets_angular_material_examples_input_autosize_textarea_input_autosize_textarea_example__["a" /* InputAutosizeTextareaExample */],
    __WEBPACK_IMPORTED_MODULE_47__assets_angular_material_examples_input_clearable_input_clearable_example__["a" /* InputClearableExample */],
    __WEBPACK_IMPORTED_MODULE_48__assets_angular_material_examples_input_error_state_matcher_input_error_state_matcher_example__["a" /* InputErrorStateMatcherExample */],
    __WEBPACK_IMPORTED_MODULE_49__assets_angular_material_examples_input_errors_input_errors_example__["a" /* InputErrorsExample */],
    __WEBPACK_IMPORTED_MODULE_50__assets_angular_material_examples_input_form_input_form_example__["a" /* InputFormExample */],
    __WEBPACK_IMPORTED_MODULE_51__assets_angular_material_examples_input_hint_input_hint_example__["a" /* InputHintExample */],
    __WEBPACK_IMPORTED_MODULE_52__assets_angular_material_examples_input_overview_input_overview_example__["a" /* InputOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_53__assets_angular_material_examples_input_prefix_suffix_input_prefix_suffix_example__["a" /* InputPrefixSuffixExample */],
    __WEBPACK_IMPORTED_MODULE_54__assets_angular_material_examples_list_overview_list_overview_example__["a" /* ListOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_55__assets_angular_material_examples_list_sections_list_sections_example__["a" /* ListSectionsExample */],
    __WEBPACK_IMPORTED_MODULE_56__assets_angular_material_examples_list_selection_list_selection_example__["a" /* ListSelectionExample */],
    __WEBPACK_IMPORTED_MODULE_57__assets_angular_material_examples_menu_icons_menu_icons_example__["a" /* MenuIconsExample */],
    __WEBPACK_IMPORTED_MODULE_58__assets_angular_material_examples_menu_overview_menu_overview_example__["a" /* MenuOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_59__assets_angular_material_examples_nested_menu_nested_menu_example__["a" /* NestedMenuExample */],
    __WEBPACK_IMPORTED_MODULE_60__assets_angular_material_examples_paginator_configurable_paginator_configurable_example__["a" /* PaginatorConfigurableExample */],
    __WEBPACK_IMPORTED_MODULE_61__assets_angular_material_examples_paginator_overview_paginator_overview_example__["a" /* PaginatorOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_62__assets_angular_material_examples_progress_bar_buffer_progress_bar_buffer_example__["a" /* ProgressBarBufferExample */],
    __WEBPACK_IMPORTED_MODULE_63__assets_angular_material_examples_progress_bar_configurable_progress_bar_configurable_example__["a" /* ProgressBarConfigurableExample */],
    __WEBPACK_IMPORTED_MODULE_64__assets_angular_material_examples_progress_bar_determinate_progress_bar_determinate_example__["a" /* ProgressBarDeterminateExample */],
    __WEBPACK_IMPORTED_MODULE_65__assets_angular_material_examples_progress_bar_indeterminate_progress_bar_indeterminate_example__["a" /* ProgressBarIndeterminateExample */],
    __WEBPACK_IMPORTED_MODULE_66__assets_angular_material_examples_progress_bar_query_progress_bar_query_example__["a" /* ProgressBarQueryExample */],
    __WEBPACK_IMPORTED_MODULE_67__assets_angular_material_examples_progress_spinner_configurable_progress_spinner_configurable_example__["a" /* ProgressSpinnerConfigurableExample */],
    __WEBPACK_IMPORTED_MODULE_68__assets_angular_material_examples_progress_spinner_overview_progress_spinner_overview_example__["a" /* ProgressSpinnerOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_69__assets_angular_material_examples_radio_ng_model_radio_ng_model_example__["a" /* RadioNgModelExample */],
    __WEBPACK_IMPORTED_MODULE_70__assets_angular_material_examples_radio_overview_radio_overview_example__["a" /* RadioOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_71__assets_angular_material_examples_select_custom_trigger_select_custom_trigger_example__["a" /* SelectCustomTriggerExample */],
    __WEBPACK_IMPORTED_MODULE_72__assets_angular_material_examples_select_disabled_select_disabled_example__["a" /* SelectDisabledExample */],
    __WEBPACK_IMPORTED_MODULE_73__assets_angular_material_examples_select_error_state_matcher_select_error_state_matcher_example__["a" /* SelectErrorStateMatcherExample */],
    __WEBPACK_IMPORTED_MODULE_74__assets_angular_material_examples_select_form_select_form_example__["a" /* SelectFormExample */],
    __WEBPACK_IMPORTED_MODULE_75__assets_angular_material_examples_select_hint_error_select_hint_error_example__["a" /* SelectHintErrorExample */],
    __WEBPACK_IMPORTED_MODULE_76__assets_angular_material_examples_select_multiple_select_multiple_example__["a" /* SelectMultipleExample */],
    __WEBPACK_IMPORTED_MODULE_77__assets_angular_material_examples_select_no_ripple_select_no_ripple_example__["a" /* SelectNoRippleExample */],
    __WEBPACK_IMPORTED_MODULE_78__assets_angular_material_examples_select_optgroup_select_optgroup_example__["a" /* SelectOptgroupExample */],
    __WEBPACK_IMPORTED_MODULE_79__assets_angular_material_examples_select_overview_select_overview_example__["a" /* SelectOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_80__assets_angular_material_examples_select_panel_class_select_panel_class_example__["a" /* SelectPanelClassExample */],
    __WEBPACK_IMPORTED_MODULE_81__assets_angular_material_examples_select_reset_select_reset_example__["a" /* SelectResetExample */],
    __WEBPACK_IMPORTED_MODULE_82__assets_angular_material_examples_select_value_binding_select_value_binding_example__["a" /* SelectValueBindingExample */],
    __WEBPACK_IMPORTED_MODULE_83__assets_angular_material_examples_sidenav_fab_sidenav_fab_example__["a" /* SidenavFabExample */],
    __WEBPACK_IMPORTED_MODULE_84__assets_angular_material_examples_sidenav_overview_sidenav_overview_example__["a" /* SidenavOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_85__assets_angular_material_examples_slide_toggle_configurable_slide_toggle_configurable_example__["a" /* SlideToggleConfigurableExample */],
    __WEBPACK_IMPORTED_MODULE_86__assets_angular_material_examples_slide_toggle_forms_slide_toggle_forms_example__["a" /* SlideToggleFormsExample */],
    __WEBPACK_IMPORTED_MODULE_87__assets_angular_material_examples_slide_toggle_overview_slide_toggle_overview_example__["a" /* SlideToggleOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_88__assets_angular_material_examples_slider_configurable_slider_configurable_example__["a" /* SliderConfigurableExample */],
    __WEBPACK_IMPORTED_MODULE_89__assets_angular_material_examples_slider_overview_slider_overview_example__["a" /* SliderOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_90__assets_angular_material_examples_snack_bar_component_snack_bar_component_example__["a" /* PizzaPartyComponent */], __WEBPACK_IMPORTED_MODULE_90__assets_angular_material_examples_snack_bar_component_snack_bar_component_example__["b" /* SnackBarComponentExample */],
    __WEBPACK_IMPORTED_MODULE_91__assets_angular_material_examples_snack_bar_overview_snack_bar_overview_example__["a" /* SnackBarOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_92__assets_angular_material_examples_sort_overview_sort_overview_example__["a" /* SortOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_93__assets_angular_material_examples_stepper_overview_stepper_overview_example__["a" /* StepperOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_94__assets_angular_material_examples_table_basic_table_basic_example__["a" /* TableBasicExample */],
    __WEBPACK_IMPORTED_MODULE_95__assets_angular_material_examples_table_filtering_table_filtering_example__["a" /* TableFilteringExample */],
    __WEBPACK_IMPORTED_MODULE_96__assets_angular_material_examples_table_http_table_http_example__["a" /* TableHttpExample */],
    __WEBPACK_IMPORTED_MODULE_97__assets_angular_material_examples_table_overview_table_overview_example__["a" /* TableOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_98__assets_angular_material_examples_table_pagination_table_pagination_example__["a" /* TablePaginationExample */],
    __WEBPACK_IMPORTED_MODULE_99__assets_angular_material_examples_table_sorting_table_sorting_example__["a" /* TableSortingExample */],
    __WEBPACK_IMPORTED_MODULE_100__assets_angular_material_examples_tabs_overview_tabs_overview_example__["a" /* TabsOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_101__assets_angular_material_examples_tabs_template_label_tabs_template_label_example__["a" /* TabsTemplateLabelExample */],
    __WEBPACK_IMPORTED_MODULE_102__assets_angular_material_examples_toolbar_multirow_toolbar_multirow_example__["a" /* ToolbarMultirowExample */],
    __WEBPACK_IMPORTED_MODULE_103__assets_angular_material_examples_toolbar_overview_toolbar_overview_example__["a" /* ToolbarOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_104__assets_angular_material_examples_tooltip_overview_tooltip_overview_example__["a" /* TooltipOverviewExample */],
    __WEBPACK_IMPORTED_MODULE_105__assets_angular_material_examples_tooltip_position_tooltip_position_example__["a" /* TooltipPositionExample */]
];


/***/ }),

/***/ "../../../../../src/app/main/content/components/angular-material/example-viewer/example-viewer.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-viewer-wrapper mat-white-bg mat-elevation-z2\">\r\n\r\n    <div class=\"example-viewer-header\">\r\n\r\n        <div class=\"example-viewer-title\">{{exampleData?.title}}</div>\r\n\r\n        <button mat-icon-button type=\"button\" (click)=\"toggleSourceView()\"\r\n                [matTooltip]=\"'View source'\">\r\n            <mat-icon>\r\n                <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"100%\" height=\"100%\" viewBox=\"0 0 24 24\" fit=\"\"\r\n                     preserveAspectRatio=\"xMidYMid meet\" focusable=\"false\">\r\n                    <path fill=\"none\" d=\"M0 0h24v24H0V0z\"></path>\r\n                    <path\r\n                        d=\"M9.4 16.6L4.8 12l4.6-4.6L8 6l-6 6 6 6 1.4-1.4zm5.2 0l4.6-4.6-4.6-4.6L16 6l6 6-6 6-1.4-1.4z\"></path>\r\n                </svg>\r\n            </mat-icon>\r\n        </button>\r\n    </div>\r\n\r\n    <div class=\"example-viewer-source\" [fxShow]=\"showSource\">\r\n\r\n        <mat-tab-group [(selectedIndex)]=\"selectedIndex\">\r\n            <mat-tab label=\"HTML\"></mat-tab>\r\n            <mat-tab label=\"TS\"></mat-tab>\r\n            <mat-tab label=\"CSS\"></mat-tab>\r\n        </mat-tab-group>\r\n        <div class=\"tab-content\">\r\n            <section class=\"tab\" *ngIf=\"selectedIndex === 0\"\r\n                     [@animate]=\"{value:'*',params:{opacity:'0',duration:'200ms'}}\">\r\n                <button mat-icon-button type=\"button\" class=\"example-source-copy\"\r\n                        title=\"Copy example source\" aria-label=\"Copy example source to clipboard\"\r\n                        (click)=\"copySource(htmlView.el.nativeElement.innerText)\">\r\n                    <mat-icon>content_copy</mat-icon>\r\n                </button>\r\n                <fuse-highlight lang=\"html\"\r\n                                [path]=\"'/assets/angular-material-examples/'+example+'/'+example+'-example.html'\">\r\n                </fuse-highlight>\r\n            </section>\r\n\r\n            <section class=\"tab\" *ngIf=\"selectedIndex === 1\"\r\n                     [@animate]=\"{value:'*',params:{opacity:'0',duration:'200ms'}}\">\r\n                <button mat-icon-button type=\"button\" class=\"example-source-copy\"\r\n                        title=\"Copy example source\" aria-label=\"Copy example source to clipboard\"\r\n                        (click)=\"copySource(tsView.el.nativeElement.innerText)\">\r\n                    <mat-icon>content_copy</mat-icon>\r\n                </button>\r\n                <fuse-highlight lang=\"typescript\"\r\n                                [path]=\"'/assets/angular-material-examples/'+example+'/'+example+'-example.ts'\">\r\n                </fuse-highlight>\r\n            </section>\r\n\r\n            <section class=\"tab\" *ngIf=\"selectedIndex === 2\"\r\n                     [@animate]=\"{value:'*',params:{opacity:'0',duration:'200ms'}}\">\r\n                <button mat-icon-button type=\"button\" class=\"example-source-copy\"\r\n                        title=\"Copy example source\" aria-label=\"Copy example source to clipboard\"\r\n                        (click)=\"copySource(cssView.el.nativeElement.innerText)\">\r\n                    <mat-icon>content_copy</mat-icon>\r\n                </button>\r\n                <fuse-highlight lang=\"css\"\r\n                                [path]=\"'/assets/angular-material-examples/'+example+'/'+example+'-example.css'\">\r\n                </fuse-highlight>\r\n            </section>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"example-viewer-body\" [fxHide]=\"showSource\">\r\n        <!--<ng-template [portalHost]=\"selectedPortal\"></ng-template>-->\r\n        <!--<ng-template [cdkPortalHost]=\"selectedPortal\"></ng-template>-->\r\n        <div #previewContainer></div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/components/angular-material/example-viewer/example-viewer.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\nfuse-example-viewer {\n  display: block;\n  padding: 24px 0; }\nfuse-example-viewer .example-viewer-wrapper {\n    border: 1px solid rgba(0, 0, 0, 0.03);\n    -webkit-box-shadow: 0 2px 2px rgba(0, 0, 0, 0.24), 0 0 2px rgba(0, 0, 0, 0.12);\n            box-shadow: 0 2px 2px rgba(0, 0, 0, 0.24), 0 0 2px rgba(0, 0, 0, 0.12);\n    margin: 4px; }\nfuse-example-viewer .example-viewer-wrapper h3 {\n      margin-top: 10px; }\nfuse-example-viewer .example-viewer-header {\n    -ms-flex-line-pack: center;\n        align-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    padding: 8px 20px;\n    color: rgba(0, 0, 0, 0.54);\n    background: rgba(0, 0, 0, 0.03);\n    -webkit-box-flex: 1;\n        -ms-flex: 1 1 auto;\n            flex: 1 1 auto; }\nfuse-example-viewer .example-viewer-header .example-viewer-title {\n      -webkit-box-flex: 1;\n          -ms-flex: 1 1 auto;\n              flex: 1 1 auto; }\nfuse-example-viewer .example-viewer-source .tab-content {\n    background: #263238; }\nfuse-example-viewer .example-viewer-source .tab-content .tab {\n      position: relative; }\nfuse-example-viewer .example-viewer-source .tab-content .tab .example-source-copy {\n        position: absolute;\n        top: 8px;\n        display: none;\n        right: 8px; }\nfuse-example-viewer .example-viewer-source .tab-content .tab .example-source-copy mat-icon {\n          color: rgba(255, 255, 255, 0.87); }\nfuse-example-viewer .example-viewer-source .tab-content .tab:hover .example-source-copy {\n        display: inline-block; }\nfuse-example-viewer .example-viewer-source .tab-content .tab .example-source {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        padding: 0;\n        margin: 0;\n        min-height: 150px;\n        border-bottom: 1px solid rgba(0, 0, 0, 0.12); }\nfuse-example-viewer .example-viewer-source .tab-content .tab .example-source > pre {\n          width: 100%; }\nfuse-example-viewer .example-viewer-body {\n    padding: 24px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/components/angular-material/example-viewer/example-viewer.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FuseExampleViewerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_first__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/first.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_components_copier_copier_service__ = __webpack_require__("../../../../../src/app/core/components/copier/copier.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__example_components__ = __webpack_require__("../../../../../src/app/main/content/components/angular-material/example-components.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismjs_components_prism_scss__ = __webpack_require__("../../../../prismjs/components/prism-scss.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_prismjs_components_prism_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_prismjs_components_prism_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_prismjs_components_prism_typescript__ = __webpack_require__("../../../../prismjs/components/prism-typescript.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_prismjs_components_prism_typescript___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_prismjs_components_prism_typescript__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var FuseExampleViewerComponent = /** @class */ (function () {
    function FuseExampleViewerComponent(snackbar, copier, _resolver) {
        this.snackbar = snackbar;
        this.copier = copier;
        this._resolver = _resolver;
        this.selectedIndex = 0;
        /** Whether the source for the example is being displayed. */
        this.showSource = false;
    }
    Object.defineProperty(FuseExampleViewerComponent.prototype, "container", {
        get: function () {
            return this._previewContainer;
        },
        set: function (value) {
            this._previewContainer = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FuseExampleViewerComponent.prototype, "example", {
        get: function () {
            return this._example;
        },
        set: function (example) {
            if (example && __WEBPACK_IMPORTED_MODULE_4__example_components__["b" /* EXAMPLE_COMPONENTS */][example]) {
                this._example = example;
                this.exampleData = __WEBPACK_IMPORTED_MODULE_4__example_components__["b" /* EXAMPLE_COMPONENTS */][example];
            }
            else {
                console.log('MISSING EXAMPLE: ', example);
            }
        },
        enumerable: true,
        configurable: true
    });
    FuseExampleViewerComponent.prototype.toggleSourceView = function () {
        this.showSource = !this.showSource;
    };
    FuseExampleViewerComponent.prototype.copySource = function (text) {
        if (this.copier.copyText(text)) {
            this.snackbar.open('Code copied', '', { duration: 2500 });
        }
        else {
            this.snackbar.open('Copy failed. Please try again!', '', { duration: 2500 });
        }
    };
    FuseExampleViewerComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            var cmpFactory = _this._resolver.resolveComponentFactory(_this.exampleData.component);
            _this.previewRef = _this._previewContainer.createComponent(cmpFactory);
        }, 0);
    };
    FuseExampleViewerComponent.prototype.ngOnDestroy = function () {
        if (this.previewRef) {
            this.previewRef.destroy();
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('previewContainer', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"])
    ], FuseExampleViewerComponent.prototype, "_previewContainer", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], FuseExampleViewerComponent.prototype, "example", null);
    FuseExampleViewerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-example-viewer',
            template: __webpack_require__("../../../../../src/app/main/content/components/angular-material/example-viewer/example-viewer.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/components/angular-material/example-viewer/example-viewer.scss")],
            providers: [__WEBPACK_IMPORTED_MODULE_3__core_components_copier_copier_service__["a" /* CopierService */]],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
            animations: __WEBPACK_IMPORTED_MODULE_7__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["K" /* MatSnackBar */],
            __WEBPACK_IMPORTED_MODULE_3__core_components_copier_copier_service__["a" /* CopierService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ComponentFactoryResolver"]])
    ], FuseExampleViewerComponent);
    return FuseExampleViewerComponent;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/autocomplete-display/autocomplete-display-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-form {\r\n    min-width: 150px;\r\n    max-width: 500px;\r\n    width: 100%;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/autocomplete-display/autocomplete-display-example.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"example-form\">\r\n    <mat-form-field class=\"example-full-width\">\r\n        <input type=\"text\" placeholder=\"Assignee\" aria-label=\"Assignee\" matInput [formControl]=\"myControl\" [matAutocomplete]=\"auto\">\r\n        <mat-autocomplete #auto=\"matAutocomplete\" [displayWith]=\"displayFn\">\r\n            <mat-option *ngFor=\"let option of filteredOptions | async\" [value]=\"option\">\r\n                {{ option.name }}\r\n            </mat-option>\r\n        </mat-autocomplete>\r\n    </mat-form-field>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/autocomplete-display/autocomplete-display-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export User */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutocompleteDisplayExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var User = /** @class */ (function () {
    function User(name) {
        this.name = name;
    }
    return User;
}());

/**
 * @title Display value autocomplete
 */
var AutocompleteDisplayExample = /** @class */ (function () {
    function AutocompleteDisplayExample() {
        this.myControl = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */]();
        this.options = [
            new User('Mary'),
            new User('Shelley'),
            new User('Igor')
        ];
    }
    AutocompleteDisplayExample.prototype.ngOnInit = function () {
        var _this = this;
        this.filteredOptions = this.myControl.valueChanges
            .startWith(null)
            .map(function (user) { return user && typeof user === 'object' ? user.name : user; })
            .map(function (name) { return name ? _this.filter(name) : _this.options.slice(); });
    };
    AutocompleteDisplayExample.prototype.filter = function (name) {
        return this.options.filter(function (option) {
            return option.name.toLowerCase().indexOf(name.toLowerCase()) === 0;
        });
    };
    AutocompleteDisplayExample.prototype.displayFn = function (user) {
        if (user) {
            return user.name;
        }
    };
    AutocompleteDisplayExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'autocomplete-display-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/autocomplete-display/autocomplete-display-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/autocomplete-display/autocomplete-display-example.css")]
        })
    ], AutocompleteDisplayExample);
    return AutocompleteDisplayExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/autocomplete-filter/autocomplete-filter-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-form {\r\n    min-width: 150px;\r\n    max-width: 500px;\r\n    width: 100%;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/autocomplete-filter/autocomplete-filter-example.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"example-form\">\r\n    <mat-form-field class=\"example-full-width\">\r\n        <input type=\"text\" placeholder=\"Pick one\" aria-label=\"Number\" matInput [formControl]=\"myControl\" [matAutocomplete]=\"auto\">\r\n        <mat-autocomplete #auto=\"matAutocomplete\">\r\n            <mat-option *ngFor=\"let option of filteredOptions | async\" [value]=\"option\">\r\n                {{ option }}\r\n            </mat-option>\r\n        </mat-autocomplete>\r\n    </mat-form-field>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/autocomplete-filter/autocomplete-filter-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutocompleteFilterExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




/**
 * @title Filter autocomplete
 */
var AutocompleteFilterExample = /** @class */ (function () {
    function AutocompleteFilterExample() {
        this.myControl = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */]();
        this.options = [
            'One',
            'Two',
            'Three'
        ];
    }
    AutocompleteFilterExample.prototype.ngOnInit = function () {
        var _this = this;
        this.filteredOptions = this.myControl.valueChanges
            .startWith(null)
            .map(function (val) { return val ? _this.filter(val) : _this.options.slice(); });
    };
    AutocompleteFilterExample.prototype.filter = function (val) {
        return this.options.filter(function (option) {
            return option.toLowerCase().indexOf(val.toLowerCase()) === 0;
        });
    };
    AutocompleteFilterExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'autocomplete-filter-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/autocomplete-filter/autocomplete-filter-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/autocomplete-filter/autocomplete-filter-example.css")]
        })
    ], AutocompleteFilterExample);
    return AutocompleteFilterExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/autocomplete-overview/autocomplete-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-form {\r\n    min-width: 150px;\r\n    max-width: 500px;\r\n    width: 100%;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/autocomplete-overview/autocomplete-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"example-form\">\r\n    <mat-form-field class=\"example-full-width\">\r\n        <input matInput placeholder=\"State\" aria-label=\"State\" [matAutocomplete]=\"auto\" [formControl]=\"stateCtrl\">\r\n        <mat-autocomplete #auto=\"matAutocomplete\">\r\n            <mat-option *ngFor=\"let state of filteredStates | async\" [value]=\"state.name\">\r\n                <img style=\"vertical-align:middle;\" aria-hidden src=\"{{state.flag}}\" width=\"25\"/>\r\n                <span>{{ state.name }}</span>\r\n                |\r\n                <small>Population: {{state.population}}</small>\r\n            </mat-option>\r\n        </mat-autocomplete>\r\n    </mat-form-field>\r\n\r\n    <br/>\r\n\r\n    <mat-slide-toggle\r\n        [checked]=\"stateCtrl.disabled\"\r\n        (change)=\"stateCtrl.disabled ? stateCtrl.enable() : stateCtrl.disable()\">\r\n        Disable Input?\r\n    </mat-slide-toggle>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/autocomplete-overview/autocomplete-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutocompleteOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * @title Autocomplete overview
 */
var AutocompleteOverviewExample = /** @class */ (function () {
    function AutocompleteOverviewExample() {
        var _this = this;
        this.states = [
            {
                name: 'Arkansas',
                population: '2.978M',
                // https://commons.wikimedia.org/wiki/File:Flag_of_Arkansas.svg
                flag: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg'
            },
            {
                name: 'California',
                population: '39.14M',
                // https://commons.wikimedia.org/wiki/File:Flag_of_California.svg
                flag: 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'
            },
            {
                name: 'Florida',
                population: '20.27M',
                // https://commons.wikimedia.org/wiki/File:Flag_of_Florida.svg
                flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Florida.svg'
            },
            {
                name: 'Texas',
                population: '27.47M',
                // https://commons.wikimedia.org/wiki/File:Flag_of_Texas.svg
                flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Texas.svg'
            }
        ];
        this.stateCtrl = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */]();
        this.filteredStates = this.stateCtrl.valueChanges
            .startWith(null)
            .map(function (state) { return state ? _this.filterStates(state) : _this.states.slice(); });
    }
    AutocompleteOverviewExample.prototype.filterStates = function (name) {
        return this.states.filter(function (state) {
            return state.name.toLowerCase().indexOf(name.toLowerCase()) === 0;
        });
    };
    AutocompleteOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'autocomplete-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/autocomplete-overview/autocomplete-overview-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/autocomplete-overview/autocomplete-overview-example.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AutocompleteOverviewExample);
    return AutocompleteOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/autocomplete-simple/autocomplete-simple-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-form {\r\n    min-width: 150px;\r\n    max-width: 500px;\r\n    width: 100%;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/autocomplete-simple/autocomplete-simple-example.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"example-form\">\r\n    <mat-form-field class=\"example-full-width\">\r\n        <input type=\"text\" placeholder=\"Pick one\" aria-label=\"Number\" matInput [formControl]=\"myControl\" [matAutocomplete]=\"auto\">\r\n        <mat-autocomplete #auto=\"matAutocomplete\">\r\n            <mat-option *ngFor=\"let option of options\" [value]=\"option\">\r\n                {{ option }}\r\n            </mat-option>\r\n        </mat-autocomplete>\r\n    </mat-form-field>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/autocomplete-simple/autocomplete-simple-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutocompleteSimpleExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/**
 * @title Simple autocomplete
 */
var AutocompleteSimpleExample = /** @class */ (function () {
    function AutocompleteSimpleExample() {
        this.myControl = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */]();
        this.options = [
            'One',
            'Two',
            'Three'
        ];
    }
    AutocompleteSimpleExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'autocomplete-simple-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/autocomplete-simple/autocomplete-simple-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/autocomplete-simple/autocomplete-simple-example.css")]
        })
    ], AutocompleteSimpleExample);
    return AutocompleteSimpleExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/button-overview/button-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<button mat-button>Click me!</button>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/button-overview/button-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ButtonOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic buttons
 */
var ButtonOverviewExample = /** @class */ (function () {
    function ButtonOverviewExample() {
    }
    ButtonOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'button-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/button-overview/button-overview-example.html")
        })
    ], ButtonOverviewExample);
    return ButtonOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/button-toggle-exclusive/button-toggle-exclusive-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-selected-value {\r\n    margin: 15px 0;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/button-toggle-exclusive/button-toggle-exclusive-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-button-toggle-group #group=\"matButtonToggleGroup\">\r\n    <mat-button-toggle value=\"left\">\r\n        <mat-icon>format_align_left</mat-icon>\r\n    </mat-button-toggle>\r\n    <mat-button-toggle value=\"center\">\r\n        <mat-icon>format_align_center</mat-icon>\r\n    </mat-button-toggle>\r\n    <mat-button-toggle value=\"right\">\r\n        <mat-icon>format_align_right</mat-icon>\r\n    </mat-button-toggle>\r\n    <mat-button-toggle value=\"justify\" disabled>\r\n        <mat-icon>format_align_justify</mat-icon>\r\n    </mat-button-toggle>\r\n</mat-button-toggle-group>\r\n<div class=\"example-selected-value\">Selected value: {{group.value}}</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/button-toggle-exclusive/button-toggle-exclusive-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ButtonToggleExclusiveExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Exclusive selection
 */
var ButtonToggleExclusiveExample = /** @class */ (function () {
    function ButtonToggleExclusiveExample() {
    }
    ButtonToggleExclusiveExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'button-toggle-exclusive-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/button-toggle-exclusive/button-toggle-exclusive-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/button-toggle-exclusive/button-toggle-exclusive-example.css")]
        })
    ], ButtonToggleExclusiveExample);
    return ButtonToggleExclusiveExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/button-toggle-overview/button-toggle-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-button-toggle>Toggle me!</mat-button-toggle>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/button-toggle-overview/button-toggle-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ButtonToggleOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic button-toggles
 */
var ButtonToggleOverviewExample = /** @class */ (function () {
    function ButtonToggleOverviewExample() {
    }
    ButtonToggleOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'button-toggle-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/button-toggle-overview/button-toggle-overview-example.html")
        })
    ], ButtonToggleOverviewExample);
    return ButtonToggleOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/button-types/button-types-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-button-row {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    -ms-flex-pack: distribute;\r\n        justify-content: space-around;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/button-types/button-types-example.html":
/***/ (function(module, exports) {

module.exports = "<h3>Basic Buttons</h3>\r\n<div class=\"button-row\">\r\n    <button mat-button>Basic</button>\r\n    <button mat-button color=\"primary\">Primary</button>\r\n    <button mat-button color=\"accent\">Accent</button>\r\n    <button mat-button color=\"warn\">Warn</button>\r\n    <button mat-button disabled>Disabled</button>\r\n    <a mat-button routerLink=\".\">Link</a>\r\n</div>\r\n\r\n<h3>Raised Buttons</h3>\r\n<div class=\"button-row\">\r\n    <button mat-raised-button>Basic</button>\r\n    <button mat-raised-button color=\"primary\">Primary</button>\r\n    <button mat-raised-button color=\"accent\">Accent</button>\r\n    <button mat-raised-button color=\"warn\">Warn</button>\r\n    <button mat-raised-button disabled>Disabled</button>\r\n    <a mat-raised-button routerLink=\".\">Link</a>\r\n</div>\r\n\r\n<h3>Icon Buttons</h3>\r\n<div class=\"button-row\">\r\n    <button mat-icon-button>\r\n        <mat-icon class=\"mat-24\" aria-label=\"Example icon-button with a heart icon\">favorite</mat-icon>\r\n    </button>\r\n    <button mat-icon-button color=\"primary\">\r\n        <mat-icon class=\"mat-24\" aria-label=\"Example icon-button with a heart icon\">favorite</mat-icon>\r\n    </button>\r\n    <button mat-icon-button color=\"accent\">\r\n        <mat-icon class=\"mat-24\" aria-label=\"Example icon-button with a heart icon\">favorite</mat-icon>\r\n    </button>\r\n    <button mat-icon-button color=\"warn\">\r\n        <mat-icon class=\"mat-24\" aria-label=\"Example icon-button with a heart icon\">favorite</mat-icon>\r\n    </button>\r\n    <button mat-icon-button disabled>\r\n        <mat-icon class=\"mat-24\" aria-label=\"Example icon-button with a heart icon\">favorite</mat-icon>\r\n    </button>\r\n</div>\r\n\r\n<h3>Fab Buttons</h3>\r\n<div class=\"button-row\">\r\n    <button mat-fab>Basic</button>\r\n    <button mat-fab color=\"primary\">Primary</button>\r\n    <button mat-fab color=\"accent\">Accent</button>\r\n    <button mat-fab color=\"warn\">Warn</button>\r\n    <button mat-fab disabled>Disabled</button>\r\n    <button mat-fab>\r\n        <mat-icon class=\"mat-24\" aria-label=\"Example icon-button with a heart icon\">favorite</mat-icon>\r\n    </button>\r\n    <a mat-fab routerLink=\".\">Link</a>\r\n</div>\r\n\r\n<h3>Mini Fab Buttons</h3>\r\n<div class=\"button-row\">\r\n    <button mat-mini-fab>Basic</button>\r\n    <button mat-mini-fab color=\"primary\">Primary</button>\r\n    <button mat-mini-fab color=\"accent\">Accent</button>\r\n    <button mat-mini-fab color=\"warn\">Warn</button>\r\n    <button mat-mini-fab disabled>Disabled</button>\r\n    <button mat-mini-fab>\r\n        <mat-icon class=\"mat-24\" aria-label=\"Example icon-button with a heart icon\">favorite</mat-icon>\r\n    </button>\r\n    <a mat-mini-fab routerLink=\".\">Link</a>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/button-types/button-types-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ButtonTypesExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Button varieties
 */
var ButtonTypesExample = /** @class */ (function () {
    function ButtonTypesExample() {
    }
    ButtonTypesExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'button-types-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/button-types/button-types-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/button-types/button-types-example.css")]
        })
    ], ButtonTypesExample);
    return ButtonTypesExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/card-fancy/card-fancy-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-card {\r\n    width: 400px;\r\n}\r\n\r\n.example-header-image {\r\n    background-image: url('/assets/images/examples/shiba1.jpg');\r\n    background-size: cover;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/card-fancy/card-fancy-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"example-card\">\r\n    <mat-card-header>\r\n        <div mat-card-avatar class=\"example-header-image\"></div>\r\n        <mat-card-title>Shiba Inu</mat-card-title>\r\n        <mat-card-subtitle>Dog Breed</mat-card-subtitle>\r\n    </mat-card-header>\r\n    <img mat-card-image src=\"assets/images/examples/shiba2.jpg\" alt=\"Photo of a Shiba Inu\">\r\n    <mat-card-content>\r\n        <p>\r\n            The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan.\r\n            A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally\r\n            bred for hunting.\r\n        </p>\r\n    </mat-card-content>\r\n    <mat-card-actions>\r\n        <button mat-button>LIKE</button>\r\n        <button mat-button>SHARE</button>\r\n    </mat-card-actions>\r\n</mat-card>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/card-fancy/card-fancy-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardFancyExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Card with multiple sections
 */
var CardFancyExample = /** @class */ (function () {
    function CardFancyExample() {
    }
    CardFancyExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'card-fancy-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/card-fancy/card-fancy-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/card-fancy/card-fancy-example.css")]
        })
    ], CardFancyExample);
    return CardFancyExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/card-overview/card-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-card>Simple card</mat-card>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/card-overview/card-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic cards
 */
var CardOverviewExample = /** @class */ (function () {
    function CardOverviewExample() {
    }
    CardOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'card-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/card-overview/card-overview-example.html")
        })
    ], CardOverviewExample);
    return CardOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/cdk-table-basic/cdk-table-basic-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* Structure */\r\n.example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    min-width: 300px;\r\n}\r\n/*\r\n * Styles to make the demo's cdk-table match the material design spec\r\n * https://material.io/guidelines/components/data-tables.html\r\n */\r\n.example-table {\r\n    -webkit-box-flex: 1;\r\n        -ms-flex: 1 1 auto;\r\n            flex: 1 1 auto;\r\n    overflow: auto;\r\n    max-height: 500px;\r\n}\r\n.example-header-row, .example-row {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    border-bottom: 1px solid #CCC;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    height: 32px;\r\n    padding: 0 8px;\r\n}\r\n.example-cell, .example-header-cell {\r\n    -webkit-box-flex: 1;\r\n        -ms-flex: 1;\r\n            flex: 1;\r\n}\r\n.example-header-cell {\r\n    font-size: 12px;\r\n    font-weight: bold;\r\n    color: rgba(0, 0, 0, 0.54);\r\n}\r\n.example-cell {\r\n    font-size: 13px;\r\n    color: rgba(0, 0, 0, 0.87);\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/cdk-table-basic/cdk-table-basic-example.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container mat-elevation-z8\">\r\n    <cdk-table #table [dataSource]=\"dataSource\" class=\"example-table\">\r\n        <!--- Note that these columns can be defined in any order.\r\n              The actual rendered columns are set as a property on the row definition\" -->\r\n\r\n        <!-- ID Column -->\r\n        <ng-container cdkColumnDef=\"userId\">\r\n            <cdk-header-cell *cdkHeaderCellDef class=\"example-header-cell\"> ID</cdk-header-cell>\r\n            <cdk-cell *cdkCellDef=\"let row\" class=\"example-cell\"> {{row.id}}</cdk-cell>\r\n        </ng-container>\r\n\r\n        <!-- Progress Column -->\r\n        <ng-container cdkColumnDef=\"progress\">\r\n            <cdk-header-cell *cdkHeaderCellDef class=\"example-header-cell\"> Progress</cdk-header-cell>\r\n            <cdk-cell *cdkCellDef=\"let row\" class=\"example-cell\"> {{row.progress}}%</cdk-cell>\r\n        </ng-container>\r\n\r\n        <!-- Name Column -->\r\n        <ng-container cdkColumnDef=\"userName\">\r\n            <cdk-header-cell *cdkHeaderCellDef class=\"example-header-cell\"> Name</cdk-header-cell>\r\n            <cdk-cell *cdkCellDef=\"let row\" class=\"example-cell\"> {{row.name}}</cdk-cell>\r\n        </ng-container>\r\n\r\n        <!-- Color Column -->\r\n        <ng-container cdkColumnDef=\"color\">\r\n            <cdk-header-cell *cdkHeaderCellDef class=\"example-header-cell\">Color</cdk-header-cell>\r\n            <cdk-cell *cdkCellDef=\"let row\" class=\"example-cell\"\r\n                      [style.color]=\"row.color\">\r\n                {{row.color}}\r\n            </cdk-cell>\r\n        </ng-container>\r\n\r\n        <cdk-header-row *cdkHeaderRowDef=\"displayedColumns\" class=\"example-header-row\"></cdk-header-row>\r\n        <cdk-row *cdkRowDef=\"let row; columns: displayedColumns;\" class=\"example-row\"></cdk-row>\r\n    </cdk-table>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/cdk-table-basic/cdk-table-basic-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CdkTableBasicExample; });
/* unused harmony export ExampleDatabase */
/* unused harmony export ExampleDataSource */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_cdk_collections__ = __webpack_require__("../../../cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






/**
 * @title Basic CDK data-table
 */
var CdkTableBasicExample = /** @class */ (function () {
    function CdkTableBasicExample() {
        this.displayedColumns = ['userId', 'userName', 'progress', 'color'];
        this.exampleDatabase = new ExampleDatabase();
    }
    CdkTableBasicExample.prototype.ngOnInit = function () {
        this.dataSource = new ExampleDataSource(this.exampleDatabase);
    };
    CdkTableBasicExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'cdk-table-basic-example',
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/cdk-table-basic/cdk-table-basic-example.css")],
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/cdk-table-basic/cdk-table-basic-example.html")
        })
    ], CdkTableBasicExample);
    return CdkTableBasicExample;
}());

/** Constants used to fill up our data base. */
var COLORS = [
    'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple',
    'fuchsia', 'lime', 'teal', 'aqua', 'blue', 'navy', 'black', 'gray'
];
var NAMES = [
    'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
    'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
    'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];
/** An example database that the data source uses to retrieve data for the table. */
var ExampleDatabase = /** @class */ (function () {
    function ExampleDatabase() {
        /** Stream that emits whenever the data has been modified. */
        this.dataChange = new __WEBPACK_IMPORTED_MODULE_2_rxjs_BehaviorSubject__["BehaviorSubject"]([]);
        // Fill up the database with 100 users.
        for (var i = 0; i < 100; i++) {
            this.addUser();
        }
    }
    Object.defineProperty(ExampleDatabase.prototype, "data", {
        get: function () {
            return this.dataChange.value;
        },
        enumerable: true,
        configurable: true
    });
    /** Adds a new user to the database. */
    ExampleDatabase.prototype.addUser = function () {
        var copiedData = this.data.slice();
        copiedData.push(this.createNewUser());
        this.dataChange.next(copiedData);
    };
    /** Builds and returns a new User. */
    ExampleDatabase.prototype.createNewUser = function () {
        var name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
            NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';
        return {
            id: (this.data.length + 1).toString(),
            name: name,
            progress: Math.round(Math.random() * 100).toString(),
            color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
        };
    };
    return ExampleDatabase;
}());

/**
 * Data source to provide what data should be rendered in the table. Note that the data source
 * can retrieve its data in any way. In this case, the data source is provided a reference
 * to a common data base, ExampleDatabase. It is not the data source's responsibility to manage
 * the underlying data. Instead, it only needs to take the data and send the table exactly what
 * should be rendered.
 */
var ExampleDataSource = /** @class */ (function (_super) {
    __extends(ExampleDataSource, _super);
    function ExampleDataSource(_exampleDatabase) {
        var _this = _super.call(this) || this;
        _this._exampleDatabase = _exampleDatabase;
        return _this;
    }
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    ExampleDataSource.prototype.connect = function () {
        return this._exampleDatabase.dataChange;
    };
    ExampleDataSource.prototype.disconnect = function () {
    };
    return ExampleDataSource;
}(__WEBPACK_IMPORTED_MODULE_1__angular_cdk_collections__["a" /* DataSource */]));



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/checkbox-configurable/checkbox-configurable-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-h2 {\r\n    margin: 10px;\r\n}\r\n\r\n.example-section {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -ms-flex-line-pack: center;\r\n        align-content: center;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    height: 60px;\r\n}\r\n\r\n.example-margin {\r\n    margin: 0 10px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/checkbox-configurable/checkbox-configurable-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-card>\r\n    <mat-card-content>\r\n        <h2 class=\"example-h2\">Checkbox configuration</h2>\r\n\r\n        <section class=\"example-section\">\r\n            <mat-checkbox class=\"example-margin\" [(ngModel)]=\"checked\">Checked</mat-checkbox>\r\n            <mat-checkbox class=\"example-margin\" [(ngModel)]=\"indeterminate\">Indeterminate</mat-checkbox>\r\n        </section>\r\n\r\n        <section class=\"example-section\">\r\n            <label class=\"example-margin\">Align:</label>\r\n            <mat-radio-group [(ngModel)]=\"align\">\r\n                <mat-radio-button class=\"example-margin\" value=\"start\">Start</mat-radio-button>\r\n                <mat-radio-button class=\"example-margin\" value=\"end\">End</mat-radio-button>\r\n            </mat-radio-group>\r\n        </section>\r\n\r\n        <section class=\"example-section\">\r\n            <mat-checkbox class=\"example-margin\" [(ngModel)]=\"disabled\">Disabled</mat-checkbox>\r\n        </section>\r\n    </mat-card-content>\r\n</mat-card>\r\n\r\n<mat-card class=\"result\">\r\n    <mat-card-content>\r\n        <h2 class=\"example-h2\">Result</h2>\r\n\r\n        <section class=\"example-section\">\r\n            <mat-checkbox\r\n                class=\"example-margin\"\r\n                [(ngModel)]=\"checked\"\r\n                [(indeterminate)]=\"indeterminate\"\r\n                [align]=\"align\"\r\n                [disabled]=\"disabled\">\r\n                I'm a checkbox\r\n            </mat-checkbox>\r\n        </section>\r\n    </mat-card-content>\r\n</mat-card>\r\n\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/checkbox-configurable/checkbox-configurable-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckboxConfigurableExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Configurable checkbox
 */
var CheckboxConfigurableExample = /** @class */ (function () {
    function CheckboxConfigurableExample() {
        this.checked = false;
        this.indeterminate = false;
        this.align = 'start';
        this.disabled = false;
    }
    CheckboxConfigurableExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'checkbox-configurable-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/checkbox-configurable/checkbox-configurable-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/checkbox-configurable/checkbox-configurable-example.css")]
        })
    ], CheckboxConfigurableExample);
    return CheckboxConfigurableExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/checkbox-overview/checkbox-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-checkbox>Check me!</mat-checkbox>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/checkbox-overview/checkbox-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckboxOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic checkboxes
 */
var CheckboxOverviewExample = /** @class */ (function () {
    function CheckboxOverviewExample() {
    }
    CheckboxOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'checkbox-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/checkbox-overview/checkbox-overview-example.html")
        })
    ], CheckboxOverviewExample);
    return CheckboxOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/chips-input/chips-input-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".demo-chip-list {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/chips-input/chips-input-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field class=\"demo-chip-list\">\r\n    <mat-chip-list #chipList>\r\n        <mat-chip *ngFor=\"let fruit of fruits\" [selectable]=\"selectable\"\r\n                  [removable]=\"removable\" (remove)=\"remove(fruit)\">\r\n            {{fruit.name}}\r\n            <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\r\n        </mat-chip>\r\n        <input placeholder=\"New fruit...\"\r\n               [matChipInputFor]=\"chipList\"\r\n               [matChipInputSeparatorKeyCodes]=\"separatorKeysCodes\"\r\n               [matChipInputAddOnBlur]=\"addOnBlur\"\r\n               (matChipInputTokenEnd)=\"add($event)\"/>\r\n    </mat-chip-list>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/chips-input/chips-input-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChipsInputExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_cdk_keycodes__ = __webpack_require__("../../../cdk/esm5/keycodes.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/**
 * @title Chips with input
 */
var ChipsInputExample = /** @class */ (function () {
    function ChipsInputExample() {
        this.visible = true;
        this.selectable = true;
        this.removable = true;
        this.addOnBlur = true;
        // Enter, comma
        this.separatorKeysCodes = [__WEBPACK_IMPORTED_MODULE_1__angular_cdk_keycodes__["g" /* ENTER */], __WEBPACK_IMPORTED_MODULE_1__angular_cdk_keycodes__["c" /* COMMA */]];
        this.fruits = [
            { name: 'Lemon' },
            { name: 'Lime' },
            { name: 'Apple' }
        ];
    }
    ChipsInputExample.prototype.add = function (event) {
        var input = event.input;
        var value = event.value;
        // Add our person
        if ((value || '').trim()) {
            this.fruits.push({ name: value.trim() });
        }
        // Reset the input value
        if (input) {
            input.value = '';
        }
    };
    ChipsInputExample.prototype.remove = function (fruit) {
        var index = this.fruits.indexOf(fruit);
        if (index >= 0) {
            this.fruits.splice(index, 1);
        }
    };
    ChipsInputExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'chips-input-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/chips-input/chips-input-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/chips-input/chips-input-example.css")]
        })
    ], ChipsInputExample);
    return ChipsInputExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/chips-overview/chips-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/chips-overview/chips-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-chip-list>\r\n    <mat-chip>One fish</mat-chip>\r\n    <mat-chip>Two fish</mat-chip>\r\n    <mat-chip color=\"primary\" selected=\"true\">Primary fish</mat-chip>\r\n    <mat-chip color=\"accent\" selected=\"true\">Accent fish</mat-chip>\r\n</mat-chip-list>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/chips-overview/chips-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChipsOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic chips
 */
var ChipsOverviewExample = /** @class */ (function () {
    function ChipsOverviewExample() {
    }
    ChipsOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'chips-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/chips-overview/chips-overview-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/chips-overview/chips-overview-example.css")]
        })
    ], ChipsOverviewExample);
    return ChipsOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/chips-stacked/chips-stacked-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "mat-chip {\r\n    max-width: 200px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/chips-stacked/chips-stacked-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-chip-list class=\"mat-chip-list-stacked\">\r\n    <mat-chip *ngFor=\"let chip of availableColors\" selected=\"true\" [color]=\"chip.color\">\r\n        {{chip.name}}\r\n    </mat-chip>\r\n</mat-chip-list>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/chips-stacked/chips-stacked-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChipsStackedExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Stacked chips
 */
var ChipsStackedExample = /** @class */ (function () {
    function ChipsStackedExample() {
        this.availableColors = [
            {
                name: 'none',
                color: ''
            },
            {
                name: 'Primary',
                color: 'primary'
            },
            {
                name: 'Accent',
                color: 'accent'
            },
            {
                name: 'Warn',
                color: 'warn'
            }
        ];
    }
    ChipsStackedExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'chips-stacked-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/chips-stacked/chips-stacked-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/chips-stacked/chips-stacked-example.css")]
        })
    ], ChipsStackedExample);
    return ChipsStackedExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-api/datepicker-api-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-api/datepicker-api-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field class=\"example-full-width\">\r\n    <input matInput [matDatepicker]=\"picker\" placeholder=\"Choose a date\">\r\n    <mat-datepicker #picker></mat-datepicker>\r\n</mat-form-field>\r\n<button mat-raised-button (click)=\"picker.open()\">Open</button>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-api/datepicker-api-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerApiExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Datepicker open method */
var DatepickerApiExample = /** @class */ (function () {
    function DatepickerApiExample() {
    }
    DatepickerApiExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'datepicker-api-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-api/datepicker-api-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-api/datepicker-api-example.css")]
        })
    ], DatepickerApiExample);
    return DatepickerApiExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-disabled/datepicker-disabled-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-disabled/datepicker-disabled-example.html":
/***/ (function(module, exports) {

module.exports = "<p>\r\n    <mat-form-field>\r\n        <input matInput [matDatepicker]=\"dp1\" placeholder=\"Completely disabled\" disabled>\r\n        <mat-datepicker-toggle matSuffix [for]=\"dp1\"></mat-datepicker-toggle>\r\n        <mat-datepicker #dp1></mat-datepicker>\r\n    </mat-form-field>\r\n</p>\r\n\r\n<p>\r\n    <mat-form-field>\r\n        <input matInput [matDatepicker]=\"dp2\" placeholder=\"Popup disabled\">\r\n        <mat-datepicker-toggle matSuffix [for]=\"dp2\" disabled></mat-datepicker-toggle>\r\n        <mat-datepicker #dp2></mat-datepicker>\r\n    </mat-form-field>\r\n</p>\r\n\r\n<p>\r\n    <mat-form-field>\r\n        <input matInput [matDatepicker]=\"dp3\" placeholder=\"Input disabled\" disabled>\r\n        <mat-datepicker-toggle matSuffix [for]=\"dp3\"></mat-datepicker-toggle>\r\n        <mat-datepicker #dp3 disabled=\"false\"></mat-datepicker>\r\n    </mat-form-field>\r\n</p>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-disabled/datepicker-disabled-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerDisabledExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Disabled datepicker */
var DatepickerDisabledExample = /** @class */ (function () {
    function DatepickerDisabledExample() {
    }
    DatepickerDisabledExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'datepicker-disabled-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-disabled/datepicker-disabled-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-disabled/datepicker-disabled-example.css")]
        })
    ], DatepickerDisabledExample);
    return DatepickerDisabledExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-events/datepicker-events-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-events {\r\n    width: 400px;\r\n    height: 200px;\r\n    border: 1px solid #555;\r\n    overflow: auto;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-events/datepicker-events-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <input matInput [matDatepicker]=\"picker\" placeholder=\"Input & change events\"\r\n           (dateInput)=\"addEvent('input', $event)\" (dateChange)=\"addEvent('change', $event)\">\r\n    <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n    <mat-datepicker #picker></mat-datepicker>\r\n</mat-form-field>\r\n\r\n<div class=\"example-events\">\r\n    <div *ngFor=\"let e of events\">{{e}}</div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-events/datepicker-events-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerEventsExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Datepicker input and change events */
var DatepickerEventsExample = /** @class */ (function () {
    function DatepickerEventsExample() {
        this.events = [];
    }
    DatepickerEventsExample.prototype.addEvent = function (type, event) {
        this.events.push(type + ": " + event.value);
    };
    DatepickerEventsExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'datepicker-events-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-events/datepicker-events-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-events/datepicker-events-example.css")]
        })
    ], DatepickerEventsExample);
    return DatepickerEventsExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-filter/datepicker-filter-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-filter/datepicker-filter-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field class=\"example-full-width\">\r\n    <input matInput [matDatepickerFilter]=\"myFilter\" [matDatepicker]=\"picker\" placeholder=\"Choose a date\">\r\n    <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n    <mat-datepicker #picker></mat-datepicker>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-filter/datepicker-filter-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerFilterExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Datepicker with filter validation */
var DatepickerFilterExample = /** @class */ (function () {
    function DatepickerFilterExample() {
        this.myFilter = function (d) {
            var day = d.getDay();
            // Prevent Saturday and Sunday from being selected.
            return day !== 0 && day !== 6;
        };
    }
    DatepickerFilterExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'datepicker-filter-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-filter/datepicker-filter-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-filter/datepicker-filter-example.css")]
        })
    ], DatepickerFilterExample);
    return DatepickerFilterExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-formats/datepicker-formats-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-formats/datepicker-formats-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <input matInput [matDatepicker]=\"dp\" placeholder=\"Verbose datepicker\" [formControl]=\"date\">\r\n    <mat-datepicker-toggle matSuffix [for]=\"dp\"></mat-datepicker-toggle>\r\n    <mat-datepicker #dp></mat-datepicker>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-formats/datepicker-formats-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export MY_FORMATS */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerFormatsExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material_moment_adapter__ = __webpack_require__("../../../material-moment-adapter/esm5/material-moment-adapter.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material_core__ = __webpack_require__("../../../material/esm5/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
var MY_FORMATS = {
    parse: {
        dateInput: 'LL'
    },
    display: {
        dateInput: 'LL',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY'
    }
};
/** @title Datepicker with custom formats */
var DatepickerFormatsExample = /** @class */ (function () {
    function DatepickerFormatsExample() {
        this.date = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */](__WEBPACK_IMPORTED_MODULE_4_moment__());
    }
    DatepickerFormatsExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'datepicker-formats-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-formats/datepicker-formats-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-formats/datepicker-formats-example.css")],
            providers: [
                // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
                // application's root module. We provide it at the component level here, due to limitations of
                // our example generation script.
                { provide: __WEBPACK_IMPORTED_MODULE_3__angular_material_core__["c" /* DateAdapter */],
                    useClass: __WEBPACK_IMPORTED_MODULE_2__angular_material_moment_adapter__["b" /* MomentDateAdapter */],
                    deps: [__WEBPACK_IMPORTED_MODULE_3__angular_material_core__["g" /* MAT_DATE_LOCALE */]]
                },
                { provide: __WEBPACK_IMPORTED_MODULE_3__angular_material_core__["f" /* MAT_DATE_FORMATS */],
                    useValue: MY_FORMATS
                }
            ]
        })
    ], DatepickerFormatsExample);
    return DatepickerFormatsExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-locale/datepicker-locale-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-locale/datepicker-locale-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <input matInput [matDatepicker]=\"dp\" placeholder=\"Different locale\">\r\n    <mat-datepicker-toggle matSuffix [for]=\"dp\"></mat-datepicker-toggle>\r\n    <mat-datepicker #dp></mat-datepicker>\r\n</mat-form-field>\r\n\r\n<button mat-button (click)=\"french()\">Dynamically switch to French</button>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-locale/datepicker-locale-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerLocaleExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material_moment_adapter__ = __webpack_require__("../../../material-moment-adapter/esm5/material-moment-adapter.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material_core__ = __webpack_require__("../../../material/esm5/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/** @title Datepicker with different locale */
var DatepickerLocaleExample = /** @class */ (function () {
    function DatepickerLocaleExample(adapter) {
        this.adapter = adapter;
    }
    DatepickerLocaleExample.prototype.french = function () {
        this.adapter.setLocale('fr');
    };
    DatepickerLocaleExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'datepicker-locale-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-locale/datepicker-locale-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-locale/datepicker-locale-example.css")],
            providers: [
                // The locale would typically be provided on the root module of your application. We do it at
                // the component level here, due to limitations of our example generation script.
                {
                    provide: __WEBPACK_IMPORTED_MODULE_2__angular_material_core__["g" /* MAT_DATE_LOCALE */],
                    useValue: 'ja-JP'
                },
                // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
                // `MatMomentDateModule` in your applications root module. We provide it at the component level
                // here, due to limitations of our example generation script.
                {
                    provide: __WEBPACK_IMPORTED_MODULE_2__angular_material_core__["c" /* DateAdapter */],
                    useClass: __WEBPACK_IMPORTED_MODULE_1__angular_material_moment_adapter__["b" /* MomentDateAdapter */],
                    deps: [__WEBPACK_IMPORTED_MODULE_2__angular_material_core__["g" /* MAT_DATE_LOCALE */]]
                },
                {
                    provide: __WEBPACK_IMPORTED_MODULE_2__angular_material_core__["f" /* MAT_DATE_FORMATS */],
                    useValue: __WEBPACK_IMPORTED_MODULE_1__angular_material_moment_adapter__["a" /* MAT_MOMENT_DATE_FORMATS */]
                }
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_material_core__["c" /* DateAdapter */]])
    ], DatepickerLocaleExample);
    return DatepickerLocaleExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-min-max/datepicker-min-max-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-min-max/datepicker-min-max-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field class=\"example-full-width\">\r\n    <input matInput [min]=\"minDate\" [max]=\"maxDate\" [matDatepicker]=\"picker\" placeholder=\"Choose a date\">\r\n    <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n    <mat-datepicker #picker></mat-datepicker>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-min-max/datepicker-min-max-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerMinMaxExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Datepicker with min & max validation */
var DatepickerMinMaxExample = /** @class */ (function () {
    function DatepickerMinMaxExample() {
        this.minDate = new Date(2000, 0, 1);
        this.maxDate = new Date(2020, 0, 1);
    }
    DatepickerMinMaxExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'datepicker-min-max-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-min-max/datepicker-min-max-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-min-max/datepicker-min-max-example.css")]
        })
    ], DatepickerMinMaxExample);
    return DatepickerMinMaxExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-moment/datepicker-moment-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-moment/datepicker-moment-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <input matInput [matDatepicker]=\"dp\" placeholder=\"Moment.js datepicker\" [formControl]=\"date\">\r\n    <mat-datepicker-toggle matSuffix [for]=\"dp\"></mat-datepicker-toggle>\r\n    <mat-datepicker #dp></mat-datepicker>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-moment/datepicker-moment-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerMomentExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material_moment_adapter__ = __webpack_require__("../../../material-moment-adapter/esm5/material-moment-adapter.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_material_core__ = __webpack_require__("../../../material/esm5/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





/** @title Datepicker that uses Moment.js dates */
var DatepickerMomentExample = /** @class */ (function () {
    function DatepickerMomentExample() {
        // Datepicker takes `Moment` objects instead of `Date` objects.
        this.date = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */](__WEBPACK_IMPORTED_MODULE_4_moment__([2017, 0, 1]));
    }
    DatepickerMomentExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'datepicker-moment-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-moment/datepicker-moment-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-moment/datepicker-moment-example.css")],
            providers: [
                // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
                // `MatMomentDateModule` in your applications root module. We provide it at the component level
                // here, due to limitations of our example generation script.
                {
                    provide: __WEBPACK_IMPORTED_MODULE_3__angular_material_core__["c" /* DateAdapter */],
                    useClass: __WEBPACK_IMPORTED_MODULE_2__angular_material_moment_adapter__["b" /* MomentDateAdapter */],
                    deps: [__WEBPACK_IMPORTED_MODULE_3__angular_material_core__["g" /* MAT_DATE_LOCALE */]]
                },
                {
                    provide: __WEBPACK_IMPORTED_MODULE_3__angular_material_core__["f" /* MAT_DATE_FORMATS */],
                    useValue: __WEBPACK_IMPORTED_MODULE_2__angular_material_moment_adapter__["a" /* MAT_MOMENT_DATE_FORMATS */]
                }
            ]
        })
    ], DatepickerMomentExample);
    return DatepickerMomentExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-overview/datepicker-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-overview/datepicker-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <input matInput [matDatepicker]=\"picker\" placeholder=\"Choose a date\">\r\n    <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n    <mat-datepicker #picker></mat-datepicker>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-overview/datepicker-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Basic datepicker */
var DatepickerOverviewExample = /** @class */ (function () {
    function DatepickerOverviewExample() {
    }
    DatepickerOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'datepicker-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-overview/datepicker-overview-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-overview/datepicker-overview-example.css")]
        })
    ], DatepickerOverviewExample);
    return DatepickerOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-start-view/datepicker-start-view-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-start-view/datepicker-start-view-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <input matInput [matDatepicker]=\"picker\" placeholder=\"Choose a date\">\r\n    <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n    <mat-datepicker #picker startView=\"year\" [startAt]=\"startDate\"></mat-datepicker>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-start-view/datepicker-start-view-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerStartViewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Datepicker start date */
var DatepickerStartViewExample = /** @class */ (function () {
    function DatepickerStartViewExample() {
        this.startDate = new Date(1990, 0, 1);
    }
    DatepickerStartViewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'datepicker-start-view-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-start-view/datepicker-start-view-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-start-view/datepicker-start-view-example.css")]
        })
    ], DatepickerStartViewExample);
    return DatepickerStartViewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-touch/datepicker-touch-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-touch/datepicker-touch-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field class=\"example-full-width\">\r\n    <input matInput [matDatepicker]=\"picker\" placeholder=\"Choose a date\">\r\n    <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\r\n    <mat-datepicker touchUi=\"true\" #picker></mat-datepicker>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-touch/datepicker-touch-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerTouchExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Datepicker touch UI */
var DatepickerTouchExample = /** @class */ (function () {
    function DatepickerTouchExample() {
    }
    DatepickerTouchExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'datepicker-touch-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-touch/datepicker-touch-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-touch/datepicker-touch-example.css")]
        })
    ], DatepickerTouchExample);
    return DatepickerTouchExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-value/datepicker-value-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-value/datepicker-value-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <input matInput [matDatepicker]=\"picker1\" placeholder=\"Angular forms\" [formControl]=\"date\">\r\n    <mat-datepicker-toggle matSuffix [for]=\"picker1\"></mat-datepicker-toggle>\r\n    <mat-datepicker #picker1></mat-datepicker>\r\n</mat-form-field>\r\n\r\n<mat-form-field>\r\n    <input matInput [matDatepicker]=\"picker2\" placeholder=\"Angular forms (w/ deserialization)\"\r\n           [formControl]=\"serializedDate\">\r\n    <mat-datepicker-toggle matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\r\n    <mat-datepicker #picker2></mat-datepicker>\r\n</mat-form-field>\r\n\r\n<mat-form-field>\r\n    <input matInput [matDatepicker]=\"picker3\" placeholder=\"Value binding\" [value]=\"date.value\">\r\n    <mat-datepicker-toggle matSuffix [for]=\"picker3\"></mat-datepicker-toggle>\r\n    <mat-datepicker #picker3></mat-datepicker>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/datepicker-value/datepicker-value-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DatepickerValueExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/** @title Datepicker selected value */
var DatepickerValueExample = /** @class */ (function () {
    function DatepickerValueExample() {
        this.date = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */](new Date());
        this.serializedDate = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */]((new Date()).toISOString());
    }
    DatepickerValueExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'datepicker-value-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-value/datepicker-value-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/datepicker-value/datepicker-value-example.css")]
        })
    ], DatepickerValueExample);
    return DatepickerValueExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/dialog-content/dialog-content-example-dialog.html":
/***/ (function(module, exports) {

module.exports = "<h2 mat-dialog-title>Install Angular</h2>\r\n<mat-dialog-content>\r\n    <h3>DEVELOP ACROSS ALL PLATFORMS</h3>\r\n    <p>Learn one way to build applications with Angular and reuse your code and abilities to build\r\n        apps for any deployment target. For web, mobile web, native mobile and native desktop.</p>\r\n\r\n    <h3>SPEED & PERFORMANCE</h3>\r\n    <p>Achieve the maximum speed possible on the Web Platform today, and take it further, via Web\r\n        Workers and server-side rendering. Angular puts you in control over scalability. Meet huge data requirements\r\n        by building data models on RxJS, Immutable.js or another push-model.</p>\r\n\r\n    <h3>INCREDIBLE TOOLING</h3>\r\n    <p>Build features quickly with simple, declarative templates. Extend the template language with your own\r\n        components and use a wide array of existing components. Get immediate Angular-specific help and feedback\r\n        with nearly every IDE and editor. All this comes together so you can focus on building amazing apps rather\r\n        than trying to make the code work.</p>\r\n\r\n    <h3>LOVED BY MILLIONS</h3>\r\n    <p>From prototype through global deployment, Angular delivers the productivity and scalable infrastructure\r\n        that supports Google's largest applications.</p>\r\n</mat-dialog-content>\r\n<mat-dialog-actions>\r\n    <button mat-button [mat-dialog-close]=\"true\" tabindex=\"1\">Install</button>\r\n    <button mat-button mat-dialog-close tabindex=\"-1\">Cancel</button>\r\n</mat-dialog-actions>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/dialog-content/dialog-content-example.html":
/***/ (function(module, exports) {

module.exports = "<button mat-button (click)=\"openDialog()\">Open dialog</button>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/dialog-content/dialog-content-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DialogContentExample; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return DialogContentExampleDialog; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @title Dialog with header, scrollable content and actions
 */
var DialogContentExample = /** @class */ (function () {
    function DialogContentExample(dialog) {
        this.dialog = dialog;
    }
    DialogContentExample.prototype.openDialog = function () {
        var dialogRef = this.dialog.open(DialogContentExampleDialog, {
            height: '350px'
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("Dialog result: " + result);
        });
    };
    DialogContentExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'dialog-content-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/dialog-content/dialog-content-example.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["m" /* MatDialog */]])
    ], DialogContentExample);
    return DialogContentExample;
}());

var DialogContentExampleDialog = /** @class */ (function () {
    function DialogContentExampleDialog() {
    }
    DialogContentExampleDialog = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'dialog-content-example-dialog',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/dialog-content/dialog-content-example-dialog.html")
        })
    ], DialogContentExampleDialog);
    return DialogContentExampleDialog;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/dialog-data/dialog-data-example-dialog.html":
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Favorite Animal</h1>\r\n<div mat-dialog-content>\r\n    My favorite animal is:\r\n    <ul>\r\n        <li>\r\n            <span *ngIf=\"data.animal === 'panda'\">&#10003;</span>\r\n            Panda\r\n        </li>\r\n        <li>\r\n            <span *ngIf=\"data.animal === 'unicorn'\">&#10003;</span>\r\n            Unicorn\r\n        </li>\r\n        <li>\r\n            <span *ngIf=\"data.animal === 'lion'\">&#10003;</span>\r\n            Lion\r\n        </li>\r\n    </ul>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/dialog-data/dialog-data-example.html":
/***/ (function(module, exports) {

module.exports = "<button mat-button (click)=\"openDialog()\">Open dialog</button>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/dialog-data/dialog-data-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DialogDataExample; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return DialogDataExampleDialog; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


/**
 * @title Injecting data when opening a dialog
 */
var DialogDataExample = /** @class */ (function () {
    function DialogDataExample(dialog) {
        this.dialog = dialog;
    }
    DialogDataExample.prototype.openDialog = function () {
        this.dialog.open(DialogDataExampleDialog, {
            data: {
                animal: 'panda'
            }
        });
    };
    DialogDataExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'dialog-data-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/dialog-data/dialog-data-example.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["m" /* MatDialog */]])
    ], DialogDataExample);
    return DialogDataExample;
}());

var DialogDataExampleDialog = /** @class */ (function () {
    function DialogDataExampleDialog(data) {
        this.data = data;
    }
    DialogDataExampleDialog = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'dialog-data-example-dialog',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/dialog-data/dialog-data-example-dialog.html")
        }),
        __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["e" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [Object])
    ], DialogDataExampleDialog);
    return DialogDataExampleDialog;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/dialog-elements/dialog-elements-example-dialog.html":
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Dialog with elements</h1>\r\n<div mat-dialog-content>This dialog showcases the title, close, content and actions elements.</div>\r\n<div mat-dialog-actions>\r\n    <button mat-button mat-dialog-close>Close</button>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/dialog-elements/dialog-elements-example.html":
/***/ (function(module, exports) {

module.exports = "<button mat-button (click)=\"openDialog()\">Launch dialog</button>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/dialog-elements/dialog-elements-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DialogElementsExample; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return DialogElementsExampleDialog; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @title Dialog elements
 */
var DialogElementsExample = /** @class */ (function () {
    function DialogElementsExample(dialog) {
        this.dialog = dialog;
    }
    DialogElementsExample.prototype.openDialog = function () {
        this.dialog.open(DialogElementsExampleDialog);
    };
    DialogElementsExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'dialog-elements-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/dialog-elements/dialog-elements-example.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["m" /* MatDialog */]])
    ], DialogElementsExample);
    return DialogElementsExample;
}());

var DialogElementsExampleDialog = /** @class */ (function () {
    function DialogElementsExampleDialog() {
    }
    DialogElementsExampleDialog = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'dialog-elements-example-dialog',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/dialog-elements/dialog-elements-example-dialog.html")
        })
    ], DialogElementsExampleDialog);
    return DialogElementsExampleDialog;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/dialog-overview/dialog-overview-example-dialog.html":
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Hi {{data.name}}</h1>\r\n<div mat-dialog-content>\r\n    <p>What's your favorite animal?</p>\r\n    <mat-form-field>\r\n        <input matInput tabindex=\"1\" [(ngModel)]=\"data.animal\">\r\n    </mat-form-field>\r\n</div>\r\n<div mat-dialog-actions>\r\n    <button mat-button [mat-dialog-close]=\"data.animal\" tabindex=\"2\">Ok</button>\r\n    <button mat-button (click)=\"onNoClick()\" tabindex=\"-1\">No Thanks</button>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/dialog-overview/dialog-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<ol>\r\n    <li>\r\n        <mat-form-field>\r\n            <input matInput [(ngModel)]=\"name\" placeholder=\"What's your name?\">\r\n        </mat-form-field>\r\n    </li>\r\n    <li>\r\n        <button mat-raised-button (click)=\"openDialog()\">Pick one</button>\r\n    </li>\r\n    <li *ngIf=\"animal\">\r\n        You chose: <i>{{animal}}</i>\r\n    </li>\r\n</ol>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/dialog-overview/dialog-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DialogOverviewExample; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return DialogOverviewExampleDialog; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


/**
 * @title Dialog Overview
 */
var DialogOverviewExample = /** @class */ (function () {
    function DialogOverviewExample(dialog) {
        this.dialog = dialog;
    }
    DialogOverviewExample.prototype.openDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
            width: '250px',
            data: {
                name: this.name,
                animal: this.animal
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log('The dialog was closed');
            _this.animal = result;
        });
    };
    DialogOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'dialog-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/dialog-overview/dialog-overview-example.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["m" /* MatDialog */]])
    ], DialogOverviewExample);
    return DialogOverviewExample;
}());

var DialogOverviewExampleDialog = /** @class */ (function () {
    function DialogOverviewExampleDialog(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    DialogOverviewExampleDialog.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    DialogOverviewExampleDialog = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'dialog-overview-example-dialog',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/dialog-overview/dialog-overview-example-dialog.html")
        }),
        __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["e" /* MAT_DIALOG_DATA */])),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["o" /* MatDialogRef */], Object])
    ], DialogOverviewExampleDialog);
    return DialogOverviewExampleDialog;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/elevation-overview/elevation-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-container {\r\n    padding: 16px;\r\n    margin-bottom: 16px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/elevation-overview/elevation-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container\"\r\n     [class.mat-elevation-z2]=\"!isActive\"\r\n     [class.mat-elevation-z8]=\"isActive\">\r\n    Example\r\n</div>\r\n\r\n<button mat-button (click)=\"isActive = !isActive\">Toggle Elevation</button>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/elevation-overview/elevation-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ElevationOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Elevation CSS classes
 */
var ElevationOverviewExample = /** @class */ (function () {
    function ElevationOverviewExample() {
        this.isActive = false;
    }
    ElevationOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'elevation-overview-example',
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/elevation-overview/elevation-overview-example.css")],
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/elevation-overview/elevation-overview-example.html")
        })
    ], ElevationOverviewExample);
    return ElevationOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/expansion-overview/expansion-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-expansion-panel>\r\n    <mat-expansion-panel-header>\r\n        <mat-panel-title>\r\n            Personal data\r\n        </mat-panel-title>\r\n        <mat-panel-description>\r\n            Type your name and age\r\n        </mat-panel-description>\r\n    </mat-expansion-panel-header>\r\n\r\n    <mat-form-field>\r\n        <input matInput placeholder=\"First name\">\r\n    </mat-form-field>\r\n\r\n    <mat-form-field>\r\n        <input matInput placeholder=\"Age\">\r\n    </mat-form-field>\r\n</mat-expansion-panel>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/expansion-overview/expansion-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpansionOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic expansion panel
 */
var ExpansionOverviewExample = /** @class */ (function () {
    function ExpansionOverviewExample() {
    }
    ExpansionOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'expansion-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/expansion-overview/expansion-overview-example.html")
        })
    ], ExpansionOverviewExample);
    return ExpansionOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/expansion-steps/expansion-steps-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-headers-align .mat-expansion-panel-header-title,\r\n.example-headers-align .mat-expansion-panel-header-description {\r\n    -ms-flex-preferred-size: 0;\r\n        flex-basis: 0;\r\n}\r\n\r\n.example-headers-align .mat-expansion-panel-header-description {\r\n    -webkit-box-pack: justify;\r\n        -ms-flex-pack: justify;\r\n            justify-content: space-between;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/expansion-steps/expansion-steps-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-accordion class=\"example-headers-align\">\r\n    <mat-expansion-panel [expanded]=\"step === 0\" (opened)=\"setStep(0)\" hideToggle=\"true\">\r\n        <mat-expansion-panel-header>\r\n            <mat-panel-title>\r\n                Personal data\r\n            </mat-panel-title>\r\n            <mat-panel-description>\r\n                Type your name and age\r\n                <mat-icon>account_circle</mat-icon>\r\n            </mat-panel-description>\r\n        </mat-expansion-panel-header>\r\n\r\n        <mat-form-field>\r\n            <input matInput placeholder=\"First name\">\r\n        </mat-form-field>\r\n\r\n        <mat-form-field>\r\n            <input matInput type=\"number\" min=\"1\" placeholder=\"Age\">\r\n        </mat-form-field>\r\n\r\n        <mat-action-row>\r\n            <button mat-button color=\"primary\" (click)=\"nextStep()\">Next</button>\r\n        </mat-action-row>\r\n    </mat-expansion-panel>\r\n\r\n    <mat-expansion-panel [expanded]=\"step === 1\" (opened)=\"setStep(1)\" hideToggle=\"true\">\r\n        <mat-expansion-panel-header>\r\n            <mat-panel-title>\r\n                Destination\r\n            </mat-panel-title>\r\n            <mat-panel-description>\r\n                Type the country name\r\n                <mat-icon>map</mat-icon>\r\n            </mat-panel-description>\r\n        </mat-expansion-panel-header>\r\n\r\n        <mat-form-field>\r\n            <input matInput placeholder=\"Country\">\r\n        </mat-form-field>\r\n\r\n        <mat-action-row>\r\n            <button mat-button color=\"warn\" (click)=\"prevStep()\">Previous</button>\r\n            <button mat-button color=\"primary\" (click)=\"nextStep()\">Next</button>\r\n        </mat-action-row>\r\n    </mat-expansion-panel>\r\n\r\n    <mat-expansion-panel [expanded]=\"step === 2\" (opened)=\"setStep(2)\" hideToggle=\"true\">\r\n        <mat-expansion-panel-header>\r\n            <mat-panel-title>\r\n                Day of the trip\r\n            </mat-panel-title>\r\n            <mat-panel-description>\r\n                Inform the date you wish to travel\r\n                <mat-icon>date_range</mat-icon>\r\n            </mat-panel-description>\r\n        </mat-expansion-panel-header>\r\n\r\n        <mat-form-field>\r\n            <input matInput placeholder=\"Date\" [matDatepicker]=\"picker\" (focus)=\"picker.open()\" readonly>\r\n        </mat-form-field>\r\n        <mat-datepicker #picker></mat-datepicker>\r\n\r\n        <mat-action-row>\r\n            <button mat-button color=\"warn\" (click)=\"prevStep()\">Previous</button>\r\n            <button mat-button color=\"primary\" (click)=\"nextStep()\">End</button>\r\n        </mat-action-row>\r\n    </mat-expansion-panel>\r\n\r\n</mat-accordion>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/expansion-steps/expansion-steps-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpansionStepsExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Expansion panel as accordion
 */
var ExpansionStepsExample = /** @class */ (function () {
    function ExpansionStepsExample() {
        this.step = 0;
    }
    ExpansionStepsExample.prototype.setStep = function (index) {
        this.step = index;
    };
    ExpansionStepsExample.prototype.nextStep = function () {
        this.step++;
    };
    ExpansionStepsExample.prototype.prevStep = function () {
        this.step--;
    };
    ExpansionStepsExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'expansion-steps-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/expansion-steps/expansion-steps-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/expansion-steps/expansion-steps-example.css")]
        })
    ], ExpansionStepsExample);
    return ExpansionStepsExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-custom-control/form-field-custom-control-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "div {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n}\r\n\r\ninput {\r\n    border: none;\r\n    background: none;\r\n    padding: 0;\r\n    outline: none;\r\n    font: inherit;\r\n    text-align: center;\r\n}\r\n\r\nspan {\r\n    opacity: 0;\r\n    -webkit-transition: opacity 200ms;\r\n    transition: opacity 200ms;\r\n}\r\n\r\n:host.floating span {\r\n    opacity: 1;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-custom-control/form-field-custom-control-example.html":
/***/ (function(module, exports) {

module.exports = "<div [formGroup]=\"parts\">\r\n    <input class=\"area\" formControlName=\"area\" size=\"3\" [disabled]=\"disabled\">\r\n    <span>&ndash;</span>\r\n    <input class=\"exchange\" formControlName=\"exchange\" size=\"3\" [disabled]=\"disabled\">\r\n    <span>&ndash;</span>\r\n    <input class=\"subscriber\" formControlName=\"subscriber\" size=\"4\" [disabled]=\"disabled\">\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-custom-control/form-field-custom-control-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export MyTel */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return MyTelInput; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormFieldCustomControlExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_cdk_a11y__ = __webpack_require__("../../../cdk/esm5/a11y.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_cdk_coercion__ = __webpack_require__("../../../cdk/esm5/coercion.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material_form_field__ = __webpack_require__("../../../material/esm5/form-field.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_Subject__ = __webpack_require__("../../../../rxjs/_esm5/Subject.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/** Data structure for holding telephone number. */
var MyTel = /** @class */ (function () {
    function MyTel(area, exchange, subscriber) {
        this.area = area;
        this.exchange = exchange;
        this.subscriber = subscriber;
    }
    return MyTel;
}());

/** Custom `MatFormFieldControl` for telephone number input. */
var MyTelInput = /** @class */ (function () {
    function MyTelInput(fb, fm, elRef, renderer) {
        var _this = this;
        this.fm = fm;
        this.elRef = elRef;
        this.stateChanges = new __WEBPACK_IMPORTED_MODULE_5_rxjs_Subject__["a" /* Subject */]();
        this.focused = false;
        this.ngControl = null;
        this.errorState = false;
        this.controlType = 'my-tel-input';
        this.id = "my-tel-input-" + MyTelInput_1.nextId++;
        this.describedBy = '';
        this._required = false;
        this._disabled = false;
        this.parts = fb.group({
            'area': '',
            'exchange': '',
            'subscriber': ''
        });
        fm.monitor(elRef.nativeElement, renderer, true).subscribe(function (origin) {
            _this.focused = !!origin;
            _this.stateChanges.next();
        });
    }
    MyTelInput_1 = MyTelInput;
    Object.defineProperty(MyTelInput.prototype, "empty", {
        get: function () {
            var n = this.parts.value;
            return !n.area && !n.exchange && !n.subscriber;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyTelInput.prototype, "shouldPlaceholderFloat", {
        get: function () {
            return this.focused || !this.empty;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyTelInput.prototype, "placeholder", {
        get: function () {
            return this._placeholder;
        },
        set: function (plh) {
            this._placeholder = plh;
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyTelInput.prototype, "required", {
        get: function () {
            return this._required;
        },
        set: function (req) {
            this._required = Object(__WEBPACK_IMPORTED_MODULE_1__angular_cdk_coercion__["c" /* coerceBooleanProperty */])(req);
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyTelInput.prototype, "disabled", {
        get: function () {
            return this._disabled;
        },
        set: function (dis) {
            this._disabled = Object(__WEBPACK_IMPORTED_MODULE_1__angular_cdk_coercion__["c" /* coerceBooleanProperty */])(dis);
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyTelInput.prototype, "value", {
        get: function () {
            var n = this.parts.value;
            if (n.area.length == 3 && n.exchange.length == 3 && n.subscriber.length == 4) {
                return new MyTel(n.area, n.exchange, n.subscriber);
            }
            return null;
        },
        set: function (tel) {
            tel = tel || new MyTel('', '', '');
            this.parts.setValue({
                area: tel.area,
                exchange: tel.exchange,
                subscriber: tel.subscriber
            });
            this.stateChanges.next();
        },
        enumerable: true,
        configurable: true
    });
    MyTelInput.prototype.ngOnDestroy = function () {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef.nativeElement);
    };
    MyTelInput.prototype.setDescribedByIds = function (ids) {
        this.describedBy = ids.join(' ');
    };
    MyTelInput.prototype.onContainerClick = function (event) {
        if (event.target.tagName.toLowerCase() != 'input') {
            this.elRef.nativeElement.querySelector('input').focus();
        }
    };
    MyTelInput.nextId = 0;
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Input"])(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], MyTelInput.prototype, "placeholder", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Input"])(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], MyTelInput.prototype, "required", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Input"])(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], MyTelInput.prototype, "disabled", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Input"])(),
        __metadata("design:type", MyTel),
        __metadata("design:paramtypes", [MyTel])
    ], MyTelInput.prototype, "value", null);
    MyTelInput = MyTelInput_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'my-tel-input',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/form-field-custom-control/form-field-custom-control-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/form-field-custom-control/form-field-custom-control-example.css")],
            providers: [
                {
                    provide: __WEBPACK_IMPORTED_MODULE_4__angular_material_form_field__["b" /* MatFormFieldControl */],
                    useExisting: MyTelInput_1
                }
            ],
            host: {
                '[class.floating]': 'shouldPlaceholderFloat',
                '[id]': 'id',
                '[attr.aria-describedby]': 'describedBy'
            }
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_0__angular_cdk_a11y__["f" /* FocusMonitor */], __WEBPACK_IMPORTED_MODULE_2__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_2__angular_core__["Renderer2"]])
    ], MyTelInput);
    return MyTelInput;
    var MyTelInput_1;
}());

/** @title Form field with custom telephone number input control. */
var FormFieldCustomControlExample = /** @class */ (function () {
    function FormFieldCustomControlExample() {
    }
    FormFieldCustomControlExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'form-field-custom-control-example',
            template: "\n        <mat-form-field>\n            <my-tel-input placeholder=\"Phone number\" required></my-tel-input>\n            <mat-icon matSuffix>phone</mat-icon>\n            <mat-hint>Include area code</mat-hint>\n        </mat-form-field>\n    "
        })
    ], FormFieldCustomControlExample);
    return FormFieldCustomControlExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-error/form-field-error-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n}\r\n\r\n.example-container > * {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-error/form-field-error-example.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container\">\r\n    <mat-form-field>\r\n        <input matInput placeholder=\"Enter your email\" [formControl]=\"email\" required>\r\n        <mat-error *ngIf=\"email.invalid\">{{getErrorMessage()}}</mat-error>\r\n    </mat-form-field>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-error/form-field-error-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormFieldErrorExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/** @title Form field with error messages */
var FormFieldErrorExample = /** @class */ (function () {
    function FormFieldErrorExample() {
        this.email = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].email]);
    }
    FormFieldErrorExample.prototype.getErrorMessage = function () {
        return this.email.hasError('required') ? 'You must enter a value' :
            this.email.hasError('email') ? 'Not a valid email' :
                '';
    };
    FormFieldErrorExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'form-field-error-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/form-field-error/form-field-error-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/form-field-error/form-field-error-example.css")]
        })
    ], FormFieldErrorExample);
    return FormFieldErrorExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-hint/form-field-hint-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n}\r\n\r\n.example-container > * {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-hint/form-field-hint-example.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container\">\r\n    <mat-form-field hintLabel=\"Max 10 characters\">\r\n        <input matInput #input maxlength=\"10\" placeholder=\"Enter some input\">\r\n        <mat-hint align=\"end\">{{input.value?.length || 0}}/10</mat-hint>\r\n    </mat-form-field>\r\n\r\n    <mat-form-field>\r\n        <mat-select placeholder=\"Select me\">\r\n            <mat-option value=\"option\">Option</mat-option>\r\n        </mat-select>\r\n        <mat-hint align=\"end\">Here's the dropdown arrow ^</mat-hint>\r\n    </mat-form-field>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-hint/form-field-hint-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormFieldHintExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Form field with hints */
var FormFieldHintExample = /** @class */ (function () {
    function FormFieldHintExample() {
    }
    FormFieldHintExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'form-field-hint-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/form-field-hint/form-field-hint-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/form-field-hint/form-field-hint-example.css")]
        })
    ], FormFieldHintExample);
    return FormFieldHintExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-overview/form-field-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n}\r\n\r\n.example-container > * {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-overview/form-field-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container\">\r\n    <mat-form-field>\r\n        <input matInput placeholder=\"Input\">\r\n    </mat-form-field>\r\n\r\n    <mat-form-field>\r\n        <textarea matInput placeholder=\"Textarea\"></textarea>\r\n    </mat-form-field>\r\n\r\n    <mat-form-field>\r\n        <mat-select placeholder=\"Select\">\r\n            <mat-option value=\"option\">Option</mat-option>\r\n        </mat-select>\r\n    </mat-form-field>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-overview/form-field-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormFieldOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Simple form field */
var FormFieldOverviewExample = /** @class */ (function () {
    function FormFieldOverviewExample() {
    }
    FormFieldOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'form-field-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/form-field-overview/form-field-overview-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/form-field-overview/form-field-overview-example.css")]
        })
    ], FormFieldOverviewExample);
    return FormFieldOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-placeholder/form-field-placeholder-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n}\r\n\r\n.example-container > * {\r\n    width: 100%;\r\n}\r\n\r\n.example-container form {\r\n    margin-bottom: 20px;\r\n}\r\n\r\n.example-container form > * {\r\n    margin: 5px 0;\r\n}\r\n\r\n.example-container .mat-radio-button {\r\n    margin: 0 5px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-placeholder/form-field-placeholder-example.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container\">\r\n    <form class=\"example-container\" [formGroup]=\"options\">\r\n        <mat-checkbox formControlName=\"hideRequired\">Hide required marker</mat-checkbox>\r\n        <div>\r\n            <label>Float placeholder: </label>\r\n            <mat-radio-group formControlName=\"floatPlaceholder\">\r\n                <mat-radio-button value=\"auto\">Auto</mat-radio-button>\r\n                <mat-radio-button value=\"always\">Always</mat-radio-button>\r\n                <mat-radio-button value=\"never\">Never</mat-radio-button>\r\n            </mat-radio-group>\r\n        </div>\r\n    </form>\r\n\r\n    <mat-form-field\r\n        [hideRequiredMarker]=\"options.value.hideRequired\"\r\n        [floatPlaceholder]=\"options.value.floatPlaceholder\">\r\n        <input matInput placeholder=\"Simple placeholder\" required>\r\n    </mat-form-field>\r\n\r\n    <mat-form-field\r\n        [hideRequiredMarker]=\"options.value.hideRequired\"\r\n        [floatPlaceholder]=\"options.value.floatPlaceholder\">\r\n        <mat-select required>\r\n            <mat-option>-- None --</mat-option>\r\n            <mat-option value=\"option\">Option</mat-option>\r\n        </mat-select>\r\n        <mat-placeholder>\r\n            <mat-icon>favorite</mat-icon>\r\n            <b> Fancy</b> <i> placeholder</i></mat-placeholder>\r\n    </mat-form-field>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-placeholder/form-field-placeholder-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormFieldPlaceholderExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/** @title Form field with placeholder */
var FormFieldPlaceholderExample = /** @class */ (function () {
    function FormFieldPlaceholderExample(fb) {
        this.options = fb.group({
            hideRequired: false,
            floatPlaceholder: 'auto'
        });
    }
    FormFieldPlaceholderExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'form-field-placeholder-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/form-field-placeholder/form-field-placeholder-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/form-field-placeholder/form-field-placeholder-example.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormBuilder */]])
    ], FormFieldPlaceholderExample);
    return FormFieldPlaceholderExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-prefix-suffix/form-field-prefix-suffix-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n}\r\n\r\n.example-container > * {\r\n    width: 100%;\r\n}\r\n\r\n.example-right-align {\r\n    text-align: right;\r\n}\r\n\r\ninput.example-right-align::-webkit-outer-spin-button,\r\ninput.example-right-align::-webkit-inner-spin-button {\r\n    display: none;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-prefix-suffix/form-field-prefix-suffix-example.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container\">\r\n    <mat-form-field>\r\n        <input matInput placeholder=\"Enter your password\" [type]=\"hide ? 'password' : 'text'\">\r\n        <mat-icon matSuffix (click)=\"hide = !hide\">{{hide ? 'visibility' : 'visibility_off'}}</mat-icon>\r\n    </mat-form-field>\r\n\r\n    <mat-form-field>\r\n        <input matInput placeholder=\"Amount\" type=\"number\" class=\"example-right-align\">\r\n        <span matPrefix>$&nbsp;</span>\r\n        <span matSuffix>.00</span>\r\n    </mat-form-field>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-prefix-suffix/form-field-prefix-suffix-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormFieldPrefixSuffixExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Form field with prefix & suffix */
var FormFieldPrefixSuffixExample = /** @class */ (function () {
    function FormFieldPrefixSuffixExample() {
        this.hide = true;
    }
    FormFieldPrefixSuffixExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'form-field-prefix-suffix-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/form-field-prefix-suffix/form-field-prefix-suffix-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/form-field-prefix-suffix/form-field-prefix-suffix-example.css")]
        })
    ], FormFieldPrefixSuffixExample);
    return FormFieldPrefixSuffixExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-theming/form-field-theming-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n}\r\n\r\n.example-container > * {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-theming/form-field-theming-example.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"example-container\" [formGroup]=\"options\" [style.fontSize.px]=\"getFontSize()\">\r\n    <mat-form-field [color]=\"options.value.color\">\r\n        <mat-select placeholder=\"Color\" formControlName=\"color\">\r\n            <mat-option value=\"primary\">Primary</mat-option>\r\n            <mat-option value=\"accent\">Accent</mat-option>\r\n            <mat-option value=\"warn\">Warn</mat-option>\r\n        </mat-select>\r\n    </mat-form-field>\r\n\r\n    <mat-form-field [color]=\"options.value.color\">\r\n        <input matInput type=\"number\" placeholder=\"Font size (px)\" formControlName=\"fontSize\" min=\"10\">\r\n        <mat-error *ngIf=\"options.get('fontSize')?.invalid\">Min size: 10px</mat-error>\r\n    </mat-form-field>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/form-field-theming/form-field-theming-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormFieldThemingExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/** @title Form field theming */
var FormFieldThemingExample = /** @class */ (function () {
    function FormFieldThemingExample(fb) {
        this.options = fb.group({
            'color': 'primary',
            'fontSize': [16, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].min(10)]
        });
    }
    FormFieldThemingExample.prototype.getFontSize = function () {
        return Math.max(10, this.options.value.fontSize);
    };
    FormFieldThemingExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'form-field-theming-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/form-field-theming/form-field-theming-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/form-field-theming/form-field-theming-example.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormBuilder */]])
    ], FormFieldThemingExample);
    return FormFieldThemingExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/grid-list-dynamic/grid-list-dynamic-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-grid-list cols=\"4\" rowHeight=\"100px\">\r\n    <mat-grid-tile\r\n        *ngFor=\"let tile of tiles\"\r\n        [colspan]=\"tile.cols\"\r\n        [rowspan]=\"tile.rows\"\r\n        [style.background]=\"tile.color\">\r\n        {{tile.text}}\r\n    </mat-grid-tile>\r\n</mat-grid-list>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/grid-list-dynamic/grid-list-dynamic-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GridListDynamicExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Dynamic grid-list
 */
var GridListDynamicExample = /** @class */ (function () {
    function GridListDynamicExample() {
        this.tiles = [
            {
                text: 'One',
                cols: 3,
                rows: 1,
                color: 'lightblue'
            },
            {
                text: 'Two',
                cols: 1,
                rows: 2,
                color: 'lightgreen'
            },
            {
                text: 'Three',
                cols: 1,
                rows: 1,
                color: 'lightpink'
            },
            {
                text: 'Four',
                cols: 2,
                rows: 1,
                color: '#DDBDF1'
            }
        ];
    }
    GridListDynamicExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'grid-list-dynamic-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/grid-list-dynamic/grid-list-dynamic-example.html")
        })
    ], GridListDynamicExample);
    return GridListDynamicExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/grid-list-overview/grid-list-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "mat-grid-tile {\r\n    background: lightblue;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/grid-list-overview/grid-list-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-grid-list cols=\"2\" rowHeight=\"2:1\">\r\n    <mat-grid-tile>1</mat-grid-tile>\r\n    <mat-grid-tile>2</mat-grid-tile>\r\n    <mat-grid-tile>3</mat-grid-tile>\r\n    <mat-grid-tile>4</mat-grid-tile>\r\n</mat-grid-list>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/grid-list-overview/grid-list-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GridListOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic grid-list
 */
var GridListOverviewExample = /** @class */ (function () {
    function GridListOverviewExample() {
    }
    GridListOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'grid-list-overview-example',
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/grid-list-overview/grid-list-overview-example.css")],
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/grid-list-overview/grid-list-overview-example.html")
        })
    ], GridListOverviewExample);
    return GridListOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/icon-overview/icon-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-icon>home</mat-icon>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/icon-overview/icon-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IconOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic icons
 */
var IconOverviewExample = /** @class */ (function () {
    function IconOverviewExample() {
    }
    IconOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'icon-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/icon-overview/icon-overview-example.html")
        })
    ], IconOverviewExample);
    return IconOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/icon-svg/icon-svg-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-icon svgIcon=\"thumbs-up\"></mat-icon>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/icon-svg/icon-svg-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IconSvgExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * @title SVG icons
 */
var IconSvgExample = /** @class */ (function () {
    function IconSvgExample(iconRegistry, sanitizer) {
        iconRegistry.addSvgIcon('thumbs-up', sanitizer.bypassSecurityTrustResourceUrl('assets/images/examples/thumbup-icon.svg'));
    }
    IconSvgExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'icon-svg-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/icon-svg/icon-svg-example.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_material__["u" /* MatIconRegistry */], __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["DomSanitizer"]])
    ], IconSvgExample);
    return IconSvgExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-autosize-textarea/input-autosize-textarea-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-autosize-textarea/input-autosize-textarea-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n  <textarea matInput placeholder=\"Autosize textarea\" matTextareaAutosize matAutosizeMinRows=\"2\"\r\n            matAutosizeMaxRows=\"5\"></textarea>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-autosize-textarea/input-autosize-textarea-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputAutosizeTextareaExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Auto-resizing textarea */
var InputAutosizeTextareaExample = /** @class */ (function () {
    function InputAutosizeTextareaExample() {
    }
    InputAutosizeTextareaExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'input-autosize-textarea-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/input-autosize-textarea/input-autosize-textarea-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/input-autosize-textarea/input-autosize-textarea-example.css")]
        })
    ], InputAutosizeTextareaExample);
    return InputAutosizeTextareaExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-clearable/input-clearable-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-form-field {\r\n    width: 200px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-clearable/input-clearable-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field class=\"example-form-field\">\r\n    <input matInput type=\"text\" placeholder=\"Clearable input\" [(ngModel)]=\"value\"/>\r\n    <button mat-button *ngIf=\"value\" matSuffix mat-icon-button aria-label=\"Clear\" (click)=\"value=''\">\r\n        <mat-icon>close</mat-icon>\r\n    </button>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-clearable/input-clearable-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputClearableExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Input with a clear button
 */
var InputClearableExample = /** @class */ (function () {
    function InputClearableExample() {
        this.value = 'Clear me';
    }
    InputClearableExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'input-clearable-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/input-clearable/input-clearable-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/input-clearable/input-clearable-example.css")]
        })
    ], InputClearableExample);
    return InputClearableExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-error-state-matcher/input-error-state-matcher-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-form {\r\n    min-width: 150px;\r\n    max-width: 500px;\r\n    width: 100%;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-error-state-matcher/input-error-state-matcher-example.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"example-form\">\r\n    <mat-form-field class=\"example-full-width\">\r\n        <input matInput placeholder=\"Email\" [formControl]=\"emailFormControl\"\r\n               [errorStateMatcher]=\"matcher\">\r\n        <mat-hint>Errors appear instantly!</mat-hint>\r\n        <mat-error *ngIf=\"emailFormControl.hasError('email') && !emailFormControl.hasError('required')\">\r\n            Please enter a valid email address\r\n        </mat-error>\r\n        <mat-error *ngIf=\"emailFormControl.hasError('required')\">\r\n            Email is <strong>required</strong>\r\n        </mat-error>\r\n    </mat-form-field>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-error-state-matcher/input-error-state-matcher-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export MyErrorStateMatcher */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputErrorStateMatcherExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/** Error when invalid control is dirty, touched, or submitted. */
var MyErrorStateMatcher = /** @class */ (function () {
    function MyErrorStateMatcher() {
    }
    MyErrorStateMatcher.prototype.isErrorState = function (control, form) {
        var isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    };
    return MyErrorStateMatcher;
}());

/** @title Input with a custom ErrorStateMatcher */
var InputErrorStateMatcherExample = /** @class */ (function () {
    function InputErrorStateMatcherExample() {
        this.emailFormControl = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */]('', [
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required,
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].email
        ]);
        this.matcher = new MyErrorStateMatcher();
    }
    InputErrorStateMatcherExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'input-error-state-matcher-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/input-error-state-matcher/input-error-state-matcher-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/input-error-state-matcher/input-error-state-matcher-example.css")]
        })
    ], InputErrorStateMatcherExample);
    return InputErrorStateMatcherExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-errors/input-errors-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-form {\r\n    min-width: 150px;\r\n    max-width: 500px;\r\n    width: 100%;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-errors/input-errors-example.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"example-form\">\r\n    <mat-form-field class=\"example-full-width\">\r\n        <input matInput placeholder=\"Email\" [formControl]=\"emailFormControl\">\r\n        <mat-error *ngIf=\"emailFormControl.hasError('email') && !emailFormControl.hasError('required')\">\r\n            Please enter a valid email address\r\n        </mat-error>\r\n        <mat-error *ngIf=\"emailFormControl.hasError('required')\">\r\n            Email is <strong>required</strong>\r\n        </mat-error>\r\n    </mat-form-field>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-errors/input-errors-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputErrorsExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/**
 * @title Input with error messages
 */
var InputErrorsExample = /** @class */ (function () {
    function InputErrorsExample() {
        this.emailFormControl = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */]('', [
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required,
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].email
        ]);
    }
    InputErrorsExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'input-errors-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/input-errors/input-errors-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/input-errors/input-errors-example.css")]
        })
    ], InputErrorsExample);
    return InputErrorsExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-form/input-form-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-form {\r\n    min-width: 150px;\r\n    max-width: 500px;\r\n    width: 100%;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-form/input-form-example.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"example-form\">\r\n    <mat-form-field class=\"example-full-width\">\r\n        <input matInput placeholder=\"Company (disabled)\" disabled value=\"Google\">\r\n    </mat-form-field>\r\n\r\n    <table class=\"example-full-width\" cellspacing=\"0\">\r\n        <tr>\r\n            <td>\r\n                <mat-form-field class=\"example-full-width\">\r\n                    <input matInput placeholder=\"First name\">\r\n                </mat-form-field>\r\n            </td>\r\n            <td>\r\n                <mat-form-field class=\"example-full-width\">\r\n                    <input matInput placeholder=\"Long Last Name That Will Be Truncated\">\r\n                </mat-form-field>\r\n            </td>\r\n        </tr>\r\n    </table>\r\n\r\n    <p>\r\n        <mat-form-field class=\"example-full-width\">\r\n            <textarea matInput placeholder=\"Address\">1600 Amphitheatre Pkwy</textarea>\r\n        </mat-form-field>\r\n        <mat-form-field class=\"example-full-width\">\r\n            <textarea matInput placeholder=\"Address 2\"></textarea>\r\n        </mat-form-field>\r\n    </p>\r\n\r\n    <table class=\"example-full-width\" cellspacing=\"0\">\r\n        <tr>\r\n            <td>\r\n                <mat-form-field class=\"example-full-width\">\r\n                    <input matInput placeholder=\"City\">\r\n                </mat-form-field>\r\n            </td>\r\n            <td>\r\n                <mat-form-field class=\"example-full-width\">\r\n                    <input matInput placeholder=\"State\">\r\n                </mat-form-field>\r\n            </td>\r\n            <td>\r\n                <mat-form-field class=\"example-full-width\">\r\n                    <input matInput #postalCode maxlength=\"5\" placeholder=\"Postal Code\" value=\"94043\">\r\n                    <mat-hint align=\"end\">{{postalCode.value.length}} / 5</mat-hint>\r\n                </mat-form-field>\r\n            </td>\r\n        </tr>\r\n    </table>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-form/input-form-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputFormExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Inputs in a form
 */
var InputFormExample = /** @class */ (function () {
    function InputFormExample() {
    }
    InputFormExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'input-form-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/input-form/input-form-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/input-form/input-form-example.css")]
        })
    ], InputFormExample);
    return InputFormExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-hint/input-hint-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-form {\r\n    min-width: 150px;\r\n    max-width: 500px;\r\n    width: 100%;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-hint/input-hint-example.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"example-form\">\r\n\r\n    <mat-form-field class=\"example-full-width\">\r\n        <input matInput #message maxlength=\"256\" placeholder=\"Message\">\r\n        <mat-hint align=\"start\"><strong>Don't disclose personal info</strong></mat-hint>\r\n        <mat-hint align=\"end\">{{message.value.length}} / 256</mat-hint>\r\n    </mat-form-field>\r\n\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-hint/input-hint-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputHintExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Input with hints
 */
var InputHintExample = /** @class */ (function () {
    function InputHintExample() {
    }
    InputHintExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'input-hint-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/input-hint/input-hint-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/input-hint/input-hint-example.css")]
        })
    ], InputHintExample);
    return InputHintExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-overview/input-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-form {\r\n    min-width: 150px;\r\n    max-width: 500px;\r\n    width: 100%;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-overview/input-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"example-form\">\r\n    <mat-form-field class=\"example-full-width\">\r\n        <input matInput placeholder=\"Favorite food\" value=\"Sushi\">\r\n    </mat-form-field>\r\n\r\n    <mat-form-field class=\"example-full-width\">\r\n        <textarea matInput placeholder=\"Leave a comment\"></textarea>\r\n    </mat-form-field>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-overview/input-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic Inputs
 */
var InputOverviewExample = /** @class */ (function () {
    function InputOverviewExample() {
    }
    InputOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'input-overview-example',
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/input-overview/input-overview-example.css")],
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/input-overview/input-overview-example.html")
        })
    ], InputOverviewExample);
    return InputOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-prefix-suffix/input-prefix-suffix-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-form {\r\n    min-width: 150px;\r\n    max-width: 500px;\r\n    width: 100%;\r\n}\r\n\r\n.example-full-width {\r\n    width: 100%;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-prefix-suffix/input-prefix-suffix-example.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"example-form\">\r\n\r\n    <mat-form-field class=\"example-full-width\">\r\n        <span matPrefix>+1 &nbsp;</span>\r\n        <input type=\"tel\" matInput placeholder=\"Telephone\">\r\n        <mat-icon matSuffix>mode_edit</mat-icon>\r\n    </mat-form-field>\r\n\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/input-prefix-suffix/input-prefix-suffix-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputPrefixSuffixExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Inputs with prefixes and suffixes
 */
var InputPrefixSuffixExample = /** @class */ (function () {
    function InputPrefixSuffixExample() {
    }
    InputPrefixSuffixExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'input-prefix-suffix-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/input-prefix-suffix/input-prefix-suffix-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/input-prefix-suffix/input-prefix-suffix-example.css")]
        })
    ], InputPrefixSuffixExample);
    return InputPrefixSuffixExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/list-overview/list-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-list>\r\n    <mat-list-item>Item 1</mat-list-item>\r\n    <mat-list-item>Item 2</mat-list-item>\r\n    <mat-list-item>Item 3</mat-list-item>\r\n</mat-list>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/list-overview/list-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic list
 */
var ListOverviewExample = /** @class */ (function () {
    function ListOverviewExample() {
    }
    ListOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'list-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/list-overview/list-overview-example.html")
        })
    ], ListOverviewExample);
    return ListOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/list-sections/list-sections-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".mat-list-icon {\r\n    color: rgba(0, 0, 0, 0.54);\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/list-sections/list-sections-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-list>\r\n    <h3 mat-subheader>Folders</h3>\r\n    <mat-list-item *ngFor=\"let folder of folders\">\r\n        <mat-icon mat-list-icon>folder</mat-icon>\r\n        <h4 mat-line>{{folder.name}}</h4>\r\n        <p mat-line> {{folder.updated | date}} </p>\r\n    </mat-list-item>\r\n    <mat-divider></mat-divider>\r\n    <h3 mat-subheader>Notes</h3>\r\n    <mat-list-item *ngFor=\"let note of notes\">\r\n        <mat-icon mat-list-icon>note</mat-icon>\r\n        <h4 mat-line>{{note.name}}</h4>\r\n        <p mat-line> {{note.updated | date}} </p>\r\n    </mat-list-item>\r\n</mat-list>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/list-sections/list-sections-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListSectionsExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title List with sections
 */
var ListSectionsExample = /** @class */ (function () {
    function ListSectionsExample() {
        this.folders = [
            {
                name: 'Photos',
                updated: new Date('1/1/16')
            },
            {
                name: 'Recipes',
                updated: new Date('1/17/16')
            },
            {
                name: 'Work',
                updated: new Date('1/28/16')
            }
        ];
        this.notes = [
            {
                name: 'Vacation Itinerary',
                updated: new Date('2/20/16')
            },
            {
                name: 'Kitchen Remodel',
                updated: new Date('1/18/16')
            }
        ];
    }
    ListSectionsExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'list-sections-example',
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/list-sections/list-sections-example.css")],
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/list-sections/list-sections-example.html")
        })
    ], ListSectionsExample);
    return ListSectionsExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/list-selection/list-selection-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No styles for this example. */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/list-selection/list-selection-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-selection-list #shoes>\r\n    <mat-list-option *ngFor=\"let shoe of typesOfShoes\">\r\n        {{shoe}}\r\n    </mat-list-option>\r\n</mat-selection-list>\r\n\r\n<p>\r\n    Options selected: {{shoes.selectedOptions.selected.length}}\r\n</p>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/list-selection/list-selection-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListSelectionExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title List with selection
 */
var ListSelectionExample = /** @class */ (function () {
    function ListSelectionExample() {
        this.typesOfShoes = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];
    }
    ListSelectionExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'list-selection-example',
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/list-selection/list-selection-example.css")],
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/list-selection/list-selection-example.html")
        })
    ], ListSelectionExample);
    return ListSelectionExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/menu-icons/menu-icons-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/menu-icons/menu-icons-example.html":
/***/ (function(module, exports) {

module.exports = "<button mat-icon-button [matMenuTriggerFor]=\"menu\">\r\n    <mat-icon>more_vert</mat-icon>\r\n</button>\r\n<mat-menu #menu=\"matMenu\">\r\n    <button mat-menu-item>\r\n        <mat-icon>dialpad</mat-icon>\r\n        <span>Redial</span>\r\n    </button>\r\n    <button mat-menu-item disabled>\r\n        <mat-icon>voicemail</mat-icon>\r\n        <span>Check voicemail</span>\r\n    </button>\r\n    <button mat-menu-item>\r\n        <mat-icon>notifications_off</mat-icon>\r\n        <span>Disable alerts</span>\r\n    </button>\r\n</mat-menu>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/menu-icons/menu-icons-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuIconsExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Menu with icons
 */
var MenuIconsExample = /** @class */ (function () {
    function MenuIconsExample() {
    }
    MenuIconsExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'menu-icons-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/menu-icons/menu-icons-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/menu-icons/menu-icons-example.css")]
        })
    ], MenuIconsExample);
    return MenuIconsExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/menu-overview/menu-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/menu-overview/menu-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<button mat-button [matMenuTriggerFor]=\"menu\">Menu</button>\r\n<mat-menu #menu=\"matMenu\">\r\n    <button mat-menu-item>Item 1</button>\r\n    <button mat-menu-item>Item 2</button>\r\n</mat-menu>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/menu-overview/menu-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic menu
 */
var MenuOverviewExample = /** @class */ (function () {
    function MenuOverviewExample() {
    }
    MenuOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'menu-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/menu-overview/menu-overview-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/menu-overview/menu-overview-example.css")]
        })
    ], MenuOverviewExample);
    return MenuOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/nested-menu/nested-menu-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/nested-menu/nested-menu-example.html":
/***/ (function(module, exports) {

module.exports = "<button mat-button [matMenuTriggerFor]=\"animals\">Animal index</button>\r\n\r\n<mat-menu #animals=\"matMenu\">\r\n    <button mat-menu-item [matMenuTriggerFor]=\"vertebrates\">Vertebrates</button>\r\n    <button mat-menu-item [matMenuTriggerFor]=\"invertebrates\">Invertebrates</button>\r\n</mat-menu>\r\n\r\n<mat-menu #vertebrates=\"matMenu\">\r\n    <button mat-menu-item [matMenuTriggerFor]=\"fish\">Fishes</button>\r\n    <button mat-menu-item [matMenuTriggerFor]=\"amphibians\">Amphibians</button>\r\n    <button mat-menu-item [matMenuTriggerFor]=\"reptiles\">Reptiles</button>\r\n    <button mat-menu-item>Birds</button>\r\n    <button mat-menu-item>Mammals</button>\r\n</mat-menu>\r\n\r\n<mat-menu #invertebrates=\"matMenu\">\r\n    <button mat-menu-item>Insects</button>\r\n    <button mat-menu-item>Molluscs</button>\r\n    <button mat-menu-item>Crustaceans</button>\r\n    <button mat-menu-item>Corals</button>\r\n    <button mat-menu-item>Arachnids</button>\r\n    <button mat-menu-item>Velvet worms</button>\r\n    <button mat-menu-item>Horseshoe crabs</button>\r\n</mat-menu>\r\n\r\n<mat-menu #fish=\"matMenu\">\r\n    <button mat-menu-item>Baikal oilfish</button>\r\n    <button mat-menu-item>Bala shark</button>\r\n    <button mat-menu-item>Ballan wrasse</button>\r\n    <button mat-menu-item>Bamboo shark</button>\r\n    <button mat-menu-item>Banded killifish</button>\r\n</mat-menu>\r\n\r\n<mat-menu #amphibians=\"matMenu\">\r\n    <button mat-menu-item>Sonoran desert toad</button>\r\n    <button mat-menu-item>Western toad</button>\r\n    <button mat-menu-item>Arroyo toad</button>\r\n    <button mat-menu-item>Yosemite toad</button>\r\n</mat-menu>\r\n\r\n<mat-menu #reptiles=\"matMenu\">\r\n    <button mat-menu-item>Banded Day Gecko</button>\r\n    <button mat-menu-item>Banded Gila Monster</button>\r\n    <button mat-menu-item>Black Tree Monitor</button>\r\n    <button mat-menu-item>Blue Spiny Lizard</button>\r\n    <button mat-menu-item disabled>Velociraptor</button>\r\n</mat-menu>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/nested-menu/nested-menu-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NestedMenuExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Nested menu
 */
var NestedMenuExample = /** @class */ (function () {
    function NestedMenuExample() {
    }
    NestedMenuExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'nested-menu-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/nested-menu/nested-menu-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/nested-menu/nested-menu-example.css")]
        })
    ], NestedMenuExample);
    return NestedMenuExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/paginator-configurable/paginator-configurable-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    List length:\r\n    <input matInput [(ngModel)]=\"length\">\r\n</mat-form-field>\r\n\r\n<mat-form-field>\r\n    Page size:\r\n    <input matInput [(ngModel)]=\"pageSize\">\r\n</mat-form-field>\r\n<mat-form-field>\r\n    Page size options:\r\n    <input matInput\r\n           [ngModel]=\"pageSizeOptions\"\r\n           (ngModelChange)=\"setPageSizeOptions($event)\">\r\n</mat-form-field>\r\n\r\n<mat-paginator [length]=\"length\"\r\n               [pageSize]=\"pageSize\"\r\n               [pageSizeOptions]=\"pageSizeOptions\"\r\n               (page)=\"pageEvent = $event\">\r\n</mat-paginator>\r\n\r\n<div *ngIf=\"pageEvent\">\r\n    <h5>Page Change Event Properties</h5>\r\n    <div>List length: {{pageEvent.length}}</div>\r\n    <div>Page size: {{pageEvent.pageSize}}</div>\r\n    <div>Page index: {{pageEvent.pageIndex}}</div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/paginator-configurable/paginator-configurable-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaginatorConfigurableExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Configurable paginator
 */
var PaginatorConfigurableExample = /** @class */ (function () {
    function PaginatorConfigurableExample() {
        // MatPaginator Inputs
        this.length = 100;
        this.pageSize = 10;
        this.pageSizeOptions = [5, 10, 25, 100];
    }
    PaginatorConfigurableExample.prototype.setPageSizeOptions = function (setPageSizeOptionsInput) {
        this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(function (str) { return +str; });
    };
    PaginatorConfigurableExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'paginator-configurable-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/paginator-configurable/paginator-configurable-example.html")
        })
    ], PaginatorConfigurableExample);
    return PaginatorConfigurableExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/paginator-overview/paginator-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-paginator [length]=\"100\"\r\n               [pageSize]=\"10\"\r\n               [pageSizeOptions]=\"[5, 10, 25, 100]\">\r\n</mat-paginator>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/paginator-overview/paginator-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaginatorOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Paginator
 */
var PaginatorOverviewExample = /** @class */ (function () {
    function PaginatorOverviewExample() {
    }
    PaginatorOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'paginator-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/paginator-overview/paginator-overview-example.html")
        })
    ], PaginatorOverviewExample);
    return PaginatorOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-bar-buffer/progress-bar-buffer-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-progress-bar mode=\"buffer\"></mat-progress-bar>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-bar-buffer/progress-bar-buffer-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressBarBufferExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Buffer progress-bar
 */
var ProgressBarBufferExample = /** @class */ (function () {
    function ProgressBarBufferExample() {
    }
    ProgressBarBufferExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'progress-bar-buffer-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/progress-bar-buffer/progress-bar-buffer-example.html")
        })
    ], ProgressBarBufferExample);
    return ProgressBarBufferExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-bar-configurable/progress-bar-configurable-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-h2 {\r\n    margin: 10px;\r\n}\r\n\r\n.example-section {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -ms-flex-line-pack: center;\r\n        align-content: center;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    height: 60px;\r\n}\r\n\r\n.example-margin {\r\n    margin: 0 10px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-bar-configurable/progress-bar-configurable-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-card>\r\n    <mat-card-content>\r\n        <h2 class=\"example-h2\">Progress bar configuration</h2>\r\n\r\n        <section class=\"example-section\">\r\n            <label class=\"example-margin\">Color:</label>\r\n            <mat-radio-group [(ngModel)]=\"color\">\r\n                <mat-radio-button class=\"example-margin\" value=\"primary\">\r\n                    Primary\r\n                </mat-radio-button>\r\n                <mat-radio-button class=\"example-margin\" value=\"accent\">\r\n                    Accent\r\n                </mat-radio-button>\r\n                <mat-radio-button class=\"example-margin\" value=\"warn\">\r\n                    Warn\r\n                </mat-radio-button>\r\n            </mat-radio-group>\r\n        </section>\r\n\r\n        <section class=\"example-section\">\r\n            <label class=\"example-margin\">Mode:</label>\r\n            <mat-radio-group [(ngModel)]=\"mode\">\r\n                <mat-radio-button class=\"example-margin\" value=\"determinate\">\r\n                    Determinate\r\n                </mat-radio-button>\r\n                <mat-radio-button class=\"example-margin\" value=\"indeterminate\">\r\n                    Indeterminate\r\n                </mat-radio-button>\r\n                <mat-radio-button class=\"example-margin\" value=\"buffer\">\r\n                    Buffer\r\n                </mat-radio-button>\r\n                <mat-radio-button class=\"example-margin\" value=\"query\">\r\n                    Query\r\n                </mat-radio-button>\r\n            </mat-radio-group>\r\n        </section>\r\n\r\n        <section class=\"example-section\" *ngIf=\"mode == 'determinate' || mode == 'buffer'\">\r\n            <label class=\"example-margin\">Progress:</label>\r\n            <mat-slider class=\"example-margin\" [(ngModel)]=\"value\"></mat-slider>\r\n        </section>\r\n        <section class=\"example-section\" *ngIf=\"mode == 'buffer'\">\r\n            <label class=\"example-margin\">Buffer:</label>\r\n            <mat-slider class=\"example-margin\" [(ngModel)]=\"bufferValue\"></mat-slider>\r\n        </section>\r\n    </mat-card-content>\r\n</mat-card>\r\n\r\n<mat-card>\r\n    <mat-card-content>\r\n        <h2 class=\"example-h2\">Result</h2>\r\n\r\n        <section class=\"example-section\">\r\n            <mat-progress-bar\r\n                class=\"example-margin\"\r\n                [color]=\"color\"\r\n                [mode]=\"mode\"\r\n                [value]=\"value\"\r\n                [bufferValue]=\"bufferValue\">\r\n            </mat-progress-bar>\r\n        </section>\r\n    </mat-card-content>\r\n</mat-card>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-bar-configurable/progress-bar-configurable-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressBarConfigurableExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Configurable progress-bar
 */
var ProgressBarConfigurableExample = /** @class */ (function () {
    function ProgressBarConfigurableExample() {
        this.color = 'primary';
        this.mode = 'determinate';
        this.value = 50;
        this.bufferValue = 75;
    }
    ProgressBarConfigurableExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'progress-bar-configurable-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/progress-bar-configurable/progress-bar-configurable-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/progress-bar-configurable/progress-bar-configurable-example.css")]
        })
    ], ProgressBarConfigurableExample);
    return ProgressBarConfigurableExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-bar-determinate/progress-bar-determinate-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-progress-bar mode=\"determinate\" value=\"40\"></mat-progress-bar>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-bar-determinate/progress-bar-determinate-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressBarDeterminateExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Determinate progress-bar
 */
var ProgressBarDeterminateExample = /** @class */ (function () {
    function ProgressBarDeterminateExample() {
    }
    ProgressBarDeterminateExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'progress-bar-determinate-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/progress-bar-determinate/progress-bar-determinate-example.html")
        })
    ], ProgressBarDeterminateExample);
    return ProgressBarDeterminateExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-bar-indeterminate/progress-bar-indeterminate-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-progress-bar mode=\"indeterminate\"></mat-progress-bar>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-bar-indeterminate/progress-bar-indeterminate-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressBarIndeterminateExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Indeterminate progress-bar
 */
var ProgressBarIndeterminateExample = /** @class */ (function () {
    function ProgressBarIndeterminateExample() {
    }
    ProgressBarIndeterminateExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'progress-bar-indeterminate-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/progress-bar-indeterminate/progress-bar-indeterminate-example.html")
        })
    ], ProgressBarIndeterminateExample);
    return ProgressBarIndeterminateExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-bar-query/progress-bar-query-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-progress-bar mode=\"query\"></mat-progress-bar>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-bar-query/progress-bar-query-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressBarQueryExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Query progress-bar
 */
var ProgressBarQueryExample = /** @class */ (function () {
    function ProgressBarQueryExample() {
    }
    ProgressBarQueryExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'progress-bar-query-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/progress-bar-query/progress-bar-query-example.html")
        })
    ], ProgressBarQueryExample);
    return ProgressBarQueryExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-spinner-configurable/progress-spinner-configurable-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-h2 {\r\n    margin: 10px;\r\n}\r\n\r\n.example-section {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -ms-flex-line-pack: center;\r\n        align-content: center;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    height: 60px;\r\n}\r\n\r\n.example-margin {\r\n    margin: 0 10px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-spinner-configurable/progress-spinner-configurable-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-card>\r\n    <mat-card-content>\r\n        <h2 class=\"example-h2\">Progress spinner configuration</h2>\r\n\r\n        <section class=\"example-section\">\r\n            <label class=\"example-margin\">Color:</label>\r\n            <mat-radio-group [(ngModel)]=\"color\">\r\n                <mat-radio-button class=\"example-margin\" value=\"primary\">\r\n                    Primary\r\n                </mat-radio-button>\r\n                <mat-radio-button class=\"example-margin\" value=\"accent\">\r\n                    Accent\r\n                </mat-radio-button>\r\n                <mat-radio-button class=\"example-margin\" value=\"warn\">\r\n                    Warn\r\n                </mat-radio-button>\r\n            </mat-radio-group>\r\n        </section>\r\n\r\n        <section class=\"example-section\">\r\n            <label class=\"example-margin\">Mode:</label>\r\n            <mat-radio-group [(ngModel)]=\"mode\">\r\n                <mat-radio-button class=\"example-margin\" value=\"determinate\">\r\n                    Determinate\r\n                </mat-radio-button>\r\n                <mat-radio-button class=\"example-margin\" value=\"indeterminate\">\r\n                    Indeterminate\r\n                </mat-radio-button>\r\n            </mat-radio-group>\r\n        </section>\r\n\r\n        <section class=\"example-section\" *ngIf=\"mode == 'determinate'\">\r\n            <label class=\"example-margin\">Progress:</label>\r\n            <mat-slider class=\"example-margin\" [(ngModel)]=\"value\"></mat-slider>\r\n        </section>\r\n    </mat-card-content>\r\n</mat-card>\r\n<mat-card>\r\n    <mat-card-content>\r\n        <h2 class=\"example-h2\">Result</h2>\r\n\r\n        <mat-progress-spinner\r\n            class=\"example-margin\"\r\n            [color]=\"color\"\r\n            [mode]=\"mode\"\r\n            [value]=\"value\">\r\n        </mat-progress-spinner>\r\n    </mat-card-content>\r\n</mat-card>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-spinner-configurable/progress-spinner-configurable-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressSpinnerConfigurableExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Configurable progress spinner
 */
var ProgressSpinnerConfigurableExample = /** @class */ (function () {
    function ProgressSpinnerConfigurableExample() {
        this.color = 'primary';
        this.mode = 'determinate';
        this.value = 50;
    }
    ProgressSpinnerConfigurableExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'progress-spinner-configurable-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/progress-spinner-configurable/progress-spinner-configurable-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/progress-spinner-configurable/progress-spinner-configurable-example.css")]
        })
    ], ProgressSpinnerConfigurableExample);
    return ProgressSpinnerConfigurableExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-spinner-overview/progress-spinner-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-spinner></mat-spinner>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/progress-spinner-overview/progress-spinner-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressSpinnerOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic progress-spinner
 */
var ProgressSpinnerOverviewExample = /** @class */ (function () {
    function ProgressSpinnerOverviewExample() {
    }
    ProgressSpinnerOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'progress-spinner-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/progress-spinner-overview/progress-spinner-overview-example.html")
        })
    ], ProgressSpinnerOverviewExample);
    return ProgressSpinnerOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/radio-ng-model/radio-ng-model-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-radio-group {\r\n    display: -webkit-inline-box;\r\n    display: -ms-inline-flexbox;\r\n    display: inline-flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n}\r\n\r\n.example-radio-button {\r\n    margin: 5px;\r\n}\r\n\r\n.example-selected-value {\r\n    margin: 15px 0;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/radio-ng-model/radio-ng-model-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-radio-group class=\"example-radio-group\" [(ngModel)]=\"favoriteSeason\">\r\n    <mat-radio-button class=\"example-radio-button\" *ngFor=\"let season of seasons\" [value]=\"season\">\r\n        {{season}}\r\n    </mat-radio-button>\r\n</mat-radio-group>\r\n<div class=\"example-selected-value\">Your favorite season is: {{favoriteSeason}}</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/radio-ng-model/radio-ng-model-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RadioNgModelExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Radios with ngModel
 */
var RadioNgModelExample = /** @class */ (function () {
    function RadioNgModelExample() {
        this.seasons = [
            'Winter',
            'Spring',
            'Summer',
            'Autumn'
        ];
    }
    RadioNgModelExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'radio-ng-model-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/radio-ng-model/radio-ng-model-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/radio-ng-model/radio-ng-model-example.css")]
        })
    ], RadioNgModelExample);
    return RadioNgModelExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/radio-overview/radio-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".mat-radio-button ~ .mat-radio-button {\r\n    padding-right: 16px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/radio-overview/radio-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-radio-group>\r\n    <mat-radio-button value=\"1\">Option 1</mat-radio-button>\r\n    <mat-radio-button value=\"2\">Option 2</mat-radio-button>\r\n</mat-radio-group>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/radio-overview/radio-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RadioOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic radios
 */
var RadioOverviewExample = /** @class */ (function () {
    function RadioOverviewExample() {
    }
    RadioOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'radio-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/radio-overview/radio-overview-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/radio-overview/radio-overview-example.css")]
        })
    ], RadioOverviewExample);
    return RadioOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-custom-trigger/select-custom-trigger-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-additional-selection {\r\n    opacity: 0.75;\r\n    font-size: 0.75em;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-custom-trigger/select-custom-trigger-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <mat-select placeholder=\"Toppings\" [formControl]=\"toppings\" multiple>\r\n        <mat-select-trigger>\r\n            {{toppings.value ? toppings.value[0] : ''}}\r\n            <span *ngIf=\"toppings.value?.length > 1\" class=\"example-additional-selection\">\r\n                (+{{toppings.value.length - 1}} others)\r\n            </span>\r\n        </mat-select-trigger>\r\n        <mat-option *ngFor=\"let topping of toppingList\" [value]=\"topping\">{{topping}}</mat-option>\r\n    </mat-select>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-custom-trigger/select-custom-trigger-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectCustomTriggerExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/** @title Select with custom trigger text */
var SelectCustomTriggerExample = /** @class */ (function () {
    function SelectCustomTriggerExample() {
        this.toppings = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */]();
        this.toppingList = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
    }
    SelectCustomTriggerExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'select-custom-trigger-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/select-custom-trigger/select-custom-trigger-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/select-custom-trigger/select-custom-trigger-example.css")]
        })
    ], SelectCustomTriggerExample);
    return SelectCustomTriggerExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-disabled/select-disabled-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-disabled/select-disabled-example.html":
/***/ (function(module, exports) {

module.exports = "<p>\r\n    <mat-checkbox [formControl]=\"disableSelect\">Disable select</mat-checkbox>\r\n</p>\r\n<p>\r\n    <mat-form-field>\r\n        <mat-select placeholder=\"Choose an option\" [disabled]=\"disableSelect.value\">\r\n            <mat-option value=\"option1\">Option 1</mat-option>\r\n            <mat-option value=\"option2\" disabled>Option 2 (disabled)</mat-option>\r\n            <mat-option value=\"option3\">Option 3</mat-option>\r\n        </mat-select>\r\n    </mat-form-field>\r\n</p>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-disabled/select-disabled-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectDisabledExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/** @title Disabled select */
var SelectDisabledExample = /** @class */ (function () {
    function SelectDisabledExample() {
        this.disableSelect = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */](false);
    }
    SelectDisabledExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'select-disabled-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/select-disabled/select-disabled-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/select-disabled/select-disabled-example.css")]
        })
    ], SelectDisabledExample);
    return SelectDisabledExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-error-state-matcher/select-error-state-matcher-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-error-state-matcher/select-error-state-matcher-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <mat-select placeholder=\"Choose one\" [formControl]=\"selected\" [errorStateMatcher]=\"matcher\">\r\n        <mat-option>Clear</mat-option>\r\n        <mat-option value=\"valid\">Valid option</mat-option>\r\n        <mat-option value=\"invalid\">Invalid option</mat-option>\r\n    </mat-select>\r\n    <mat-hint>Errors appear instantly!</mat-hint>\r\n    <mat-error *ngIf=\"selected.hasError('required')\">You must make a selection</mat-error>\r\n    <mat-error *ngIf=\"selected.hasError('pattern') && !selected.hasError('required')\">\r\n        Your selection is invalid\r\n    </mat-error>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-error-state-matcher/select-error-state-matcher-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export MyErrorStateMatcher */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectErrorStateMatcherExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/** Error when invalid control is dirty, touched, or submitted. */
var MyErrorStateMatcher = /** @class */ (function () {
    function MyErrorStateMatcher() {
    }
    MyErrorStateMatcher.prototype.isErrorState = function (control, form) {
        var isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    };
    return MyErrorStateMatcher;
}());

/** @title Select with a custom ErrorStateMatcher */
var SelectErrorStateMatcherExample = /** @class */ (function () {
    function SelectErrorStateMatcherExample() {
        this.selected = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */]('valid', [
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required,
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].pattern('valid')
        ]);
        this.matcher = new MyErrorStateMatcher();
    }
    SelectErrorStateMatcherExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'select-error-state-matcher-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/select-error-state-matcher/select-error-state-matcher-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/select-error-state-matcher/select-error-state-matcher-example.css")]
        })
    ], SelectErrorStateMatcherExample);
    return SelectErrorStateMatcherExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-form/select-form-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-form/select-form-example.html":
/***/ (function(module, exports) {

module.exports = "<form>\r\n    <mat-form-field>\r\n        <mat-select placeholder=\"Favorite food\" [(ngModel)]=\"selectedValue\" name=\"food\">\r\n            <mat-option *ngFor=\"let food of foods\" [value]=\"food.value\">\r\n                {{food.viewValue}}\r\n            </mat-option>\r\n        </mat-select>\r\n    </mat-form-field>\r\n\r\n    <p> Selected value: {{selectedValue}} </p>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-form/select-form-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectFormExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Select in a form
 */
var SelectFormExample = /** @class */ (function () {
    function SelectFormExample() {
        this.foods = [
            {
                value: 'steak-0',
                viewValue: 'Steak'
            },
            {
                value: 'pizza-1',
                viewValue: 'Pizza'
            },
            {
                value: 'tacos-2',
                viewValue: 'Tacos'
            }
        ];
    }
    SelectFormExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'select-form-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/select-form/select-form-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/select-form/select-form-example.css")]
        })
    ], SelectFormExample);
    return SelectFormExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-hint-error/select-hint-error-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-hint-error/select-hint-error-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <mat-select placeholder=\"Favorite animal\" [formControl]=\"animalControl\" required>\r\n        <mat-option>--</mat-option>\r\n        <mat-option *ngFor=\"let animal of animals\" [value]=\"animal\">\r\n            {{animal.name}}\r\n        </mat-option>\r\n    </mat-select>\r\n    <mat-error *ngIf=\"animalControl.hasError('required')\">Please choose an animal</mat-error>\r\n    <mat-hint>{{animalControl.value?.sound}}</mat-hint>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-hint-error/select-hint-error-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectHintErrorExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/** @title Select with form field features */
var SelectHintErrorExample = /** @class */ (function () {
    function SelectHintErrorExample() {
        this.animalControl = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required]);
        this.animals = [
            {
                name: 'Dog',
                sound: 'Woof!'
            },
            {
                name: 'Cat',
                sound: 'Meow!'
            },
            {
                name: 'Cow',
                sound: 'Moo!'
            },
            {
                name: 'Fox',
                sound: 'Wa-pa-pa-pa-pa-pa-pow!'
            }
        ];
    }
    SelectHintErrorExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'select-hint-error-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/select-hint-error/select-hint-error-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/select-hint-error/select-hint-error-example.css")]
        })
    ], SelectHintErrorExample);
    return SelectHintErrorExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-multiple/select-multiple-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-multiple/select-multiple-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <mat-select placeholder=\"Toppings\" [formControl]=\"toppings\" multiple>\r\n        <mat-option *ngFor=\"let topping of toppingList\" [value]=\"topping\">{{topping}}</mat-option>\r\n    </mat-select>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-multiple/select-multiple-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectMultipleExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/** @title Select with multiple selection */
var SelectMultipleExample = /** @class */ (function () {
    function SelectMultipleExample() {
        this.toppings = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */]();
        this.toppingList = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
    }
    SelectMultipleExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'select-multiple-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/select-multiple/select-multiple-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/select-multiple/select-multiple-example.css")]
        })
    ], SelectMultipleExample);
    return SelectMultipleExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-no-ripple/select-no-ripple-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-no-ripple/select-no-ripple-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <mat-select placeholder=\"Select an option\" disableRipple>\r\n        <mat-option value=\"1\">Option 1</mat-option>\r\n        <mat-option value=\"2\">Option 2</mat-option>\r\n        <mat-option value=\"3\">Option 3</mat-option>\r\n    </mat-select>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-no-ripple/select-no-ripple-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectNoRippleExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Select with no option ripple */
var SelectNoRippleExample = /** @class */ (function () {
    function SelectNoRippleExample() {
    }
    SelectNoRippleExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'select-no-ripple-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/select-no-ripple/select-no-ripple-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/select-no-ripple/select-no-ripple-example.css")]
        })
    ], SelectNoRippleExample);
    return SelectNoRippleExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-optgroup/select-optgroup-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-optgroup/select-optgroup-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <mat-select placeholder=\"Pokemon\" [formControl]=\"pokemonControl\">\r\n        <mat-option>-- None --</mat-option>\r\n        <mat-optgroup *ngFor=\"let group of pokemonGroups\" [label]=\"group.name\"\r\n                      [disabled]=\"group.disabled\">\r\n            <mat-option *ngFor=\"let pokemon of group.pokemon\" [value]=\"pokemon.value\">\r\n                {{ pokemon.viewValue }}\r\n            </mat-option>\r\n        </mat-optgroup>\r\n    </mat-select>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-optgroup/select-optgroup-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectOptgroupExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/** @title Select with option groups */
var SelectOptgroupExample = /** @class */ (function () {
    function SelectOptgroupExample() {
        this.pokemonControl = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */]();
        this.pokemonGroups = [
            {
                name: 'Grass',
                pokemon: [
                    {
                        value: 'bulbasaur-0',
                        viewValue: 'Bulbasaur'
                    },
                    {
                        value: 'oddish-1',
                        viewValue: 'Oddish'
                    },
                    {
                        value: 'bellsprout-2',
                        viewValue: 'Bellsprout'
                    }
                ]
            },
            {
                name: 'Water',
                pokemon: [
                    {
                        value: 'squirtle-3',
                        viewValue: 'Squirtle'
                    },
                    {
                        value: 'psyduck-4',
                        viewValue: 'Psyduck'
                    },
                    {
                        value: 'horsea-5',
                        viewValue: 'Horsea'
                    }
                ]
            },
            {
                name: 'Fire',
                disabled: true,
                pokemon: [
                    {
                        value: 'charmander-6',
                        viewValue: 'Charmander'
                    },
                    {
                        value: 'vulpix-7',
                        viewValue: 'Vulpix'
                    },
                    {
                        value: 'flareon-8',
                        viewValue: 'Flareon'
                    }
                ]
            },
            {
                name: 'Psychic',
                pokemon: [
                    {
                        value: 'mew-9',
                        viewValue: 'Mew'
                    },
                    {
                        value: 'mewtwo-10',
                        viewValue: 'Mewtwo'
                    }
                ]
            }
        ];
    }
    SelectOptgroupExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'select-optgroup-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/select-optgroup/select-optgroup-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/select-optgroup/select-optgroup-example.css")]
        })
    ], SelectOptgroupExample);
    return SelectOptgroupExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-overview/select-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-overview/select-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <mat-select placeholder=\"Favorite food\">\r\n        <mat-option *ngFor=\"let food of foods\" [value]=\"food.value\">\r\n            {{ food.viewValue }}\r\n        </mat-option>\r\n    </mat-select>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-overview/select-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic select
 */
var SelectOverviewExample = /** @class */ (function () {
    function SelectOverviewExample() {
        this.foods = [
            {
                value: 'steak-0',
                viewValue: 'Steak'
            },
            {
                value: 'pizza-1',
                viewValue: 'Pizza'
            },
            {
                value: 'tacos-2',
                viewValue: 'Tacos'
            }
        ];
    }
    SelectOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'select-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/select-overview/select-overview-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/select-overview/select-overview-example.css")]
        })
    ], SelectOverviewExample);
    return SelectOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-panel-class/select-panel-class-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-panel-red .mat-select-content {\r\n    background: rgba(255, 0, 0, 0.5);\r\n}\r\n\r\n.example-panel-green .mat-select-content {\r\n    background: rgba(0, 255, 0, 0.5);\r\n}\r\n\r\n.example-panel-blue .mat-select-content {\r\n    background: rgba(0, 0, 255, 0.5);\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-panel-class/select-panel-class-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <mat-select placeholder=\"Panel color\" [formControl]=\"panelColor\"\r\n                panelClass=\"example-panel-{{panelColor.value}}\">\r\n        <mat-option value=\"red\">Red</mat-option>\r\n        <mat-option value=\"green\">Green</mat-option>\r\n        <mat-option value=\"blue\">Blue</mat-option>\r\n    </mat-select>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-panel-class/select-panel-class-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectPanelClassExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/**
 * @title Select with custom panel styling
 */
var SelectPanelClassExample = /** @class */ (function () {
    function SelectPanelClassExample() {
        this.panelColor = new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["d" /* FormControl */]('red');
    }
    SelectPanelClassExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'select-panel-class-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/select-panel-class/select-panel-class-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/select-panel-class/select-panel-class-example.css")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None
        })
    ], SelectPanelClassExample);
    return SelectPanelClassExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-reset/select-reset-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-reset/select-reset-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <mat-select placeholder=\"State\">\r\n        <mat-option>None</mat-option>\r\n        <mat-option *ngFor=\"let state of states\" [value]=\"state\">{{state}}</mat-option>\r\n    </mat-select>\r\n</mat-form-field>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-reset/select-reset-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectResetExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Select with reset option */
var SelectResetExample = /** @class */ (function () {
    function SelectResetExample() {
        this.states = [
            'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware',
            'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky',
            'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi',
            'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico',
            'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania',
            'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
            'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
        ];
    }
    SelectResetExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'select-reset-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/select-reset/select-reset-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/select-reset/select-reset-example.css")]
        })
    ], SelectResetExample);
    return SelectResetExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-value-binding/select-value-binding-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-value-binding/select-value-binding-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <mat-select [(value)]=\"selected\">\r\n        <mat-option>None</mat-option>\r\n        <mat-option value=\"option1\">Option 1</mat-option>\r\n        <mat-option value=\"option2\">Option 2</mat-option>\r\n        <mat-option value=\"option3\">Option 3</mat-option>\r\n    </mat-select>\r\n</mat-form-field>\r\n\r\n<p>You selected: {{selected}}</p>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/select-value-binding/select-value-binding-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectValueBindingExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/** @title Select with 2-way value binding */
var SelectValueBindingExample = /** @class */ (function () {
    function SelectValueBindingExample() {
        this.selected = 'option2';
    }
    SelectValueBindingExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'select-value-binding-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/select-value-binding/select-value-binding-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/select-value-binding/select-value-binding-example.css")]
        })
    ], SelectValueBindingExample);
    return SelectValueBindingExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/sidenav-fab/sidenav-fab-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-sidenav-fab-container {\r\n    width: 500px;\r\n    height: 300px;\r\n    border: 1px solid rgba(0, 0, 0, 0.5);\r\n}\r\n\r\n.example-sidenav-fab-container mat-sidenav {\r\n    max-width: 200px;\r\n}\r\n\r\n.example-sidenav-fab-container .mat-sidenav-content,\r\n.example-sidenav-fab-container mat-sidenav {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    overflow: visible;\r\n}\r\n\r\n.example-scrolling-content {\r\n    overflow: auto;\r\n    height: 100%;\r\n}\r\n\r\n.example-fab.mat-mini-fab {\r\n    position: absolute;\r\n    right: 20px;\r\n    bottom: 10px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/sidenav-fab/sidenav-fab-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"example-sidenav-fab-container\">\r\n    <mat-sidenav #sidenav mode=\"side\" opened=\"true\">\r\n        <button mat-mini-fab class=\"example-fab\" (click)=\"sidenav.toggle()\">\r\n            <mat-icon>add</mat-icon>\r\n        </button>\r\n        <div class=\"example-scrolling-content\">\r\n            Lorem ipsum dolor sit amet, pede a libero aenean phasellus, lectus metus sint ut risus,\r\n            fusce vel in pellentesque. Nisl rutrum etiam morbi consectetuer tempor magna, aenean nullam\r\n            nunc id, neque vivamus interdum sociis nulla scelerisque sem, dolor id wisi turpis magna\r\n            aliquam magna. Risus accumsan hac eget etiam donec sed, senectus erat mattis quam, tempor\r\n            vel urna occaecat cras, metus urna augue nec at. Et morbi amet dui praesent, nec eu at,\r\n            ligula ipsum dui sollicitudin, quis nisl massa viverra ligula, mauris fermentum orci arcu\r\n            enim fringilla. Arcu erat nulla in aenean lacinia ullamcorper, urna ante nam et sagittis,\r\n            tristique vehicula nibh ipsum vivamus, proin proin. Porta commodo nibh quis libero amet.\r\n            Taciti dui, sapien consectetuer.\r\n        </div>\r\n    </mat-sidenav>\r\n    <button mat-mini-fab class=\"example-fab\" (click)=\"sidenav.toggle()\">\r\n        <mat-icon>add</mat-icon>\r\n    </button>\r\n    <div class=\"example-scrolling-content\">\r\n        Lorem ipsum dolor sit amet, pede a libero aenean phasellus, lectus metus sint ut risus, fusce\r\n        vel in pellentesque. Nisl rutrum etiam morbi consectetuer tempor magna, aenean nullam nunc id,\r\n        neque vivamus interdum sociis nulla scelerisque sem, dolor id wisi turpis magna aliquam magna.\r\n        Risus accumsan hac eget etiam donec sed, senectus erat mattis quam, tempor vel urna occaecat\r\n        cras, metus urna augue nec at. Et morbi amet dui praesent, nec eu at, ligula ipsum dui\r\n        sollicitudin, quis nisl massa viverra ligula, mauris fermentum orci arcu enim fringilla. Arcu\r\n        erat nulla in aenean lacinia ullamcorper, urna ante nam et sagittis, tristique vehicula nibh\r\n        ipsum vivamus, proin proin. Porta commodo nibh quis libero amet. Taciti dui, sapien\r\n        consectetuer.\r\n    </div>\r\n</mat-sidenav-container>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/sidenav-fab/sidenav-fab-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidenavFabExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Sidenav with a FAB
 */
var SidenavFabExample = /** @class */ (function () {
    function SidenavFabExample() {
    }
    SidenavFabExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'sidenav-fab-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/sidenav-fab/sidenav-fab-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/sidenav-fab/sidenav-fab-example.css")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
            preserveWhitespaces: false
        })
    ], SidenavFabExample);
    return SidenavFabExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/sidenav-overview/sidenav-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-container {\r\n    width: 500px;\r\n    height: 300px;\r\n    border: 1px solid rgba(0, 0, 0, 0.5);\r\n}\r\n\r\n.example-sidenav-content {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    height: 100%;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    -webkit-box-pack: center;\r\n        -ms-flex-pack: center;\r\n            justify-content: center;\r\n}\r\n\r\n.example-sidenav {\r\n    padding: 20px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/sidenav-overview/sidenav-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container class=\"example-container\">\r\n    <mat-sidenav #sidenav class=\"example-sidenav\">\r\n        Jolly good!\r\n    </mat-sidenav>\r\n\r\n    <div class=\"example-sidenav-content\">\r\n        <button type=\"button\" mat-button (click)=\"sidenav.open()\">\r\n            Open sidenav\r\n        </button>\r\n    </div>\r\n\r\n</mat-sidenav-container>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/sidenav-overview/sidenav-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidenavOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic sidenav
 */
var SidenavOverviewExample = /** @class */ (function () {
    function SidenavOverviewExample() {
    }
    SidenavOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'sidenav-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/sidenav-overview/sidenav-overview-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/sidenav-overview/sidenav-overview-example.css")]
        })
    ], SidenavOverviewExample);
    return SidenavOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/slide-toggle-configurable/slide-toggle-configurable-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-h2 {\r\n    margin: 10px;\r\n}\r\n\r\n.example-section {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -ms-flex-line-pack: center;\r\n        align-content: center;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    height: 60px;\r\n}\r\n\r\n.example-margin {\r\n    margin: 10px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/slide-toggle-configurable/slide-toggle-configurable-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-card>\r\n    <mat-card-content>\r\n        <h2 class=\"example-h2\">Slider configuration</h2>\r\n\r\n        <section class=\"example-section\">\r\n            <label class=\"example-margin\">Color:</label>\r\n            <mat-radio-group [(ngModel)]=\"color\">\r\n                <mat-radio-button class=\"example-margin\" value=\"primary\">\r\n                    Primary\r\n                </mat-radio-button>\r\n                <mat-radio-button class=\"example-margin\" value=\"accent\">\r\n                    Accent\r\n                </mat-radio-button>\r\n                <mat-radio-button class=\"example-margin\" value=\"warn\">\r\n                    Warn\r\n                </mat-radio-button>\r\n            </mat-radio-group>\r\n        </section>\r\n\r\n        <section class=\"example-section\">\r\n            <mat-checkbox class=\"example-margin\" [(ngModel)]=\"checked\">Checked</mat-checkbox>\r\n        </section>\r\n\r\n        <section class=\"example-section\">\r\n            <mat-checkbox class=\"example-margin\" [(ngModel)]=\"disabled\">Disabled</mat-checkbox>\r\n        </section>\r\n    </mat-card-content>\r\n</mat-card>\r\n\r\n<mat-card class=\"result\">\r\n    <mat-card-content>\r\n        <h2 class=\"example-h2\">Result</h2>\r\n\r\n        <section class=\"example-section\">\r\n            <mat-slide-toggle\r\n                class=\"example-margin\"\r\n                [color]=\"color\"\r\n                [checked]=\"checked\"\r\n                [disabled]=\"disabled\">\r\n                Slide me!\r\n            </mat-slide-toggle>\r\n        </section>\r\n    </mat-card-content>\r\n</mat-card>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/slide-toggle-configurable/slide-toggle-configurable-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SlideToggleConfigurableExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Configurable slide-toggle
 */
var SlideToggleConfigurableExample = /** @class */ (function () {
    function SlideToggleConfigurableExample() {
        this.color = 'accent';
        this.checked = false;
        this.disabled = false;
    }
    SlideToggleConfigurableExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'slide-toggle-configurable-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/slide-toggle-configurable/slide-toggle-configurable-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/slide-toggle-configurable/slide-toggle-configurable-example.css")]
        })
    ], SlideToggleConfigurableExample);
    return SlideToggleConfigurableExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/slide-toggle-forms/slide-toggle-forms-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-form mat-slide-toggle {\r\n    margin: 8px 0;\r\n    display: block;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/slide-toggle-forms/slide-toggle-forms-example.html":
/***/ (function(module, exports) {

module.exports = "<p>Slide Toggle using a simple NgModel.</p>\r\n\r\n<mat-slide-toggle [(ngModel)]=\"isChecked\">Slide Toggle Checked: {{ isChecked }}</mat-slide-toggle>\r\n\r\n<p>Slide Toggle inside of a Template-driven form</p>\r\n\r\n<form class=\"example-form\" #form=\"ngForm\" (ngSubmit)=\"onFormSubmit(form.value)\" ngNativeValidate>\r\n\r\n    <mat-slide-toggle ngModel name=\"enableWifi\">Enable Wifi</mat-slide-toggle>\r\n    <mat-slide-toggle ngModel name=\"acceptTerms\" required>Accept Terms of Service</mat-slide-toggle>\r\n\r\n    <button mat-raised-button type=\"submit\">Save Settings</button>\r\n</form>\r\n\r\n<p>Slide Toggle inside of a Reactive form</p>\r\n\r\n<form class=\"example-form\" [formGroup]=\"formGroup\" (ngSubmit)=\"onFormSubmit(formGroup.value)\" ngNativeValidate>\r\n\r\n    <mat-slide-toggle formControlName=\"enableWifi\">Enable Wifi</mat-slide-toggle>\r\n    <mat-slide-toggle formControlName=\"acceptTerms\">Accept Terms of Service</mat-slide-toggle>\r\n\r\n    <p>Form Group Status: {{ formGroup.status}}</p>\r\n\r\n    <button mat-rasied-button type=\"submit\">Save Settings</button>\r\n</form>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/slide-toggle-forms/slide-toggle-forms-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SlideToggleFormsExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @title Slide-toggle with forms
 */
var SlideToggleFormsExample = /** @class */ (function () {
    function SlideToggleFormsExample(formBuilder) {
        this.isChecked = true;
        this.formGroup = formBuilder.group({
            enableWifi: '',
            acceptTerms: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].requiredTrue]
        });
    }
    SlideToggleFormsExample.prototype.onFormSubmit = function (formValue) {
        alert(JSON.stringify(formValue, null, 2));
    };
    SlideToggleFormsExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'slide-toggle-forms-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/slide-toggle-forms/slide-toggle-forms-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/slide-toggle-forms/slide-toggle-forms-example.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormBuilder */]])
    ], SlideToggleFormsExample);
    return SlideToggleFormsExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/slide-toggle-overview/slide-toggle-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-slide-toggle>Slide me!</mat-slide-toggle>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/slide-toggle-overview/slide-toggle-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SlideToggleOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic slide-toggles
 */
var SlideToggleOverviewExample = /** @class */ (function () {
    function SlideToggleOverviewExample() {
    }
    SlideToggleOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'slide-toggle-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/slide-toggle-overview/slide-toggle-overview-example.html")
        })
    ], SlideToggleOverviewExample);
    return SlideToggleOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/slider-configurable/slider-configurable-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-h2 {\r\n    margin: 10px;\r\n}\r\n\r\n.example-section {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -ms-flex-line-pack: center;\r\n        align-content: center;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    height: 60px;\r\n}\r\n\r\n.example-margin {\r\n    margin: 10px;\r\n}\r\n\r\n.mat-slider-horizontal {\r\n    width: 300px;\r\n}\r\n\r\n.mat-slider-vertical {\r\n    height: 300px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/slider-configurable/slider-configurable-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-card>\r\n    <mat-card-content>\r\n        <h2 class=\"example-h2\">Slider configuration</h2>\r\n\r\n        <section class=\"example-section\">\r\n            <mat-form-field class=\"example-margin\">\r\n                <input matInput type=\"number\" placeholder=\"Value\" [(ngModel)]=\"value\">\r\n            </mat-form-field>\r\n            <mat-form-field class=\"example-margin\">\r\n                <input matInput type=\"number\" placeholder=\"Min value\" [(ngModel)]=\"min\">\r\n            </mat-form-field>\r\n            <mat-form-field class=\"example-margin\">\r\n                <input matInput type=\"number\" placeholder=\"Max value\" [(ngModel)]=\"max\">\r\n            </mat-form-field>\r\n            <mat-form-field class=\"example-margin\">\r\n                <input matInput type=\"number\" placeholder=\"Step size\" [(ngModel)]=\"step\">\r\n            </mat-form-field>\r\n        </section>\r\n\r\n        <section class=\"example-section\">\r\n            <mat-checkbox class=\"example-margin\" [(ngModel)]=\"showTicks\">Show ticks</mat-checkbox>\r\n            <mat-checkbox class=\"example-margin\" [(ngModel)]=\"autoTicks\" *ngIf=\"showTicks\">\r\n                Auto ticks\r\n            </mat-checkbox>\r\n            <mat-form-field class=\"example-margin\" *ngIf=\"showTicks && !autoTicks\">\r\n                <input matInput type=\"number\" placeholder=\"Tick interval\" [(ngModel)]=\"tickInterval\">\r\n            </mat-form-field>\r\n        </section>\r\n\r\n        <section class=\"example-section\">\r\n            <mat-checkbox class=\"example-margin\" [(ngModel)]=\"thumbLabel\">Show thumb label</mat-checkbox>\r\n        </section>\r\n\r\n        <section class=\"example-section\">\r\n            <mat-checkbox class=\"example-margin\" [(ngModel)]=\"vertical\">Vertical</mat-checkbox>\r\n            <mat-checkbox class=\"example-margin\" [(ngModel)]=\"invert\">Inverted</mat-checkbox>\r\n        </section>\r\n\r\n        <section class=\"example-section\">\r\n            <mat-checkbox class=\"example-margin\" [(ngModel)]=\"disabled\">Disabled</mat-checkbox>\r\n        </section>\r\n\r\n    </mat-card-content>\r\n</mat-card>\r\n\r\n<mat-card class=\"result\">\r\n    <mat-card-content>\r\n        <h2 class=\"example-h2\">Result</h2>\r\n\r\n        <mat-slider\r\n            class=\"example-margin\"\r\n            [disabled]=\"disabled\"\r\n            [invert]=\"invert\"\r\n            [max]=\"max\"\r\n            [min]=\"min\"\r\n            [step]=\"step\"\r\n            [thumb-label]=\"thumbLabel\"\r\n            [tick-interval]=\"tickInterval\"\r\n            [value]=\"value\"\r\n            [vertical]=\"vertical\">\r\n        </mat-slider>\r\n    </mat-card-content>\r\n</mat-card>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/slider-configurable/slider-configurable-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SliderConfigurableExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Configurable slider
 */
var SliderConfigurableExample = /** @class */ (function () {
    function SliderConfigurableExample() {
        this.autoTicks = false;
        this.disabled = false;
        this.invert = false;
        this.max = 100;
        this.min = 0;
        this.showTicks = false;
        this.step = 1;
        this.thumbLabel = false;
        this.value = 0;
        this.vertical = false;
        this._tickInterval = 1;
    }
    Object.defineProperty(SliderConfigurableExample.prototype, "tickInterval", {
        get: function () {
            return this.showTicks ? (this.autoTicks ? 'auto' : this._tickInterval) : 0;
        },
        set: function (v) {
            this._tickInterval = Number(v);
        },
        enumerable: true,
        configurable: true
    });
    SliderConfigurableExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'slider-configurable-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/slider-configurable/slider-configurable-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/slider-configurable/slider-configurable-example.css")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewEncapsulation"].None,
            preserveWhitespaces: false
        })
    ], SliderConfigurableExample);
    return SliderConfigurableExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/slider-overview/slider-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\nmat-slider {\r\n    width: 300px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/slider-overview/slider-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-slider></mat-slider>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/slider-overview/slider-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SliderOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic slider
 */
var SliderOverviewExample = /** @class */ (function () {
    function SliderOverviewExample() {
    }
    SliderOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'slider-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/slider-overview/slider-overview-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/slider-overview/slider-overview-example.css")]
        })
    ], SliderOverviewExample);
    return SliderOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/snack-bar-component/snack-bar-component-example-snack.html":
/***/ (function(module, exports) {

module.exports = "<span class=\"example-pizza-party\">\r\n    Pizza party!!! 🍕\r\n</span>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/snack-bar-component/snack-bar-component-example.html":
/***/ (function(module, exports) {

module.exports = "<button mat-button (click)=\"openSnackBar()\" aria-label=\"Show an example snack-bar\">\r\n    Pizza party\r\n</button>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/snack-bar-component/snack-bar-component-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return SnackBarComponentExample; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PizzaPartyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @title Snack-bar with a custom component
 */
var SnackBarComponentExample = /** @class */ (function () {
    function SnackBarComponentExample(snackBar) {
        this.snackBar = snackBar;
    }
    SnackBarComponentExample.prototype.openSnackBar = function () {
        this.snackBar.openFromComponent(PizzaPartyComponent, {
            duration: 500
        });
    };
    SnackBarComponentExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'snack-bar-component-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/snack-bar-component/snack-bar-component-example.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["K" /* MatSnackBar */]])
    ], SnackBarComponentExample);
    return SnackBarComponentExample;
}());

var PizzaPartyComponent = /** @class */ (function () {
    function PizzaPartyComponent() {
    }
    PizzaPartyComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'snack-bar-component-example-snack',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/snack-bar-component/snack-bar-component-example-snack.html"),
            styles: [".example-pizza-party { color: hotpink; }"]
        })
    ], PizzaPartyComponent);
    return PizzaPartyComponent;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/snack-bar-overview/snack-bar-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-form-field>\r\n    <input matInput value=\"Disco party!\" placeholder=\"Message\" #message>\r\n</mat-form-field>\r\n\r\n<mat-form-field>\r\n    <input matInput value=\"Dance\" placeholder=\"Action\" #action>\r\n</mat-form-field>\r\n\r\n<button mat-button (click)=\"openSnackBar(message.value, action.value)\">Show snack-bar</button>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/snack-bar-overview/snack-bar-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SnackBarOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @title Basic snack-bar
 */
var SnackBarOverviewExample = /** @class */ (function () {
    function SnackBarOverviewExample(snackBar) {
        this.snackBar = snackBar;
    }
    SnackBarOverviewExample.prototype.openSnackBar = function (message, action) {
        this.snackBar.open(message, action, {
            duration: 2000
        });
    };
    SnackBarOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'snack-bar-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/snack-bar-overview/snack-bar-overview-example.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_material__["K" /* MatSnackBar */]])
    ], SnackBarOverviewExample);
    return SnackBarOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/sort-overview/sort-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".mat-sort-header-container {\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/sort-overview/sort-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<table matSort (matSortChange)=\"sortData($event)\">\r\n    <tr>\r\n        <th mat-sort-header=\"name\">Dessert (100g)</th>\r\n        <th mat-sort-header=\"calories\">Calories</th>\r\n        <th mat-sort-header=\"fat\">Fat (g)</th>\r\n        <th mat-sort-header=\"carbs\">Carbs (g)</th>\r\n        <th mat-sort-header=\"protein\">Protein (g)</th>\r\n    </tr>\r\n\r\n    <tr *ngFor=\"let dessert of sortedData\">\r\n        <td>{{dessert.name}}</td>\r\n        <td>{{dessert.calories}}</td>\r\n        <td>{{dessert.fat}}</td>\r\n        <td>{{dessert.carbs}}</td>\r\n        <td>{{dessert.protein}}</td>\r\n    </tr>\r\n</table>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/sort-overview/sort-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SortOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * @title Sorting overview
 */
var SortOverviewExample = /** @class */ (function () {
    function SortOverviewExample() {
        this.desserts = [
            {
                name: 'Frozen yogurt',
                calories: '159',
                fat: '6',
                carbs: '24',
                protein: '4'
            },
            {
                name: 'Ice cream sandwich',
                calories: '237',
                fat: '9',
                carbs: '37',
                protein: '4'
            },
            {
                name: 'Eclair',
                calories: '262',
                fat: '16',
                carbs: '24',
                protein: '6'
            },
            {
                name: 'Cupcake',
                calories: '305',
                fat: '4',
                carbs: '67',
                protein: '4'
            },
            {
                name: 'Gingerbread',
                calories: '356',
                fat: '16',
                carbs: '49',
                protein: '4'
            }
        ];
        this.sortedData = this.desserts.slice();
    }
    SortOverviewExample.prototype.sortData = function (sort) {
        var data = this.desserts.slice();
        if (!sort.active || sort.direction == '') {
            this.sortedData = data;
            return;
        }
        this.sortedData = data.sort(function (a, b) {
            var isAsc = sort.direction == 'asc';
            switch (sort.active) {
                case 'name':
                    return compare(a.name, b.name, isAsc);
                case 'calories':
                    return compare(+a.calories, +b.calories, isAsc);
                case 'fat':
                    return compare(+a.fat, +b.fat, isAsc);
                case 'carbs':
                    return compare(+a.carbs, +b.carbs, isAsc);
                case 'protein':
                    return compare(+a.protein, +b.protein, isAsc);
                default:
                    return 0;
            }
        });
    };
    SortOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'sort-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/sort-overview/sort-overview-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/sort-overview/sort-overview-example.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SortOverviewExample);
    return SortOverviewExample;
}());

function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}


/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/stepper-overview/stepper-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/** No CSS for this example */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/stepper-overview/stepper-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<button mat-raised-button (click)=\"isLinear = true\" id=\"toggle-linear\">Enable linear mode</button>\r\n\r\n<mat-horizontal-stepper [linear]=\"isLinear\">\r\n    <mat-step [stepControl]=\"firstFormGroup\">\r\n        <form [formGroup]=\"firstFormGroup\">\r\n            <ng-template matStepLabel>Fill out your name</ng-template>\r\n            <mat-form-field>\r\n                <input matInput placeholder=\"Last name, First name\" formControlName=\"firstCtrl\" required>\r\n            </mat-form-field>\r\n            <div>\r\n                <button mat-button matStepperNext>Next</button>\r\n            </div>\r\n        </form>\r\n    </mat-step>\r\n    <mat-step [stepControl]=\"secondFormGroup\">\r\n        <form [formGroup]=\"secondFormGroup\">\r\n            <ng-template matStepLabel>Fill out your address</ng-template>\r\n            <mat-form-field>\r\n                <input matInput placeholder=\"Address\" formControlName=\"secondCtrl\" required>\r\n            </mat-form-field>\r\n            <div>\r\n                <button mat-button matStepperPrevious>Back</button>\r\n                <button mat-button matStepperNext>Next</button>\r\n            </div>\r\n        </form>\r\n    </mat-step>\r\n    <mat-step>\r\n        <ng-template matStepLabel>Done</ng-template>\r\n        You are now done.\r\n        <div>\r\n            <button mat-button matStepperPrevious>Back</button>\r\n        </div>\r\n    </mat-step>\r\n</mat-horizontal-stepper>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/stepper-overview/stepper-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StepperOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @title Stepper overview
 */
var StepperOverviewExample = /** @class */ (function () {
    function StepperOverviewExample(_formBuilder) {
        this._formBuilder = _formBuilder;
        this.isLinear = false;
    }
    StepperOverviewExample.prototype.ngOnInit = function () {
        this.firstFormGroup = this._formBuilder.group({
            firstCtrl: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required]
        });
        this.secondFormGroup = this._formBuilder.group({
            secondCtrl: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["l" /* Validators */].required]
        });
    };
    StepperOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'stepper-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/stepper-overview/stepper-overview-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/stepper-overview/stepper-overview-example.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["c" /* FormBuilder */]])
    ], StepperOverviewExample);
    return StepperOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-basic/table-basic-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    max-height: 500px;\r\n    min-width: 300px;\r\n}\r\n\r\n.mat-table {\r\n    overflow: auto;\r\n    max-height: 500px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-basic/table-basic-example.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container mat-elevation-z8\">\r\n    <mat-table #table [dataSource]=\"dataSource\">\r\n\r\n        <!--- Note that these columns can be defined in any order.\r\n              The actual rendered columns are set as a property on the row definition\" -->\r\n\r\n        <!-- Position Column -->\r\n        <ng-container matColumnDef=\"position\">\r\n            <mat-header-cell *matHeaderCellDef> No.</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.position}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Name Column -->\r\n        <ng-container matColumnDef=\"name\">\r\n            <mat-header-cell *matHeaderCellDef> Name</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.name}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Weight Column -->\r\n        <ng-container matColumnDef=\"weight\">\r\n            <mat-header-cell *matHeaderCellDef> Weight</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.weight}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Color Column -->\r\n        <ng-container matColumnDef=\"symbol\">\r\n            <mat-header-cell *matHeaderCellDef> Symbol</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.symbol}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n    </mat-table>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-basic/table-basic-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableBasicExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/**
 * @title Basic table
 */
var TableBasicExample = /** @class */ (function () {
    function TableBasicExample() {
        this.displayedColumns = ['position', 'name', 'weight', 'symbol'];
        this.dataSource = new __WEBPACK_IMPORTED_MODULE_1__angular_material__["Q" /* MatTableDataSource */](ELEMENT_DATA);
    }
    TableBasicExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'table-basic-example',
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/table-basic/table-basic-example.css")],
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/table-basic/table-basic-example.html")
        })
    ], TableBasicExample);
    return TableBasicExample;
}());

var ELEMENT_DATA = [
    {
        position: 1,
        name: 'Hydrogen',
        weight: 1.0079,
        symbol: 'H'
    },
    {
        position: 2,
        name: 'Helium',
        weight: 4.0026,
        symbol: 'He'
    },
    {
        position: 3,
        name: 'Lithium',
        weight: 6.941,
        symbol: 'Li'
    },
    {
        position: 4,
        name: 'Beryllium',
        weight: 9.0122,
        symbol: 'Be'
    },
    {
        position: 5,
        name: 'Boron',
        weight: 10.811,
        symbol: 'B'
    },
    {
        position: 6,
        name: 'Carbon',
        weight: 12.0107,
        symbol: 'C'
    },
    {
        position: 7,
        name: 'Nitrogen',
        weight: 14.0067,
        symbol: 'N'
    },
    {
        position: 8,
        name: 'Oxygen',
        weight: 15.9994,
        symbol: 'O'
    },
    {
        position: 9,
        name: 'Fluorine',
        weight: 18.9984,
        symbol: 'F'
    },
    {
        position: 10,
        name: 'Neon',
        weight: 20.1797,
        symbol: 'Ne'
    },
    {
        position: 11,
        name: 'Sodium',
        weight: 22.9897,
        symbol: 'Na'
    },
    {
        position: 12,
        name: 'Magnesium',
        weight: 24.305,
        symbol: 'Mg'
    },
    {
        position: 13,
        name: 'Aluminum',
        weight: 26.9815,
        symbol: 'Al'
    },
    {
        position: 14,
        name: 'Silicon',
        weight: 28.0855,
        symbol: 'Si'
    },
    {
        position: 15,
        name: 'Phosphorus',
        weight: 30.9738,
        symbol: 'P'
    },
    {
        position: 16,
        name: 'Sulfur',
        weight: 32.065,
        symbol: 'S'
    },
    {
        position: 17,
        name: 'Chlorine',
        weight: 35.453,
        symbol: 'Cl'
    },
    {
        position: 18,
        name: 'Argon',
        weight: 39.948,
        symbol: 'Ar'
    },
    {
        position: 19,
        name: 'Potassium',
        weight: 39.0983,
        symbol: 'K'
    },
    {
        position: 20,
        name: 'Calcium',
        weight: 40.078,
        symbol: 'Ca'
    }
];


/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-filtering/table-filtering-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* Structure */\r\n.example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    min-width: 300px;\r\n}\r\n.example-header {\r\n    min-height: 64px;\r\n    padding: 8px 24px 0;\r\n}\r\n.mat-form-field {\r\n    font-size: 14px;\r\n    width: 100%;\r\n}\r\n.mat-table {\r\n    overflow: auto;\r\n    max-height: 500px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-filtering/table-filtering-example.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container mat-elevation-z8\">\r\n    <div class=\"example-header\">\r\n        <mat-form-field>\r\n            <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\r\n        </mat-form-field>\r\n    </div>\r\n\r\n    <mat-table #table [dataSource]=\"dataSource\">\r\n\r\n        <!-- Position Column -->\r\n        <ng-container matColumnDef=\"position\">\r\n            <mat-header-cell *matHeaderCellDef> No.</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.position}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Name Column -->\r\n        <ng-container matColumnDef=\"name\">\r\n            <mat-header-cell *matHeaderCellDef> Name</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.name}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Weight Column -->\r\n        <ng-container matColumnDef=\"weight\">\r\n            <mat-header-cell *matHeaderCellDef> Weight</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.weight}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Color Column -->\r\n        <ng-container matColumnDef=\"symbol\">\r\n            <mat-header-cell *matHeaderCellDef> Symbol</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.symbol}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n    </mat-table>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-filtering/table-filtering-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableFilteringExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/**
 * @title Table with filtering
 */
var TableFilteringExample = /** @class */ (function () {
    function TableFilteringExample() {
        this.displayedColumns = ['position', 'name', 'weight', 'symbol'];
        this.dataSource = new __WEBPACK_IMPORTED_MODULE_1__angular_material__["Q" /* MatTableDataSource */](ELEMENT_DATA);
    }
    TableFilteringExample.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    TableFilteringExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'table-filtering-example',
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/table-filtering/table-filtering-example.css")],
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/table-filtering/table-filtering-example.html")
        })
    ], TableFilteringExample);
    return TableFilteringExample;
}());

var ELEMENT_DATA = [
    {
        position: 1,
        name: 'Hydrogen',
        weight: 1.0079,
        symbol: 'H'
    },
    {
        position: 2,
        name: 'Helium',
        weight: 4.0026,
        symbol: 'He'
    },
    {
        position: 3,
        name: 'Lithium',
        weight: 6.941,
        symbol: 'Li'
    },
    {
        position: 4,
        name: 'Beryllium',
        weight: 9.0122,
        symbol: 'Be'
    },
    {
        position: 5,
        name: 'Boron',
        weight: 10.811,
        symbol: 'B'
    },
    {
        position: 6,
        name: 'Carbon',
        weight: 12.0107,
        symbol: 'C'
    },
    {
        position: 7,
        name: 'Nitrogen',
        weight: 14.0067,
        symbol: 'N'
    },
    {
        position: 8,
        name: 'Oxygen',
        weight: 15.9994,
        symbol: 'O'
    },
    {
        position: 9,
        name: 'Fluorine',
        weight: 18.9984,
        symbol: 'F'
    },
    {
        position: 10,
        name: 'Neon',
        weight: 20.1797,
        symbol: 'Ne'
    },
    {
        position: 11,
        name: 'Sodium',
        weight: 22.9897,
        symbol: 'Na'
    },
    {
        position: 12,
        name: 'Magnesium',
        weight: 24.305,
        symbol: 'Mg'
    },
    {
        position: 13,
        name: 'Aluminum',
        weight: 26.9815,
        symbol: 'Al'
    },
    {
        position: 14,
        name: 'Silicon',
        weight: 28.0855,
        symbol: 'Si'
    },
    {
        position: 15,
        name: 'Phosphorus',
        weight: 30.9738,
        symbol: 'P'
    },
    {
        position: 16,
        name: 'Sulfur',
        weight: 32.065,
        symbol: 'S'
    },
    {
        position: 17,
        name: 'Chlorine',
        weight: 35.453,
        symbol: 'Cl'
    },
    {
        position: 18,
        name: 'Argon',
        weight: 39.948,
        symbol: 'Ar'
    },
    {
        position: 19,
        name: 'Potassium',
        weight: 39.0983,
        symbol: 'K'
    },
    {
        position: 20,
        name: 'Calcium',
        weight: 40.078,
        symbol: 'Ca'
    }
];


/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-http/table-http-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/* Structure */\r\n.example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    max-height: 500px;\r\n    min-width: 300px;\r\n    position: relative;\r\n}\r\n.example-header {\r\n    min-height: 64px;\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    padding-left: 24px;\r\n    font-size: 20px;\r\n}\r\n.example-table {\r\n    overflow: auto;\r\n    min-height: 300px;\r\n}\r\n.example-loading-shade {\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n    bottom: 56px;\r\n    right: 0;\r\n    background: rgba(0, 0, 0, 0.15);\r\n    z-index: 1;\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    -webkit-box-pack: center;\r\n        -ms-flex-pack: center;\r\n            justify-content: center;\r\n}\r\n.example-rate-limit-reached {\r\n    color: #980000;\r\n    max-width: 360px;\r\n    text-align: center;\r\n}\r\n/* Column Widths */\r\n.mat-column-number,\r\n.mat-column-state {\r\n    max-width: 64px;\r\n}\r\n.mat-column-created {\r\n    max-width: 124px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-http/table-http-example.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container mat-elevation-z8\">\r\n    <div class=\"example-loading-shade\"\r\n         *ngIf=\"isLoadingResults || isRateLimitReached\">\r\n        <mat-spinner *ngIf=\"isLoadingResults\"></mat-spinner>\r\n        <div class=\"example-rate-limit-reached\" *ngIf=\"isRateLimitReached\">\r\n            GitHub's API rate limit has been reached. It will be reset in one minute.\r\n        </div>\r\n    </div>\r\n\r\n    <mat-table #table [dataSource]=\"dataSource\" class=\"example-table\"\r\n               matSort matSortActive=\"created\" matSortDisableClear matSortDirection=\"asc\">\r\n\r\n        <!--- Note that these columns can be defined in any order.\r\n              The actual rendered columns are set as a property on the row definition\" -->\r\n\r\n        <!-- Number Column -->\r\n        <ng-container matColumnDef=\"number\">\r\n            <mat-header-cell *matHeaderCellDef>#</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let row\">{{ row.number }}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Title Column -->\r\n        <ng-container matColumnDef=\"title\">\r\n            <mat-header-cell *matHeaderCellDef>Title</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let row\">{{ row.title }}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- State Column -->\r\n        <ng-container matColumnDef=\"state\">\r\n            <mat-header-cell *matHeaderCellDef>State</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let row\">{{ row.state }}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Created Column -->\r\n        <ng-container matColumnDef=\"created\">\r\n            <mat-header-cell *matHeaderCellDef\r\n                             mat-sort-header\r\n                             disableClear=\"true\">\r\n                Created\r\n            </mat-header-cell>\r\n            <mat-cell *matCellDef=\"let row\">{{ row.created_at | date }}</mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n    </mat-table>\r\n\r\n    <mat-paginator [length]=\"resultsLength\" [pageSize]=\"30\">\r\n    </mat-paginator>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-http/table-http-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableHttpExample; });
/* unused harmony export ExampleHttpDao */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_of__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_switchMap__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/switchMap.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * @title Table retrieving data through HTTP
 */
var TableHttpExample = /** @class */ (function () {
    function TableHttpExample(http) {
        this.http = http;
        this.displayedColumns = ['created', 'state', 'number', 'title'];
        this.dataSource = new __WEBPACK_IMPORTED_MODULE_2__angular_material__["Q" /* MatTableDataSource */]();
        this.resultsLength = 0;
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
    }
    TableHttpExample.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.exampleDatabase = new ExampleHttpDao(this.http);
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
        __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].merge(this.sort.sortChange, this.paginator.page)
            .startWith(null)
            .switchMap(function () {
            setTimeout(function () {
                _this.isLoadingResults = true;
            });
            return _this.exampleDatabase.getRepoIssues(_this.sort.active, _this.sort.direction, _this.paginator.pageIndex);
        })
            .map(function (data) {
            // Flip flag to show that loading has finished.
            _this.isLoadingResults = false;
            _this.isRateLimitReached = false;
            _this.resultsLength = data.total_count;
            return data.items;
        })
            .catch(function () {
            setTimeout(function () {
                _this.isLoadingResults = false;
                // Catch if the GitHub API has reached its rate limit. Return empty data.
                _this.isRateLimitReached = true;
            });
            return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].of([]);
        })
            .subscribe(function (data) { return _this.dataSource.data = data; });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["z" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__angular_material__["z" /* MatPaginator */])
    ], TableHttpExample.prototype, "paginator", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["M" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__angular_material__["M" /* MatSort */])
    ], TableHttpExample.prototype, "sort", void 0);
    TableHttpExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'table-http-example',
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/table-http/table-http-example.css")],
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/table-http/table-http-example.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */]])
    ], TableHttpExample);
    return TableHttpExample;
}());

/** An example database that the data source uses to retrieve data for the table. */
var ExampleHttpDao = /** @class */ (function () {
    function ExampleHttpDao(http) {
        this.http = http;
    }
    ExampleHttpDao.prototype.getRepoIssues = function (sort, order, page) {
        var href = 'https://api.github.com/search/issues';
        var requestUrl = href + "?q=repo:angular/material2&sort=" + sort + "&order=" + order + "&page=" + (page + 1);
        return this.http.get(requestUrl);
    };
    return ExampleHttpDao;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-overview/table-overview-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    min-width: 300px;\r\n}\r\n\r\n.example-header {\r\n    min-height: 64px;\r\n    padding: 8px 24px 0;\r\n}\r\n\r\n.mat-form-field {\r\n    font-size: 14px;\r\n    width: 100%;\r\n}\r\n\r\n.mat-table {\r\n    overflow: auto;\r\n    max-height: 500px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-overview/table-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-header\">\r\n    <mat-form-field>\r\n        <input matInput (keyup)=\"applyFilter($event.target.value)\" placeholder=\"Filter\">\r\n    </mat-form-field>\r\n</div>\r\n\r\n<div class=\"example-container mat-elevation-z8\">\r\n\r\n    <mat-table [dataSource]=\"dataSource\" matSort>\r\n\r\n        <!-- ID Column -->\r\n        <ng-container matColumnDef=\"id\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> ID</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let row\"> {{row.id}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Progress Column -->\r\n        <ng-container matColumnDef=\"progress\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> Progress</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let row\"> {{row.progress}}%</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Name Column -->\r\n        <ng-container matColumnDef=\"name\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> Name</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let row\"> {{row.name}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Color Column -->\r\n        <ng-container matColumnDef=\"color\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> Color</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let row\" [style.color]=\"row.color\"> {{row.color}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\">\r\n        </mat-row>\r\n    </mat-table>\r\n\r\n    <mat-paginator [pageSizeOptions]=\"[5, 10, 25, 100]\"></mat-paginator>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-overview/table-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @title Data table with sorting, pagination, and filtering.
 */
var TableOverviewExample = /** @class */ (function () {
    function TableOverviewExample() {
        this.displayedColumns = ['id', 'name', 'progress', 'color'];
        // Create 100 users
        var users = [];
        for (var i = 1; i <= 100; i++) {
            users.push(createNewUser(i));
        }
        // Assign the data to the data source for the table to render
        this.dataSource = new __WEBPACK_IMPORTED_MODULE_1__angular_material__["Q" /* MatTableDataSource */](users);
    }
    /**
     * Set the paginator and sort after the view init since this component will
     * be able to query its view for the initialized paginator and sort.
     */
    TableOverviewExample.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    };
    TableOverviewExample.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["z" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_material__["z" /* MatPaginator */])
    ], TableOverviewExample.prototype, "paginator", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["M" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_material__["M" /* MatSort */])
    ], TableOverviewExample.prototype, "sort", void 0);
    TableOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'table-overview-example',
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/table-overview/table-overview-example.css")],
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/table-overview/table-overview-example.html")
        }),
        __metadata("design:paramtypes", [])
    ], TableOverviewExample);
    return TableOverviewExample;
}());

/** Builds and returns a new User. */
function createNewUser(id) {
    var name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
        NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';
    return {
        id: id.toString(),
        name: name,
        progress: Math.round(Math.random() * 100).toString(),
        color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
    };
}
/** Constants used to fill up our data base. */
var COLORS = [
    'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple',
    'fuchsia', 'lime', 'teal', 'aqua', 'blue', 'navy', 'black', 'gray'
];
var NAMES = [
    'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
    'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
    'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];


/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-pagination/table-pagination-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    min-width: 300px;\r\n}\r\n\r\n.mat-table {\r\n    overflow: auto;\r\n    max-height: 500px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-pagination/table-pagination-example.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container mat-elevation-z8\">\r\n    <mat-table #table [dataSource]=\"dataSource\">\r\n\r\n        <!-- Position Column -->\r\n        <ng-container matColumnDef=\"position\">\r\n            <mat-header-cell *matHeaderCellDef> No.</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.position}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Name Column -->\r\n        <ng-container matColumnDef=\"name\">\r\n            <mat-header-cell *matHeaderCellDef> Name</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.name}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Weight Column -->\r\n        <ng-container matColumnDef=\"weight\">\r\n            <mat-header-cell *matHeaderCellDef> Weight</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.weight}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Color Column -->\r\n        <ng-container matColumnDef=\"symbol\">\r\n            <mat-header-cell *matHeaderCellDef> Symbol</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.symbol}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n    </mat-table>\r\n\r\n    <mat-paginator #paginator\r\n                   [pageSize]=\"10\"\r\n                   [pageSizeOptions]=\"[5, 10, 20]\">\r\n    </mat-paginator>\r\n</div>"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-pagination/table-pagination-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TablePaginationExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @title Table with pagination
 */
var TablePaginationExample = /** @class */ (function () {
    function TablePaginationExample() {
        this.displayedColumns = ['position', 'name', 'weight', 'symbol'];
        this.dataSource = new __WEBPACK_IMPORTED_MODULE_1__angular_material__["Q" /* MatTableDataSource */](ELEMENT_DATA);
    }
    /**
     * Set the paginator after the view init since this component will
     * be able to query its view for the initialized paginator.
     */
    TablePaginationExample.prototype.ngAfterViewInit = function () {
        this.dataSource.paginator = this.paginator;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["z" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_material__["z" /* MatPaginator */])
    ], TablePaginationExample.prototype, "paginator", void 0);
    TablePaginationExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'table-pagination-example',
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/table-pagination/table-pagination-example.css")],
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/table-pagination/table-pagination-example.html")
        })
    ], TablePaginationExample);
    return TablePaginationExample;
}());

var ELEMENT_DATA = [
    {
        position: 1,
        name: 'Hydrogen',
        weight: 1.0079,
        symbol: 'H'
    },
    {
        position: 2,
        name: 'Helium',
        weight: 4.0026,
        symbol: 'He'
    },
    {
        position: 3,
        name: 'Lithium',
        weight: 6.941,
        symbol: 'Li'
    },
    {
        position: 4,
        name: 'Beryllium',
        weight: 9.0122,
        symbol: 'Be'
    },
    {
        position: 5,
        name: 'Boron',
        weight: 10.811,
        symbol: 'B'
    },
    {
        position: 6,
        name: 'Carbon',
        weight: 12.0107,
        symbol: 'C'
    },
    {
        position: 7,
        name: 'Nitrogen',
        weight: 14.0067,
        symbol: 'N'
    },
    {
        position: 8,
        name: 'Oxygen',
        weight: 15.9994,
        symbol: 'O'
    },
    {
        position: 9,
        name: 'Fluorine',
        weight: 18.9984,
        symbol: 'F'
    },
    {
        position: 10,
        name: 'Neon',
        weight: 20.1797,
        symbol: 'Ne'
    },
    {
        position: 11,
        name: 'Sodium',
        weight: 22.9897,
        symbol: 'Na'
    },
    {
        position: 12,
        name: 'Magnesium',
        weight: 24.305,
        symbol: 'Mg'
    },
    {
        position: 13,
        name: 'Aluminum',
        weight: 26.9815,
        symbol: 'Al'
    },
    {
        position: 14,
        name: 'Silicon',
        weight: 28.0855,
        symbol: 'Si'
    },
    {
        position: 15,
        name: 'Phosphorus',
        weight: 30.9738,
        symbol: 'P'
    },
    {
        position: 16,
        name: 'Sulfur',
        weight: 32.065,
        symbol: 'S'
    },
    {
        position: 17,
        name: 'Chlorine',
        weight: 35.453,
        symbol: 'Cl'
    },
    {
        position: 18,
        name: 'Argon',
        weight: 39.948,
        symbol: 'Ar'
    },
    {
        position: 19,
        name: 'Potassium',
        weight: 39.0983,
        symbol: 'K'
    },
    {
        position: 20,
        name: 'Calcium',
        weight: 40.078,
        symbol: 'Ca'
    }
];


/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-sorting/table-sorting-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-container {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -webkit-box-orient: vertical;\r\n    -webkit-box-direction: normal;\r\n        -ms-flex-direction: column;\r\n            flex-direction: column;\r\n    min-width: 300px;\r\n}\r\n\r\n.mat-table {\r\n    overflow: auto;\r\n    max-height: 500px;\r\n}\r\n\r\n.mat-header-cell.mat-sort-header-sorted {\r\n    color: black;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-sorting/table-sorting-example.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-container mat-elevation-z8\">\r\n    <mat-table #table [dataSource]=\"dataSource\" matSort>\r\n\r\n        <!-- Position Column -->\r\n        <ng-container matColumnDef=\"position\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> No.</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.position}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Name Column -->\r\n        <ng-container matColumnDef=\"name\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> Name</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.name}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Weight Column -->\r\n        <ng-container matColumnDef=\"weight\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> Weight</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.weight}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <!-- Color Column -->\r\n        <ng-container matColumnDef=\"symbol\">\r\n            <mat-header-cell *matHeaderCellDef mat-sort-header> Symbol</mat-header-cell>\r\n            <mat-cell *matCellDef=\"let element\"> {{element.symbol}}</mat-cell>\r\n        </ng-container>\r\n\r\n        <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n        <mat-row *matRowDef=\"let row; columns: displayedColumns;\"></mat-row>\r\n    </mat-table>\r\n</div>"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/table-sorting/table-sorting-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TableSortingExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @title Table with sorting
 */
var TableSortingExample = /** @class */ (function () {
    function TableSortingExample() {
        this.displayedColumns = ['position', 'name', 'weight', 'symbol'];
        this.dataSource = new __WEBPACK_IMPORTED_MODULE_1__angular_material__["Q" /* MatTableDataSource */](ELEMENT_DATA);
    }
    /**
     * Set the sort after the view init since this component will
     * be able to query its view for the initialized sort.
     */
    TableSortingExample.prototype.ngAfterViewInit = function () {
        this.dataSource.sort = this.sort;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1__angular_material__["M" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_material__["M" /* MatSort */])
    ], TableSortingExample.prototype, "sort", void 0);
    TableSortingExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'table-sorting-example',
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/table-sorting/table-sorting-example.css")],
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/table-sorting/table-sorting-example.html")
        })
    ], TableSortingExample);
    return TableSortingExample;
}());

var ELEMENT_DATA = [
    {
        position: 1,
        name: 'Hydrogen',
        weight: 1.0079,
        symbol: 'H'
    },
    {
        position: 2,
        name: 'Helium',
        weight: 4.0026,
        symbol: 'He'
    },
    {
        position: 3,
        name: 'Lithium',
        weight: 6.941,
        symbol: 'Li'
    },
    {
        position: 4,
        name: 'Beryllium',
        weight: 9.0122,
        symbol: 'Be'
    },
    {
        position: 5,
        name: 'Boron',
        weight: 10.811,
        symbol: 'B'
    },
    {
        position: 6,
        name: 'Carbon',
        weight: 12.0107,
        symbol: 'C'
    },
    {
        position: 7,
        name: 'Nitrogen',
        weight: 14.0067,
        symbol: 'N'
    },
    {
        position: 8,
        name: 'Oxygen',
        weight: 15.9994,
        symbol: 'O'
    },
    {
        position: 9,
        name: 'Fluorine',
        weight: 18.9984,
        symbol: 'F'
    },
    {
        position: 10,
        name: 'Neon',
        weight: 20.1797,
        symbol: 'Ne'
    },
    {
        position: 11,
        name: 'Sodium',
        weight: 22.9897,
        symbol: 'Na'
    },
    {
        position: 12,
        name: 'Magnesium',
        weight: 24.305,
        symbol: 'Mg'
    },
    {
        position: 13,
        name: 'Aluminum',
        weight: 26.9815,
        symbol: 'Al'
    },
    {
        position: 14,
        name: 'Silicon',
        weight: 28.0855,
        symbol: 'Si'
    },
    {
        position: 15,
        name: 'Phosphorus',
        weight: 30.9738,
        symbol: 'P'
    },
    {
        position: 16,
        name: 'Sulfur',
        weight: 32.065,
        symbol: 'S'
    },
    {
        position: 17,
        name: 'Chlorine',
        weight: 35.453,
        symbol: 'Cl'
    },
    {
        position: 18,
        name: 'Argon',
        weight: 39.948,
        symbol: 'Ar'
    },
    {
        position: 19,
        name: 'Potassium',
        weight: 39.0983,
        symbol: 'K'
    },
    {
        position: 20,
        name: 'Calcium',
        weight: 40.078,
        symbol: 'Ca'
    }
];


/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/tabs-overview/tabs-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-tab-group>\r\n    <mat-tab label=\"Tab 1\">Content 1</mat-tab>\r\n    <mat-tab label=\"Tab 2\">Content 2</mat-tab>\r\n</mat-tab-group>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/tabs-overview/tabs-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic tabs
 */
var TabsOverviewExample = /** @class */ (function () {
    function TabsOverviewExample() {
    }
    TabsOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'tabs-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/tabs-overview/tabs-overview-example.html")
        })
    ], TabsOverviewExample);
    return TabsOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/tabs-template-label/tabs-template-label-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".demo-tab-group {\r\n    border: 1px solid #E8E8E8;\r\n}\r\n\r\n.demo-tab-content {\r\n    padding: 16px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/tabs-template-label/tabs-template-label-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-tab-group class=\"demo-tab-group\">\r\n    <mat-tab label=\"Tab 1\">\r\n        <div class=\"demo-tab-content\">\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla venenatis ante augue.\r\n            Phasellus volutpat neque ac dui mattis vulputate. Etiam consequat aliquam cursus.\r\n            In sodales pretium ultrices. Maecenas lectus est, sollicitudin consectetur felis nec,\r\n            feugiat ultricies mi. Aliquam erat volutpat. Nam placerat, tortor in ultrices porttitor,\r\n            orci enim rutrum enim, vel tempor sapien arcu a tellus.\r\n        </div>\r\n    </mat-tab>\r\n    <mat-tab label=\"Tab 2\">\r\n        <ng-template mat-tab-label>\r\n            ⭐\r\n        </ng-template>\r\n        <div class=\"demo-tab-content\">\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla venenatis ante augue.\r\n            Phasellus volutpat neque ac dui mattis vulputate. Etiam consequat aliquam cursus.\r\n            In sodales pretium ultrices. Maecenas lectus est, sollicitudin consectetur felis nec,\r\n            feugiat ultricies mi. Aliquam erat volutpat. Nam placerat, tortor in ultrices porttitor,\r\n            orci enim rutrum enim, vel tempor sapien arcu a tellus.\r\n        </div>\r\n    </mat-tab>\r\n    <mat-tab label=\"Tab 3\" disabled>\r\n        No content\r\n    </mat-tab>\r\n    <mat-tab label=\"Tab 4\">\r\n        <div class=\"demo-tab-content\">\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla venenatis ante augue.\r\n            Phasellus volutpat neque ac dui mattis vulputate. Etiam consequat aliquam cursus.\r\n            In sodales pretium ultrices. Maecenas lectus est, sollicitudin consectetur felis nec,\r\n            feugiat ultricies mi. Aliquam erat volutpat. Nam placerat, tortor in ultrices porttitor,\r\n            orci enim rutrum enim, vel tempor sapien arcu a tellus.\r\n            <br/>\r\n            <br/>\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla venenatis ante augue.\r\n            Phasellus volutpat neque ac dui mattis vulputate. Etiam consequat aliquam cursus.\r\n            In sodales pretium ultrices. Maecenas lectus est, sollicitudin consectetur felis nec,\r\n            feugiat ultricies mi. Aliquam erat volutpat. Nam placerat, tortor in ultrices porttitor,\r\n            orci enim rutrum enim, vel tempor sapien arcu a tellus.\r\n        </div>\r\n    </mat-tab>\r\n    <mat-tab label=\"Tab 5\">\r\n        No content\r\n    </mat-tab>\r\n    <mat-tab label=\"Tab 6\">\r\n        No content\r\n    </mat-tab>\r\n</mat-tab-group>"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/tabs-template-label/tabs-template-label-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsTemplateLabelExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Complex Example
 */
var TabsTemplateLabelExample = /** @class */ (function () {
    function TabsTemplateLabelExample() {
    }
    TabsTemplateLabelExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'tabs-template-label-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/tabs-template-label/tabs-template-label-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/tabs-template-label/tabs-template-label-example.css")]
        })
    ], TabsTemplateLabelExample);
    return TabsTemplateLabelExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/toolbar-multirow/toolbar-multirow-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-icon {\r\n    padding: 0 14px;\r\n}\r\n\r\n.example-spacer {\r\n    -webkit-box-flex: 1;\r\n        -ms-flex: 1 1 auto;\r\n            flex: 1 1 auto;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/toolbar-multirow/toolbar-multirow-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\">\r\n    <mat-toolbar-row>\r\n        <span>Custom Toolbar</span>\r\n    </mat-toolbar-row>\r\n\r\n    <mat-toolbar-row>\r\n        <span>Second Line</span>\r\n        <span class=\"example-spacer\"></span>\r\n        <mat-icon class=\"example-icon\">verified_user</mat-icon>\r\n    </mat-toolbar-row>\r\n\r\n    <mat-toolbar-row>\r\n        <span>Third Line</span>\r\n        <span class=\"example-spacer\"></span>\r\n        <mat-icon class=\"example-icon\">favorite</mat-icon>\r\n        <mat-icon class=\"example-icon\">delete</mat-icon>\r\n    </mat-toolbar-row>\r\n</mat-toolbar>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/toolbar-multirow/toolbar-multirow-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToolbarMultirowExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Multi-row toolbar
 */
var ToolbarMultirowExample = /** @class */ (function () {
    function ToolbarMultirowExample() {
    }
    ToolbarMultirowExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'toolbar-multirow-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/toolbar-multirow/toolbar-multirow-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/toolbar-multirow/toolbar-multirow-example.css")]
        })
    ], ToolbarMultirowExample);
    return ToolbarMultirowExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/toolbar-overview/toolbar-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<mat-toolbar>My App</mat-toolbar>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/toolbar-overview/toolbar-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToolbarOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic toolbar
 */
var ToolbarOverviewExample = /** @class */ (function () {
    function ToolbarOverviewExample() {
    }
    ToolbarOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'toolbar-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/toolbar-overview/toolbar-overview-example.html")
        })
    ], ToolbarOverviewExample);
    return ToolbarOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/tooltip-overview/tooltip-overview-example.html":
/***/ (function(module, exports) {

module.exports = "<span matTooltip=\"Tooltip!\">I have a tooltip</span>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/tooltip-overview/tooltip-overview-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TooltipOverviewExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Basic tooltip
 */
var TooltipOverviewExample = /** @class */ (function () {
    function TooltipOverviewExample() {
    }
    TooltipOverviewExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'tooltip-overview-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/tooltip-overview/tooltip-overview-example.html")
        })
    ], TooltipOverviewExample);
    return TooltipOverviewExample;
}());



/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/tooltip-position/tooltip-position-example.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-tooltip-host {\r\n    display: -webkit-inline-box;\r\n    display: -ms-inline-flexbox;\r\n    display: inline-flex;\r\n    -webkit-box-align: center;\r\n        -ms-flex-align: center;\r\n            align-items: center;\r\n    margin: 50px;\r\n}\r\n\r\n.example-select {\r\n    margin: 0 10px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/tooltip-position/tooltip-position-example.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"example-tooltip-host\" matTooltip=\"Tooltip!\" [matTooltipPosition]=\"position\">\r\n    <span>Show tooltip</span>\r\n    <mat-form-field>\r\n        <mat-select class=\"example-select\" [(ngModel)]=\"position\">\r\n            <mat-option value=\"before\">Before</mat-option>\r\n            <mat-option value=\"after\">After</mat-option>\r\n            <mat-option value=\"above\">Above</mat-option>\r\n            <mat-option value=\"below\">Below</mat-option>\r\n            <mat-option value=\"left\">Left</mat-option>\r\n            <mat-option value=\"right\">Right</mat-option>\r\n        </mat-select>\r\n    </mat-form-field>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/assets/angular-material-examples/tooltip-position/tooltip-position-example.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TooltipPositionExample; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * @title Tooltip with custom position
 */
var TooltipPositionExample = /** @class */ (function () {
    function TooltipPositionExample() {
        this.position = 'before';
    }
    TooltipPositionExample = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'tooltip-position-example',
            template: __webpack_require__("../../../../../src/assets/angular-material-examples/tooltip-position/tooltip-position-example.html"),
            styles: [__webpack_require__("../../../../../src/assets/angular-material-examples/tooltip-position/tooltip-position-example.css")]
        })
    ], TooltipPositionExample);
    return TooltipPositionExample;
}());



/***/ }),

/***/ "../../../../rxjs/_esm5/add/observable/of.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__observable_of__ = __webpack_require__("../../../../rxjs/_esm5/observable/of.js");
/** PURE_IMPORTS_START .._.._Observable,.._.._observable_of PURE_IMPORTS_END */


__WEBPACK_IMPORTED_MODULE_0__Observable__["Observable"].of = __WEBPACK_IMPORTED_MODULE_1__observable_of__["a" /* of */];
//# sourceMappingURL=of.js.map 


/***/ }),

/***/ "../../../../rxjs/_esm5/add/operator/first.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__operator_first__ = __webpack_require__("../../../../rxjs/_esm5/operator/first.js");
/** PURE_IMPORTS_START .._.._Observable,.._.._operator_first PURE_IMPORTS_END */


__WEBPACK_IMPORTED_MODULE_0__Observable__["Observable"].prototype.first = __WEBPACK_IMPORTED_MODULE_1__operator_first__["a" /* first */];
//# sourceMappingURL=first.js.map 


/***/ }),

/***/ "../../../../rxjs/_esm5/add/operator/switchMap.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__operator_switchMap__ = __webpack_require__("../../../../rxjs/_esm5/operator/switchMap.js");
/** PURE_IMPORTS_START .._.._Observable,.._.._operator_switchMap PURE_IMPORTS_END */


__WEBPACK_IMPORTED_MODULE_0__Observable__["Observable"].prototype.switchMap = __WEBPACK_IMPORTED_MODULE_1__operator_switchMap__["a" /* switchMap */];
//# sourceMappingURL=switchMap.js.map 


/***/ }),

/***/ "../../../material-moment-adapter/esm5/material-moment-adapter.es5.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export MomentDateModule */
/* unused harmony export MatMomentDateModule */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return MomentDateAdapter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MAT_MOMENT_DATE_FORMATS; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_tslib__ = __webpack_require__("../../../../tslib/tslib.es6.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */







/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
// TODO(mmalerba): See if we can clean this up at some point.
var moment = __WEBPACK_IMPORTED_MODULE_3_moment___default.a || __WEBPACK_IMPORTED_MODULE_3_moment__;
/**
 * Creates an array and fills it with values.
 * @template T
 * @param {?} length
 * @param {?} valueFunction
 * @return {?}
 */
function range(length, valueFunction) {
    var /** @type {?} */ valuesArray = Array(length);
    for (var /** @type {?} */ i = 0; i < length; i++) {
        valuesArray[i] = valueFunction(i);
    }
    return valuesArray;
}
/**
 * Adapts Moment.js Dates for use with Angular Material.
 */
var MomentDateAdapter = /** @class */ (function (_super) {
    Object(__WEBPACK_IMPORTED_MODULE_2_tslib__["b" /* __extends */])(MomentDateAdapter, _super);
    function MomentDateAdapter(dateLocale) {
        var _this = _super.call(this) || this;
        _this.setLocale(dateLocale || moment.locale());
        return _this;
    }
    /**
     * @param {?} locale
     * @return {?}
     */
    MomentDateAdapter.prototype.setLocale = /**
     * @param {?} locale
     * @return {?}
     */
    function (locale) {
        var _this = this;
        _super.prototype.setLocale.call(this, locale);
        var /** @type {?} */ momentLocaleData = moment.localeData(locale);
        this._localeData = {
            firstDayOfWeek: momentLocaleData.firstDayOfWeek(),
            longMonths: momentLocaleData.months(),
            shortMonths: momentLocaleData.monthsShort(),
            dates: range(31, function (i) { return _this.createDate(2017, 0, i + 1).format('D'); }),
            longDaysOfWeek: momentLocaleData.weekdays(),
            shortDaysOfWeek: momentLocaleData.weekdaysShort(),
            narrowDaysOfWeek: momentLocaleData.weekdaysMin(),
        };
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.getYear = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return this.clone(date).year();
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.getMonth = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return this.clone(date).month();
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.getDate = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return this.clone(date).date();
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.getDayOfWeek = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return this.clone(date).day();
    };
    /**
     * @param {?} style
     * @return {?}
     */
    MomentDateAdapter.prototype.getMonthNames = /**
     * @param {?} style
     * @return {?}
     */
    function (style) {
        // Moment.js doesn't support narrow month names, so we just use short if narrow is requested.
        return style == 'long' ? this._localeData.longMonths : this._localeData.shortMonths;
    };
    /**
     * @return {?}
     */
    MomentDateAdapter.prototype.getDateNames = /**
     * @return {?}
     */
    function () {
        return this._localeData.dates;
    };
    /**
     * @param {?} style
     * @return {?}
     */
    MomentDateAdapter.prototype.getDayOfWeekNames = /**
     * @param {?} style
     * @return {?}
     */
    function (style) {
        if (style == 'long') {
            return this._localeData.longDaysOfWeek;
        }
        if (style == 'short') {
            return this._localeData.shortDaysOfWeek;
        }
        return this._localeData.narrowDaysOfWeek;
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.getYearName = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return this.clone(date).format('YYYY');
    };
    /**
     * @return {?}
     */
    MomentDateAdapter.prototype.getFirstDayOfWeek = /**
     * @return {?}
     */
    function () {
        return this._localeData.firstDayOfWeek;
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.getNumDaysInMonth = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return this.clone(date).daysInMonth();
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.clone = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return date.clone().locale(this.locale);
    };
    /**
     * @param {?} year
     * @param {?} month
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.createDate = /**
     * @param {?} year
     * @param {?} month
     * @param {?} date
     * @return {?}
     */
    function (year, month, date) {
        // Moment.js will create an invalid date if any of the components are out of bounds, but we
        // explicitly check each case so we can throw more descriptive errors.
        if (month < 0 || month > 11) {
            throw Error("Invalid month index \"" + month + "\". Month index has to be between 0 and 11.");
        }
        if (date < 1) {
            throw Error("Invalid date \"" + date + "\". Date has to be greater than 0.");
        }
        var /** @type {?} */ result = moment({ year: year, month: month, date: date }).locale(this.locale);
        // If the result isn't valid, the date must have been out of bounds for this month.
        if (!result.isValid()) {
            throw Error("Invalid date \"" + date + "\" for month with index \"" + month + "\".");
        }
        return result;
    };
    /**
     * @return {?}
     */
    MomentDateAdapter.prototype.today = /**
     * @return {?}
     */
    function () {
        return moment().locale(this.locale);
    };
    /**
     * @param {?} value
     * @param {?} parseFormat
     * @return {?}
     */
    MomentDateAdapter.prototype.parse = /**
     * @param {?} value
     * @param {?} parseFormat
     * @return {?}
     */
    function (value, parseFormat) {
        if (value && typeof value == 'string') {
            return moment(value, parseFormat, this.locale);
        }
        return value ? moment(value).locale(this.locale) : null;
    };
    /**
     * @param {?} date
     * @param {?} displayFormat
     * @return {?}
     */
    MomentDateAdapter.prototype.format = /**
     * @param {?} date
     * @param {?} displayFormat
     * @return {?}
     */
    function (date, displayFormat) {
        date = this.clone(date);
        if (!this.isValid(date)) {
            throw Error('MomentDateAdapter: Cannot format invalid date.');
        }
        return date.format(displayFormat);
    };
    /**
     * @param {?} date
     * @param {?} years
     * @return {?}
     */
    MomentDateAdapter.prototype.addCalendarYears = /**
     * @param {?} date
     * @param {?} years
     * @return {?}
     */
    function (date, years) {
        return this.clone(date).add({ years: years });
    };
    /**
     * @param {?} date
     * @param {?} months
     * @return {?}
     */
    MomentDateAdapter.prototype.addCalendarMonths = /**
     * @param {?} date
     * @param {?} months
     * @return {?}
     */
    function (date, months) {
        return this.clone(date).add({ months: months });
    };
    /**
     * @param {?} date
     * @param {?} days
     * @return {?}
     */
    MomentDateAdapter.prototype.addCalendarDays = /**
     * @param {?} date
     * @param {?} days
     * @return {?}
     */
    function (date, days) {
        return this.clone(date).add({ days: days });
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.toIso8601 = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return this.clone(date).format();
    };
    /**
     * Returns the given value if given a valid Moment or null. Deserializes valid ISO 8601 strings
     * (https://www.ietf.org/rfc/rfc3339.txt) and valid Date objects into valid Moments and empty
     * string into null. Returns an invalid date for all other values.
     */
    /**
     * Returns the given value if given a valid Moment or null. Deserializes valid ISO 8601 strings
     * (https://www.ietf.org/rfc/rfc3339.txt) and valid Date objects into valid Moments and empty
     * string into null. Returns an invalid date for all other values.
     * @param {?} value
     * @return {?}
     */
    MomentDateAdapter.prototype.deserialize = /**
     * Returns the given value if given a valid Moment or null. Deserializes valid ISO 8601 strings
     * (https://www.ietf.org/rfc/rfc3339.txt) and valid Date objects into valid Moments and empty
     * string into null. Returns an invalid date for all other values.
     * @param {?} value
     * @return {?}
     */
    function (value) {
        var /** @type {?} */ date;
        if (value instanceof Date) {
            date = moment(value);
        }
        if (typeof value === 'string') {
            if (!value) {
                return null;
            }
            date = moment(value, moment.ISO_8601).locale(this.locale);
        }
        if (date && this.isValid(date)) {
            return date;
        }
        return _super.prototype.deserialize.call(this, value);
    };
    /**
     * @param {?} obj
     * @return {?}
     */
    MomentDateAdapter.prototype.isDateInstance = /**
     * @param {?} obj
     * @return {?}
     */
    function (obj) {
        return moment.isMoment(obj);
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.isValid = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return this.clone(date).isValid();
    };
    /**
     * @return {?}
     */
    MomentDateAdapter.prototype.invalid = /**
     * @return {?}
     */
    function () {
        return moment.invalid();
    };
    MomentDateAdapter.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
    ];
    /** @nocollapse */
    MomentDateAdapter.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Optional"] }, { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"], args: [__WEBPACK_IMPORTED_MODULE_1__angular_material__["c" /* MAT_DATE_LOCALE */],] },] },
    ]; };
    return MomentDateAdapter;
}(__WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* DateAdapter */]));

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

var MAT_MOMENT_DATE_FORMATS = {
    parse: {
        dateInput: 'l',
    },
    display: {
        dateInput: 'l',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

var MomentDateModule = /** @class */ (function () {
    function MomentDateModule() {
    }
    MomentDateModule.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"], args: [{
                    providers: [
                        __WEBPACK_IMPORTED_MODULE_1__angular_material__["d" /* MAT_DATE_LOCALE_PROVIDER */],
                        { provide: __WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* DateAdapter */], useClass: MomentDateAdapter, deps: [__WEBPACK_IMPORTED_MODULE_1__angular_material__["c" /* MAT_DATE_LOCALE */]] }
                    ],
                },] },
    ];
    /** @nocollapse */
    MomentDateModule.ctorParameters = function () { return []; };
    return MomentDateModule;
}());
var ɵ0 = MAT_MOMENT_DATE_FORMATS;
var MatMomentDateModule = /** @class */ (function () {
    function MatMomentDateModule() {
    }
    MatMomentDateModule.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"], args: [{
                    imports: [MomentDateModule],
                    providers: [{ provide: __WEBPACK_IMPORTED_MODULE_1__angular_material__["b" /* MAT_DATE_FORMATS */], useValue: ɵ0 }],
                },] },
    ];
    /** @nocollapse */
    MatMomentDateModule.ctorParameters = function () { return []; };
    return MatMomentDateModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Generated bundle index. Do not edit.
 */


//# sourceMappingURL=material-moment-adapter.es5.js.map


/***/ })

});
//# sourceMappingURL=apps.module.chunk.js.map