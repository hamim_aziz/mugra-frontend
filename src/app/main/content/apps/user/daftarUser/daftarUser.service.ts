import { Injectable } from '@angular/core';
import { 
    CanActivate, 
    ActivatedRouteSnapshot, 
    RouterStateSnapshot, 
    Router,
    Resolve
} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Location } from '@angular/common';
import 'rxjs/add/operator/map';
import { MatSnackBar } from '@angular/material';
import { SessionService } from './../../session.service';

@Injectable()
export class DaftarUserService implements Resolve<any>
{
    products: any[];
    onProductsChanged: BehaviorSubject<any> = new BehaviorSubject({});
    token = this._sessionService.get().token;
    role = this._sessionService.get().role;
    id = this._sessionService.get().role;

    constructor(
        private http: Http,
        private router: Router,
        private location: Location,
        private snackBar: MatSnackBar,
        private _sessionService: SessionService
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProducts()
            ]).then(
                () => {
                    resolve();
                },
                reject
            ).catch(
                (error) => {
                    console.log(error);
                }
            );
        });
    }

    setHeader(options: RequestOptions) {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        options.headers = headers;
    }

    getProducts(): Promise<any>
    {
        let options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url + '/admin/users', options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    this.products = response.data;
                    if (this.role < 4) {
                        this.products = [];
                        this.snackBar.open(response.message, response.status, {
                            verticalPosition: 'top',
                            duration        : 5000
                        });
                    }
                    this.onProductsChanged.next(this.products);
                    resolve(response);
                }, (error) => {
                    // this.router.navigate(["/apps/login"]);
                    console.log(error);
                    reject(error);
                });
        });
    }
}
