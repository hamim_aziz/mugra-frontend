webpackJsonp(["monitor.module"],{

/***/ "../../../../../src/app/main/content/apps/monitor_rute/monitor.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MonitorModule", function() { return MonitorModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__ = __webpack_require__("../../../../@swimlane/ngx-charts/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__ = __webpack_require__("../../../../../src/app/core/modules/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__ = __webpack_require__("../../../../../src/app/core/components/widget/widget.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__monitorRute_monitorRute_component__ = __webpack_require__("../../../../../src/app/main/content/apps/monitor_rute/monitorRute/monitorRute.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__monitorRute_monitorRute_service__ = __webpack_require__("../../../../../src/app/main/content/apps/monitor_rute/monitorRute/monitorRute.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_components_confirm_dialog_confirm_dialog_component__ = __webpack_require__("../../../../../src/app/core/components/confirm-dialog/confirm-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_cdk_table__ = __webpack_require__("../../../cdk/esm5/table.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ng2_dnd__ = __webpack_require__("../../../../ng2-dnd/ng2-dnd.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__auth_guard__ = __webpack_require__("../../../../../src/app/main/content/apps/auth.guard.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var routes = [
    {
        path: 'daftarRute',
        component: __WEBPACK_IMPORTED_MODULE_5__monitorRute_monitorRute_component__["a" /* MonitorRuteComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_11__auth_guard__["a" /* AuthGuard */]],
        resolve: {
            data: __WEBPACK_IMPORTED_MODULE_6__monitorRute_monitorRute_service__["a" /* MonitorRuteService */]
        }
    }
];
var MonitorModule = /** @class */ (function () {
    function MonitorModule() {
    }
    MonitorModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__["a" /* FuseWidgetModule */],
                __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__["NgxChartsModule"],
                __WEBPACK_IMPORTED_MODULE_9__angular_cdk_table__["m" /* CdkTableModule */],
                __WEBPACK_IMPORTED_MODULE_10_ng2_dnd__["a" /* DndModule */],
                __WEBPACK_IMPORTED_MODULE_8__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
                })
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_9__angular_cdk_table__["m" /* CdkTableModule */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__monitorRute_monitorRute_component__["a" /* MonitorRuteComponent */],
                __WEBPACK_IMPORTED_MODULE_7__core_components_confirm_dialog_confirm_dialog_component__["a" /* FuseConfirmDialogComponent */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__monitorRute_monitorRute_component__["a" /* MonitorRuteComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__monitorRute_monitorRute_service__["a" /* MonitorRuteService */],
                __WEBPACK_IMPORTED_MODULE_10_ng2_dnd__["d" /* DragDropSortableService */],
                __WEBPACK_IMPORTED_MODULE_10_ng2_dnd__["c" /* DragDropService */],
                __WEBPACK_IMPORTED_MODULE_10_ng2_dnd__["b" /* DragDropConfig */]
            ]
        })
    ], MonitorModule);
    return MonitorModule;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/monitor_rute/monitorRute/monitorRute.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"products\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\"\r\n             fxLayout=\"column\" fxLayoutAlign=\"center center\"\r\n             fxLayout.gt-xs=\"row\" fxLayoutAlign.gt-xs=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div class=\"logo my-12 m-sm-0\"\r\n                 fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <mat-icon class=\"logo-icon mr-16\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'50ms',scale:'0.2'}}\">navigation</mat-icon>\r\n                <span class=\"logo-text h1\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">Rute</span>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <!-- SEARCH -->\r\n            <!-- <div class=\"search-input-wrapper mx-12 m-md-0\"\r\n                 fxFlex=\"1 0 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <label for=\"search\" class=\"mr-8\">\r\n                    <mat-icon class=\"secondary-text\">search</mat-icon>\r\n                </label>\r\n                <mat-form-field floatPlaceholder=\"never\" fxFlex=\"1 0 auto\">\r\n                    <input id=\"search\" matInput #filter placeholder=\"Search\">\r\n                </mat-form-field>\r\n            </div> -->\r\n            <!-- / SEARCH -->\r\n\r\n            <!-- <button mat-raised-button\r\n                    [routerLink]=\"'/apps/rute/detailRute/new'\"\r\n                    class=\"add-product-button mat-white-bg my-12 mt-sm-0\"\r\n                    [disabled]=\"hapus\">\r\n                <span>Tambah Rute</span>\r\n            </button>\r\n\r\n            <button *ngIf=\"!hapus\" \r\n                    mat-raised-button\r\n                    (click)=\"toggleHapus()\"\r\n                    class=\"add-product-button mat-white-bg my-12 mt-sm-0\">\r\n                <span>Hapus</span>\r\n            </button>\r\n            <button *ngIf=\"hapus\" \r\n                    mat-raised-button\r\n                    (click)=\"toggleHapus()\"\r\n                    class=\"add-product-button mat-white-bg my-12 mt-sm-0\">\r\n                <span>Selesai</span>\r\n            </button> -->\r\n\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <mat-table class=\"products-table\"\r\n                       #table [dataSource]=\"dataSource\"\r\n                       matSort\r\n                       [@animateStagger]=\"{value:'50'}\"\r\n                       fusePerfectScrollbar>\r\n\r\n                <!-- ID Column -->\r\n                <ng-container cdkColumnDef=\"id\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>No.</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product; let i = index\">\r\n                        <p class=\"text-truncate\">{{ (paginator.pageIndex * paginator.pageSize) + (i + 1) }}.</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Jenis Column -->\r\n                <ng-container cdkColumnDef=\"jenis\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxHide fxShow.gt-sm>Jenis</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxHide fxShow.gt-sm>\r\n                        <p class=\"price text-truncate\">\r\n                            {{product.jenis}}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Asal Column -->\r\n                <ng-container cdkColumnDef=\"asal\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header>Asal</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\">\r\n                        <p class=\"text-truncate\">{{product.asal}}</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Tujuan Column -->\r\n                <ng-container cdkColumnDef=\"tujuan\">\r\n                    <mat-header-cell *cdkHeaderCellDef fxHide mat-sort-header fxShow.gt-md>Tujuan</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxHide fxShow.gt-md>\r\n                        <p class=\"category text-truncate\"> {{product.tujuan}} </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Tanggal Column -->\r\n                <ng-container cdkColumnDef=\"tanggal\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxHide fxShow.gt-xs>Tanggal</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxHide fxShow.gt-xs>\r\n                        <p class=\"price text-truncate\">\r\n                            {{ product.tanggal.substring(9, 11) }} {{ bulan(product.tanggal) }} {{ product.tanggal.substring(0, 4) }}\r\n                        </p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Kuota Motor Column -->\r\n                <ng-container cdkColumnDef=\"kuota_motor\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxHide fxShow.gt-xs>Kuota Motor</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxHide fxShow.gt-xs>\r\n                        <span class=\"quantity-indicator text-truncate\"\r\n                              [ngClass]=\"{'mat-red-500-bg':product.kuota_motor <= 5, 'mat-amber-500-bg':product.kuota_motor > 5 && product.kuota_motor <= 25,'mat-green-600-bg':product.kuota_motor > 25}\">\r\n                        </span>\r\n                        <span>\r\n                            {{product.kuota_motor}}\r\n                        </span>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <!-- Sisa Column -->\r\n                <ng-container cdkColumnDef=\"sisa\">\r\n                    <mat-header-cell *cdkHeaderCellDef mat-sort-header fxHide fxShow.gt-xs>Kuota Orang</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product\" fxHide fxShow.gt-xs>\r\n                        <span class=\"quantity-indicator text-truncate\"\r\n                              [ngClass]=\"{'mat-red-500-bg':product.sisa <= 5, 'mat-amber-500-bg':product.sisa > 5 && product.sisa <= 25,'mat-green-600-bg':product.sisa > 25}\">\r\n                        </span>\r\n                        <span>\r\n                            {{product.sisa}}\r\n                        </span>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <mat-header-row *cdkHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n\r\n                <mat-row *cdkRowDef=\"let product; columns: displayedColumns;\"\r\n                         class=\"product\"\r\n                         matRipple>\r\n                </mat-row>\r\n\r\n            </mat-table>\r\n\r\n            <mat-paginator #paginator\r\n                           [length]=\"dataSource.filteredData.length\"\r\n                           [pageIndex]=\"0\"\r\n                           [pageSize]=\"25\"\r\n                           [pageSizeOptions]=\"[5, 10, 25, 100]\">\r\n            </mat-paginator>\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/monitor_rute/monitorRute/monitorRute.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n:host .header .search-input-wrapper {\n  max-width: 480px; }\n:host .header .add-product-button {\n  background-color: #F96D01; }\n@media (max-width: 599px) {\n  :host .header {\n    height: 176px !important;\n    min-height: 176px !important;\n    max-height: 176px !important; } }\n@media (max-width: 599px) {\n  :host .top-bg {\n    height: 240px; } }\n:host .products-table {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  border-bottom: 1px solid rgba(0, 0, 0, 0.12);\n  overflow: auto; }\n:host .products-table .mat-header-row {\n    min-height: 40px;\n    background-color: rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-row {\n    min-height: 60px;\n    height: 60px !important; }\n:host .products-table .product {\n    position: relative;\n    cursor: pointer;\n    height: 60px; }\n:host .products-table .mat-cell {\n    min-width: 0;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n:host .products-table .mat-column-id {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 38px;\n            flex: 0 1 38px; }\n:host .products-table .mat-column-kuota_motor, :host .products-table .mat-column-sisa {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center !important;\n        -ms-flex-pack: center !important;\n            justify-content: center !important; }\n:host .products-table .text-truncate {\n    overflow: visible !important; }\n:host .products-table .mat-column-image {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 84px;\n            flex: 0 1 84px; }\n:host .products-table .mat-column-image .product-image {\n      width: 52px;\n      height: 52px;\n      border: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-column-buttons {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 80px;\n            flex: 0 1 80px; }\n:host .products-table .quantity-indicator {\n    display: inline-block;\n    vertical-align: middle;\n    width: 8px;\n    height: 8px;\n    border-radius: 4px;\n    margin-right: 8px; }\n:host .products-table .quantity-indicator + span {\n      display: inline-block;\n      vertical-align: middle; }\n:host .products-table .active-icon {\n    border-radius: 50%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/monitor_rute/monitorRute/monitorRute.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MonitorRuteComponent; });
/* unused harmony export FilesDataSource */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__monitorRute_service__ = __webpack_require__("../../../../../src/app/main/content/apps/monitor_rute/monitorRute/monitorRute.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_cdk_collections__ = __webpack_require__("../../../cdk/esm5/collections.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_services_config_service__ = __webpack_require__("../../../../../src/app/core/services/config.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_startWith__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_add_operator_debounceTime__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/debounceTime.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/distinctUntilChanged.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_rxjs_add_observable_fromEvent__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/fromEvent.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__core_fuseUtils__ = __webpack_require__("../../../../../src/app/core/fuseUtils.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__core_components_confirm_dialog_confirm_dialog_component__ = __webpack_require__("../../../../../src/app/core/components/confirm-dialog/confirm-dialog.component.ts");
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var MonitorRuteComponent = /** @class */ (function () {
    function MonitorRuteComponent(fuseConfig, productsService, dialog, router) {
        this.fuseConfig = fuseConfig;
        this.productsService = productsService;
        this.dialog = dialog;
        this.router = router;
        this.hapus = false;
        this.displayedColumns = ['id', 'asal', 'tujuan', 'tanggal', 'jenis', 'kuota_motor', 'sisa'];
        this.fuseConfig.setSettings({
            layout: {
                navigation: 'none',
                toolbar: 'none',
                footer: 'none'
            }
        });
    }
    MonitorRuteComponent.prototype.ngOnInit = function () {
        this.dataSource = new FilesDataSource(this.productsService, this.paginator, this.sort);
        // Observable.fromEvent(this.filter.nativeElement, 'keyup')
        //           .debounceTime(150)
        //           .distinctUntilChanged()
        //           .subscribe(() => {
        //               if ( !this.dataSource )
        //               {
        //                   return;
        //               }
        //               this.dataSource.filter = this.filter.nativeElement.value;
        //           });
    };
    MonitorRuteComponent.prototype.toggleHapus = function () {
        this.hapus == true ? this.hapus = false : this.hapus = true;
    };
    MonitorRuteComponent.prototype.hapusDialog = function (id, asal, tujuan, navigate) {
        var _this = this;
        var pesan = "Hapus Rute ";
        var deleteDialog = this.dialog.open(__WEBPACK_IMPORTED_MODULE_16__core_components_confirm_dialog_confirm_dialog_component__["a" /* FuseConfirmDialogComponent */], {
            width: '250',
            data: { id: id, asal: asal, tujuan: tujuan, url: "/admin/rute/", navigate: navigate }
        });
        deleteDialog.afterClosed().subscribe(function () {
            _this.dataSource = new FilesDataSource(_this.productsService, _this.paginator, _this.sort);
            __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].fromEvent(_this.filter.nativeElement, 'keyup')
                .debounceTime(150)
                .distinctUntilChanged()
                .subscribe(function () {
                if (!_this.dataSource) {
                    return;
                }
                _this.dataSource.filter = _this.filter.nativeElement.value;
            });
        });
    };
    MonitorRuteComponent.prototype.bulan = function (date) {
        if (date.substring(5, 7) === '01') {
            return "Januari";
        }
        else if (date.substring(5, 7) === '02') {
            return "Februari";
        }
        else if (date.substring(5, 7) === '03') {
            return "Maret";
        }
        else if (date.substring(5, 7) === '04') {
            return "April";
        }
        else if (date.substring(5, 7) === '05') {
            return "Mei";
        }
        else if (date.substring(5, 7) === '06') {
            return "Juni";
        }
        else if (date.substring(5, 7) === '07') {
            return "Juli";
        }
        else if (date.substring(5, 7) === '08') {
            return "Agustus";
        }
        else if (date.substring(5, 7) === '09') {
            return "September";
        }
        else if (date.substring(5, 7) === '10') {
            return "Oktober";
        }
        else if (date.substring(5, 7) === '11') {
            return "November";
        }
        else if (date.substring(5, 7) === '12') {
            return "Desember";
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_6__angular_material__["z" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__angular_material__["z" /* MatPaginator */])
    ], MonitorRuteComponent.prototype, "paginator", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('filter'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], MonitorRuteComponent.prototype, "filter", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_6__angular_material__["M" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6__angular_material__["M" /* MatSort */])
    ], MonitorRuteComponent.prototype, "sort", void 0);
    MonitorRuteComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-products',
            template: __webpack_require__("../../../../../src/app/main/content/apps/monitor_rute/monitorRute/monitorRute.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/monitor_rute/monitorRute/monitorRute.component.scss")],
            animations: __WEBPACK_IMPORTED_MODULE_5__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__core_services_config_service__["a" /* FuseConfigService */],
            __WEBPACK_IMPORTED_MODULE_1__monitorRute_service__["a" /* MonitorRuteService */],
            __WEBPACK_IMPORTED_MODULE_6__angular_material__["m" /* MatDialog */],
            __WEBPACK_IMPORTED_MODULE_8__angular_router__["f" /* Router */]])
    ], MonitorRuteComponent);
    return MonitorRuteComponent;
}());

var FilesDataSource = /** @class */ (function (_super) {
    __extends(FilesDataSource, _super);
    function FilesDataSource(productsService, _paginator, _sort) {
        var _this = _super.call(this) || this;
        _this.productsService = productsService;
        _this._paginator = _paginator;
        _this._sort = _sort;
        _this._filterChange = new __WEBPACK_IMPORTED_MODULE_7_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        _this._filteredDataChange = new __WEBPACK_IMPORTED_MODULE_7_rxjs_BehaviorSubject__["BehaviorSubject"]('');
        _this.filteredData = _this.productsService.products;
        return _this;
    }
    Object.defineProperty(FilesDataSource.prototype, "filteredData", {
        get: function () {
            return this._filteredDataChange.value;
        },
        set: function (value) {
            this._filteredDataChange.next(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FilesDataSource.prototype, "filter", {
        get: function () {
            return this._filterChange.value;
        },
        set: function (filter) {
            this._filterChange.next(filter);
        },
        enumerable: true,
        configurable: true
    });
    /** Connect function called by the table to retrieve one stream containing the data to render. */
    FilesDataSource.prototype.connect = function () {
        var _this = this;
        var displayDataChanges = [
            this.productsService.onProductsChanged,
            this._paginator.page,
            this._filterChange,
            this._sort.sortChange
        ];
        return __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].merge.apply(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"], displayDataChanges).map(function () {
            var data = _this.productsService.products.slice();
            data = _this.filterData(data);
            _this.filteredData = data.slice();
            data = _this.sortData(data);
            // Grab the page's slice of data.
            var startIndex = _this._paginator.pageIndex * _this._paginator.pageSize;
            return data.splice(startIndex, _this._paginator.pageSize);
        });
    };
    FilesDataSource.prototype.filterData = function (data) {
        if (!this.filter) {
            return data;
        }
        return __WEBPACK_IMPORTED_MODULE_15__core_fuseUtils__["a" /* FuseUtils */].filterArrayByString(data, this.filter);
    };
    FilesDataSource.prototype.sortData = function (data) {
        var _this = this;
        if (!this._sort.active || this._sort.direction === '') {
            return data;
        }
        return data.sort(function (a, b) {
            var propertyA = '';
            var propertyB = '';
            switch (_this._sort.active) {
                case 'id':
                    _a = [a.id, b.id], propertyA = _a[0], propertyB = _a[1];
                    break;
                case 'asal':
                    _b = [a.asal, b.asal], propertyA = _b[0], propertyB = _b[1];
                    break;
                case 'tujuan':
                    _c = [a.tujuan, b.tujuan], propertyA = _c[0], propertyB = _c[1];
                    break;
                case 'tanggal':
                    _d = [a.tanggal, b.tanggal], propertyA = _d[0], propertyB = _d[1];
                    break;
                case 'jenis':
                    _e = [a.jenis, b.jenis], propertyA = _e[0], propertyB = _e[1];
                    break;
                case 'kuota_motor':
                    _f = [a.kouta_motor, b.kouta_motor], propertyA = _f[0], propertyB = _f[1];
                    break;
                case 'status':
                    _g = [a.status, b.status], propertyA = _g[0], propertyB = _g[1];
                    break;
            }
            var valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            var valueB = isNaN(+propertyB) ? propertyB : +propertyB;
            return (valueA < valueB ? -1 : 1) * (_this._sort.direction === 'asc' ? 1 : -1);
            var _a, _b, _c, _d, _e, _f, _g;
        });
    };
    FilesDataSource.prototype.disconnect = function () {
    };
    return FilesDataSource;
}(__WEBPACK_IMPORTED_MODULE_2__angular_cdk_collections__["a" /* DataSource */]));



/***/ }),

/***/ "../../../../../src/app/main/content/apps/monitor_rute/monitorRute/monitorRute.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MonitorRuteService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__session_service__ = __webpack_require__("../../../../../src/app/main/content/apps/session.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MonitorRuteService = /** @class */ (function () {
    function MonitorRuteService(http, _sessionService) {
        this.http = http;
        this._sessionService = _sessionService;
        this.onProductsChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({});
        this.token = this._sessionService.get().token;
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    MonitorRuteService.prototype.resolve = function (route, state) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getProducts()
            ]).then(function () {
                resolve();
            }, reject);
        });
    };
    MonitorRuteService.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        options.headers = headers;
    };
    MonitorRuteService.prototype.getProducts = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/monitor', options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                _this.products = response['data'];
                for (var i = 0; i < _this.products.length; ++i) {
                    if (_this.products[i]['status'] == 1) {
                        if (_this.products[i]['jenis'] == 1) {
                            _this.products[i]['jenis'] = 'Mudik';
                        }
                        else {
                            _this.products[i]['jenis'] = 'Balik';
                        }
                    }
                    else {
                        _this.products.splice(i, 1);
                        i = i - 1;
                    }
                }
                _this.onProductsChanged.next(_this.products);
                resolve(response);
            }, reject);
        });
    };
    MonitorRuteService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__session_service__["a" /* SessionService */]])
    ], MonitorRuteService);
    return MonitorRuteService;
}());



/***/ })

});
//# sourceMappingURL=monitor.module.chunk.js.map