import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { DetailTitipMotorService } from './detailTitipMotor.service';
import { fuseAnimations } from '../../../../../core/animations';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { Subscription } from 'rxjs/Subscription';
import { Product } from './detailTitipMotor.model';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { MatSnackBar, MatDialog } from '@angular/material';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { FuseConfirmDialogComponent } from './../../../../../core/components/confirm-dialog/confirm-dialog.component';
import swal from 'sweetalert2';
import { SessionService } from './../../session.service';
import { saveAs } from 'file-saver/FileSaver';

@Component({
    selector     : 'fuse-e-commerce-product',
    templateUrl  : './detailTitipMotor.component.html',
    styleUrls    : ['./detailTitipMotor.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class DetailTitipMotorComponent implements OnInit, OnDestroy
{
    product = new Product();
    onProductChanged: Subscription;
    pageType: string;
    productForm: FormGroup;
    ruteAll: any;
    token = this._sessionService.get().token;
    role = this._sessionService.get().role;

    constructor(
        private productService: DetailTitipMotorService,
        private formBuilder: FormBuilder,
        public snackBar: MatSnackBar,
        private location: Location,
        private http: Http,
        private route: ActivatedRoute,
        private dialog: MatDialog,
        private router: Router,
        private _sessionService: SessionService
    )
    {

    }

    ngOnInit()
    {

        this.ruteAll = this.productService.ruteAll;
        // Subscribe to update product on changes
        this.onProductChanged =
            this.productService.onProductChanged
                .subscribe(product => {

                    if ( product )
                    {
                        this.product = new Product(product);
                        this.pageType = 'edit';
                        console.log(this.product);
                    }
                    else
                    {
                        this.pageType = 'new';
                        this.product = new Product();
                    }

                    this.productForm = this.createProductForm();
                });
    }

    createProductForm()
    {
        return this.formBuilder.group({
            rute_id                : new FormControl(this.product.rute_id, [Validators.required]),
            nama                   : new FormControl(this.product.nama, [Validators.required]),
            alamat                 : new FormControl(this.product.alamat, [Validators.required]),
            no_hp                  : new FormControl(this.product.no_hp, [Validators.required, Validators.pattern(/^[0-9]{9,}$/)]),
            no_kendaraan           : new FormControl(this.product.no_kendaraan, [Validators.required, Validators.pattern(/^[A-Z]{1,2}\-[0-9]{1,4}\-[A-Z]{1,3}$/)]),
            no_ktp                 : new FormControl(this.product.no_ktp),
            no_sim                 : new FormControl(this.product.no_sim)
        });
    }

    saveProduct()
    {
        const data = this.productForm.getRawValue();
        this.productService.saveProduct(data)
            .then((response: any) => {
                if (response.status) {
                    this.productService.onProductChanged.next(data);
                    this.back();
                    swal({
                        title: 'Update TitipMotor Berhasil!',
                        text: 'TitipMotor ' + data.nama + ' Telah Update',
                        type: 'success',
                        timer: 2000
                    });
                } else {
                    swal({
                        title: 'Edit TitipMotor Gagal!',
                        text: response.message,
                        type: 'error'
                    });
                }
            })
            .catch((error) => {
                const eror = JSON.parse(error._body);
                swal({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
            });
    }

    addProduct()
    {
        const data = this.productForm.getRawValue();
        this.productService.addProduct(data)
            .then((response: any) => {

                if (response.status) {
                    this.productService.onProductChanged.next(data);
                    this.back();
                    swal({
                        title: 'Tambah TitipMotor Berhasil!',
                        text: 'TitipMotor ' + data.nama + ' Telah Ditambahkan',
                        type: 'success',
                        timer: 2000
                    });
                } else {
                    swal({
                        title: 'Tambah TitipMotor Gagal!',
                        text: response.message,
                        type: 'error'
                    });
                }
            })
            .catch((error) => {
                const eror = JSON.parse(error._body);
                swal({
                    title: 'Error !',
                    text: eror.message,
                    type: 'error'
                });
            });
    }

    ngOnDestroy()
    {
        this.onProductChanged.unsubscribe();
    }

    hapus(): void{
        swal({
            title: 'Hapus TitipMotor' + this.product.nama,
            text: 'TitipMotor yang telah dihapus tidak bisa dikembalikan lagi',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus !',
            cancelButtonText: 'Batal!',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                this.productService.hapus(this.productForm.getRawValue())
                    .then((response: any) => {
                        if (response.status) {
                            this.back();
                            swal({
                                title: 'Hapus TitipMotor Berhasil!',
                                text: 'TitipMotor' + this.product.nama + ' Telah Dihapus',
                                type: 'success',
                                timer: 2000
                            });
                        } else {
                            swal(
                                'Hapus TitipMotor Gagal!',
                                response.message,
                                'error'
                            );
                        }
                    })
                    .catch((error) => {
                        const eror = JSON.parse(error._body);
                        swal({
                            title: 'Error !',
                            text: eror.message,
                            type: 'error'
                        });
                    });
            }
        });
    }

    print(tiket) {
        const fileURL = environment.setting.base_url + '/titipMotor/' + tiket + '/download';
        window.open(fileURL, '_blank');
    }

    back(){
        this.router.navigate(['/apps/titipMotor/daftarTitipMotor']);
    }
}
