webpackJsonp(["pemudik.module"],{

/***/ "../../../../../src/app/main/content/apps/pemudik/daftarPemudik/daftarPemudik.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"products\" class=\"page-layout carded fullwidth\" fusePerfectScrollbar>\r\n\r\n    <!-- TOP BACKGROUND -->\r\n    <div class=\"top-bg mat-accent-bg\"></div>\r\n    <!-- / TOP BACKGROUND -->\r\n\r\n    <!-- CENTER -->\r\n    <div class=\"center\">\r\n\r\n        <!-- HEADER -->\r\n        <div class=\"header white-fg\"\r\n             fxLayout=\"column\" fxLayoutAlign=\"center center\"\r\n             fxLayout.gt-xs=\"row\" fxLayoutAlign.gt-xs=\"space-between center\">\r\n\r\n            <!-- APP TITLE -->\r\n            <div class=\"logo my-12 m-sm-0\"\r\n                 fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <mat-icon class=\"logo-icon mr-16\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'50ms',scale:'0.2'}}\">people</mat-icon>\r\n                <span class=\"logo-text h1\" *fuseIfOnDom [@animate]=\"{value:'*',params:{delay:'100ms',x:'-25px'}}\">Daftar Pemudik</span>\r\n            </div>\r\n            <!-- / APP TITLE -->\r\n\r\n            <!-- SEARCH -->\r\n            <div class=\"search-input-wrapper mx-12 m-md-0\"\r\n                 fxFlex=\"1 0 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                <label for=\"search\" class=\"mr-8\">\r\n                    <mat-icon class=\"secondary-text\">search</mat-icon>\r\n                </label>\r\n                <mat-form-field floatPlaceholder=\"never\" fxFlex=\"1 0 auto\">\r\n                    <input [(ngModel)]=\"search_query\" id=\"search\" matInput #filter placeholder=\"Search\" (keyup.enter)=\"search()\">\r\n                </mat-form-field>\r\n            </div>\r\n            <!-- / SEARCH -->\r\n\r\n            <div class=\"my-12\">\r\n                <button mat-raised-button\r\n                        (click)=\"clear()\"\r\n                        class=\"add-product-button m-sm-0\">\r\n                    <span>Clear</span>\r\n                </button>\r\n            </div>\r\n\r\n            <div class=\"my-12\">\r\n                <button mat-raised-button class=\"add-product-button m-sm-0\" [matMenuTriggerFor]=\"filter\">\r\n                    <span>Filter</span>\r\n                </button>\r\n\r\n                <mat-menu #filter=\"matMenu\">\r\n                    <button (click)=\"filterButton('0')\" mat-menu-item>Belum Disetujui</button>\r\n                    <button (click)=\"filterButton('1')\" mat-menu-item>Disetujui</button>\r\n                    <button (click)=\"filterButton('2')\" mat-menu-item>Pending</button>\r\n                    <button (click)=\"filterButton('3')\" mat-menu-item>Hadir</button>\r\n                    <button (click)=\"filterButton('4')\" mat-menu-item>Cancel</button>\r\n                    <button (click)=\"filterButton('5')\" mat-menu-item>Ditolak</button>\r\n                </mat-menu>\r\n                \r\n            </div>\r\n\r\n        </div>\r\n        <!-- / HEADER -->\r\n\r\n        <!-- CONTENT CARD -->\r\n        <div class=\"content-card mat-white-bg\">\r\n\r\n            <mat-table class=\"products-table\"\r\n                       #table [dataSource]=\"dataSource\"\r\n                       matSort matSortActive=\"created\" matSortDisableClear matSortDirection=\"asc\"\r\n                       [@animateStagger]=\"{value:'50'}\"\r\n                       fusePerfectScrollbar>\r\n\r\n                <ng-container matColumnDef=\"id\">\r\n                    <mat-header-cell *matHeaderCellDef>ID</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let product; let i = index\">\r\n                        <p class=\"text-truncate\">{{ (paginator.pageIndex * paginator.pageSize) + (i + 1) }}.</p>\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"kode_tiket\">\r\n                  <mat-header-cell *matHeaderCellDef>kode_tiket</mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let row\">{{ row.nomor_tiket }}</mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"nama\">\r\n                  <mat-header-cell *matHeaderCellDef>Nama</mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let row\">{{ row.nama }}</mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"nik\">\r\n                  <mat-header-cell *matHeaderCellDef>NIK</mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let row\">{{ row.nik }}</mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"jenis_kelamin\">\r\n                  <mat-header-cell *matHeaderCellDef fxHide.xs>Jenis Kelamin</mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let row\" fxHide.xs>{{ row.gender == 1 ? 'Laki-laki' : 'Perempuan' }}</mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"rute\">\r\n                  <mat-header-cell *matHeaderCellDef>Rute</mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let row\">{{ row.tiket.rute.kota.asal }} - {{ row.tiket.rute.kota.tujuan }} - {{ row.tiket.rute.keterangan }}</mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"kendaraan\">\r\n                  <mat-header-cell *matHeaderCellDef>Kendaraan</mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let row\">{{ row.tiket.kendaraan.nomor }}</mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container matColumnDef=\"kursi\">\r\n                  <mat-header-cell *matHeaderCellDef>Kursi</mat-header-cell>\r\n                  <mat-cell *matCellDef=\"let row\">{{ row.kursi }}</mat-cell>\r\n                </ng-container>\r\n\r\n                <ng-container cdkColumnDef=\"status\">\r\n                    <mat-header-cell *cdkHeaderCellDef>Status</mat-header-cell>\r\n                    <mat-cell *cdkCellDef=\"let row\">\r\n                        <mat-icon *ngIf=\"row.status == 5\" class=\"active-icon danger s-16\">cancel</mat-icon>\r\n                        <mat-icon *ngIf=\"row.status == 4\" class=\"active-icon mat-green-600-bg s-16\">check</mat-icon>\r\n                        <mat-icon *ngIf=\"row.status == 3\" class=\"active-icon mat-green-600-bg s-16\">check</mat-icon>\r\n                        <mat-icon *ngIf=\"row.status == 2\" class=\"active-icon danger s-16\">warning</mat-icon>\r\n                        <mat-icon *ngIf=\"row.status == 1\" class=\"active-icon mat-green-600-bg s-16\">check</mat-icon>\r\n                        <mat-icon *ngIf=\"row.status == 0\" class=\"active-icon warning s-16\">warning</mat-icon>\r\n                        <p *ngIf=\"row.status == 5\" class=\"text-truncate\">Ditolak</p>\r\n                        <p *ngIf=\"row.status == 4\" class=\"text-truncate\">Cancel</p>\r\n                        <p *ngIf=\"row.status == 3\" class=\"text-truncate\">Hadir</p>\r\n                        <p *ngIf=\"row.status == 2\" class=\"text-truncate\">Pending</p>\r\n                        <p *ngIf=\"row.status == 1\" class=\"text-truncate\">Disetujui</p>\r\n                        <p *ngIf=\"row.status == 0\" class=\"text-truncate\">Belum Disetujui</p>\r\n                        <!-- {{product.status}} -->\r\n                    </mat-cell>\r\n                </ng-container>\r\n\r\n\r\n                <mat-header-row *matHeaderRowDef=\"displayedColumns\"></mat-header-row>\r\n\r\n                <mat-row *cdkRowDef=\"let product; let i = index; columns: displayedColumns;\"\r\n                         class=\"product\"\r\n                         matRipple\r\n                         (click)=\"clickTiket(product.nomor_tiket, product.status)\">\r\n                </mat-row>\r\n\r\n            </mat-table>\r\n\r\n            <mat-paginator [length]=\"resultsLength\" [pageSize]=\"25\">\r\n            </mat-paginator>\r\n\r\n        </div>\r\n        <!-- / CONTENT CARD -->\r\n    </div>\r\n    <!-- / CENTER -->\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/main/content/apps/pemudik/daftarPemudik/daftarPemudik.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/**\n * Applies styles for users in high contrast mode. Note that this only applies\n * to Microsoft browsers. Chrome can be included by checking for the `html[hc]`\n * attribute, however Chrome handles high contrast differently.\n */\n/* Theme for the ripple elements.*/\n/* stylelint-disable material/no-prefixes */\n/* stylelint-enable */\n:host .header .search-input-wrapper {\n  max-width: 480px; }\n:host .header .add-product-button {\n  background-color: #F96D01; }\n@media (max-width: 599px) {\n  :host .header {\n    height: 176px !important;\n    min-height: 176px !important;\n    max-height: 176px !important; } }\n@media (max-width: 599px) {\n  :host .top-bg {\n    height: 240px; } }\n:host .products-table {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto;\n  border-bottom: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-header-row {\n    min-width: 1000px;\n    min-height: 35px;\n    background-color: rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-row {\n    min-width: 1000px; }\n:host .products-table .product {\n    position: relative;\n    cursor: pointer;\n    height: 60px; }\n:host .products-table .mat-cell {\n    min-width: 0;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n:host .products-table .mat-column-id {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 38px;\n            flex: 0 1 38px; }\n:host .products-table .mat-column-kode_tiket {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 8%;\n            flex: 0 1 8%; }\n:host .products-table .mat-column-rute {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 20%;\n            flex: 0 1 20%; }\n:host .products-table .mat-column-nama, :host .products-table .mat-column-nik {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 18%;\n            flex: 0 1 18%; }\n:host .products-table .warning {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 5%;\n            flex: 0 1 5%;\n    color: #f95d01 !important; }\n:host .products-table .mat-column-image {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 84px;\n            flex: 0 1 84px; }\n:host .products-table .mat-column-image .product-image {\n      width: 52px;\n      height: 52px;\n      border: 1px solid rgba(0, 0, 0, 0.12); }\n:host .products-table .mat-column-buttons {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 80px;\n            flex: 0 1 80px; }\n:host .products-table .quantity-indicator {\n    display: inline-block;\n    vertical-align: middle;\n    width: 8px;\n    height: 8px;\n    border-radius: 4px;\n    margin-right: 8px; }\n:host .products-table .quantity-indicator + span {\n      display: inline-block;\n      vertical-align: middle; }\n:host .products-table .active-icon {\n    border-radius: 50%;\n    margin-right: 5px; }\n:host .products-table .danger {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 5%;\n            flex: 0 1 5%;\n    color: #f90101 !important; }\n:host .products-table .warning {\n    -webkit-box-flex: 0;\n        -ms-flex: 0 1 5%;\n            flex: 0 1 5%;\n    color: #f95d01 !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main/content/apps/pemudik/daftarPemudik/daftarPemudik.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarPemudikComponent; });
/* unused harmony export ExampleHttpDao */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_observable_merge__ = __webpack_require__("../../../../rxjs/_esm5/observable/merge.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_of__ = __webpack_require__("../../../../rxjs/_esm5/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_operators_catchError__ = __webpack_require__("../../../../rxjs/_esm5/operators/catchError.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_operators_map__ = __webpack_require__("../../../../rxjs/_esm5/operators/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_operators_startWith__ = __webpack_require__("../../../../rxjs/_esm5/operators/startWith.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_operators_switchMap__ = __webpack_require__("../../../../rxjs/_esm5/operators/switchMap.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__core_animations__ = __webpack_require__("../../../../../src/app/core/animations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var DaftarPemudikComponent = /** @class */ (function () {
    function DaftarPemudikComponent(http, router) {
        this.http = http;
        this.router = router;
        this.displayedColumns = ['id', 'kode_tiket', 'nama', 'nik', 'jenis_kelamin', 'rute', 'kendaraan', 'kursi', 'status'];
        this.dataSource = new __WEBPACK_IMPORTED_MODULE_2__angular_material__["Q" /* MatTableDataSource */]();
        this.resultsLength = 0;
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
    }
    DaftarPemudikComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        if (localStorage.getItem('pemudik-page')) {
            this.paginator.pageIndex = Number(localStorage.getItem('pemudik-page'));
        }
        this.exampleDatabase = new ExampleHttpDao(this.http);
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
        this.sub = Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_merge__["a" /* merge */])(this.sort.sortChange, this.paginator.page)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators_startWith__["a" /* startWith */])({}), Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators_switchMap__["a" /* switchMap */])(function () {
            _this.isLoadingResults = true;
            return _this.exampleDatabase.getPemudik(_this.sort.active, _this.sort.direction, _this.paginator.pageIndex);
        }), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators_map__["a" /* map */])(function (data) {
            _this.isLoadingResults = false;
            _this.isRateLimitReached = false;
            _this.resultsLength = data.data['total'];
            return data.data;
        }), Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators_catchError__["a" /* catchError */])(function () {
            _this.isLoadingResults = false;
            _this.isRateLimitReached = true;
            return Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_of__["a" /* of */])([]);
        })).subscribe(function (data) {
            _this.dataSource.data = data['data'];
        });
    };
    DaftarPemudikComponent.prototype.clickTiket = function (nomor_tiket, status) {
        if (status != 2 && status != 4) {
            localStorage.setItem('pemudik-page', String(this.paginator.pageIndex));
            localStorage.removeItem('tiket-page');
            localStorage.removeItem('motor-item');
            var url = '/apps/tiket/detailTiket/' + nomor_tiket;
            this.router.navigate([url]);
        }
    };
    DaftarPemudikComponent.prototype.clear = function () {
        var _this = this;
        this.paginator.pageIndex = 0;
        this.sub.unsubscribe();
        this.sub = Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_merge__["a" /* merge */])(this.sort.sortChange, this.paginator.page)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators_startWith__["a" /* startWith */])({}), Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators_switchMap__["a" /* switchMap */])(function () {
            _this.isLoadingResults = true;
            return _this.exampleDatabase.getPemudik(_this.sort.active, _this.sort.direction, _this.paginator.pageIndex);
        }), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators_map__["a" /* map */])(function (data) {
            _this.isLoadingResults = false;
            _this.isRateLimitReached = false;
            _this.resultsLength = data.data['total'];
            return data.data;
        }), Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators_catchError__["a" /* catchError */])(function () {
            _this.isLoadingResults = false;
            _this.isRateLimitReached = true;
            return Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_of__["a" /* of */])([]);
        })).subscribe(function (data) {
            _this.dataSource.data = data['data'];
        });
    };
    DaftarPemudikComponent.prototype.search = function () {
        var _this = this;
        var search = "";
        if (this.search_query) {
            search = this.search_query;
        }
        else {
            search = "";
        }
        this.sub.unsubscribe();
        this.paginator.pageIndex = 0;
        this.sub = Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_merge__["a" /* merge */])(this.sort.sortChange, this.paginator.page)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators_startWith__["a" /* startWith */])({}), Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators_switchMap__["a" /* switchMap */])(function () {
            _this.isLoadingResults = true;
            return _this.exampleDatabase.getPemudikSearch(_this.sort.active, _this.sort.direction, search, _this.paginator.pageIndex);
        }), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators_map__["a" /* map */])(function (data) {
            _this.isLoadingResults = false;
            _this.isRateLimitReached = false;
            _this.resultsLength = data.data['total'];
            return data.data;
        }), Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators_catchError__["a" /* catchError */])(function () {
            _this.isLoadingResults = false;
            _this.isRateLimitReached = true;
            return Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_of__["a" /* of */])([]);
        })).subscribe(function (data) {
            _this.dataSource.data = data['data'];
        });
    };
    DaftarPemudikComponent.prototype.filterButton = function (filter) {
        var _this = this;
        this.sub.unsubscribe();
        this.paginator.pageIndex = 0;
        this.sub = Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_observable_merge__["a" /* merge */])(this.sort.sortChange, this.paginator.page)
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_7_rxjs_operators_startWith__["a" /* startWith */])({}), Object(__WEBPACK_IMPORTED_MODULE_8_rxjs_operators_switchMap__["a" /* switchMap */])(function () {
            _this.isLoadingResults = true;
            return _this.exampleDatabase.getPemudikSearch(_this.sort.active, _this.sort.direction, filter, _this.paginator.pageIndex);
        }), Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators_map__["a" /* map */])(function (data) {
            _this.isLoadingResults = false;
            _this.isRateLimitReached = false;
            _this.resultsLength = data.data['total'];
            return data.data;
        }), Object(__WEBPACK_IMPORTED_MODULE_5_rxjs_operators_catchError__["a" /* catchError */])(function () {
            _this.isLoadingResults = false;
            _this.isRateLimitReached = true;
            return Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_of__["a" /* of */])([]);
        })).subscribe(function (data) {
            _this.dataSource.data = data['data'];
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["z" /* MatPaginator */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__angular_material__["z" /* MatPaginator */])
    ], DaftarPemudikComponent.prototype, "paginator", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2__angular_material__["M" /* MatSort */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__angular_material__["M" /* MatSort */])
    ], DaftarPemudikComponent.prototype, "sort", void 0);
    DaftarPemudikComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'fuse-e-commerce-products',
            template: __webpack_require__("../../../../../src/app/main/content/apps/pemudik/daftarPemudik/daftarPemudik.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main/content/apps/pemudik/daftarPemudik/daftarPemudik.component.scss")],
            animations: __WEBPACK_IMPORTED_MODULE_9__core_animations__["a" /* fuseAnimations */]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_11__angular_router__["f" /* Router */]])
    ], DaftarPemudikComponent);
    return DaftarPemudikComponent;
}());

/** An example database that the data source uses to retrieve data for the table. */
var ExampleHttpDao = /** @class */ (function () {
    function ExampleHttpDao(http) {
        this.http = http;
    }
    ExampleHttpDao.prototype.getPemudik = function (sort, order, page) {
        var href = __WEBPACK_IMPORTED_MODULE_10__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/biasa';
        var requestUrl = href + "?page=" + (page + 1);
        return this.http.get(requestUrl, httpOptions);
    };
    ExampleHttpDao.prototype.getPemudikSearch = function (sort, order, search, page) {
        var href = __WEBPACK_IMPORTED_MODULE_10__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik/biasa/search/' + search + ("?page=" + (page + 1));
        return this.http.get(href, httpOptions);
    };
    return ExampleHttpDao;
}());

var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpHeaders */]({
        'Content-Type': 'application/json',
        'Authorization': "Bearer " + JSON.parse(localStorage.getItem('currentUser')).token
    })
};


/***/ }),

/***/ "../../../../../src/app/main/content/apps/pemudik/daftarPemudik/daftarPemudik.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DaftarPemudikService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__ = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DaftarPemudikService = /** @class */ (function () {
    function DaftarPemudikService(http) {
        this.http = http;
        this.token = sessionStorage.getItem('token');
        this.onProductsChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs_BehaviorSubject__["BehaviorSubject"]({});
    }
    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    DaftarPemudikService.prototype.resolve = function (route, state) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            Promise.all([
                _this.getProducts()
            ]).then(function () {
                resolve();
            }, reject);
        });
    };
    DaftarPemudikService.prototype.setHeader = function (options) {
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Headers */]();
        headers.append('Authorization', "Bearer " + this.token);
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        options.headers = headers;
    };
    DaftarPemudikService.prototype.getProducts = function () {
        var _this = this;
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["g" /* RequestOptions */]();
        this.setHeader(options);
        return new Promise(function (resolve, reject) {
            _this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].setting.base_url + '/admin/pemudik', options)
                .map(function (res) { return res.json(); })
                .subscribe(function (response) {
                _this.products = response['data']['data'];
                for (var i = 0; i < _this.products.length; ++i) {
                    if (_this.products[i]['jenis'] == 1) {
                        _this.products[i]['jenis'] = 'Mudik';
                    }
                    else {
                        _this.products[i]['jenis'] = 'Balik';
                    }
                }
                _this.onProductsChanged.next(_this.products);
                resolve(response);
            }, function (err) {
                reject(err);
            });
        });
    };
    DaftarPemudikService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_http__["c" /* Http */]])
    ], DaftarPemudikService);
    return DaftarPemudikService;
}());



/***/ }),

/***/ "../../../../../src/app/main/content/apps/pemudik/pemudik.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PemudikModule", function() { return PemudikModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__ = __webpack_require__("../../../../@swimlane/ngx-charts/release/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__ = __webpack_require__("../../../../../src/app/core/modules/shared.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__ = __webpack_require__("../../../../../src/app/core/components/widget/widget.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__daftarPemudik_daftarPemudik_component__ = __webpack_require__("../../../../../src/app/main/content/apps/pemudik/daftarPemudik/daftarPemudik.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__daftarPemudik_daftarPemudik_service__ = __webpack_require__("../../../../../src/app/main/content/apps/pemudik/daftarPemudik/daftarPemudik.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__core_components_confirm_dialog_confirm_dialog_component__ = __webpack_require__("../../../../../src/app/core/components/confirm-dialog/confirm-dialog.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__auth_guard__ = __webpack_require__("../../../../../src/app/main/content/apps/auth.guard.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var routes = [
    {
        path: 'daftarPemudik',
        component: __WEBPACK_IMPORTED_MODULE_5__daftarPemudik_daftarPemudik_component__["a" /* DaftarPemudikComponent */],
        canActivate: [__WEBPACK_IMPORTED_MODULE_9__auth_guard__["a" /* AuthGuard */]]
    }
];
var PemudikModule = /** @class */ (function () {
    function PemudikModule() {
    }
    PemudikModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__core_modules_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["g" /* RouterModule */].forChild(routes),
                __WEBPACK_IMPORTED_MODULE_4__core_components_widget_widget_module__["a" /* FuseWidgetModule */],
                __WEBPACK_IMPORTED_MODULE_2__swimlane_ngx_charts__["NgxChartsModule"],
                __WEBPACK_IMPORTED_MODULE_8__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
                })
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__daftarPemudik_daftarPemudik_component__["a" /* DaftarPemudikComponent */],
                __WEBPACK_IMPORTED_MODULE_7__core_components_confirm_dialog_confirm_dialog_component__["a" /* FuseConfirmDialogComponent */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__daftarPemudik_daftarPemudik_component__["a" /* DaftarPemudikComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__daftarPemudik_daftarPemudik_service__["a" /* DaftarPemudikService */]
            ]
        })
    ], PemudikModule);
    return PemudikModule;
}());



/***/ })

});
//# sourceMappingURL=pemudik.module.chunk.js.map