import {Component, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {merge} from 'rxjs/observable/merge';
import {of as observableOf} from 'rxjs/observable/of';
import {catchError} from 'rxjs/operators/catchError';
import {map} from 'rxjs/operators/map';
import {startWith} from 'rxjs/operators/startWith';
import {switchMap} from 'rxjs/operators/switchMap';
import { fuseAnimations } from '../../../../../core/animations';
import { environment } from './../../../../../../environments/environment';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { SessionService } from './../../session.service';

@Component({
    selector   : 'fuse-e-commerce-products',
    templateUrl: './daftarTiket.component.html',
    styleUrls  : ['./daftarTiket.component.scss'],
    animations : fuseAnimations
})
export class DaftarTiketComponent implements AfterViewInit
{
    displayedColumns = ['id', 'kode_tiket', 'rute', 'tanggal', 'kendaraan', 'telp', 'dewasa', 'anak', 'status'];
    exampleDatabase: ExampleHttpDao | null;
    dataSource = new MatTableDataSource();
    sub: Subscription;

    resultsLength = 0;
    isLoadingResults = false;
    isRateLimitReached = false;

    search_query: any;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild('filter') filter: ElementRef;

    constructor(private http: HttpClient, private router: Router, private _sessionService: SessionService, private _cdr: ChangeDetectorRef) {}

    ngAfterViewInit() {
        if (localStorage.getItem('tiket-page')) {
            // console.log('tiket-page: ', localStorage.getItem('tiket-page'));
            this.paginator.pageIndex = Number(localStorage.getItem('tiket-page'));
        }
        this.exampleDatabase = new ExampleHttpDao(this.http, this._sessionService);

        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        this.sub = merge(this.sort.sortChange, this.paginator.page)
            .pipe(
                startWith({}),
                switchMap(() => {
                    // this.isLoadingResults = true;
                    return this.exampleDatabase!.getPemudik(
                        this.sort.active, this.sort.direction, this.paginator.pageIndex);
                }),
                map(data => {
                    // this.isLoadingResults = false;
                    // this.isRateLimitReached = false;
                    this.resultsLength = data.data['total'];

                    return data.data;
                }),
                catchError(() => {
                    // this.isLoadingResults = false;
                    // this.isRateLimitReached = true;
                    return observableOf([]);
                })
            ).subscribe(data => {
                this.dataSource.data = data['data'];
            });
      }

      clickTiket(nomor_tiket, status): void{
          if (status != 2 && status != 4) {
              localStorage.setItem('tiket-page', String(this.paginator.pageIndex));
              localStorage.removeItem('pemudik-page');
              localStorage.removeItem('motor-page');
              const url = '/apps/tiket/detailTiket/' + nomor_tiket;
              this.router.navigate([url]);
          }
      }

      clear(){
          this.sub.unsubscribe();
          this.paginator.pageIndex = 0;
          this.sub = merge(this.sort.sortChange, this.paginator.page)
              .pipe(
                  startWith({}),
                  switchMap(() => {
                      this.isLoadingResults = true;
                      return this.exampleDatabase!.getPemudik(
                          this.sort.active, this.sort.direction, this.paginator.pageIndex);
                  }),
                  map(data => {
                      this.isLoadingResults = false;
                      this.isRateLimitReached = false;
                      this.resultsLength = data.data['total'];
                      return data.data;
                  }),
                  catchError(() => {
                      this.isLoadingResults = false;
                      this.isRateLimitReached = true;
                      return observableOf([]);
                  })
              ).subscribe(data => {
                  this.dataSource.data = data['data'];
              });
      }

      search(){
          this.sub.unsubscribe();
          this.paginator.pageIndex = 0;
          const search = this.search_query;
          this.sub = merge(this.sort.sortChange, this.paginator.page)
              .pipe(
                  startWith({}),
                  switchMap(() => {
                      this.isLoadingResults = true;
                      return this.exampleDatabase!.getPemudikSearch(
                          this.sort.active, this.sort.direction, search, this.paginator.pageIndex);
                  }),
                  map(data => {
                      this.isLoadingResults = false;
                      this.isRateLimitReached = false;
                      this.resultsLength = data.data['total'];
                      return data.data;
                  }),
                  catchError(() => {
                      this.isLoadingResults = false;
                      this.isRateLimitReached = true;
                      return observableOf([]);
                  })
              ).subscribe(data => {
                  this.dataSource.data = data['data'];
              });
      }

      filterButton(filter: any){
          this.sub.unsubscribe();
          this.paginator.pageIndex = 0;
          this.sub = merge(this.sort.sortChange, this.paginator.page)
              .pipe(
                  startWith({}),
                  switchMap(() => {
                      this.isLoadingResults = true;
                      return this.exampleDatabase!.getPemudikFilter(
                          this.sort.active, this.sort.direction, filter, this.paginator.pageIndex);
                  }),
                  map(data => {
                      this.isLoadingResults = false;
                      this.isRateLimitReached = false;
                      this.resultsLength = data.data['total'];
                      return data.data;
                  }),
                  catchError(() => {
                      this.isLoadingResults = false;
                      this.isRateLimitReached = true;
                      return observableOf([]);
                  })
              ).subscribe(data => {
                  this.dataSource.data = data['data'];
              });
      }

      bulan(date){
          if (date.substring(5, 7) === '01') {
              return 'Januari';
          } else if (date.substring(5, 7) === '02') {
              return 'Februari';
          } else if (date.substring(5, 7) === '03') {
              return 'Maret';
          } else if (date.substring(5, 7) === '04') {
              return 'April';
          } else if (date.substring(5, 7) === '05') {
              return 'Mei';
          } else if (date.substring(5, 7) === '06') {
              return 'Juni';
          } else if (date.substring(5, 7) === '07') {
              return 'Juli';
          } else if (date.substring(5, 7) === '08') {
              return 'Agustus';
          } else if (date.substring(5, 7) === '09') {
              return 'September';
          } else if (date.substring(5, 7) === '10') {
              return 'Oktober';
          } else if (date.substring(5, 7) === '11') {
              return 'November';
          } else if (date.substring(5, 7) === '12') {
              return 'Desember';
          }
      }
}

// tslint:disable-next-line: class-name
export interface pemudikApi {
    data: pemudikApiData[];
    total: number;
}

// tslint:disable-next-line: class-name
export interface pemudikApiData {
    data: any;
    total: number;
}

/** An example database that the data source uses to retrieve data for the table. */
export class ExampleHttpDao {

      constructor(private http: HttpClient, private _sessionService: SessionService) {}

      getPemudik(sort: string, order: string, page: number): Observable<pemudikApi> {
          const href = environment.setting.base_url + '/admin/tiket';
          const requestUrl = `${href}?page=${page + 1}`;

          return this.http.get<pemudikApi>(requestUrl, httpOptions);
      }

      getPemudikFilter(sort: string, order: string, search: string, page: number): Observable<pemudikApi> {
          const href = environment.setting.base_url + '/admin/tiket/' + search + `?page=${page + 1}`;

          return this.http.get<pemudikApi>(href, httpOptions);
      }

      getPemudikSearch(sort: string, order: string, search: string, page: number): Observable<pemudikApi> {
          const href = environment.setting.base_url + '/admin/tiket/search/' + search + `?page=${page + 1}`;

          return this.http.get<pemudikApi>(href, httpOptions);
      }

  }

  const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('currentUser')).token
      })
};
