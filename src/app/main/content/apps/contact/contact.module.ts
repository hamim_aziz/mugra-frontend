import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { DaftarContactComponent } from './daftarContact/daftarContact.component';
import { DaftarContactService } from './daftarContact/daftarContact.service';
import { DetailContactComponent } from './detailContact/detailContact.component';
import { DetailContactService } from './detailContact/detailContact.service';
import { FuseConfirmDialogComponent } from './../../../../core/components/confirm-dialog/confirm-dialog.component';
import { AgmCoreModule } from '@agm/core';
import { CdkTableModule } from '@angular/cdk/table';
import { DragDropSortableService, DndModule, DragDropService, DragDropConfig } from 'ng2-dnd';
import { AuthGuard } from './../auth.guard';
import { MatTableModule } from '@angular/material/table';

const routes: Routes = [
    {
        path     : 'daftarContact',
        component: DaftarContactComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DaftarContactService
        }
    },
    {
        path     : 'detailContact/:id',
        component: DetailContactComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DetailContactService
        }
    }
];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        CdkTableModule,
        DndModule,
        MatTableModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        })
    ],
    exports : [
        CdkTableModule
    ],
    entryComponents: [
        DaftarContactComponent,
        FuseConfirmDialogComponent
    ],
    declarations: [
        DaftarContactComponent,
        DetailContactComponent
    ],
    providers   : [
        DaftarContactService,
        DetailContactService,
        DragDropSortableService, 
        DragDropService, 
        DragDropConfig
    ]
})
export class ContactModule
{
}
