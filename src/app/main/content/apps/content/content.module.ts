import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../../../core/modules/shared.module';
import { FuseWidgetModule } from '../../../../core/components/widget/widget.module';
import { DaftarContentComponent } from './daftarContent/daftarContent.component';
import { DaftarContentService } from './daftarContent/daftarContent.service';
import { DetailContentComponent } from './detailContent/detailContent.component';
import { DetailContentService } from './detailContent/detailContent.service';
import { FuseConfirmDialogComponent } from './../../../../core/components/confirm-dialog/confirm-dialog.component';
import { AgmCoreModule } from '@agm/core';
import { CdkTableModule } from '@angular/cdk/table';
import { DragDropSortableService, DndModule, DragDropService, DragDropConfig } from 'ng2-dnd';
import { AuthGuard } from './../auth.guard';
import { MatTableModule } from '@angular/material/table';
import { QuillModule } from 'ngx-quill';

const routes: Routes = [
    {
        path     : 'daftarContent',
        component: DaftarContentComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DaftarContentService
        }
    },
    {
        path     : 'detailContent/:id',
        component: DetailContentComponent,
        canActivate : [AuthGuard],
        resolve  : {
            data: DetailContentService
        }
    }
];

@NgModule({
    imports     : [
        SharedModule,
        RouterModule.forChild(routes),
        FuseWidgetModule,
        NgxChartsModule,
        CdkTableModule,
        DndModule,
        MatTableModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),
        QuillModule
    ],
    exports : [
        CdkTableModule
    ],
    entryComponents: [
        DaftarContentComponent,
        FuseConfirmDialogComponent
    ],
    declarations: [
        DaftarContentComponent,
        DetailContentComponent
    ],
    providers   : [
        DaftarContentService,
        DetailContentService,
        DragDropSortableService, 
        DragDropService, 
        DragDropConfig
    ]
})
export class ContentModule
{
}
