import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DaftarSmsService } from './daftarSms.service';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { fuseAnimations } from '../../../../../core/animations';
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Router } from '@angular/router';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { FuseUtils } from '../../../../../core/fuseUtils';
import { FuseConfirmDialogComponent } from './../../../../../core/components/confirm-dialog/confirm-dialog.component';
import { SwalComponent } from '@toverux/ngx-sweetalert2';
import swal from 'sweetalert2';
import { SessionService } from './../../session.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector   : 'fuse-e-commerce-smss',
    templateUrl: './daftarSms.component.html',
    styleUrls  : ['./daftarSms.component.scss'],
    animations : fuseAnimations
})
export class DaftarSmsComponent implements OnInit
{
    hapus = false;
    dataSource: FilesDataSource | null;
    displayedColumns = ['id', 'tipe', 'pesan'];
    role = this._sessionService.get().role;
    smss = this.smssService.smss;
    date = [];
    dateIndo = [];

    pesanForm: FormGroup;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild('filter') filter: ElementRef;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private smssService: DaftarSmsService,
        private dialog: MatDialog,
        private router: Router,
        private _sessionService: SessionService,
        private formBuilder: FormBuilder
    )
    {
    }

    ngOnInit()
    {
        if (sessionStorage.getItem("pageIndexSms")) {
            this.paginator.pageIndex = Number(sessionStorage.getItem("pageIndexSms"));
        }

        this.dataSource = new FilesDataSource(this.smssService, this.paginator, this.sort);
        this.pesanForm = this.createProductForm();
    }

    createProductForm()
    {
        return this.formBuilder.group({
            telepon              : new FormControl('', [Validators.required]),
            pesan                : new FormControl('', [Validators.required])
        });
    }

    kirim(): void {
        this.smssService.kirimSms(this.pesanForm.getRawValue())
            .then((response) => {
                if (response.status) {
                    swal(
                        'Terkirim',
                        response.message,
                        'success'
                    )
                } else {
                    swal(
                        'Gagal!',
                        response.message,
                        'error'
                    )
                }
            })
            .catch((error) => {
                swal(
                    'Error!',
                    error.message,
                    'error'
                )
            })
    }

    clickSms(sms): void{
        swal({
            title: 'Edit Sms',
            html:
                `<input id="swal-input1" class="swal2-input" value="${sms.tipe}" required>` +
                `<input id="swal-input2" class="swal2-input" value="${sms.pesan}" required>`,
            focusConfirm: false,
            showCancelButton: true,
            showCloseButton: true,
            confirmButtonText: 'Save',
            preConfirm: () => {
                return [
                    (<HTMLInputElement>document.getElementById('swal-input1')).value,
                    (<HTMLInputElement>document.getElementById('swal-input2')).value
                ]
            }
        }).then((result) => {
            if (result.value) {
                let obj = {
                    tipe         : (<HTMLInputElement>document.getElementById('swal-input1')).value,
                    pesan        : (<HTMLInputElement>document.getElementById('swal-input2')).value,
                    id           : sms.id
                };
                this.smssService.updateSms(obj)
                    .then((response: any) => {
                        if (response.status) {
                            swal(
                                'Updated!',
                                response.message,
                                'success'
                            )
                        }
                    })
                    .catch((error: any) => {
                        swal(
                            'Failed',
                            'Failed update data',
                            'error'
                        )
                    })
            }
            if (result.dismiss == swal.DismissReason.cancel) {
            }
        });
    }

}

export class FilesDataSource extends DataSource<any>
{
    _filterChange = new BehaviorSubject('');
    _filteredDataChange = new BehaviorSubject('');

    get filteredData(): any
    {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any)
    {
        this._filteredDataChange.next(value);
    }

    get filter(): string
    {
        return this._filterChange.value;
    }

    set filter(filter: string)
    {
        this._filterChange.next(filter);
    }

    constructor(
        private smssService: DaftarSmsService,
        private _paginator: MatPaginator,
        private _sort: MatSort
    )
    {
        super();
        this.filteredData = this.smssService.smss;
    }

    /** Connect function called by the table to retrieve one stream containing the data to render. */
    connect(): Observable<any[]>
    {
        const displayDataChanges = [
            this.smssService.onSmssChanged,
            this._paginator.page,
            this._filterChange,
            this._sort.sortChange
        ];

        return Observable.merge(...displayDataChanges).map(() => {
            let data = this.smssService.smss.slice();

            data = this.filterData(data);

            this.filteredData = [...data];

            data = this.sortData(data);

            // Grab the page's slice of data.
            if (!this.filter) {
                sessionStorage.setItem("pageIndexSms", String(this._paginator.pageIndex));
            }
            
            const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
            return data.splice(startIndex, this._paginator.pageSize);
        });
    }

    filterData(data)
    {
        if ( !this.filter )
        {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    sortData(data): any[]
    {
        if ( !this._sort.active || this._sort.direction === '' )
        {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch ( this._sort.active )
            {
                case 'id':
                    [propertyA, propertyB] = [a.id, b.id];
                    break;
                case 'asal':
                    [propertyA, propertyB] = [a.asal, b.asal];
                    break;
                case 'tujuan':
                    [propertyA, propertyB] = [a.tujuan, b.tujuan];
                    break;
                case 'tanggal':
                    [propertyA, propertyB] = [a.tanggal, b.tanggal];
                    break;
                case 'jenis':
                    [propertyA, propertyB] = [a.jenis, b.jenis];
                    break;
                case 'kuota_motor':
                    [propertyA, propertyB] = [a.kouta_motor, b.kouta_motor];
                    break;
                case 'status':
                    [propertyA, propertyB] = [a.status, b.status];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
        });
    }

    disconnect()
    {
    }
}
