import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from './../../../../../../environments/environment';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { SessionService } from './../../session.service';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class DaftarTitipMotorService implements Resolve<any>
{
    products: any[];
    onProductsChanged: BehaviorSubject<any> = new BehaviorSubject({});
    token = this._sessionService.get().token;

    constructor(
        private http: Http,
        private _sessionService: SessionService,
        private snackBar: MatSnackBar
    )
    {
    }

    /**
     * Resolve
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProducts()
            ]).then(
                () => {
                    resolve();
                },
                reject
            )
            .catch(
                (error) => {
                    const eror = JSON.parse(error._body);
                    this.snackBar.open(eror.message, eror.errors.email ? eror.errors.email : eror.errors.password, {
                        verticalPosition: 'top',
                        duration        : 5000
                    });
                }
            );
        });
    }

    setHeader(options: RequestOptions) {
        const headers = new Headers();
        headers.append('Authorization', `Bearer ${this.token}`);
        options.headers = headers;
    }

    getProducts(): Promise<any>
    {
        const options = new RequestOptions();
        this.setHeader(options);
        return new Promise((resolve, reject) => {
            this.http.get(environment.setting.base_url+'/admin/titipMotor', options)
                .map((res: Response) => res.json())
                .subscribe((response: any) => {
                    console.log(response);
                    this.products = response['data']['data'];
                    this.onProductsChanged.next(this.products);
                    resolve(response);
                }, (error) => {
                    reject(error);
                });
        });
    }
}
